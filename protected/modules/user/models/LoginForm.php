<?php

/**
 * 
 * 注册的表单
 * 
 */
class LoginForm extends CFormModel {

    public $UserName;  //用户名
    public $Password;  //密码


    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('UserName,Password', 'required'),
            array('UserName', 'checkLogin'),
        );
    }

    public function checkLogin(){
         if(!yii::app()->user->login($this->UserName, $this->Password)){
            $this->addError('UserName',yii::app()->user->getError());
            return false;
         }
         return true;   
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'UserName' => '用户名',
            'Password' => '密码',
        );
    }
}
