<?php

/**
 * 
 * 注册的表单
 * 这注册  包括手机端与PC端的注册
 * 
 */
class RegisterForm extends CFormModel {

    public $UserName;  //用户名昵称
    public $Password;  //密码
    public $Repassword; //确认密码
    public $Remember; //已阅读
    public $EmailPhone; //邮箱或手机
    public $PhoneCode; //手机验证码
    public $VerifyCode; //验证码
    public $Phone = ''; //手机号码
    public $Email = ''; //邮箱

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */

    public function rules() {
        return array(
            // username and password are required
            array('UserName,EmailPhone,Remember,Password,Repassword', 'required'),
            array('UserName', 'match', 'pattern' => '/^[a-z 0-9 _]{3,}$/i', 'message' => '用户名只能包含大小写字母，0-9，下划线'),
            array('UserName', 'checkUserName'),
            array('Password', 'length', 'tooShort' => '密码不能低于6位！', 'min' => 6),
            array('Repassword', 'compare', 'compareAttribute' => 'Password', 'message' => '两次输入密码不一致！'),
            array('EmailPhone', 'checkEmailPhone'),
            array('PhoneCode', 'safe'),
            //array('Remember', 'boolean'),
            array('VerifyCode', 'checkVerify'),
        );
    }

    public function checkUserName() {
        if (is_numeric($this->UserName)) {
            $this->addError('UserName', '用户名不能是纯数字');
            return false;
        }

        if (MemberModel::model()->find(BaseModel::getC(array('UserName' => $this->UserName)))) {
            $this->addError('UserName', '用户名被占用');
            return false;
        }
        return true;
    }

    public function checkVerify() {
        if ($this->Email) {
            if (!yii::app()->getController()->createAction('captcha')->validate($this->VerifyCode, false)) {
                $this->addError('VerifyCode', '验证码不正确');
                return false;
            }
        }
        return true;
    }

    /**
     * 验证用户填写的是邮箱还是手机
     * 返回是1则是手机，，2则为邮箱
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function checkEmailPhone() {

        if (is_numeric($this->EmailPhone) && strlen($this->EmailPhone) == 11) { //是手机
            $this->Phone = $this->EmailPhone;
            if (!preg_match('/^1[34578][0-9]{9}$/', $this->EmailPhone)) {
                $this->addError('EmailPhone', '手机格式不正确');
                return false;
            }
            if (MemberModel::model()->find(BaseModel::getC(array('Phone' => $this->Phone)))) {
                $this->addError('EmailPhone', '手机号被占用');
                return false;
            }
        } else {
            $this->Email = $this->EmailPhone;
            if (!preg_match('/^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/', $this->EmailPhone)) {
                $this->addError('EmailPhone', '邮箱格式不正确');
                return false;
            }
            if (MemberModel::model()->find(BaseModel::getC(array('Email' => $this->Email)))) {
                $this->addError('EmailPhone', '邮箱被占用');
                return false;
            }
        }
        return true;
    }

    public function checkPhoneCode() {
        if ($this->Phone) {
            if (!SnsVerification::getInstance()->verification($this->PhoneCode, $this->Phone)) {
                $this->addError('PhoneCode', SnsVerification::getInstance()->getError());
                return false;
            }
        }
        return true;
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'UserName' => '用户名',
            'Password' => '密&#12288;码',
            'Repassword' => '确认密码',
            'Remember' => '同意',
            'EmailPhone' => '邮箱或手机',
            'PhoneCode' => '手机验证码',
            'VerifyCode' => '验证码',
        );
    }

    public function register() {
        //然后再验证手机短信是否是需要验证，，，因为手机短信当如果正确的话是直接被踢除了，所以需要其它的验证通过了之后再验证手机验证码是否正确，这样
        //就不会存在其它验证不通过，手机验证码验证通过让验证码失效
        $model = MemberModel::model();
        $model->setIsNewRecord(true);
        $model->setScenario('admin');
        $model->attributes = $this->attributes;
        $model->save(false);
        return yii::app()->user->login($model->UserName, $this->Password);
    }

}
