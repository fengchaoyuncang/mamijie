<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class FindPasswordForm extends CFormModel {

    public $username;
    public $VerifyCode;
    public $model;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('username', 'required'),
            array('username', 'checkusername'),
            array('VerifyCode', 'checkVerify'),
        );
    }

    public function checkusername(){
        if(is_numeric($this->username) && strlen($this->username) == 11){
            $this->model = MemberModel::model()->find(BaseModel::getC(array('Phone' => $this->username)));  
        }else if(strpos($this->username, '@') !== false){
            $this->model = MemberModel::model()->find(BaseModel::getC(array('Email' => $this->username)));  
        }else{
            $this->model = MemberModel::model()->find(BaseModel::getC(array('username' => $this->username)));                
        }
        if(!$this->model){
            $this->addError('username', '不存在此用户');
            return false;
        }
        return true;
    }


    public function checkVerify(){
        if(!yii::app()->getController()->createAction('captcha')->validate($this->VerifyCode, false)){
            $this->addError('VerifyCode', '验证码不正确');
            return false;
        }        
    }


    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'username' => '名称',
            'VerifyCode' => '验证码',
        );
    }

}
