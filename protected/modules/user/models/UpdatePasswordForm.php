<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UpdatePasswordForm extends CFormModel {

    public $password;
    public $repassword;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('password', 'length', 'tooShort' => '密码不能低于6位！', 'min' => 6),
            array('password', 'required', 'message' => '密码不能为空！'),
            array('repassword', 'compare', 'compareAttribute' => 'password', 'message' => '两次输入密码不一致！'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'password' => '密&#12288;码',
            'password' => '重复密码',
        );
    }
    //修改密码
    public function updatePass($userModel){
        $userModel->Password = $this->password;
        $userModel->save(false, array('Password'));
    }

}
