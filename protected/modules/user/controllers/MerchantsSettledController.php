<?php
/**
 * 商家入注控制器
 */
class MerchantsSettledController extends UserBaseC
{

/*	public function init(){
		parent::init();
		if(Yii::app()->user->kind == 1){
			$this->error('需要商家会员才能查看');
		}
	}*/
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		//$this->redirect('/');
		$model=new MerchantsSettledModel;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['MerchantsSettledModel']))
		{
			$model->attributes=$_POST['MerchantsSettledModel'];
			$model->UserID = yii::app()->user->id;
			if($model->save()){
				$this->success('添加成功', yii::app()->createUrl('/user/merchantsSettled/view', array('id' => $model->MerchantsSettledID)),true);
			}else{
				$this->error($model->getOneError(),'',true);
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * 入驻列表
	 * @return [type] [description]
	 */
	public function actionIndex(){
		if(!$uid = yii::app()->user->isLogin()){
			$this->redirect(self::U('/user/user/login'));
		}
		if(is_array($uid)){
			$this->redirect(self::U('/user/user/login'));
		}
        $objModels = MerchantsSettledModel::model()->find(BaseModel::getC(array('Status' => array('sign' => 'not in','data' => '2,3'),'UserID' => yii::app()->user->id)));
        //print_r($objModels);exit;

        $this->assign('data', $objModels);
        $this->render('index');  		
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MerchantsSettledModel']))
		{
			$model->attributes=$_POST['MerchantsSettledModel'];
			$model->UserID = yii::app()->user->id;
			if($model->save()){
				$this->success('添加成功', yii::app()->createUrl('/user/merchantsSettled/view', array('id' => $model->MerchantsSettledID)));
			}else{
				$this->error($model->getOneError());
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->error('没有开放此功能');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MerchantsSettledModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MerchantsSettledModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MerchantsSettledModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='merchants-settled-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	/**
	 * 入驻介绍
	 * @return [type] [description]
	 */
	public function actionIntroduction(){
		$this->layout = 'theme.views.layouts.frontNoContent';
		$this->render();
	}
}
