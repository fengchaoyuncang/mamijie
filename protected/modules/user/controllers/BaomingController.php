<?php
class BaomingController extends UserLoginBaseC{


	public function actionCreate()
	{
		$this->redirect('http://www.1zw.com/sellers/public/complete.html');
		$objModel = new GoodsBmModel('front_create');
		$objModel->unsetAttributes();
		if(IS_POST)
		{
			//赋值
			$objModel->attributes = $_POST;
		
			if($objModel->save())
			{			
				$this->success('添加成功', $this->createUrl('index'));		
			}else
			{
                $error = $objModel->getOneError();
                $this->error($error ? $error : '添加失败');			
			}
		}
		$this->render('create', array('model' => $objModel));		
	}	
	public function actionUpdate($id)
	{
		$objModel = $this->loadModel($id);
		$objModel->setScenario('front_update');            
		if(IS_POST)
		{		
			//赋值
			$objModel->attributes = $_POST;
		
			if($objModel->save())
			{			
				$this->success('成功修改', $this->createUrl('index'));
			}else
			{
				$error = $objModel->getOneError();
				$this->error($error ? $error : '修改失败！');				
			}
		}
		$this->render('update',array('model' => $objModel));	
	}



	public function actionIndex(){
/*		if(yii::app()->user->Type == 0){
			$this->error('只能商家才可以报名');
		}*/
		//获取缓存数据
        $datas = GoodsBmModel::getDataUser(yii::app()->user->id);
        //总数
        $arrResult['count'] = count($datas);
        //每页显示多少条
        $intPageSize = 30;
        //第几页
        $intPage = empty($_GET['page']) ? 1 : $_GET['page'];
        //是否要分页，看总数与每页显示多少的大小
        if($arrResult['count'] > $intPageSize){
            $objPager = new CPagination($arrResult['count']);
            $objPager->setCurrentPage($intPage - 1 < 0 ? 0 : $intPage - 1);
            $objPager->pageSize = $intPageSize;
            $arrResult['page'] = $objPager; 
            //限定取个数
            $count = 0;
            $begin = $objPager->getOffset();
            foreach ($datas as $key => $data) {
                $count++;
                if($count <= $begin){
                    continue;
                }else{
                    if($count > ($begin+$intPageSize)){
                        break;
                    }
                    $arrResult['data'][$key] = $data;
                }
            }            
        }else{
            $arrResult['data'] = $datas;
        }
        empty($arrResult['data']) ? $arrResult['data'] = array() : '';
        $this->assign('data', $arrResult);
        $this->render();  		
	}

	public function actionCancel($id){
		$model = $this->loadModel($id);
		if($model->Status == 0  && $model->AdminID == 0){
			$model->Status = 4;
			$model->save(false, array('Status'));
			$this->success('取消成功');
		}else{
			$this->error('请求出错');
		}
	}
	public function actionDelete($id)
	{
		$this->error('关闭删除');
		$this->loadModel($id)->delete();
		$this->success('删除成功');		
	}

	public function loadModel($id)
	{
		$model=GoodsBmModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}	

?>