<?php
//class MemberController extends UserBaseC{
class MemberController extends UserLoginBaseC{
    //消息列表
	public function actionMessage(){
        $datas = MessageModel::getListUser(yii::app()->user->id);
        $arrResult['count'] = count($datas);
        $intPageSize = 30;
        $intPage = empty($_GET['page']) ? 1 : $_GET['page'];
        if($arrResult['count'] > $intPageSize){
            $objPager = new CPagination($arrResult['count']);
            $objPager->setCurrentPage($intPage - 1 < 0 ? 0 : $intPage - 1);
            $objPager->pageSize = $intPageSize;
            $arrResult['page'] = $objPager; 
            //限定取个数
            $count = 0;
            $begin = $objPager->getOffset();
            foreach ($datas as $key => $data) {
                $count++;
                if($count <= $begin){
                    continue;
                }else{
                    if($count > ($begin+$intPageSize)){
                        break;
                    }
                    $arrResult['data'][$key] = $data;
                }
            }            
        }else{
            $arrResult['data'] = $datas;
        }
        empty($arrResult['data']) ? $arrResult['data'] = array() : '';
        $this->assign('data', $arrResult);
        $this->render('message');  
	}
    //修改与删除消息 
	public function actionUpdateMessage(){
        $data = $_POST['id'];
        if(empty($data) || !is_array($data)){
            $this->error('提交错误');
        }
        if($_POST['type'] == 1){
            MessageModel::deleteList($data);
        }else{
            MessageModel::updateList($data);
        }
        $this->success('处理成功');
	}

    //
	public function actionIndex(){
		$this->render();
	}


    /**
     * 修改资料,包括修改密码/
     * @return [type] [description]
     */
    public function actionUpdate(){
        $objModel = $this->loadModel(yii::app()->user->id);

        if(IS_POST)
        {
            //密码验证
            $_POST['Password'] = empty($_POST['Password']) ? '' : $_POST['Password'];
            $_POST['newPassword'] = empty($_POST['newPassword']) ? '' : $_POST['newPassword'];  
            $_POST['newPassword1'] = empty($_POST['newPassword1']) ? '' : $_POST['newPassword1'];  
            if(!$objModel->updatePassword($_POST['Password'], $_POST['newPassword'], $_POST['newPassword1'])){
                $this->error($objModel->getOneError());             
            }
            unset($_POST['Password']);
            if($_POST['newPassword']){
                $objModel->Password = $_POST['newPassword'];
            }
            $objModel->attributes = $_POST;
            //修改资料
            if($objModel->save())
            {           
                $this->success('成功修改');
            }else
            {
                $error = $objModel->getOneError();
                $this->error($error ? $error : '修改失败！');                
            }
        }
        $this->render('update',array('model' => $objModel));        
    }

    public function loadModel($id)
    {
        $model=MemberModel::model()->findByPk($id);
        if($model===null)
            $this->error('找不到记录');
        return $model;
    }


    //我的账户
    public function actionMoney(){
        try {
            MemberMoneyChangeModel::getLock(yii::app()->user->id);
            $userMoney = MemberMoneyChangeModel::getUserMoney(yii::app()->user->id);
            $ruzhuMoney = MemberMoneyChangeModel::getTypeMoney(array('userID' => yii::app()->user->id, 'Type' => 32));  
            $baozhengjingMoney = MemberMoneyChangeModel::getTypeMoney(array('userID' => yii::app()->user->id, 'Type' => 33));
            $this->assign('userMoney',$userMoney);  
            $this->assign('ruzhuMoney',$ruzhuMoney);      
            $this->assign('baozhengjingMoney',$baozhengjingMoney);
            MemberMoneyChangeModel::releaseLock(yii::app()->user->id);   
        } catch (Exception $e) {
            $this->error('异常错误');
        }
        $this->render();
    }

    //资金变动记录
    public function actionMemberMoneyChange(){
        $objModel = new MemberMoneyChangeModel('search');
        $objModel->resetAttributes();
        if(isset($_GET['MemberMoneyChangeModel'])){
            $objModel->attributes = $_GET['MemberMoneyChangeModel'];
        }

        //增加搜索条件
        $objModel->UserID = yii::app()->user->id;
        //创建搜索
        $objCriteria = $objModel->createSearchCriteria(); 

        //分页
        $intCount = $objModel->count($objCriteria);
        $objPager = new CPagination($intCount);
        $objPager->pageSize = 20;
        $objPager->applyLimit($objCriteria);

       //排序
        $objSort = new CSort('MemberMoneyChangeModel');
        $objSort->defaultOrder = 'AddTime desc';
        $objSort->applyOrder($objCriteria);


        $objModels = $objModel->findAll($objCriteria);
        $this->assign('pages', $objPager);
        $this->assign('sort', $objSort);
        $this->assign('model', $objModel); 
        $this->assign('data', $objModels);
        $this->render();          
    }
    //资金变动记录
    public function actionWithdraw(){
        $objModel = new MemberWithdrawModel('search');
        $objModel->resetAttributes();
        if(isset($_GET['MemberWithdrawModel'])){
            $objModel->attributes = $_GET['MemberWithdrawModel'];
        }

        //增加搜索条件
        $objModel->UserID = yii::app()->user->id;
        //创建搜索
        $objCriteria = $objModel->createSearchCriteria(); 

        //分页
        $intCount = $objModel->count($objCriteria);
        $objPager = new CPagination($intCount);
        $objPager->pageSize = 20;
        $objPager->applyLimit($objCriteria);

       //排序
        $objSort = new CSort('MemberWithdrawModel');
        $objSort->defaultOrder = 'AddTime desc';
        $objSort->applyOrder($objCriteria);


        $objModels = $objModel->findAll($objCriteria);
        $this->assign('pages', $objPager);
        $this->assign('sort', $objSort);
        $this->assign('model', $objModel); 
        $this->assign('data', $objModels);
        $this->render();          
    }
}