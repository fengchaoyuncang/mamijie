<?php
//刚开始注册登录的时候不需要验证了
class UserController extends UserBaseC {
    public $layout = 'theme.views.layouts.frontNoContent';
    //public $layout = false;
    /**
     * Declares class-based actions.
     * 登录,,注册,,验证,,找回密码,,等等,,控制器
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
                'minLength' => 4,
                'maxLength' => 4,
                'transparent' => true,
            ),
        );
    }

    //注册
    public function actionRegister(){
        $this->title = '妈咪街注册,妈咪街官网注册';
        $this->keywords = '';
        $this->description = '妈咪街官方网站注册';        
        if(yii::app()->user->isLogin()){
            $this->redirect('/');
        }
        $this->layout = false;
        $model = new RegisterForm();
        $this->assign('model',$model);
        if(IS_POST){
            $model->attributes = $_POST['RegisterForm'];
            if($model->validate() and $model->register()){
                $this->redirect($this->createUrl('/user/member/index'));
            }
        }
        $this->render();
    }

    //登录
    public function actionLogin(){
        $this->title = '妈咪街登录,妈咪街官网登录';
        $this->keywords = '';
        $this->description = '妈咪街官方网网站登录';          
        if(yii::app()->user->isLogin()){
            $this->redirect('/');
        }        
        $this->layout = false;
        $model = new LoginForm();
        $this->assign('model',$model);
        if(IS_POST){
            $model->attributes = $_POST['LoginForm'];
            if($model->validate()){
                $this->redirect($this->createUrl('/user/member/index'));
                exit;
            }
        }
        $this->render();
    }

    //登录
    public function actionLoginqq(){
        if(yii::app()->user->isLogin()){
            $this->redirect('/');
        }        
        $this->layout = false;
        $model = new LoginForm();
        $this->assign('model',$model);
        if(IS_POST){
            $model->attributes = $_POST['LoginForm'];
            if($model->validate()){
                $this->redirect($this->createUrl('/user/member/index'));
                exit;
            }
        }
        $this->render();
    }
    

    public function actionDiaLogin(){        
        if(yii::app()->user->isLogin()){
            $content = $this->renderPartial('//layouts/common/header/user', array(), true);
            $this->assign('content', trim($content));                
            $this->render('closedialog');  
            exit;
        }        
        $this->layout = false;
        $model = new LoginForm();
        $this->assign('model',$model);
        if(IS_POST){
            $model->attributes = $_POST['LoginForm'];
            if($model->validate()){
                $content = $this->renderPartial('//layouts/common/header/user', array(), true);
                $this->assign('content', trim($content));                
                $this->render('closedialog');  
                exit;
            }
        }
        $this->render();     
    }

    public function actionDiaRegister(){
        $this->actionRegister();        
    }


    /**
     * 买家注册后邮箱验证
     * @return [type] [description]
     */
    public function actionRegEmailSuccesed($mail) {
        $this->layout = 'theme.views.layouts.frontNoContent';
        if(yii::app()->user->isLogin()){
            $this->redirect(array("/user/member/index"));    
        }        
        set_time_limit(0);
        if((($model = MemberModel::model()->find(BaseModel::getC(array('Email' => $mail)))) && $model->Status == 1)){
            if($model){
                $this->success('已经验证通过,请直接登录', $this->createUrl('login'));
            }else{
                $this->error('找不到用户记录', $this->createUrl('/front/index/index'));
            }
        }
        if($model = MemberVerificationModel::getOneData($mail)){
            if($model->Status == 0){
                $time = 120 - TIME_TIME + $model->AddTime;
                if($time > 0){  
                    if(IS_AJAX){
                        $this->error("刚发送邮件，离下次再发邮件还有{$time}秒");                      
                    }else{
                        $content = "刚发送邮件，离下次再发邮件还有{$time}秒";
                        $this->assign('content', $content);
                        $this->assign('mail', $mail);
                        $this->render();
                        exit;                          
                    }                 
                }             
            }
        }
        //发送一邮件
        if(MemberModel::sendEmail($mail)){
            $content = "邮件发送成功";
            $status = 1;   
        }else{
            $content = yii::app()->user->getFlash(); 
            $status = 0;      
        }
        if(IS_AJAX){
            $this->ajaxReturn(array('info' => $content, 'status' => $status));
        }
        $this->assign('content', $content);
        $this->assign('mail', $mail);
        $this->render();  
    }

    public function actionVerify($confirmation_token = ''){
        $return  = MemberVerificationModel::verification($confirmation_token);
        if($return == 1){
            $this->success('验证成功',$this->createUrl('/front/index/index'));
        }else if($return == 0){
            $this->error('找不到记录,已经验证通过或者不存在此记录',$this->createUrl('/front/index/index'));
        }else{
            $this->error('key已经过期,将重新发送',$this->createUrl('regEmailSuccesed', array('mail' => yii::app()->user->getFlash())));
        }
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect($this->createUrl('/front/index/index'));
    }

    //发送短信验证码
    public function actionSendcode() {
        $moblie = Yii::app()->request->getParam('moblie');

        $sendType = Yii::app()->request->getParam('sendType');
        empty($sendType) ? $sendType = 'SnsVerification' : '';
        if (empty($moblie)) {
            $this->error('手机号不能为空！');
        }
        $key = md5(Tool::getForwardedForIp());
        if (Yii::app()->cache->get($key)) {
            $this->error('请60秒后再次发送！');
        }
        if (defined('YII_DEBUG') && YII_DEBUG == false) {
            $rKey = md5($moblie);
            $redisKey = (int) RedisCluster::getInstance()->get($rKey);
            if ($redisKey >= 5) {
                $this->error('该手机号今日已达最大发送数！');
            }
        }
        if (SnsVerification::getInstance()->send($moblie, $sendType)) {
            if (defined('YII_DEBUG') && YII_DEBUG == false) {
                $redisKey = (int) $redisKey + 1;
                $time = strtotime(date('Y-m-d 23:59:59')) - time();
                RedisCluster::getInstance()->set($rKey, $redisKey, $time);
            }
            Yii::app()->cache->set($key, 1, 60);
            $this->success('短信验证码已经发送！');
        } else {
            $this->error('验证码发送失败！');
        }
    }


    public function actionSendemailFindPassword(){
        $model = MemberModel::model()->findByPk(Yii::app()->request->getParam('uid'));
        if(!$model){
            $this->ajaxReturn(array('status' => false,'send' => false));
        }        
        $title = "【妈咪街】请尽快修改您的密码";
        $code = rand(100000,999999);
        RedisCluster::getInstance()->set("FindPassword_Email_".$model->UserID, $code, 3600);
        $url = '/user/user/findPas/uid/'.$model->UserID.'/code/'.$code;
        $emailBody = '
            <table width="720" border="0" cellspacing="0" cellpadding="0">
                      <tbody><tr>
                        <td height="40" style="line-height:24px">亲爱的<a href="mailto:' . $model->UserName  . '" target="_blank">' . $model->UserName  . '</a>：</td>
                      </tr>
                      <tr>
                        <td style="line-height:24px">您在妈咪街发起了找回密码服务。<br>
                          请您在 1 小时内点击以下链接，或将链接复制到浏览器地址栏访问以修改您的密码：<br>
                          <a href="' . Yii::app()->request->hostInfo . $url . '" target="_blank">' . Yii::app()->request->hostInfo . $url . '</a>
                          <br>
                          如果您的邮箱不支持链接点击，请将以上链接地址拷贝到您的浏览器地址栏中。<br>
                          如果您没有申请注册妈咪街，请忽略此邮件
                        </td>
                      </tr>
                      <tr>
                        <td align="right" height="30">妈咪街</td>
                      </tr>
                      <tr>
                        <td align="right"><span times="" t="5" style="border-bottom: 1px dashed rgb(204, 204, 204);">' . date('Y-m-d', time()) . '</span></td>
                      </tr>
                      <tr>
                        <td valign="middle" align="center" height="40" style="color:#999">（本邮件由系统自动发出，请勿回复。）</td>
                      </tr>
                    </tbody>
            </table>
            ';
        $mail = Yii::createComponent('application.extensions.mailer.EMailer');
        if ($mail->sendEmail($model->Email, $title, $emailBody)) {
            $this->ajaxReturn(array('status' => true,'send' => true));
        }else{
            $this->ajaxReturn(array('status' => true,'send' => false));
        }         
    }





         

}
