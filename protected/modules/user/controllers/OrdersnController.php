<?php

class OrdersnController extends UserLoginBaseC{
	/**
	 * 充值逻辑处理
	 * @return [type] [description]
	 */
	public function actionIndex(){
		if(IS_POST){
			if(empty($_POST['money']) || !is_numeric($_POST['money'])){
				$this->error('请填写正确的金额');
			}

			Yii::import('ext.alipay.*');

			$alipay = new Alipay();
            $orders = array(
                'Type' => 1,
                'Money' => $_POST['money'],
            );

            if ($alipay->data($orders)->createOrders() !== true) {
                $error = $alipay->getError();
                $this->error($error ? $error : '订单创建失败，请重新提交！');
            }
            //跳转到支付宝支付页面
            $url = $alipay->payment(true);
            if ($url == false) {
                $error = $alipay->getError();
                $this->error($error ? $error : '订单处理错误！');
            } else {
                $this->success('报名成功，马上前往支付页面！', $url);
                exit;
            }            			
		}
	}
    //充值html页面
    public function actionOnline(){
        $this->render();
    }
    //用户交保证金的充值
    public function actionBaoZhengJing(){
        $model = MemberMoneyChangeModel::model();
        if($return = $model->paymentBaoZhengJing(yii::app()->user->id)){
            if(is_array($return)){
                $this->success($return['content'],$return['url']);
            }else{
                $this->success('提交成功');
            }
        }else{
            $this->error($model->getOneError());
        }
    }
    //用户交入驻费用
    public function actionRuzhu(){
        $model = MemberMoneyChangeModel::model();
        if($return = $model->paymentRuzhu(yii::app()->user->id)){
            if(is_array($return)){
                $this->success($return['content'],$return['url']);
            }else{
                $this->success('提交成功');
            }
        }else{
            $this->error($model->getOneError());
        }
    }
    //用户商品报名或者续交费用
    public function actionGoods(){
        $model = MemberMoneyChangeModel::model();
        $goodsBmID = Yii::app()->request->getParam('id');
        $num = Yii::app()->request->getParam('num');
        if($return = $model->paymentGoods(yii::app()->user->id,$goodsBmID,$num)){
            if(is_array($return)){
                $this->success($return['content'],$return['url']);
            }else{
                $this->success('提交成功');
            }
        }else{
            $this->error($model->getOneError());
        }        
    }    
    //提现申请页面
    public function actionWithdraw(){
        $model = new MemberWithdrawModel; 
        $this->assign('object', 'MemberWithdrawModel');    
        if(isset($_POST['MemberWithdrawModel'])){
            $model->attributes = $_POST['MemberWithdrawModel'];
            $model->UserID = yii::app()->user->id;
            if($model->withdraw()){
                $this->success('提交成功');
            }else{             
                $this->error($model->getOneError(), $this->createUrl('withdraw'));
            }
        }else{
            $this->assign('model', $model);
            $this->render();
        }
    }


    //保证金付了卖家没办法取消
    //商品付了商家在还没有申请的情况下可以取消
    //入驻的付了商家也没办法取消
    public function actionGoodsCancel(){
        $model = MemberMoneyChangeModel::model();
        $goodsBmID = Yii::app()->request->getParam('id');
        if($return = $model->cancelGoods(yii::app()->user->id,$goodsBmID)){
            $this->success('处理成功');
        }else{
            $this->error($model->getOneError());
        }           
    }

    //用户申请退保证金
    public function actionCannelBaoZhengJing(){
        $model = MemberMoneyChangeModel::model();
        if($return = $model->cancelBaoZhengJing(yii::app()->user->id)){
            $this->success('处理成功');
        }else{
            $this->error($model->getOneError());
        }         
    }
    //用户申请入驻费解冻
    public function actionCannelRuzhu(){
        $model = MemberMoneyChangeModel::model();
        if($return = $model->cancelRuzhu(yii::app()->user->id)){
            $this->success('处理成功');
        }else{
            $this->error($model->getOneError());
        }           
    }    
}