<?php

class UserBaseC extends FrontBaseC {
    public $menberMenus;
    public function init() {
        parent::init();
        //会员中心导航菜单
            $this->menberMenus = array(
                '个人中心' => array(
                    '我的个人中心' => array(
                        'url' => $this->createUrl('/user/member/index'),
                    ),
                    '修改资料' => array(
                        'url' => $this->createUrl('/user/member/update'),
                    ),
                    '我的消息' => array(
                        'url' => $this->createUrl('/user/member/message'),
                    ),
                ),
                '商家中心' => array(
                    '报名列表' => array(
                        'url' => $this->createUrl('/user/baoming/index'),
                    ),
                    '我的入驻' => array(
                        'url' => $this->createUrl('/user/merchantsSettled/index'),
                    ), 
                    '充值' => array(
                        'url' => $this->createUrl('/user/ordersn/online'),
                    ), 
                    '提现申请' => array(
                        'url' => $this->createUrl('/user/ordersn/withdraw'),
                    ),   
                    '我的账户' => array(
                        'url' => $this->createUrl('/user/member/money'),
                    ),   
                    '资金变动记录' => array(
                        'url' => $this->createUrl('/user/member/memberMoneyChange'),
                    ),    
                    '提现记录' => array(
                        'url' => $this->createUrl('/user/member/withdraw'),
                    ),                                           
                ),

            );



    }
}
