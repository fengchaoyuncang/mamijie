<?php

class ArticlesController extends FrontBaseC{
	public function actionIndex($id){
		$model=ArticlesModel::model()->findByPk($id);
		if($model){
			$this->title = $model->TitleSeo;
			$this->keywords = $model->KeywordsSeo;
			$this->description = $model->DescriptionSeo;
		}
		//分类及其文章
		$datas = ArticlesModel::getCatData(7);
		//print_r($datas);exit;

		$this->assign('datas', $datas);	
		$this->assign('model', $model);			
		$this->render();		
	}

	public function loadModel($id)
	{
		$model=ArticlesModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}

}