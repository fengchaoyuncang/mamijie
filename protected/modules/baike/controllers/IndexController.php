<?php
//百科页面
class IndexController extends FrontBaseC {
	public $layout = 'theme.views.layouts.common.layout.index'; 
	//百科首页
	public function actionIndex(){
		//error_reporting(E_ALL);
		//8张轮播图
		$data = AdvertisingModel::getListType(6);
		$this->assign('advertising', $data);

		//获取最新的分类与其文章（今日聚集）
		$data = CategoryArticlesNewModel::getNewCategory();
		$this->assign('new', $data);
		//print_r($data);exit;


		//与我们的妈咪说同步（妈咪说）
		$data = ArticlesNewModel::model()->getVaried(array('cid' => 1, 'page' => '', 'pagesize' => 4));
		$this->assign('mamishuo', $data);



		//育儿知识(问答/文章分类)
		//获取ID为2，获取两层数据
		$data = CategoryArticlesNewModel::getListCatID(2,2);
		$this->assign('askCategory', $data['data']);


		//育儿知识第一块
		$datacate = array();
		$d = current($data['data']);
		$cid = $d['CatID'];
		$datacate['ask'] = $datacate['imgarticle'] = $datacate['article'] = array();
		$datacate['ask'] = ArticlesAskModel::model()->getVaried(array('cid' => $cid, 'page' => '', 'pagesize' => 9));
		$datacate['imgarticle'] = ArticlesNewModel::model()->getVaried(array('cid' => $cid, 'page' => '', 'pagesize' => 4, 'IsImg' => true));
		$datacate['article'] = ArticlesNewModel::model()->getVaried(array('cid' => $cid, 'page' => '', 'pagesize' => 6, 'IsImg' => false));
		$this->assign('datacate', $datacate);
		

		//焦点新闻
		$data = ArticlesNewModel::model()->getVaried(array('hot' => 1, 'page' => '', 'pagesize' => 6));
		$this->assign('hotarticles', $data);


		
		//三个周刊
		$weekly_1 = ArticlesWeeklyModel::getList(1);		
		$weekly_2 = ArticlesWeeklyModel::getList(2);		
		$weekly_3 = ArticlesWeeklyModel::getList(3);
		$this->assign('weekly', array($weekly_1, $weekly_2, $weekly_3));
        $this->render();  
	}

	//周刊
	public function actionWeekly(){
		$id = Yii::app()->request->getParam('id');
		$model = $this->loadWeeklyModel($id);

		//此周刊类型的所有数据
		$weeklyType = ArticlesWeeklyModel::getList($model->Type);	
		$this->assign('weeklyType', $weeklyType);
		$data = $model->attributes;
		$data['data'] = ArticlesWeeklyDetailModel::getListWeekly($data['WeeklyID']);
		$this->assign('data', $data);
		$this->title = $model->TitleSeo;
		$this->keywords = $model->KeywordsSeo;
		$this->description = $model->DescriptionSeo;
		$this->render();
	}


	//问答详细页
	public function actionAskDetail($id){
		$d = GoodsModel::model()->getVariedGoods(array('page' => '', 'pagesize' => 1));
		$this->assign('arrGodsData', $d['data'][0]);
		$model = $this->loadAskModel($id);
		$this->title = $model->TitleSeo;
		$this->keywords = $model->KeywordsSeo;
		$this->description = $model->DescriptionSeo;
		$this->title = $model->Title . '【妈咪问答】 - 妈咪街';
		empty($model->Keywords) ? '' : $this->keywords = $model->Keywords;
		empty($model->Description) ? '' : $this->description = $model->Description;
		$this->render('',array(
			'model'=>$model,
		));
	}

	//ajax获取文章
	public function actionAjaxArticle(){
		//育儿知识第一块
		$cid = Yii::app()->request->getParam('id');
		if(!$cid){
			$this->error('请求出错');
		}
		$datacate['ask'] = $datacate['imgarticle'] = $datacate['article'] = array();
		$datacate['ask'] = ArticlesAskModel::model()->getVaried(array('cid' => $cid, 'page' => '', 'pagesize' => 9));
		$datacate['imgarticle'] = ArticlesNewModel::model()->getVaried(array('cid' => $cid, 'page' => '', 'pagesize' => 4, 'IsImg' => true));
		$datacate['article'] = ArticlesNewModel::model()->getVaried(array('cid' => $cid, 'page' => '', 'pagesize' => 5, 'IsImg' => false));

		$content = $this->renderPartial('data', array('datacate' => $datacate), true);

		$this->ajaxReturn(array('content' => $content, 'status' => 1));
	}

	//百科具体文章内容页面
	public function actionView($id)
	{
		$d = GoodsModel::model()->getVariedGoods(array('page' => '', 'pagesize' => 1));
		$this->assign('arrGodsData', $d['data'][0]);
		$model = $this->loadModel($id);
		$this->title = $model->TitleSeo;
		$this->keywords = $model->KeywordsSeo;
		$this->description = $model->DescriptionSeo;
		$this->render('view',array(
			'model'=>$model,
		));
	}


	public function actionAskList(){
		$data = CategoryArticlesAskModel::getListMenu(2);
		$this->assign('category', $data);

		$cid = Yii::app()->request->getParam('cid');
		$d = current($data);
		empty($cid) ? $cid = $d['CatID'] : '';

		$d = CategoryArticlesAskModel::getDataDetail($cid);
		$this->title = $d['TitleSeo'];
		$this->keywords = $d['KeywordsSeo'];
		$this->description = $d['DescriptionSeo'];


		$this->assign('cid', $cid);
		$data = ArticlesAskModel::model()->getVaried(array('cid' => $cid, 'pagesize' => 20,'gettype' => 2));
		$this->assign('data', $data);

		$this->render();
	}
	//一般前台获取的方式跟后台的不一样，前台有时候通过AJAX来取，有时候通过缓存来取，有时候不需要分页，有时候需要分页来处理。
	//这里需要获取的是  分类列表  与  这个文章列表。。。  通过缓存来取。
	public function actionArticleList(){
		$data = CategoryArticlesNewModel::getListMenu(2);
		$this->assign('category', $data);

		$cid = Yii::app()->request->getParam('cid');
		$d = current($data);
		empty($cid) ? $cid = $d['CatID'] : '';

		$d = CategoryArticlesNewModel::getDataDetail($cid);
		$this->title = $d['TitleSeo'];
		$this->keywords = $d['KeywordsSeo'];
		$this->description = $d['DescriptionSeo'];

		$this->assign('cid', $cid);
		$data = ArticlesNewModel::model()->getVaried(array('cid' => $cid, 'pagesize' => 20,'gettype' => 2));
		$this->assign('data', $data);

		$this->render();
	}


	public function loadWeeklyModel($id)
	{
		$model= ArticlesWeeklyModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}	

	public function loadAskModel($id)
	{
		$model= ArticlesAskModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}	
	public function loadModel($id)
	{
		$model= ArticlesNewModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	public function actionAjaxHelp(){
		$id = Yii::app()->request->getParam('id');
		$model = $this->loadAskModel($id);
		$help = Yii::app()->request->getParam('help');		
		if($help == 0){
			$model->NoHelp = $model->NoHelp + 1;
		}else{
			$model->Help = $model->Help + 1;
		}
		$model->save(false);
		$this->ajaxReturn(array('status' => 1, 'NoHelp' => $model->NoHelp, 'Help' => $model->Help, 'info' => '处理成功'));
	}

	public function actionXML(){
		header('Content-Type: text/xml');
		$this->layout = false;
		$html = '';
		$datas = ArticlesNewModel::model()->getVaried(array('cid' => 1,'page' => '','gettype' => 2));
		foreach($datas['data'] as $value){
            $html .= "<item>";
            $html .= "\r\n";
            $html .= "<title>";
            $html .= "\r\n";
            $html .= $value['Title'];
            $html .= "\r\n";  
            $html .= "</title>";
            $html .= "\r\n";
            $html .= "<link>";
            $html .= "\r\n";
            $html .= "http://www.mamijie.com/" . $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID']));  
            $html .= "\r\n";           
            $html .= "</link>";
            $html .= "\r\n";
            $html .= "<pubDate>";
            $html .= "\r\n";
            $html .= date('Y-m-d',$value['AddTime']);
            $html .= "\r\n";           
            $html .= "</pubDate>";
            $html .= "<source>";
            $html .= "\r\n";
            $html .= "妈咪街妈咪说";
            $html .= "\r\n";           
            $html .= "</source>";
            $html .= "\r\n";
            $html .= "<description>";
            $html .= "\r\n";
            $html .= mb_substr(trim(preg_replace('/[<>]/', '', strip_tags($value['Content']))), 0, 200, 'utf-8');
            $html .= "\r\n";           
            $html .= "</description>";
            $html .= "\r\n";
            $html .= "</item>";
            $html .= "\r\n";
		}
		$this->assign('html',$html);
		$this->render('xml');
	}
}