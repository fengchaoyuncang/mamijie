<?php

/**
 * 后台Admin模块配置
 * File Name：config.php
 * File Encoding：UTF-8
 * File New Time：2014-4-28 18:23:25
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
return array(
    'components' => array(
        'passport' => array(
            'class' => 'PassportAdminService',
        ),
    ),
    'params' => array(
        'dd' => 'zzz.com',
    ),
);

