<?php

/**
 * 角色模型
 * File Name：Role.php
 * File Encoding：UTF-8
 * File New Time：2014-5-8 11:26:26
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class Role extends BaseModel {

    public function tableName() {
        return '{{admin_role}}';
    }

    public function rules() {
        return array(
            array('name', 'required', 'message' => '请填角色名称！'),
            array('name', 'unique', 'allowEmpty' => false, 'message' => '该角色名称已经存在！'),
            //安全的。。。
            array('name,pid,status,remark,listorder', 'safe'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 数据保存前操作
     */
    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->create_time = time();
        }
        $this->update_time = time();
        return true;
    }

    /**
     * 获取角色列表
     * @param type $newCache 是否更新缓存
     * @return array
     */
    public function getList($newCache = '') {
        //缓存key
        $key = "getRoleList";
        if (is_null($newCache) || $newCache == false) {
            Yii::app()->cache->delete($key);
        }
        //读取缓存
        $cache = Yii::app()->cache->get($key);
        if (empty($cache)) {
            $model = $this->findAll();
            $categorys = array();
            if (empty($model)) {
                return $categorys;
            }
            foreach ($model as $obj) {
                $info = $obj->attributes;
                if (!empty($info)) {
                    $categorys[$info['id']] = $info;
                }
            }
            $cache = $categorys;
            //默认缓存3600秒
            Yii::app()->cache->set($key, $cache, 3600);
        }
        return $cache;
    }

    /**
     * 获取角色列表用于select
     * @return type
     */
    public function getSelectRoleList() {
        $data = $this->getList();
        $list = array();
        foreach ($data as $rs) {
            $list[$rs['id']] = $rs['name'];
        }
        return $list;
    }

    /**
     * 根据角色id获取角色名
     * @param type $roleid
     * @return boolean
     */
    public function getRoleName($roleid) {
        $roleList = $this->getList();
        if (isset($roleList[$roleid])) {
            return $roleList[$roleid]['name'];
        } else {
            return false;
        }
    }

    /**
     * 检查指定菜单是否有权限
     * @param type $data menu表中数组，单条
     * @param type $roleid 需要检查的角色ID
     * @param type $priv_data 已授权权限表数据
     * @return boolean
     */
    public function isCompetence($data, $roleid, $priv_data = array()) {
        $priv_arr = array('app', 'controller', 'action');
        if ($data['app'] == '') {
            return false;
        }
        if (empty($priv_data)) {
            //查询已授权权限
            $priv_data = Access::model()->getAccessList($roleid);
        }
        if (empty($priv_data)) {
            return false;
        }
        //菜单id
        $menuid = $data['id'];
        //菜单类型
        $type = $data['type'];
        //去除不要的数据
        foreach ($data as $key => $value) {
            if (!in_array($key, $priv_arr)) {
                unset($data[$key]);
            }
        }
        $competence = array(
            'role_id' => $roleid,
            'app' => $data['app'],
        );
        //如果是菜单项加上菜单Id用以区分，保持唯一
        if ($type == 0) {
            $competence["controller"] = $data['controller'] . $menuid;
            $competence["action"] = $data['action'] . $menuid;
        } else {
            $competence["controller"] = $data['controller'];
            $competence["action"] = $data['action'];
        }
        //$competence['status'] = $type ? 1 : 0;
        //检查是否在已授权列表中
        $implode = implode('', $competence);
        $info = in_array(implode('', $competence), $this->privArrStr($priv_data));
        if ($info) {
            return true;
        } else {
            return false;
        }
    }

    protected function privArrStr($priv_data) {
        $priv = array();
        foreach ($priv_data as $r) {
            unset($r['status']);
            $priv[] = implode('', $r);
        }
        return $priv;
    }

}
