<?php

/**
 * File Name：Access.php
 * File Encoding：UTF-8
 * File New Time：2014-5-8 13:32:47
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class Access extends BaseModel {

    public function tableName() {
        return '{{admin_access}}';
    }

    public function rules() {
        return array(
            array('role_id', 'required', 'message' => '角色ID不能为空！'),
            array('app', 'required', 'message' => '模块不能为空！'),
            array('controller', 'required', 'message' => '控制器不能为空！'),
            array('action', 'required', 'message' => '方法名不能为空！'),
            //安全的。。。
            array('status', 'safe'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 清空对应角色的全部权限
     * @param type $roleid 角色
     * @return boolean
     */
    public function accessEmpty($roleid) {
        if (empty($roleid)) {
            return false;
        }
        return $this->deleteAll($this->where(array('role_id' => $roleid)));
        $this->getAccessList($roleid, false);
    }

    /**
     * 根据角色ID返回全部权限
     * @param type $roleid 角色ID
     * @return array  
     */
    public function getAccessList($roleid, $isCache = true) {
        $strCache = 'Access_getAccessList_' . $roleid;
        $arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($IsCache && $arrCache){
            return $arrCache;
        }
        $arrResult = array();

        //查询出该角色拥有的全部权限列表
        if ($roleid == 1) {
            $data = $this->findAll();
        } else {
            $data = $this->findAll($this->where(array('role_id' => $roleid)));
        }
        foreach ($data as $rs) {
            $info = $rs->attributes;
            unset($info['status']);
            $arrResult[] = $info;
        }

        yii::app()->cache->set($strCache, $arrResult, 600);
        return $arrResult;
    }

    public function afterSave(){
        $this->getAccessList($this->role_id, false);
    }
    public function afterDelete(){
        $this->getAccessList($this->role_id, false);
    }

    /**
     * 检查用户是否有对应权限
     * @param type $where 条件
     * @return type
     */
    public function isCompetence($role_id, $app, $controller, $action) {
        $data = self::saveData();
        if(!empty($data[$role_id][$app][$controller][$action])){
            return true;
        }else{
            return false;
        }
    }

    public static function saveData($isCache = true){
        $strCache = 'Access_saveData';
        $arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($IsCache && $arrCache){
            return $arrCache;
        }
        $arrResult = array(); 
        
        $datas = self::model()->findAll();
        foreach ($datas as $key => $data) {
            $arrResult[$data->role_id][$data->app][$data->controller][$data->action] = true;
        }


        yii::app()->cache->set($strCache, $arrResult, 600);
        return $arrResult;            
    }

}
