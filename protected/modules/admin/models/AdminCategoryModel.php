<?php

/**
 * 后台审核用户对应栏目表
 * File Name：AdminCategoryModel.php
 * File Encoding：UTF-8
 * File New Time：2014-9-22 10:27:32
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class AdminCategoryModel extends BaseModel {

    public function tableName() {
        return '{{admin_category}}';
    }

    public function rules() {
        return array(
            array('uid,catid,createtime', 'safe'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 返回对应管理员负责的栏目列表
     * @param type $uid 管理员UID
     * @return boolean | array 
     */
    public function getAdminCategoryList($uid) {
        if (empty($uid)) {
            return false;
        }
        $dataList = $this->findAll($this->where(array('uid' => $uid)));
        $list = array();
        foreach ($dataList as $rs) {
            $list[] = $rs['catid'];
        }
        return $list;
    }

    /**
     * 返回管理员默认栏目ID
     * @param type $uid
     * @return int
     */
    public function getDefaultCatid($uid) {
        $info = $this->find($this->where(array('uid' => $uid, 'order' => 'createtime desc')));
        if (!empty($info)) {
            return $info['catid'];
        }
        return 0;
    }

    /**
     * 更新
     * @param type $uid
     * @param type $admin_category
     * @return boolean
     */
    public function saveCategory($uid, $admin_category) {
        if (empty($uid)) {
            return false;
        }
        $this->deleteAll($this->where(array('uid' => $uid)));
        if (!empty($admin_category)) {
            foreach ($admin_category as $catid) {
                $this->uid = $uid;
                $this->catid = $catid;
                $this->createtime = time();
                $this->setIsNewRecord(true);
                $this->save();
            }
        }
        return true;
    }

    /**
     * 根据栏目ID获取有这个栏目权限的管理员列表
     * @param type $catid 栏目ID
     * @return boolean
     */
    public function getCategoryAdminList($catid) {
        if (empty($catid)) {
            return false;
        }
        $dataList = $this->findAll($this->where(array('catid' => $catid)));
        if (empty($dataList)) {
            return false;
        }
        $list = array();
        foreach ($dataList as $rs) {
            $list[] = $rs['uid'];
        }
        return $list;
    }

}
