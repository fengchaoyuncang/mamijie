<?php

/**
 * 后台管理常用地址模型
 * File Name：AdminPanel.php
 * File Encoding：UTF-8
 * File New Time：2014-5-8 14:53:33
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class AdminPanel extends BaseModel {

    public function tableName() {
        return '{{admin_panel}}';
    }

    public function rules() {
        return array(
            array('menuid', 'required', 'message' => '菜单id不能为空！'),
            array('name', 'required', 'message' => '菜单名不能为空！'),
            array('url', 'required', 'message' => '地址不能为空！'),
            //安全的。。。
            array('userid', 'safe'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 是否已经加入常用菜单
     * @param type $id
     * @param type $uid
     * @return boolean
     */
    public function isMy($id, $uid) {
        $is = $this->count($this->where(array(
                    'menuid' => $id,
                    'userid' => $uid,
        )));
        return $is ? true : false;
    }

    /**
     * 清空对应管理员的全部常用菜单
     * @param type $roleid uid
     * @return boolean
     */
    public function panelEmpty($userid) {
        if (empty($userid)) {
            return false;
        }
        return $this->deleteAll($this->where(array('userid' => $userid)));
    }

    /**
     * 返回管理员全部的常用菜单
     * @param type $userid 管理员ID
     * @return type
     */
    public function getPanelList($userid) {
        if (empty($userid)) {
            return array();
        }
        $data = $this->findAll($this->where(array('userid' => $userid)));
        if (empty($data)) {
            return array();
        }
        $panelList = array();
        foreach ($data as $rs) {
            $panelList[] = $rs->attributes;
        }
        return $panelList;
    }

    /**
     * 数据保存前操作
     */
    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->userid = AdminBase::$uid;
        }
        return true;
    }

}
