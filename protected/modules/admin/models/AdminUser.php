<?php

/**
 * 管理员模型
 * File Name：AdminUser.php
 * File Encoding：UTF-8
 * File New Time：2014-5-4 17:20:43
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class AdminUser extends BaseModel {

    public $confirm_password;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{admin_user}}';
    }

    public function rules() {
        return array(
            //唯一性
            array('admin_name', 'unique', 'allowEmpty' => false, 'on' => 'add', 'message' => '该用户名已经存在！'),
            array('email', 'unique', 'allowEmpty' => false, 'on' => 'add', 'message' => '该邮箱已经存在！'),
            //必填项
            array('admin_name', 'required', 'message' => '请填写用户名！'),
            array('real_name', 'required', 'message' => '请填写真实姓名！'),
            array('email', 'required', 'message' => '请填写邮箱！'),
            array('password', 'required', 'on' => 'add', 'message' => '密码不能为空！'),
            //密码
            array('password', 'compare', 'compareAttribute' => 'confirm_password', 'operator' => '==', 'message' => '输入的两次密码不一致！', 'on' => 'add,update'),
            //邮箱格式验证
            array('email', 'email', 'message' => '邮箱输入错误！'),
            //安全的。。。
            array('status,potency,confirm_password,wangwang,cat_id', 'safe'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AdminUser the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 获取用户组名称（以后获取有比较完善的角色管理，建议废除）
     * @param type $potency 用户组id
     * @return string
     */
    public function userGroupsLabel($potency) {
        $groups = Role::model()->getSelectRoleList();
        return isset($groups[$potency]) ? $groups[$potency] : '普通管理员';
    }

    /**
     * 登录成功后更新相应信息
     * @param type $uid 用户id
     * @return boolean
     */
    public function loginSaveInfo($uid) {
        if (empty($uid)) {
            return false;
        }
        $user = $this->findByPk((int) $uid);
        if (!$user) {
            return false;
        }
        $user->last_login = time();
        return $user->update();
    }

    /**
     * 获取用户基本表信息
     * @param type $identifier 用户名/用户UID
     * @return boolean
     */
    public function getUserInfo($identifier) {
        $where = '';
        $value = array();
        //检测是否为uid登录方式
        if (is_int($identifier)) {
            $where = 'admin_id=:admin_id';
            $value['admin_id'] = (int) $identifier;
        } else {
            $where = 'admin_name=:admin_name';
            $value['admin_name'] = trim($identifier);
        }
        if (empty($where)) {
            return false;
        }
        $model = $this->find($where, $value);
        if (empty($model)) {
            return false;
        }
        return $model->attributes;
    }

    /**
     * 明文密码进行加密
     * @param type $password 明文密码
     * @return string 加密后的密码
     */
    public function hashPassword($password) {
        return md5(trim($password));
    }

    /**
     * 获取全部管理列表
     * @return boolean
     */
    public function getUserList() {
        $key = 'AdminUser_getUserList';
        //读取缓存
        $cache = false;//Yii::app()->cache->get($key);
        if (empty($cache)) {
            $data = $this->findAll();
            if (empty($data)) {
                return false;
            }
            $cache = array();
            foreach ($data as $rs) {
                $cache[$rs->admin_id] = $rs->attributes;
            }
            //缓存
            Yii::app()->cache->set($key, $cache, 3600);
        }
        return $cache;
    }

    /**
     * 根据管理员ID返回用户名
     * @staticvar array $username
     * @param type $uid
     * @return boolean
     */
    public function getByUidFindName($uid) {
        static $username;
        if (empty($uid)) {
            return false;
        }
        if (isset($username[$uid])) {
            return $username[$uid];
        }
        $info = $this->getUserInfo((int) $uid);
        if (empty($info)) {
            return false;
        }
        $username[$uid] = $info['real_name'];
        return $username[$uid];
    }

    /**
     * 根据栏目ID，得到一个对应管理员UID
     * @param type $cat_id
     * @return int 管理员uid
     */
    public function getAdminIDbyCatid($cat_id) {
        if (empty($cat_id)) {
            return 0;
        }
        $info = $this->find($this->where(array('cat_id' => $cat_id)));
        if (empty($info)) {
            return 0;
        }
        return $info->admin_id;
    }

    /**
     * 数据保存前操作
     */
    protected function beforeSave() {
        //是否新增
        if ($this->isNewRecord) {
            //密码处理
            if (!empty($this->password) && isset($this->password)) {
                $this->password = $this->hashPassword($this->password);
            }
        } else {
            
        }
        return true;
    }

    public function getUserListName(){
        $arrDatas = $this->getUserList();
        $arrResult = array();
        foreach ($arrDatas as $key => $arrData) {
            $arrResult[$key] = $arrData['admin_name'];
        }
        return $arrResult;
    }

}
