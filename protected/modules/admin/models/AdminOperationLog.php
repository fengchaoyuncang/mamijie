<?php

/**
 * 后台操作日志
 * File Name：AdminOperationLog.php
 * File Encoding：UTF-8
 * File New Time：2014-5-16 8:46:47
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class AdminOperationLog extends BaseModel {

    public function tableName() {
        return '{{admin_operation_log}}';
    }

    public function rules() {
        return array(
            array('uid', 'required', 'message' => 'UID不能为空！'),
            array('get', 'required', 'message' => 'GET参数不能为空！'),
            //安全的。。。
            array('time,ip,status,info', 'safe'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 记录日志
     * @param type $message 说明
     */
    public function record($message, $status = 0) {
        $fangs = 'GET';
        if (IS_AJAX) {
            $fangs = 'Ajax';
        } else if (IS_POST) {
            $fangs = 'POST';
        }
        $data = array(
            'uid' => Yii::app()->passport->uid,
            'status' => $status,
            'info' => "提示语：{$message}<br/>模块：" . GROUP_NAME . ",控制器：" . CONTROLLER_NAME . ",方法：" . ACTION_NAME . "<br/>请求方式：{$fangs}",
            'get' => $_SERVER['HTTP_REFERER'],
        );
        $this->setIsNewRecord(true);
        $this->attributes = $data;
        $this->save();
    }

    /**
     * 删除一个月前的日志
     * @return boolean
     */
    public function deleteAMonthago() {
        $where = array("time" => array("lt", time() - (86400 * 30)));
        if ($this->deleteAll($this->where($where)) !== false) {
            return true;
        }
        return false;
    }

    /**
     * 数据保存前操作
     */
    protected function beforeSave() {
        //是否新增
        if ($this->isNewRecord) {
            $this->time = time();
            $this->ip = Tool::getForwardedForIp();
        } 
        return true;
    }
}
