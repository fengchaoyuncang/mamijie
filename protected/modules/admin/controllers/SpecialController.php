<?php

class SpecialController extends AdminBase{
	public function actionCreate()
	{
		$this->baseCreate('GoodsSpecialModel');
	}
	
	public function actionUpdate($id)
	{
		$this->baseUpdate('GoodsSpecialModel');
	}



	public function actionIndex()
	{
		$this->baseIndex('GoodsSpecialModel');
	}


	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		$this->success('删除成功');		
	}

	public function loadModel($id)
	{
		$model=GoodsSpecialModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}
}