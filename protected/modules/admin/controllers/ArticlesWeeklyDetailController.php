<?php
//周刊详细页需要周刊支持，我添加，删除是要在周刊的基础上的，所以控制器需要再找到周刊才行。
class ArticlesWeeklyDetailController extends AdminBase{
	public function actionIndex(){
		
		$this->baseIndex('ArticlesWeeklyDetailModel');
	}
	public function actionCreate($WeeklyID){
		$weeklymodel = $this->loadArticlesWeeklyModel($WeeklyID);
		$ArticlesWeeklyDetailModel = new ArticlesWeeklyDetailModel('admin');
       	$ArticlesWeeklyDetailModel->WeeklyID = $WeeklyID;		
		$this->assign('weeklymodel', $weeklymodel);
		$this->baseCreate('ArticlesWeeklyDetailModel');
	}
	public function actionUpdate(){
		$id = Yii::app()->request->getParam('id');
		$model = $this->loadModel($id);
		$weeklymodel = ArticlesWeeklyModel::model()->findByPk($model->WeeklyID);
		if(!$weeklymodel){
			$this->error('请求错误');
		}		
		$this->assign('weeklymodel', $weeklymodel);	
		$this->baseUpdate($model);
	}
	public function actionDelete(){
		$this->baseDelete('ArticlesWeeklyDetailModel');
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	public function loadArticlesWeeklyModel($id)
	{
		$model=ArticlesWeeklyModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}
	public function loadModel($id)
	{
		$model=ArticlesWeeklyDetailModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}

}