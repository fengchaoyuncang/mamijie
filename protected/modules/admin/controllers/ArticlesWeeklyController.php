<?php

class ArticlesWeeklyController extends AdminBase{
	public function actionIndex(){
		$this->baseIndex('ArticlesWeeklyModel');
	}
	public function actionCreate(){
		$this->baseCreate('ArticlesWeeklyModel');
	}
	public function actionUpdate(){
		$this->baseUpdate('ArticlesWeeklyModel');
	}
	public function actionDelete(){
		$this->baseDelete('ArticlesWeeklyModel');
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	public function loadModel($id)
	{
		$model=ArticlesWeeklyModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}
	
}