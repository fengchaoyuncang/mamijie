<?php

class OpinionController extends AdminBase{
	public function actionIndex(){
		$this->baseIndex('OpinionModel');
	}
	public function actionCreate(){
		$this->baseCreate('OpinionModel');
	}
	public function actionUpdate(){
		$this->baseUpdate('OpinionModel');
	}
	public function actionDelete(){
		$this->baseDelete('OpinionModel');
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	//public function actionDelete($id)
	//{
	//	$this->loadModel($id)->delete();
	//	$this->success('删除成功');		
	//}

	public function loadModel($id)
	{
		$model= OpinionModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}

}