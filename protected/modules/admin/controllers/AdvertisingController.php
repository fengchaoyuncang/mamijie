<?php

class AdvertisingController extends AdminBase{
	public function actionCreate()
	{
		$this->baseCreate('AdvertisingModel');
	}
	
	public function actionUpdate($id)
	{
		$this->baseUpdate('AdvertisingModel');
	}



	public function actionIndex()
	{
		$this->baseIndex('AdvertisingModel');
	}

	public function actionDelete(){
		$this->baseDelete('AdvertisingModel');
	}

	public function loadModel($id)
	{
		$model=AdvertisingModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}
}