<?php

class CategoryArticleController extends AdminBase{

    public function actionIndex()
    {
        //因其不一样的展示方式。获取的这个数据不是普通的那样获取的。所以是这样的。
        $arrData = CategoryArticleModel::getListTreeArr();
        $this->assign('count', count($arrData));
        $this->assign('model', CategoryArticleModel::model());
        $this->assign('data', $arrData);
        $this->render('index');  
    }

    public function actionDelete($id)
    {
        if(CategoryArticleModel::deleteCategoryByPk($id)){
            $this->success('删除成功');     
        }
        $this->error('删除失败');  
    }

    public function actionCreate(){
        $objModel = new CategoryArticleModel('admin');
        $objModel->ParentID = Yii::app()->request->getParam('id');
        $this->baseCreate($objModel);
    }
    public function actionUpdate(){
        $this->baseUpdate('CategoryArticleModel');
    }

    public function loadModel($id)
    {
        $model=CategoryArticleModel::model()->findByPk($id);
        if($model===null)
            $this->error('找不到记录');
        return $model;
    }    
}