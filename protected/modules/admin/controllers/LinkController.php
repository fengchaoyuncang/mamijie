<?php

class LinkController extends AdminBase{
	public function actionCreate()
	{
		$this->baseCreate('LinkModel');
	}
	
	public function actionUpdate($id)
	{
		$this->baseUpdate('LinkModel');
	}



	public function actionIndex()
	{
		$this->baseIndex('LinkModel');
	}


	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		$this->success('删除成功');		
	}

	public function loadModel($id)
	{
		$model=LinkModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}
}