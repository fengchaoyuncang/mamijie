<?php
class GoodsBmController extends AdminBase{

	//$arrData 其它控制器访问这个控制器的时候加的条件。
	public function actionIndex($arrData = array()){
        $objModel = new GoodsBmModel('admin');
        $objModel->unsetAttributes();

        $objModel->attributes = $arrData;
        if(isset($_GET['GoodsBmModel']) || isset($_GET['key']))
        {
        	if(isset($_GET['key'])){
        		switch ($_GET['key']) {
        			case '1':
        				$objModel->ProductID = $_GET['keyword'];
        				break;
        			case '2':
        				$objModel->GoodsName = $_GET['keyword'];
        				break;
        			case '3':
        				$objModel->Nick = $_GET['keyword'];
        				break;
        			case '4':
        				$objModel->GoodsBmID = $_GET['keyword'];
        				break;       			
        			default:
        				# code...
        				break;
        		}
        	}
            $objModel->attributes = $_GET['GoodsBmModel'];
        }

        $this->baseIndex($objModel,array(),20,'index');          
	}



	/**
	 * 待审核商品   状态还是等于0的商品
	 * @return [type] [description]
	 */
	public function actionCheck(){
		$arrData = array(
			'Status' => 0,
		);
		//待审核的总数
		$dcount = GoodsBmModel::model()->count(BaseModel::getC(array('Status' => 0, 'AdminID' => 0)));
		$this->assign('dcount', $dcount);
		//不是超管则只能查看自己审核通过的商品		
		if(!yii::app()->passport->isAdministrator()){
			$arrData['AdminID'] = AdminBase::$uid;
		}
		$this->actionIndex($arrData);
	}

	/**
	 * 审核通过
	 * @return [type] [description]
	 */
	public function actionPass(){
		$arrData = array(
			'Status' => 1,
		);
		//不是超管则只能查看自己审核通过的商品		
		if(!yii::app()->passport->isAdministrator()){
			$arrData['AdminID'] = AdminBase::$uid;
		}
		$this->actionIndex($arrData);
	}

	/**
	 * 审核未通过
	 * @return [type] [description]
	 */
	public function actionNoPass(){
		$arrData = array(
			'Status' => 2,
		);
		//不是超管则只能查看自己审核通过的商品		
		if(!yii::app()->passport->isAdministrator()){
			$arrData['AdminID'] = AdminBase::$uid;
		}
		$this->actionIndex($arrData);
	}

	/**
	 * 邮寄样品列表
	 * @return [type] [description]
	 */
	public function actionMail(){
		$arrData = array(
			'Status' => 3,
		);
		//不是超管则只能查看自己审核通过的商品		
		if(!yii::app()->passport->isAdministrator()){
			$arrData['AdminID'] = AdminBase::$uid;
		}
		$this->actionIndex($arrData);
	}

	/**
	 * 审核历史记录
	 * @return [type] [description]
	 */
	public function actionLogs(){

		$this->baseIndex('GoodsOperationModel');  
	}
    public function actionQuickUpdate($id){//这里修改要跟其它的逻辑一起进行事务处理，所以特殊的条件来处理。
		$model = $this->loadModel($id);
		if($model->Pattern == 1 && !empty($_POST['GoodsBmModel']['Status']) && ($_POST['GoodsBmModel']['Status'] == 2 || $_POST['GoodsBmModel']['Status'] == 4)){
			
			$model->setScenario('admin');
			$model->attributes = $_POST['GoodsBmModel'];
			if(MemberMoneyChangeModel::model()->cancelGoods($model->UserID,$id,$_POST['GoodsBmModel']['Status'])){
				$this->success('处理成功');
			}else{
				$this->error(MemberMoneyChangeModel::model()->getOneError());
			}
		}
		$this->baseUpdate($model, '', true);
    }

	//修改商品
	public function actionUpdate($id){
		$model = $this->loadModel($id);
		if($model->Pattern == 1 && !empty($_POST['GoodsBmModel']['Status']) && ($_POST['GoodsBmModel']['Status'] == 2 || $_POST['GoodsBmModel']['Status'] == 4)){
			
			$model->setScenario('admin');
			$model->attributes = $_POST['GoodsBmModel'];
			if(MemberMoneyChangeModel::model()->cancelGoods($model->UserID,$id,$_POST['GoodsBmModel']['Status'])){
				$this->success('处理成功');
			}else{
				$this->error(MemberMoneyChangeModel::model()->getOneError());
			}
		}
		$this->baseUpdate($model);
	}
	//删除商品
	public function actionDelete(){
		$this->error('占时不开放修改');
		$this->baseDelete('TagModel');
	}
	//显示具体信息
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	//加载model
	public function loadModel($id)
	{
		$model=GoodsBmModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}

	/**
	 * 审核通过的商品发布到商品表
	 * @return [type] [description]
	 */
	public function actionRelease(){
		if(GoodsBmModel::release(Yii::app()->request->getParam('id'))){
			$this->success('发布成功');
		}else{
			$this->error('发布失败');
		}
	}
	/**
	 * 获取待审核的商品
	 * 如果是超管则获取100个不管什么分类
	 * 如果是其它的角色则根据自己的角色来获取。
	 * @return [type] [description]
	 */
	public function actionGetgoodsbm(){
		//先获取此用户负责的catid，如果是超管则所有的都可以，
		$arrData = array();
		if(!yii::app()->passport->isAdministrator()){
			$arrC = AdminCategoryModel::model()->getAdminCategoryList(AdminBase::$uid);
			$c = array();
			foreach ($arrC as $cat) {
				$c = CMap::mergeArray($c,CategoryModel::getArrChildid($cat));
			}
			$strC = BaseModel::getStrIn($c);
			$arrData['CatID'] = array('sign' => 'in', 'data' => $strC);
		}
		$arrData['AdminID'] = 0;
		$arrData['Status'] = 0;	
		$arrData['order'] = 'AddTime ASC';
		$arrData['limit'] = 100;
		GoodsBmModel::model()->updateAll(array('AdminID' => self::$uid,), $objCriteria = BaseModel::getC($arrData));
		$this->success('获取成功', $this->createUrl('check'));				
	}

	/**
	 * 通过报名ID来通过用户
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function actionNotice($id){
		if(!Yii::app()->request->getParam('notice')){
			$this->error('内容不能为空');
		}
		$model = $this->loadModel($id);
		if($model){
			$memberModel = MemberModel::model()->findByPk($model->UserID);
			if(Sns::getInstance()->send($memberModel->Phone, Yii::app()->request->getParam('notice'))){
				$this->success('发送成功');
			}else{
				$this->error(Sns::getInstance()->getError());
			}
		}else{
			$this->error('找不到记录');
		}
	}

	public function actionFenpei(){
		$model = $this->loadModel(Yii::app()->request->getParam('id'));
		$model->AdminID = $_POST['AdminID'];
		$this->baseUpdate($model, '', true);
	}



}


?>