<?php

/**
 * 角色管理
 * File Name：RbacController.php
 * File Encoding：UTF-8
 * File New Time：2014-5-8 11:21:39
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class RbacController extends AdminBase {

    //角色列表
    public function actionIndex() {
        $model = Role::model()->findAll(array('order' => 'id desc'));
        $this->assign('data', $model ? $model : array());
        $this->render();
    }

    //添加角色
    public function actionAdd() {
        if (IS_POST) {
            $model = new Role('add');
            $model->attributes = $_POST;
            if ($model->validate() && $model->save()) {
                $this->success('添加成功！', AdminBase::U('index'));
            } else {
                $error = $model->getOneError();
                $this->error($error ? $error : '添加失败！');
            }
        } else {
            $this->render();
        }
    }

    //编辑角色
    public function actionEdit() {
        $id = (int) Yii::app()->request->getParam('id');
        if (empty($id)) {
            $this->error('请指定需要编辑的角色！');
        }
        $model = Role::model()->findByPk($id);
        if (empty($model)) {
            $this->error('该角色不存在！');
        }
        if (IS_POST) {
            $model->attributes = $_POST;
            if ($model->validate() && $model->save()) {
                $this->success('修改成功！', AdminBase::U('index'));
            } else {
                $error = $model->getOneError();
                $this->error($error ? $error : '修改失败！');
            }
        } else {
            $this->assign('data', $model->attributes);
            $this->render();
        }
    }

    //删除
    public function actionDelete() {
        $this->baseDelete('Role', 'index');
    }

    //权限分配
    public function actionAuthorize() {
        if (IS_POST) {
            $roleid = (int) $_POST['roleid'];
            if (empty($roleid)) {
                $this->error("需要授权的角色不存在！");
            }
            //清空旧权限
            if (Access::model()->accessEmpty($roleid) === false) {
                $this->error('权限初始化错误！');
            }
            //被选中的菜单项
            $menuidAll = explode(',', Yii::app()->request->getParam('menuid'));
            if (is_array($menuidAll) && count($menuidAll) > 0) {
                //取得菜单数据
                $menuinfo = Menu::model()->findAll();
                $menu_info = array();
                foreach ($menuinfo as $_v) {
                    $menu_info[$_v->id] = array(
                        'app' => $_v->app,
                        'controller' => $_v->controller,
                        'action' => $_v->action,
                        'type' => $_v->type,
                    );
                }
                //授权数据集合
                $addauthorize = array();
                //检测数据合法性
                foreach ($menuidAll as $menuid) {
                    $info = $menu_info[$menuid];
                    if (empty($info)) {
                        continue;
                    }
                    //菜单项
                    if ($info['type'] == 0) {
                        $info['app'] = $info['app'];
                        $info['controller'] = $info['controller'] . $menuid;
                        $info['action'] = $info['action'] . $menuid;
                    }
                    $info['role_id'] = $roleid;
                    $info['status'] = $info['type'] ? 1 : 0;
                    $access = new Access();
                    $access->attributes = $info;
                    if (!$access->save()) {
                        $this->error($access->getOneError());
                    } else {
                        $addauthorize[] = $info;
                    }
                }
                $this->success("授权成功！", AdminBase::U("index"));
            } else {
                //当没有数据时，清除当前角色授权
                $this->Access->where(array("role_id" => $roleid))->delete();
                $this->error("没有接收到数据，执行清除授权成功！");
            }
        } else {
            //角色ID
            $roleid = (int) $_GET['id'];
            if (empty($roleid)) {
                $this->error("请选择需要分配权限的角色！");
            }
            //菜单缓存
            $result = Menu::model()->getList();
            //获取已权限表数据
            $priv_data = Access::model()->getAccessList($roleid);
            $json = array();
            foreach ($result as $rs) {
                $data = array(
                    'id' => $rs['id'],
                    'checked' => $rs['id'],
                    'parentid' => $rs['parentid'],
                    'name' => $rs['name'] . ($rs['type'] == 0 ? "(菜单项)" : ""),
                    'checked' => Role::model()->isCompetence($rs, $roleid, $priv_data) ? true : false,
                );
                $json[] = $data;
            }

            $this->assign('json', json_encode($json));
            $this->assign("roleid", $roleid);
            $this->assign('menuReturn', array('name' => '角色管理', 'url' => AdminBase::U('index')));
            $this->render();
        }
    }

    //成员列表
    public function actionList() {
        //角色ID
        $roleid = (int) $_GET['id'];
        if (empty($roleid)) {
            $this->error("请选择角色！");
        }
        $list = AdminUser::model()->findAll(array(
            'condition' => 'potency=:potency',
            'order' => 'admin_id DESC',
            'params' => array('potency' => $roleid),
        ));
        if (empty($list)) {
            $list = array();
        }
        $this->assign('list', $list);
        $this->assign('menuReturn', array('name' => '角色管理', 'url' => AdminBase::U('index')));
        $this->render('/management/index');
    }

}
