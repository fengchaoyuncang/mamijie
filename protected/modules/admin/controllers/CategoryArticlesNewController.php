<?php

class CategoryArticlesNewController extends AdminBase{

    public function actionIndex()
    {

        $arrData = CategoryArticlesNewModel::getListTreeArr();
        $this->assign('count', count($arrData));
        $this->assign('model', CategoryArticlesNewModel::model());
        $this->assign('data', $arrData);
        $this->render('index');  
    }

    public function actionDelete($id)
    {
        if(CategoryArticlesNewModel::deleteCategoryByPk($id)){
            $this->success('删除成功');     
        }
        $this->error('删除失败');  
    }

    public function actionCreate(){

        $objModel = new CategoryArticlesNewModel('admin');
        $objModel->ParentID = Yii::app()->request->getParam('id');
        $this->baseCreate($objModel);  
    }
    public function actionUpdate(){
        $this->baseUpdate('CategoryArticlesNewModel');
    }

    public function loadModel($id)
    {
        $model=CategoryArticlesNewModel::model()->findByPk($id);
        if($model===null)
            $this->error('找不到记录');
        return $model;
    }    
}