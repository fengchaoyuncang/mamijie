<?php
class CountController extends AdminBase{
	//广告统计
	public function actionAd(){
		$this->baseIndex('CountAdModel');
	}
	//商品统计
	public function actionGoods(){

        $objModel = new CountGoodsModel('admin');
        $objModel->unsetAttributes();
        if(isset($_GET['CountGoodsModel']))
        {
            $objModel->attributes = $_GET['CountGoodsModel'];
        }

        $objCriteria = $objModel->createSearchCriteria();

       
        //分页
        $pageId = isset($_GET['page']) ? (int) $_GET['page'] : 1;

        $count = $objModel->count($objCriteria);
        //分页
        $page = self::page($count, 20, $pageId,'',$objCriteria);

        //排序
        
        $objSort = new CSort('CountGoodsModel');
        $objSort->defaultOrder = 'Deal desc,CountID desc';        
        $objSort->applyOrder($objCriteria);







        $data = $objModel->findAll($objCriteria);
        //销量总数
        $objCriteria->select = 'sum(Deal) as Deal';
        $dangSum = $objModel->find($objCriteria)->Deal;
        $objCriteria->select = 'sum(Sales) as Sales';
        $zongSales = $objModel->find($objCriteria)->Sales;

        $this->assign($_GET);
        $this->assign('dangSum', $dangSum);
        $this->assign('zongSales', $zongSales);        
        $this->assign("Page", $page->show());
        $this->assign('data', $data);
        $this->assign('sort', $objSort);
        $this->assign('model', $objModel);
        $this->assign('count', $count);   
        $this->render();  
	}


    public function actionDeleteGoods(){
        $StartTime = isset($_GET['start_time']) ? $_GET['start_time'] : '';
        $EndTime = isset($_GET['end_time']) ? $_GET['end_time'] : '';

        if(!empty($StartTime)){
            $StartTime = strtotime($StartTime);
        }

        if(!empty($EndTime)){
            $EndTime = strtotime($EndTime) + 86400;
        }

        if(!$StartTime){
            $this->error('请求出错');
        }
        if($EndTime){
            $sql = "delete FROM tk_sales_data where TCreateTime >= {$StartTime} and TCreateTime < {$EndTime}";
        }else{
            $sql = "delete FROM tk_sales_data where TCreateTime >= {$StartTime}";
        }
        $connection=Yii::app()->db_count; 
        $command=$connection->createCommand($sql);
        $intCount=$command->execute();
        $this->success('处理成功');

    }


    public function actionGoodsOperate(){
        global $COUNT;
        //算总的
        $mixTime = $countTime = Yii::app()->request->getParam('countTime');
        $GoodsID = Yii::app()->request->getParam('GoodsID');        
        $n = Yii::app()->request->getParam('n');
        empty($n) ? $n = 1 : '';
        if($mixTime){
            $mixTime = strtotime($mixTime);
        }else{
            $mixTime = strtotime('yesterday');
        }

        //这个时间的年月日，
        $intYear = date('Y', $mixTime);
        $intMonth = date('m', $mixTime);
        $intDay = date('d', $mixTime);

        $strTime = date('Ymd', $mixTime);
        GuestDataModel::$strTableName = '{{guest_data'.$strTime.'}}';

        //这个时间的这一天的开始时间  与结束时间
        $intCStart = strtotime($intYear.'-'.$intMonth.'-'.$intDay);
        $intCEnd = $intCStart + 86400;
        //算出这一天在上架的商品
        $objModel = new GoodsModel();
        $arrWhere = array(
            'StartTime' => array('sign' => '<', 'data' => $intCEnd),
            'EndTime' => array('sign' => '>=', 'data' => $intCStart),   
            'order' => 'GoodsID asc',      
        );
        $count = $objModel->count(BaseModel::getC($arrWhere));

        if($GoodsID){
            $arrWhere['GoodsID'] = array('sign' => '>', 'data' => $GoodsID);
        }
        $objCriteria = BaseModel::getC($arrWhere);

        $objCriteria->limit = 50;
        //这一天在上架的商品
        $objDatas = $objModel->findAll($objCriteria);

        foreach($objDatas as $objData){
            //获取这商品这一天，总查看数，会员查看数，游客查看数
            $arrW = array('Year' => $intYear, 'Month' => $intMonth, 'Day' => $intDay);            
            $arrW['GoodsID'] = $objData->GoodsID;
            $View = GuestDataModel::model()->count(BaseModel::getC($arrW));
            $arrW['UserID'] = array('sign' => '>', 'data' => 0);
            $UserView = GuestDataModel::model()->count(BaseModel::getC($arrW));
            $TouristView = $View - $UserView;


            $Deal = SalesDataModel::model()->getTurnover($objData->GoodsID, $intCStart, $intCEnd);//成交量
            $MmjDeal = SalesDataModel::model()->getTurnoverGuan($objData->GoodsID, $intCStart, $intCEnd);//妈咪街成交量
            $UDeal = $Deal - $MmjDeal;//U站成交量

            $arrData = array(
                'GoodsID' => $objData->GoodsID,
                'Year' => $intYear,
                'Month' => $intMonth,
                'Day' => $intDay,
                'ProductID' => $objData->ProductID,
                'AdminID' => $objData->AdminID,
                'CatID' => $objData->CatID,
                //'ModuleID' => $objData->ModuleID,
                'StartTime' => $objData->StartTime,
                'EndTime' => $objData->EndTime,
                'Pprice' => $objData->Pprice,
                'PromoPrice' => $objData->PromoPrice,
                'View' => $View,
                'UserView' => $UserView,
                'TouristView' => $TouristView,
                'Deal' => $Deal,
                'MmjDeal' => $MmjDeal,
                'UDeal' => $UDeal,
                'Sales' => $Deal * $objData->PromoPrice,
                'RecordTime' => $intCStart,
            );

            $objModel = CountGoodsModel::model()->find(BaseModel::getC(array('GoodsID' => $objData->GoodsID, 'Year' => $arrData['Year'], 'Month' => $arrData['Month'], 'Day' => $arrData['Day'])));
            if(!$objModel){
                $objModel = new CountGoodsModel();
            }
            $objModel->attributes = $arrData;
            $objModel->save(false);
        }

        $goods_id = empty($objModel->GoodsID) ?  0 :  $objModel->GoodsID;
        $num = ceil($count/50);
        if($goods_id > 0){
            $b = $n+1;
            $this->success("总共{$count}产品,需要分析{$num}次,现在是处理完第{$n}次", $this->createUrl('goodsOperate', array('countTime' => $countTime, 'n' => $b, 'GoodsID' => $goods_id)));
        }else{
            $this->success('处理完成', $this->createUrl('goods'));
        }  

    }
	//来路分析统计
	public function actionReferer(){
		$this->assign($_GET);
		$this->baseIndex('CountRefererModel');
	}
	//销量管理
	public function actionSalesData(){

        $objModel = new SalesDataModel('admin');
        $objModel->unsetAttributes();
        if(isset($_GET['SalesDataModel']) || isset($_GET['key']))
        {
        	if(isset($_GET['key'])){
        		switch ($_GET['key']) {
        			case '1':
        				$objModel->TGoodsName = $_GET['keyword'];
        				break;
        			case '2':
        				$objModel->TProductID = $_GET['keyword'];
        				break;
        			case '3':
        				$objModel->GoodsID = $_GET['keyword'];
        				break;
        			case '4':
        				$objModel->TOrderNumber = $_GET['keyword'];
        			case '5':
        				$objModel->GoodsBmID = $_GET['keyword'];
        				break;       			
        			default:
        				# code...
        				break;
        		}
        	}
            $objModel->attributes = $_GET['SalesDataModel'];
        }

        $objCriteria = $objModel->createSearchCriteria();

       
        //分页
        $pageId = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        //信息总数
        $count = $objModel->count($objCriteria);

        $objCriteria->select = 'sum(TPrice) TPrice';
        $data = $objModel->find($objCriteria);
        $this->assign('price_num', $data->TPrice);
        $objCriteria->select = '*';


        //分页
        $page = self::page($count, 20, $pageId,'',$objCriteria);

        //排序
        
        $objSort = new CSort('SalesDataModel');
        $objSort->defaultOrder = 'AddTime DESC';
        $objSort->applyOrder($objCriteria);


        //销量多少，，查询ID等于0的，



        $data = $objModel->findAll($objCriteria);  
        $this->assign($_GET);    
        $this->assign("Page", $page->show());
        $this->assign('data', $data);
        $this->assign('sort', $objSort);
        $this->assign('model', $objModel);
        $this->assign('count', $count);   
        $this->render();  

	}
	//导入销量
    //导入
    public function actionImport() {
        $model = SalesDataModel::model();
        if (IS_POST) {
            if (empty($_FILES['file']['name'])) {
                $this->error("请选择上传文件！");
            }
            //上传临时文件地址
            $filename = $_FILES['file']['tmp_name'];
            if (strtolower(substr($_FILES['file']['name'], -3, 3)) != 'csv') {
                $this->error("上传的文件格式有误！");
            }
            if ($model->excelImport($filename)) {
                $source = (int) $_POST['source'];
                $this->success('数据读取成功，开始导入数据！', self::U('import', array('act' => 'import', 'source' => $source)));
            } else {
                $this->error('数据导入失败！');
            }
        } else {
            $action = $_GET['act'];
            $count = (int) $_GET['count'];
            if ($action == 'import') {
                $source = (int) Yii::app()->request->getParam('source');
                $key = 'SalesDataModel_excelImport';
                $memcache = new Memcache();
                $memcache->connect('127.0.0.1', 11211);
                $dataList = $memcache->get($key);
                if (empty($dataList)) {
                    $this->success('导入完成1！', self::U('import'));
                    exit;
                }
                $sum = $_GET['sum'] ? $_GET['sum'] : count($dataList);
                $i = 0;
                foreach ($dataList as $k => $rs) {
                    $model->import($rs, $source);
                    unset($dataList[$k]);
                    $i++;
                    $count++;
                    if ($i >= 100) {
                        if (empty($dataList)) {
                            $memcache->delete($key);
                        } else {
                            $memcache->set($key, $dataList, 0, 600);
                        }
                        $this->success("数据导入中....，已导入：{$count}，总数：{$sum}", self::U('import', array('count' => $count, 'act' => 'import', 'sum' => $sum, 'source' => $source)));
                        exit;
                    }
                }
                $this->success('导入完成2！', self::U('import'));
            } else {
                $this->render();
            }
        }
    }


    public function actionGuestData(){
        //不是所有的页面都需要。。。。。
        set_time_limit(0);
        //先获取时间戳
        $time = Yii::app()->request->getParam('time');
        $time = empty($time) ? TIME_TIME : strtotime($time);
        $strTime = date('Ymd', $time);
        GuestDataModel::$strTableName = '{{guest_data'.$strTime.'}}'; //设置了分析哪一张表的数据
        $table = GuestDataModel::$strTableName;
        $this->assign($_GET);

        
        $result = array();
        $result['整站']['click'] = GuestDataModel::model()->count(BaseModel::getC(array()));
        $result['整站']['ip'] = GuestDataModel::model()->count(BaseModel::getC(array('group' => 'Ip')));

        $result['首页']['click'] = GuestDataModel::model()->count(BaseModel::getC(array('Module' => 'front','Controller' => 'index','Action' => 'index')));
        $result['首页']['ip'] = GuestDataModel::model()->count(BaseModel::getC(array('Module' => 'front','Controller' => 'index','Action' => 'index','group' => 'Ip')));

        $result['23活动']['click'] = GuestDataModel::model()->count(BaseModel::getC(array('Module' => 'huodong','Controller' => 'index','Action' => 'index')));
        $result['23活动']['ip'] = GuestDataModel::model()->count(BaseModel::getC(array('Module' => 'huodong','Controller' => 'index','Action' => 'index','group' => 'Ip')));
        
        $result['品牌团']['click'] = GuestDataModel::model()->count(BaseModel::getC(array('Module' => 'front','Controller' => 'goods','Action' => 'banner')));
        $result['品牌团']['ip'] = GuestDataModel::model()->count(BaseModel::getC(array('Module' => 'front','Controller' => 'goods','Action' => 'banner','group' => 'Ip')));
        
        $result['百科页']['click'] = GuestDataModel::model()->count(BaseModel::getC(array('Module' => 'front','Controller' => 'baike')));
        $result['百科页']['ip'] = GuestDataModel::model()->count(BaseModel::getC(array('Module' => 'front','Controller' => 'baike','group' => 'Ip')));
                
        $result['19.9']['click'] = GuestDataModel::model()->count(array('condition' => 'Url like "%eid=2%"'));
        $result['19.9']['ip'] = GuestDataModel::model()->count(array('condition' => 'Url like "%eid=2%"','group' => 'Ip'));
          

        $category = CategoryModel::getList(true,true);
        foreach($category as $d){
            $result[$d['Title']]['click'] = GuestDataModel::model()->count(BaseModel::getC(array('CatID' => $d['CatID'])));
            $result[$d['Title']]['ip'] = GuestDataModel::model()->count(BaseModel::getC(array('CatID' => $d['CatID'],'group' => 'Ip')));
        }


        

        $this->assign('result',$result);
        $this->render('guestData');
    }
}

