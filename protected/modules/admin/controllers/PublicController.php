<?php

/**
 * 公共方法
 * File Name：PublicController.php
 * File Encoding：UTF-8
 * File New Time：2014-4-29 15:50:31
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class PublicController extends AdminBase {

    //特殊的控制器方法
    public function actions() {
        return array(
            //验证码
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'transparent' => true, //是否使用透明背景
                'maxLength' => 6, // 最多生成几个字符
                'minLength' => 4, // 最少生成几个字符
            ),
        );
    }

    //显示后台登录界面
    public function actionLogin() {
        if (IS_POST) {
            $username = Yii::app()->request->getParam('username');
            $password = Yii::app()->request->getParam('password');
            if (empty($username)) {
                $this->error('用户名不能为空！');
            }
            if (empty($password)) {
                $this->error('请输入密码！');
            }
            // if (!$this->validateVerifyCode(Yii::app()->request->getParam('code'))) {
            //     $this->error('验证码错误，请重新输入！');
            // }
            //验证登录
            if (Yii::app()->passport->loginAdmin($username, $password)) {
                //$this->success('登录成功！', self::U('index/index'));
                $this->redirect(array('index/index'));
            } else {
                $error = Yii::app()->passport->error ? Yii::app()->passport->error : '登录失败！';
                $this->error($error);
            }
        } else {
            //如果已经登录，直接跳转到后台首页
            if (AdminBase::$uid) {
                $this->redirect(array('index/index'));
            }
            $this->render('login');
        }
    }


    //采集孕期周刊
    //[\s\S]+?
    public function actionWeeklyPregnancy(){
        set_time_limit(0);
        $m = '([\s\S]+?)';

        for ($i=20; $i < 42; $i++) {
            $arr = array();
            $url = 'http://www.babytree.com/know/weekly.php?type=yunqi_'.$i.'_1';

            $obj = new Curl();
            $content = $obj->post($url,'');

            echo $content;exit;

            $model = ArticlesWeeklyPregnancyModel::model();
            $model->setIsNewRecord(true);
            $model->week = $i;

            $begin = '<div style=" font-size:14px; font-weight:bold;">[\s\S]+?<\/div>';
            $end = '<\!--div align="right">';
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->brief = str_replace("style='padding:8px 0 0; margin:0;text-indent:2em;font-size:12px; '", '', $matches[1]);
            $model->brief = strstr($model->brief, '<p');



            $begin = "<div style='padding:5px 20px 10px; font-family:宋体; text-indent:2em; font-size:12px; line-height:22px; color:#5b5b5b;'>";
            $end = "<\/div>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->reading = $matches[1];


            $begin = "<div style='padding:10px 20px; font-size:12px; color:#5b5b5b; font-family:宋体; line-height:22px;'>";
            $end = "<\/div>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            preg_replace("/<a[^>]*>(.*)<\/a>/isU",'${1}',$matches[1]);
            $model->matter = $matches[1];


            $begin = "<td style=\"font-size:12px;font-family:'宋体'; color:#5b5b5b; line-height:22px; padding:15px 20px 5px\">[\s\S]+?<div style='text-indent:2em;'>";
            $end = "<\/div>                        <\/td>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            preg_replace("/<a[^>]*>(.*)<\/a>/isU",'${1}',$matches[1]);
            $model->dad = $matches[1];


            $begin = "<p style=\"padding:40px 30px 0 20px;line-height:2;\">&emsp;&emsp;";
            $end = "<\/p>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->care = $matches[1];
            //echo $model->care;exit;


            $begin = "align='absbottom'>[\s－—]+";
            $end = "<\/td><\/tr>[\s\S]+?<tr><td><img src='";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->focus_title = str_replace("－", '', $matches[1]);
            $model->focus_title = str_replace("—", '', $model->focus_title);


            $begin = "<td style='border:5px solid #c3e9fe;border-top:0; border-bottom:0;font-size:12px; padding:5px 20px; font-family:宋体; color:#5b5b5b; line-height:22px;' >";
            $end = "<\/td><\/tr>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->focus_content = $matches[1];
            //echo $model->focus_content;exit;



            $begin = "<div style='text-indent:2em; padding:35px 10px 0 0;'>";
            $end = "<div align='right'>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->focus_brief = $matches[1];
            //echo $model->focus_brief;exit;




            $begin = "<td valign='bottom' style='font-size:14px; text-align:center; font-weightbold; color:#ff691d;'>";
            $end = "<\/td><td width='80'><img src='http:";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->shi_title = $matches[1];
            //echo $model->shi_title;exit;


            $begin = "<td style='border:5px solid #c3e9fe;border-top:0; color:#5b5b5b; border-bottom:0;font-size:12px; padding:10px 15px 5px; font-family:宋体; color:#5b5b5b; line-height:22px;' >";
            $end = "<\/td>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->shi_content = $matches[1];
            //echo $model->shi_content;exit;

            $begin = "<p style='text-indent:2em;padding:0; margin:0'>";
            $end = "<\/p>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->fetal_education = $matches[1];


            $begin = "<strong class=\"pinkcolor\">\[综述\]<\/strong>";
            $end = "<\div>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->brief = $matches[1];


            $begin = "胎儿[\s\S]+?情况<\/div>";
            $end = "<\div>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->fayu = $matches[1];

            $begin = "<div class=\"model-1-rbox\">[\s\S]+<p align=\"center\">[\s\S]+<img src=\"";
            $end = "\" \/><\/p>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->fayu_img = $matches[1];


            $begin = "孕妈妈的身体变化<\/div>";
            $end = "<\div>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->shentiyianhua = $matches[1];


            $begin = "<div class=\"weekly-2011-left-model-2\" id=\"pos_read\">[\s\S]+<img src=\"";
            $end = "\" align=\"left\" \/>";
            $mathch = "/{$begin}{$m}{$end}/";
            preg_match($mathch,$content, $matches);
            $model->shentiyianhua_img = $matches[1];




            $model->save(false);
            echo $i;echo 'ok</br>';
        }

        
    }


}
