<?php

class ArticlesNewController extends AdminBase{

    public function actionIndex()
    {
        $this->baseIndex('ArticlesNewModel');
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        $this->success('删除成功'); 
    }

    public function actionCreate(){
       $this->baseCreate('ArticlesNewModel');  
    }
    public function actionUpdate(){
        $this->baseUpdate('ArticlesNewModel');
    }

    public function loadModel($id)
    {
        $model=ArticlesNewModel::model()->findByPk($id);
        if($model===null)
            $this->error('找不到记录');
        return $model;
    }    
}