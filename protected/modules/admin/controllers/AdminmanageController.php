<?php

/**
 * 我的信息，个人信息相关
 * File Name：AdminmanageController.php
 * File Encoding：UTF-8
 * File New Time：2014-5-4 10:03:54
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class AdminmanageController extends AdminBase {

    //修改我的资料
    public function actionMyinfo() {
        if (IS_POST) {
            $model = AdminUser::model()->find(array(
                'select' => 'admin_id,email,real_name',
                'condition' => 'admin_id = :admin_id',
                'limit' => 1,
                'params' => array('admin_id' => self::$uid),
            ));
            if (!is_object($model)) {
                $this->error('系统出现错误！');
            }
            //设置临时rules
            $model->setValidators(array(
                array('email', 'email', 'message' => '邮箱输入错误！'),
                array('real_name,email', 'required', 'message' => '请填写完整！'),
                array('email', 'unique', 'allowEmpty' => false, 'message' => '该邮箱已经存在！'),
                array('wangwang', 'required', 'message' => '请填写旺旺！'),
            ));
            $model->attributes = $_POST;
            if ($model->validate() && $model->save()) {
                $this->success('修改成功！', AdminBase::U('myinfo'));
            } else {
                $error = $model->getOneError();
                $this->error($error ? $error : '修改失败！');
            }
        } else {
            $model = AdminUser::model()->findByPk((int) self::$uid);
            $this->assign('info', $model->attributes);
            $this->render();
        }
    }

    //修改密码
    public function actionPassword() {
        if (Yii::app()->request->isPostRequest) {
            //旧密码
            $password = Yii::app()->request->getPost('password');
            //新密码
            $new_password = Yii::app()->request->getPost('new_password');
            $new_pwdconfirm = Yii::app()->request->getPost('new_pwdconfirm');
            if (empty($password)) {
                $this->error('密码不能为空！');
            }
            if ($new_password != $new_pwdconfirm) {
                $this->error('两次输入的密码不一致！');
            }
            //验证旧密码
            if ($this->verificationPass($password)) {
                $model = AdminUser::model()->findByPk(self::$uid);
                if ($model) {
                    $model->password = AdminUser::model()->hashPassword($new_password);
                    if ($model->update() !== false) {
                        Yii::app()->passport->logoutLocal();
                        $this->success('密码修改成功，请重新登录！', AdminBase::U('public/login'));
                    } else {
                        $this->error('密码修改失败！');
                    }
                } else {
                    $this->error('系统出现错误！');
                }
            } else {
                $this->error('原始密码错误，无法进行修改！');
            }
        } else {
            $this->assign('username', self::$username);
            $this->render();
        }
    }

    //验证密码是否正确
    public function actionPublic_verifypass() {
        //旧密码
        $password = Yii::app()->request->getParam('password');
        if (empty($password)) {
            $this->error('密码不能为空！');
        }
        if ($this->verificationPass($password)) {
            $this->success('密码正确！');
        } else {
            $this->error('密码错误！');
        }
    }

    /**
     * 密码验证
     * @param type $password
     * @return boolean
     */
    private function verificationPass($password) {
        if (empty($password)) {
            return false;
        }
        //密码验证
        $userInfo = AdminUser::model()->getUserInfo(self::$uid);
        if ($userInfo['password'] == AdminUser::model()->hashPassword($password)) {
            return true;
        }
        return false;
    }

}
