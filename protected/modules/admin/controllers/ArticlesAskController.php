<?php

class ArticlesAskController extends AdminBase{

    public function actionIndex()
    {
        $this->baseIndex('ArticlesAskModel');
    }

    public function actionDelete(){
      $this->baseDelete('ArticlesAskModel');
    }

    public function actionCreate(){
        //百度编辑器的一个字段，字段名不能用有包含中括号的。
       $model = new ArticlesAskModel('admin');       
       $model->IsShow = 1;
       $model->ContentTime = TIME_TIME;
       $model->AnswerTime = TIME_TIME;
       
       $this->baseCreate($model);  
    }
    public function actionUpdate(){        
        $this->baseUpdate('ArticlesAskModel');
    }

    public function loadModel($id)
    {
        $model=ArticlesAskModel::model()->findByPk($id);
        if($model===null)
            $this->error('找不到记录');
        return $model;
    }    
}