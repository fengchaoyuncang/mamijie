<?php
/**
 * 入驻管理
 */
class MerchantsSettledController extends AdminBase{
	public function actionIndex(){
		$this->baseIndex('MerchantsSettledModel');
	}
	public function actionIndex0(){
		$_GET['MerchantsSettledModel']['Status'] = 0;
		$this->baseIndex('MerchantsSettledModel', array(), 20, 'index');
	}
	public function actionIndex1(){
		$_GET['MerchantsSettledModel']['Status'] = 1;
		$this->baseIndex('MerchantsSettledModel', array(), 20, 'index');
	}
	public function actionIndex2(){
		$_GET['MerchantsSettledModel']['Status'] = 2;
		$this->baseIndex('MerchantsSettledModel', array(), 20, 'index');
	}
	public function actionIndex3(){
		$_GET['MerchantsSettledModel']['Status'] = 3;
		$this->baseIndex('MerchantsSettledModel', array(), 20, 'index');
	}
	public function actionIndex4(){
		$_GET['MerchantsSettledModel']['Status'] = 4;
		$this->baseIndex('MerchantsSettledModel', array(), 20, 'index');
	}
	public function actionIndex5(){
		$_GET['MerchantsSettledModel']['Status'] = 5;
		$this->baseIndex('MerchantsSettledModel', array(), 20, 'index');
	}
	public function actionIndex6(){
		$_GET['MerchantsSettledModel']['Status'] = 6;
		$this->baseIndex('MerchantsSettledModel', array(), 20, 'index');
	}
	public function actionCreate(){
		$this->baseCreate('MerchantsSettledModel');
	}
	public function actionUpdate($id){
		$_POST = CMap::mergeArray($_GET,$_POST); 
		$this->baseUpdate('MerchantsSettledModel','',true);
	}
	public function actionUpdateMoney($id){
		$model = $this->loadModel($id);
		$_POST = CMap::mergeArray($_GET,$_POST); 
		$this->baseUpdate($model,'',true);
	}
	public function actionDelete($id){
		$this->baseDelete('MerchantsSettledModel');
	}
	public function actionView($id){
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	public function loadModel($id)
	{
		$model=MerchantsSettledModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}

}