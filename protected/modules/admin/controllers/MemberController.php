<?php

class MemberController extends AdminBase{
	
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$model->Password = '';
		$this->baseUpdate($model);
	}

	public function actionIndex()
	{
        $objModel = new MemberModel('admin');
        $objModel->unsetAttributes();

        if(isset($_GET['MemberModel']) || isset($_GET['key']))
        {
        	if(isset($_GET['key'])){
        		switch ($_GET['key']) {
        			case '1':
        				$objModel->UserName = $_GET['keyword'];
        				break;
        			case '2':
        				$objModel->Email = $_GET['keyword'];
        				break;
        			case '3':
        				$objModel->Phone = $_GET['keyword'];
        				break;
        			case '4':
        				$objModel->UserID = $_GET['keyword'];
        				break; 
        			case '5':
        				$objModel->WangWang = $_GET['keyword'];
        				break;             			
        			default:
        				# code...
        				break;
        		}
        	}
            $objModel->attributes = $_GET['MemberModel'];
        }
        $this->baseIndex($objModel);  
	}


	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		$this->success('删除成功');		
	}

	public function loadModel($id)
	{
		$model=MemberModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}

	public function actionLogin(){
        $uid = (int) $_GET['id'];
        if (empty($uid)) {
            $this->error('请指定需要登录的会员UID！');
        }
        Yii::app()->user->logout();
        $info = Yii::app()->user->getLocalUser($uid);
        if (empty($info)) {
            $this->error('该用户不存在！');
        }
        Yii::app()->user->registerLogin($info);
        $this->success('登录成功！', self::U('front/index/index'));		
	}
}