<?php
/**
 * 标签管理
 */
class OrdersnController extends AdminBase{
	public function actionIndex(){


    	if(!empty($_GET['keyword'])){
    		switch ($_GET['key']) {
    			case '1':
    				$_GET['OrdersnModel']['OrderNo'] = $_GET['keyword'];
    				break;
    			case '2':
    				$_GET['OrdersnModel']['AlipayNo'] = $_GET['keyword'];
    				break;
    			case '3':
    				$_GET['OrdersnModel']['UserID'] = $_GET['keyword'];
    				break;
    			case '4':
    				$_GET['OrdersnModel']['UserName'] = $_GET['keyword'];
    				break;       			
    			default:
    				# code...
    				break;
    		}
    	}
		
		$this->baseIndex('OrdersnModel');
	}
	public function actionDelete(){
		$this->baseDelete('OrdersnModel');
	}

	public function loadModel($id)
	{
		$model=OrdersnModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
		
}