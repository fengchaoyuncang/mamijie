<?php
class GoodsController extends AdminBase{

	public function actionIndex(){
        $objModel = new GoodsModel('admin');
        $objModel->unsetAttributes();
        if(isset($_GET['GoodsModel']) || isset($_GET['key']))
        {
        	if(!empty($_GET['key'])){
        		switch ($_GET['key']) {
        			case '1':
        				$objModel->ProductID = (double)$_GET['keyword'];
        				break;
        			case '2':
        				$objModel->GoodsName = $_GET['keyword'];
        				break;
        			case '3':
        				$objModel->Nick = $_GET['keyword'];
        				break;
        			case '4':
        				$objModel->GoodsBmID = $_GET['keyword'];
        				break;       			
        			default:
        				# code...
        				break;
        		}
        	}
            $objModel->attributes = $_GET['GoodsModel'];
        }

        $this->baseIndex($objModel);     
	}

    //取消活动,,这个动作不是单纯的对自己的这个做处理，而是其它相关的逻辑处理，所以需要写。
    //就是把用户的这个商品信息删除，然后把报名的状态改为已取消的状态
    public function actionActivity() {
        $objModel = $this->loadModel(Yii::app()->request->getParam('id'));
        if ($objModel->activity() !== false) {
            $content = $this->renderPartial('data', array('rs' => $objModel), true);
            $this->ajaxReturn(array('status' => true,'content' => $content));
        } else {
            $error = $model->getOneError();
            $this->error(yii::app()->user->getFlash());
        }
    }

    //添加商品
    public function actionCreate(){
        $objModel = new GoodsModel('admin');
        $objModel->Status = 1;
        $objModel->IsFreeShipping = 1;
        $objModel->StartTime = strtotime('today 9:00');
        $objModel->EndTime = strtotime('+3day 9:00');
        $this->baseCreate($objModel);
    }


	//修改商品
	public function actionUpdate($id){
        $this->baseUpdate('GoodsModel');   
	}

    //单个的修改商品//是否因为权限问题可合过去。
    public function actionUpdateOne(){
        $model = $this->loadModel(Yii::app()->request->getParam('id'));
        $_POST['GoodsModel']['Tag'] = $model->getTag();//这里添加这个是因为如果其修改了则会以为没有这个数据是把标签给删除了。
        $this->baseUpdate('GoodsModel', '', true);
    }


	//下架商品
	public function actionDelete($id){
        $model = $this->loadModel($id);
        $model->Status = 5;
        $this->baseUpdate($model, '', true);
	}
    //上架商品
    public function actionShangjia($id){
        $model = $this->loadModel($id);
        $model->Status = 1;
        $this->baseUpdate($model, '', true);//逻辑一样，只是有一个content需要传输过去。。。这样会改变这个的值，而且会把这个值给传输过去。
    }
	//显示具体信息
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	//加载model
	public function loadModel($id)
	{
		$model=GoodsModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}

    public function actionQuickUpdate(){
        $this->baseUpdate('GoodsModel', '', true);
    }

    //结算此商品
    public function actionJiesuan($id){
        $model = $this->loadModel($id);
        $GoodsBmModel = GoodsBmModel::model()->findByPk($model->GoodsBmID);
        if(!$GoodsBmModel){
            $this->error('找不到报名数据');
        }
        $userID = $GoodsBmModel->UserID;

        if(!isset($_POST['is_complete'])){//此数据用于前台显示,,,
            MemberMoneyChangeModel::getLock($userID);
            //此商品用户冻结了多少钱
            $money = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'RelationID' => $GoodsBmModel->GoodsBmID, 'Type' => 31));
            //此用户所花的保证金
            $moneyBaoZhengJing = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'Type' => 33));

            MemberMoneyChangeModel::releaseLock($userID);
            $content = $this->renderPartial('jiesuan', array('GoodsBmModel' => $GoodsBmModel,'model' => $model,'money' => $money,'moneyBaoZhengJing' => $moneyBaoZhengJing), true);
            $this->ajaxReturn(array('content' => $content, 'status' => 1));
        }else{
            if($_POST['is_complete'] == 1){//这里是真正的结算
                if(MemberMoneyChangeModel::model()->paymentGoodsAccounts($userID,$model->GoodsBmID)){
                    $this->success('处理成功');
                }else{
                   $this->error(MemberMoneyChangeModel::model()->getOneError()); 
                }
            }else{//这里只是更新一下现在的件数而已
                $GoodsBmModel->RealSale = $_POST['num'];
                $GoodsBmModel->save(false);
                $this->success('处理成功');
            }
        }
    }   
}


?>