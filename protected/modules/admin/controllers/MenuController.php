<?php

/**
 * 后台菜单选项
 * File Name：MenuController.php
 * File Encoding：UTF-8
 * File New Time：2014-5-8 9:27:25
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class MenuController extends AdminBase {

    //菜单首页
    public function actionIndex() {
        if (IS_POST) {
            $listorders = $_POST['listorders'];
            if (!empty($listorders)) {
                foreach ($listorders as $id => $v) {
                    $model = Menu::model()->findByPk($id);
                    $model->listorder = $v;
                    $model->update();
                }
                $this->success('修改成功！', AdminBase::U('index'));
                exit;
            }
        }
        $array = array();
        $result = Menu::model()->findAll(array(
            'order' => 'listorder ASC',
        ));
        $tree = new Tree();
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│ ', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        foreach ($result as $obj) {
            $r = $obj->attributes;
            $r['str_manage'] = '<a href="' . AdminBase::U("menu/add", array("parentid" => $r['id'])) . '">添加子菜单</a> | <a href="' . AdminBase::U("menu/edit", array("id" => $r['id'])) . '">修改</a> | <a class="J_ajax_del" href="' . AdminBase::U("menu/delete", array("id" => $r['id'])) . '">删除</a> ';
            $r['status'] = $r['status'] ? "显示" : "不显示";
            $array[] = $r;
        }
        $tree->init($array);
        $str = "<tr>
	<td align='center'><input name='listorders[\$id]' type='text' size='3' value='\$listorder' class='input'></td>
	<td align='center'>\$id</td>
	<td >\$spacer\$name</td>
                    <td align='center'>\$status</td>
	<td align='center'>\$str_manage</td>
	</tr>";
        $categorys = $tree->get_tree(0, $str);
        $this->assign("categorys", $categorys);
        $this->render();
    }

    //添加菜单
    public function actionAdd() {
        if (IS_POST) {
            $model = new Menu('add');
            $model->attributes = $_POST;
            $model->controller = lcfirst($model->controller);
            $model->action = lcfirst($model->action);
            if ($model->validate() && $model->save()) {
                $this->success('添加成功！', AdminBase::U('index'));
            } else {
                $error = $model->getOneError();
                $this->error($error ? $error : '添加失败！');
            }
        } else {
            $tree = new Tree();
            $parentid = (int) $_GET['parentid'];
            $result = Menu::model()->findAll(array(
                'order' => 'listorder ASC',
            ));
            $array = array();
            foreach ($result as $rs) {
                $r = $rs->attributes;
                $r['selected'] = $r['id'] == $parentid ? 'selected' : '';
                $array[] = $r;
            }
            $str = "<option value='\$id' \$selected>\$spacer \$name</option>";
            $tree->init($array);
            $select_categorys = $tree->get_tree(0, $str);
            $this->assign("select_categorys", $select_categorys);
            $this->render();
        }
    }

    //编辑菜单
    public function actionEdit() {
        $id = (int) Yii::app()->request->getParam('id');
        if (empty($id)) {
            $this->error('请指定需要编辑的菜单！');
        }
        $model = Menu::model()->findByPk($id);
        if (empty($model)) {
            $this->error('该菜单不存在！');
        }
        if (IS_POST) {
            $model->attributes = $_POST;
            $model->controller = lcfirst($model->controller);
            $model->action = lcfirst($model->action);            
            if ($model->validate() && $model->save()) {
                $this->success('修改成功！', AdminBase::U('index'));
            } else {
                $error = $model->getOneError();
                $this->error($error ? $error : '修改失败！');
            }
        } else {
            $tree = new Tree();
            $result = Menu::model()->findAll(array(
                'order' => 'listorder ASC',
            ));
            $array = array();
            foreach ($result as $rs) {
                $r = $rs->attributes;
                $r['selected'] = $r['id'] == $model->parentid ? 'selected' : '';
                $array[] = $r;
            }
            $str = "<option value='\$id' \$selected>\$spacer \$name</option>";
            $tree->init($array);
            $select_categorys = $tree->get_tree(0, $str);
            $this->assign("select_categorys", $select_categorys);
            $this->assign('data', $model->attributes);
            $this->render();
        }
    }

    //删除菜单
    public function actionDelete() {
        $id = (int) $_GET['id'];
        if (empty($id)) {
            $this->error('请指定需要删除的菜单！');
        }
        $model = Menu::model()->findByPk($id);
        if (empty($model)) {
            $this->error('该菜单不存在！');
        }
        if ($model->delete()) {
            $this->success('菜单删除成功！');
        } else {
            $this->error('删除失败！');
        }
    }

    //常用菜单
    public function actionPublic_changyong() {
        if (IS_POST) {
            //被选中的菜单项
            $menuidAll = explode(',', Yii::app()->request->getParam('menuid'));
            if (empty($menuidAll)) {
                $this->error('没有需要添加的常用菜单！');
            }
            AdminPanel::model()->panelEmpty(self::$uid);
            if (is_array($menuidAll)) {
                $menuList = Menu::model()->getList();
                foreach ($menuidAll as $menuid) {
                    if (isset($menuList[$menuid])) {
                        $model = new AdminPanel();
                        $model->menuid = $menuid;
                        $model->userid = self::$uid;
                        $model->name = $menuList[$menuid]['name'];
                        $model->url = "{$menuList[$menuid]['app']}/{$menuList[$menuid]['controller']}/{$menuList[$menuid]['action']}";
                        $model->save();
                    }
                }
            }
            $this->success('添加成功，退出后台重新登录后生效！');
        } else {
            //菜单
            $data = Menu::model()->findAll(array(
                'condition' => 'status = 1',
            ));
            if (empty($data)) {
                return false;
            }
            $result = array();
            foreach ($data as $rs) {
                $result[$rs->id] = $rs->attributes;
            }
            $json = array();
            foreach ($result as $rs) {
                $data = array(
                    'id' => $rs['id'],
                    'nocheck' => $rs['type'] ? 0 : 1,
                    'checked' => $rs['id'],
                    'parentid' => $rs['parentid'],
                    'name' => $rs['name'] . ($rs['type'] == 0 ? "(菜单项)" : ""),
                    'checked' => AdminPanel::model()->isMy($rs['id'], self::$uid) ? true : false,
                );
                $json[] = $data;
            }

            $this->assign('json', json_encode($json));
            $this->assign("roleid", $roleid);
            $this->render();
        }
    }

}
