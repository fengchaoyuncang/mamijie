<?php

class CategoryController extends AdminBase{

    public function actionIndex()
    {

        $arrData = CategoryModel::getListTreeArr();
        $this->assign('count', count($arrData));
        $this->assign('model', CategoryModel::model());
        $this->assign('data', $arrData);
        $this->render('index');  
    }

    public function actionDelete($id)
    {
        if(CategoryModel::deleteCategoryByPk($id)){
            $this->success('删除成功');     
        }
        $this->error('删除失败');  
    }

    public function actionCreate(){

        $objModel = new CategoryModel('admin');
        $objModel->ParentID = Yii::app()->request->getParam('id');
        $this->baseCreate($objModel);   
    }
    public function actionUpdate(){
        $this->baseUpdate('CategoryModel');
    }

    public function loadModel($id)
    {
        $model=CategoryModel::model()->findByPk($id);
        if($model===null)
            $this->error('找不到记录');
        return $model;
    }    
}