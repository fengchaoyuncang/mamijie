<?php

//会员的提现申请
class MemberWithdrawController extends AdminBase{

	//提现列表
	public function actionIndex()
	{
		$this->baseIndex('MemberWithdrawModel');
	}

	//提现的处理   逻辑处理方式是跟其它关联一起处理的。。。
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$status = Yii::app()->request->getParam('Status');
		if($status == 1){
			if($model->agree()){
                $content = $this->renderPartial('data', array('rs' => $model,'object' => 'MemberWithdrawModel'), true);
                $arr = array(
                    'status' => 1,
                    'content' => $content,
                );
                $this->ajaxReturn($arr);
			}else{
				$this->error($model->getOneError());
			}
		}else if($status == 2){
			if($model->noAgree()){
                $content = $this->renderPartial('data', array('rs' => $model,'object' => 'MemberWithdrawModel'), true);
                $arr = array(
                    'status' => 1,
                    'content' => $content,
                );
                $this->ajaxReturn($arr);
			}else{
				$this->error($model->getOneError());
			}
		}else{
			$this->error('请求错误');
		}
		$this->baseUpdate('MemberWithdrawModel');
	}

	public function loadModel($id)
	{
		$model= MemberWithdrawModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}
	public function loadThawModel($id)
	{
		$model= MemberThawApplyModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}

	//保证金申请列表
	public function actionThawApply(){
		$this->baseIndex('MemberThawApplyModel',array(),20,'thawIndex');
	}

	//保证金申请处理
	public function actionThawApplyUpdate($id){
		$model = $this->loadThawModel($id);
		$status = Yii::app()->request->getParam('Status');
		if($status == 1){//同意申请
			if($model->agree()){
                $content = $this->renderPartial('thawData', array('rs' => $model,'object' => 'MemberThawApplyModel'), true);
                $arr = array(
                    'status' => 1,
                    'content' => $content,
                );
                $this->ajaxReturn($arr);
			}else{
				$this->error($model->getOneError());
			}
		}else if($status == 2){//不同意申请
			if($model->noAgree()){
                $content = $this->renderPartial('thawData', array('rs' => $model,'object' => 'MemberThawApplyModel'), true);
                $arr = array(
                    'status' => 1,
                    'content' => $content,
                );
                $this->ajaxReturn($arr);
			}else{
				$this->error($model->getOneError());
			}
		}else{
			$this->error('请求错误');
		}
		$this->baseUpdate('MemberThawApplyModel');		
	}		

}