<?php

/**
 * 同步到优站
 * File Name：TongbuController.php
 * File Encoding：UTF-8
 * File New Time：2014-5-24 9:50:47
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class TongbuController extends AdminBase {

    //同步疯狂购
    public function actionFengkg() {
        if (IS_POST) {
            $this->success('即将开始同步到疯狂购！', self::U('fengkg', $_POST));
            exit;
        }
        $act = $_GET['act'];
        if ($act != 'start') {
            $this->render();
            exit;
        }
        //one = 1 为一键同步  ，默认为0
        if ((int) $_GET['one'] == 1) {
            $_GET['start_time'] = date('Y-m-d');
            $_GET['end_time'] = date('Y-m-d', time() + 86400);
            $_GET['one'] = 2;
        }
        $model = GoodsModel::model();
        //查询条件
        $where = array(
            'order' => 'GoodsID DESC',
        );
        //指定商品同步
        $product_id = $_GET['product_id'];
        if (!empty($product_id)) {
            $where = array('ProductID' => $product_id, 'order' => 'StartTime DESC,GoodsID DESC',);
            $where['EndTime'] = array('GT', time());
            $data = $model->findAll($model->where($where));
            $post = array();
            foreach ($data as $rs) {
                $post[] = $rs->attributes;
            }
            //发送数据
            Tongbu::getInstance()->fengkg($post, 'http://94258.uz.taobao.com/view/front/sync.php');
            $this->success('同步信息处理完毕！', self::U(ACTION_NAME));
            exit;
        }
        //当前处理id
        $current = $_GET['gid'];
        if ($current) {
            $where['GoodsID'] = array('LT', $current);
        }
        //上架时间
        $start_time = strtotime($_GET['start_time']) + 35999;
        $end_time = strtotime($_GET['end_time']);
        if ($start_time) {
            $where['StartTime'] = array('EGT', $start_time);
            if ($end_time) {
                $where['StartTime'] = array(array('EGT', $start_time), array('ELT', $end_time + 35999), 'and');
            }
        } else {
            $where['EndTime'] = array('GT', time());
        }
        //一键同步 开始同步正在显示的
        if ((int) $_GET['one'] == 3) {
            $time = time();
            $where['StartTime'] = array('LT', strtotime(date('Y-m-d')) + 86400);
            $where['EndTime'] = array('GT', $time);
        }
        $criteria = $model->where($where);
        $_GET['count'] = $count = $_GET['count'] ? ((int) $_GET['count']) : $model->count($criteria);
        //每次处理
        $criteria->limit = 10;
        $data = $model->findAll($criteria);
        if (empty($data)) {
            if ((int) $_GET['one'] == 2) {
                $get = array(
                    'act' => 'start',
                    'one' => 3,
                );
                $get['start_time'] = $get['end_time'] = date('Y-m-d');
                $this->success('开始同步今日在线展示的商品！', self::U(ACTION_NAME, $get));
                exit;
            }
            $tongbu_taonp_err = (int) Yii::app()->cache->get('tongbu_taonp_err');
            Yii::app()->cache->delete('tongbu_taonp_err');
            $this->success("同步信息处理完毕，同步失败数：{$tongbu_taonp_err} 条！", self::U(ACTION_NAME));
            exit;
        } else {
            $post = array();
            foreach ($data as $rs) {
                $_GET['gid'] = $rs->GoodsID;
                $post[] = $rs->attributes;
            }
            //发送数据
            Tongbu::getInstance()->fengkg($post, 'http://94258.uz.taobao.com/view/front/sync.php');
        }
        //当前第几轮
        $_GET['i'] = $i = (int) $_GET['i'] ? ((int) $_GET['i'] + 1) : 1;
        //几轮
        $lun = ceil($count / $criteria->limit);
        //进度
        $jindu = round(($i / $lun) * 100, 3);
        $this->assign('waitSecond', 100);
        $this->suc("信息总数 <font color=\"#FF0000\">{$count}</font>，总共需要执行 <font color=\"#FF0000\">{$lun}</font> 次，当前第 <font color=\"#FF0000\">{$i}</font> 次，进度 <font color=\"#FF0000\">{$jindu}%</font>", self::U(ACTION_NAME, $_GET));
    }

    //直接输出信息提示，不走日志
    protected function suc($message = '', $jumpUrl = '', $ajax = false) {
        $this->dispatchJump($message, 1, $jumpUrl, $ajax);
    }

}
