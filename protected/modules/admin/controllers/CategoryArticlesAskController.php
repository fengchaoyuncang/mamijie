<?php

class CategoryArticlesAskController extends AdminBase{

    public function actionIndex()
    {

        $arrData = CategoryArticlesAskModel::getListTreeArr();
        $this->assign('count', count($arrData));
        $this->assign('model', CategoryArticlesAskModel::model());
        $this->assign('data', $arrData);
        $this->render('index');  
    }

    public function actionDelete($id)
    {
        if(CategoryArticlesAskModel::deleteCategoryByPk($id)){
            $this->success('删除成功');     
        }
        $this->error('删除失败');  
    }

    public function actionCreate(){

        $objModel = new CategoryArticlesAskModel('admin');
        $objModel->ParentID = Yii::app()->request->getParam('id');
        $this->baseCreate($objModel);  
    }
    public function actionUpdate(){
        $this->baseUpdate('CategoryArticlesAskModel');
    }

    public function loadModel($id)
    {
        $model=CategoryArticlesAskModel::model()->findByPk($id);
        if($model===null)
            $this->error('找不到记录');
        return $model;
    }    
}