<?php

/**
 * 网站设置
 * File Name：ConfigController.php
 * File Encoding：UTF-8
 * File New Time：2014-5-4 15:27:33
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class ConfigController extends AdminBase {

    //网站配置
    public function actionIndex() {
        if (IS_POST) {
            if (empty($_POST)) {
                $this->error('网站配置为空！');
            }
            $model = ConfigModel::model();
            foreach ($_POST as $varname => $value) {
                $info = $model->findByPk($varname);
                if (empty($info)) {
                    continue;
                }
                $info->value = $value;
                $info->save(false);
            }
            //更新缓存
            ConfigModel::model()->getConfig(NULL);
            $this->success('修改成功！');
        } else {
            $this->assign('info', ConfigModel::model()->getConfig());
            $this->render();
        }
    }

}
