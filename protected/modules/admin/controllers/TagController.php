<?php
/**
 * 标签管理
 */
class TagController extends AdminBase{
	public function actionIndex(){
		$this->baseIndex('TagModel');
	}
	public function actionCreate(){
		$model = new TagModel('admin');
		$model->Status = 1;
		$this->baseCreate($model);
	}
	public function actionUpdate(){
		$this->baseUpdate('TagModel');
	}
	public function actionDelete(){
		$this->baseDelete('TagModel');
	}
	public function actionShowGoods($id){
		$_GET['TagGoodsModel']['TagID'] = $id;
		$this->baseIndex('TagGoodsModel', array('with' => 'GoodsModel'));		
	}
	/**
	 * 删除标签商品
	 * @return [type] [description]
	 */
	public function actionDeleteGoods($id){
		$m = TagGoodsModel::model()->findByPk($id);
		$tid = $m->TagID;
		$this->baseDelete('TagGoodsModel','/admin/tag/showGoods/id/'.$tid);
	}
}