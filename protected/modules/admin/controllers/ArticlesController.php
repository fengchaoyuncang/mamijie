<?php

class ArticlesController extends AdminBase{

    public function actionIndex()
    {
        $this->baseIndex('ArticlesModel');
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        $this->success('删除成功'); 
    }

    public function actionCreate(){
       $this->baseCreate('ArticlesModel');  
    }
    public function actionUpdate(){       
        $this->baseUpdate('ArticlesModel');
    }

    public function loadModel($id)
    {
        $model=ArticlesModel::model()->findByPk($id);
        if($model===null)
            $this->error('找不到记录');
        return $model;
    }    
}