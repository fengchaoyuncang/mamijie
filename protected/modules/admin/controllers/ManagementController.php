<?php

/**
 * 管理员管理
 * File Name：ManagementController.php
 * File Encoding：UTF-8
 * File New Time：2014-5-5 10:55:28
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class ManagementController extends AdminBase {

    //管理员列表
    public function actionIndex() {
        $list = AdminUser::model()->findAll(array(
            'order' => 'admin_id DESC'
        ));
        if (empty($list)) {
            $list = array();
        }
        $this->assign('list', $list);
        $this->render();
    }

    //修改
    public function actionEditor() {
        $admin_id = Yii::app()->request->getParam('admin_id');
        if (IS_POST) {
            $model = AdminUser::model()->findByPk($admin_id);
            if (!is_object($model)) {
                $this->error('系统出现错误！');
            }
            if ($_POST['password'] && $_POST['password'] != $_POST['confirm_password']) {
                $this->error('输入的两次密码不一致！');
            }
            if (empty($_POST['password'])) {
                $_POST['password'] = $_POST['confirm_password'] = $model->password;
            } else {
                $_POST['password'] = $_POST['confirm_password'] = $model->hashPassword($_POST['password']);
            }
            $admin_category = $_POST['admin_category'];
            $model->attributes = $_POST;
            if ($model->validate() && $model->save()) {
                AdminCategoryModel::model()->saveCategory($admin_id, $admin_category);
                $this->success('修改成功！', AdminBase::U('index'));
            } else {
                $error = $model->getOneError();
                $this->error($error ? $error : '修改失败！');
            }
        } else {
            if (empty($admin_id)) {
                $this->error('请指定需要修改的用户ID！');
            }
            $model = AdminUser::model()->findByPk((int) $admin_id);
            $this->assign('info', $model->attributes);
            $this->render();
        }
    }

    //添加管理员
    public function actionAdd() {
        if (IS_POST) {
            $model = new AdminUser('add');
            $model->attributes = $_POST;
            if ($model->validate() && $model->save()) {
                $this->success('添加成功！', AdminBase::U('index'));
            } else {
                $error = $model->getOneError();
                $this->error($error ? $error : '添加失败！');
            }
        } else {
            $this->render();
        }
    }

    //删除管理员
    public function actionDelete() {
        $admin_id = Yii::app()->request->getParam('admin_id');
        if ($admin_id == self::$uid) {
            $this->error('自己不能删除自己！');
        }
        $this->baseDelete('AdminUser', 'index');
    }

}
