<?php

/**
 * 网站管理后台首页
 * File Name：PublicController.php
 * File Encoding：UTF-8
 * File New Time：2014-4-29 15:50:31
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class IndexController extends AdminBase {

    //后台首页
    public function actionIndex() {
        if (IS_AJAX) {
            $data = array();
            $data['info'] = 'by admin@abc3210.com';
            $data['status'] = 1;
            $data['referer'] = $data['url'] = '';
            $this->ajaxReturn($data);
            exit;
        }
        $this->assign("SUBMENU_CONFIG", json_encode(Menu::model()->getMenuList()));
        $this->assign('uid', self::$uid);
        $this->assign('username', self::$username);
        $this->assign('userInfo', self::$userInfo);
        $this->assign('config', ConfigModel::model()->getConfig());
        $this->render();
    }

    //显示后台登录首页
    public function actionMain() {
        $this->render();
    }

    //退出登录
    public function actionLogout() {
        Yii::app()->passport->logoutLocal();
        $this->success('退出登录成功！', $this->createUrl('public/login'));
    }

    //清除全部缓存
    public function actionFlush() {
        Yii::app()->cache->flush();
        $this->success('缓存更新成功！', self::U('main'));
    }


}
