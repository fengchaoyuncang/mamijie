<?php

class MemberBrandController extends AdminBase{
	public function actionIndex(){
		$this->baseIndex('MemberBrandModel');
	}
	public function actionCreate(){
		$this->baseCreate('MemberBrandModel');
	}
	public function actionUpdate(){
		$this->baseUpdate('MemberBrandModel');
	}
	public function actionDelete(){
		$this->baseDelete('MemberBrandModel');
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	public function loadModel($id)
	{
		$model=MemberBrandModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}

}