<?php

class AttachmentController extends AdminBase{

	public function actionIndex()
	{
		$this->baseIndex('AttachmentModel');
	}

	public function actionDelete(){
		$this->baseDelete('AttachmentModel');
	}


	public function loadModel($id)
	{
		$model=AttachmentModel::model()->findByPk($id);
		if($model===null)
			$this->error('找不到记录');
		return $model;
	}
	//批量删除    因为其是delete有其关联的操作beforeDelete,所以不能用deleteAll。而是循环一起来操作。
	public function actionDeleteAll(){
		$datas = $_POST['check'];
		foreach ($datas as $data) {
			$model = AttachmentModel::model()->findByPk($data);
			if($model){
				$model->delete();
			}
		}
		$this->success('处理成功');
	}
}