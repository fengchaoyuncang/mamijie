<?php

/**
 * File Name：AdminModule.php
 * File Encoding：UTF-8
 * File New Time：2014-4-29 9:22:06
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class AdminModule extends CWebModule {

    //默认控制器
    public $defaultController = 'index';

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'admin.models.*',
            'admin.components.*',
        ));
        //载入模块配置，暂时不清楚是否可行
        Yii::app()->configure(require($this->basePath . '/config/config.php'));
        //RBAC
        define('USER_AUTH_ON', true); //开启权限认证
        define('NOT_AUTH_CONTROLLER', 'public'); //无需认证的控制器
        define('NOT_AUTH_ACTION', 'public,logout'); //无需认证的方法
        define('USER_AUTH_TYPE', 1); //认证类型 1 登录认证 2 实时认证
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }

}
