<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('thawApply'))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            用户ID：
            <?php echo $objForm->textField($model,'UserID');?>            
            &nbsp;&nbsp;&nbsp;&nbsp;
            处理人：
            <?php echo $objForm->textField($model,'AdminID');?>            
            &nbsp;&nbsp;&nbsp;&nbsp;
            处理状态：
            <?php echo $objForm->dropDownList($model,'Status', $object::getStatusHtml(),array('empty' => '全部'));?> 
            类型：
            <?php echo $objForm->dropDownList($model,'Type', $object::getTypeHtml(),array('empty' => '全部'));?>                   
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>


    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center"><?php echo $model->getAttributeLabel('MoneyID'); ?></td>
              <td align="center"><?php echo $sort->link('UserID') ?></td>
              <td align="center">用户名</td>
              <td align="center"><?php echo $sort->link('AddTime') ?></td>
              <td align="center"><?php echo $sort->link('Status') ?></td>
              <td align="center"><?php echo $sort->link('Cacus') ?></td>
              <td align="center"><?php echo $sort->link('AdminID') ?></td>
              <td align="center"><?php echo $sort->link('UpdateTime') ?></td>
              <td align="center"><?php echo $sort->link('Type') ?></td>
              <td align="center">操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr data-id="<?php echo $rs->MoneyID ?>" >
                <?php include $this->getViewFile('thawData'); ?>          
             </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    </form>
  </div>

</body>
</html>