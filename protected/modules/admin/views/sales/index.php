<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
<div class="wrap ">
  <?php include $this->getViewFile('/common/Nav') ?>
  <div class="h_a">温馨提示</div>
  <div class="prompt_text">
    <p>最好是有选择性的刷新，如果全部刷新，当信息量比较大的时候更新会比较久！</p>
  </div>
  <div class="h_a">批量更新销量</div>
  <div class="table_full">
  <table width="100%" cellspacing="0">
      <form action="<?php echo AdminBase::U("index") ?>" method="post" name="myform">
        <tbody  height="200" class="nHover td-line">
          <tr>
            <th width="100">上架时间范围</th>
            <td><input type="text" name="start_time" class="input length_2 J_date" value="" placeholder="选择起始时间"> - <input type="text" class="input length_2 J_date" name="end_time" value="" placeholder="选择结束时间"> <em>不选时更新全部商品销量</em></td>
          </tr>
          <tr>
            <th></th>
            <td>
            <input type="hidden" name="act" value="start">
            <input type="button" name="dosubmit1" value=" 开始更新 " class="btn" onClick="myform.submit();">
            </td>
          </tr>
        </tbody>
      </form>
    </table>
  </div>
</div>
</body>
</html>