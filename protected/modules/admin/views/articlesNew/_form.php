<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>
        <?php  $d = $objForm->dropDownListTable($model, 'CatID', CategoryArticlesNewModel::getListTree(), array(),array('th' => array('width' => 100))); 
               echo str_replace('amp;', '', $d);
        ?>

        <?php echo $objForm->textFieldTable($model, 'Title', array('class' => 'input length_6'), array('th' => array('width' => 100))); ?>


        <?php //echo $objForm->textAreaTable($model, 'Content', array('class' => '')); ?>

        <tr id="edit" ><!-- 使用百度编辑器来完成 -->
            <th>内容</th>
            <td>
                <script type="text/plain" id="Content" name="Content"><?php echo $model->Content ?></script>
                <?php
                echo Form::editor('Content', 'full', 1, 1, '', 10, 300, 1);
                ?>
            </td>
        </tr>



        <?php echo $objForm->textFieldTable($model, 'Sorting', array('class' => '')); ?>
        <?php echo $objForm->radioButtonListTable($model, 'IsShow', ArticlesNewModel::getIsShowHtml(), array('class' => '')); ?>
        <?php echo $objForm->checkBoxTable($model, 'IsHot', array('class' => '')); ?>
        <?php echo $objForm->checkBoxTable($model, 'IsRecommend', array('class' => '')); ?>
        <?php echo $objForm->checkBoxTable($model, 'IsNew', array('class' => '')); ?>


        <?php echo $objForm->textFieldTable($model, 'TitleSeo', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'KeywordsSeo', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'DescriptionSeo', array('class' => '')); ?>

        <?php echo $objForm->textFieldTable($model, 'AppImg', array('name' => 'AppImg','class' => 'length_6')); ?>
    <?php if(!empty($model->AppImg)){  
        $html = '<div class="div_img"><img  src="'.ConfigModel::model()->getConfig('sitefileurl') . $model->AppImg.'"><input type="hidden" value="'.$model->AppImg.'" name="ArticlesNewModel[AppImg]"><div data-id="ArticlesNewModel_AppImg" data-filename="" data-path="'.$model->AppImg.'" class="uploader-close update"></div></div>';
      }else{
        $html = '';
      }
    ?>
    <?php echo $objForm->fileFieldTable($model, 'AppImg', array('class' => 'length_1', 'style' => 'width:65px'), array('htmlOptions' => array('td' => $html))); ?>





        
        <?php echo $objForm->radioButtonListTable($model, 'IsAppIndex', array('0' => '否', '1' => '是'), array('class' => '')); ?>

        
    <?php if(!empty($model->BigImg)){  
        $html = '<div class="div_img"><img  src="'.ConfigModel::model()->getConfig('sitefileurl') . $model->BigImg.'"><input type="hidden" value="'.$model->BigImg.'" name="ArticlesNewModel[BigImg]"><div data-id="ArticlesNewModel_BigImg" data-filename="" data-path="'.$model->BigImg.'" class="uploader-close update"></div></div>';
      }else{
        $html = '';
      }
    ?>

    <?php echo $objForm->fileFieldTable($model, 'BigImg', array('class' => 'length_1', 'style' => 'width:65px'), array('htmlOptions' => array('td' => $html))); ?>






    <?php if(!empty($model->MidImg)){  
        $html = '<div class="div_img"><img  src="'.ConfigModel::model()->getConfig('sitefileurl') . $model->MidImg.'"><input type="hidden" value="'.$model->MidImg.'" name="ArticlesNewModel[MidImg]"><div data-id="ArticlesNewModel_MidImg" data-filename="" data-path="'.$model->MidImg.'" class="uploader-close update"></div></div>';
      }else{
        $html = '';
      }
    ?>

    <?php echo $objForm->fileFieldTable($model, 'MidImg', array('class' => 'length_1', 'style' => 'width:65px'), array('htmlOptions' => array('td' => $html))); ?>




    <?php if(!empty($model->SmlImg)){  
        $html = '<div class="div_img"><img  src="'.ConfigModel::model()->getConfig('sitefileurl') . $model->SmlImg.'"><input type="hidden" value="'.$model->SmlImg.'" name="ArticlesNewModel[SmlImg]"><div data-id="ArticlesNewModel_SmlImg" data-filename="" data-path="'.$model->SmlImg.'" class="uploader-close update"></div></div>';
      }else{
        $html = '';
      }
    ?>

    <?php echo $objForm->fileFieldTable($model, 'SmlImg', array('class' => 'length_1', 'style' => 'width:65px'), array('htmlOptions' => array('td' => $html))); ?>


        <?php echo $objForm->textFieldTable($model, 'Keywords', array('class' => '')); ?>
        <?php echo $objForm->textAreaTable($model, 'Description', array('class' => '')); ?>
		
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>
<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl ?>/assets/js/plupload-2.1.2/js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl ?>/assets/js/plupload-2.1.2/js/common.js"></script>
<script type="text/javascript">


  /*定义图片处理的一些全局变量 路径等*/
  var HostUrl = '<?php echo ConfigModel::model()->getConfig('sitefileurl');   ?>/';
  var savePath = 'articlesnew/';
  var deleteUrl = '<?php echo $this->createUrl("/front/public/deleteImg") ?>';
  var uploadUrl = '<?php echo $this->createUrl("/front/public/upload") ?>';  /*上传图片URL*/
  var flash_swf_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.swf';;
  var silverlight_xap_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.xap';

  
  saveImg('ArticlesNewModel_BigImg', 'ArticlesNewModel[BigImg]', savePath, false);
  saveImg('ArticlesNewModel_MidImg', 'ArticlesNewModel[MidImg]', savePath, false);
  saveImg('ArticlesNewModel_SmlImg', 'ArticlesNewModel[SmlImg]', savePath, false);



  saveImg('ArticlesNewModel_AppImg', 'ArticlesNewModel[AppImg]', savePath, false);


</script>




</body>
</html>