<?php 
$getMenu = isset($Custom)?$Custom:Menu::model()->getMenu(); 
if($getMenu) {
?>
<div class="nav">
  <?php
  if(!empty($menuReturn)){
	  echo '<div class="return"><a href="'.$menuReturn['url'].'">返回'.$menuReturn['name'].'</a></div>';
  }
  ?>
  <ul class="cc">
    <?php
	foreach($getMenu as $r){
		$app = $r['app'];
		$controller = $r['controller'];
		$action = $r['action'];
	?>
    <li <?php echo $action==Yii::app()->getController()->getAction()->id ?'class="current"':""; ?>><a href="<?php echo AdminBase::U("".$app."/".$controller."/".$action."",$r['parameter']);?>"><?php echo $r['name'];?></a></li>
    <?php
	}
	?>
  </ul>
</div>
<?php } ?>