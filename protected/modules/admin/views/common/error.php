<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>提示信息 - <?php echo Yii::app()->name; ?></title>
<link href="<?php echo yii::app()->theme->baseUrl; ?>/assets/statics/css/admin_style.css" rel="stylesheet" />
<?php include $this->getViewFile('/common/Js') ?>
</head>
<body>
<div class="wrap">
  <div id="error_tips">
    <h2><?php echo $msgTitle;?></h2>
    <div class="error_cont">
      <ul>
        <li><?php echo $error;?></li>
      </ul>
      <div class="error_return"><a href="<?php echo $jumpUrl;?>" class="btn">返回</a></div>
    </div>
  </div>
</div>
<script language="javascript">
setTimeout(function(){
	location.href = '<?php echo $jumpUrl;?>';
},<?php echo $waitSecond;?>);
</script>
</body>
</html>