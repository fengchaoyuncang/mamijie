<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('index'))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            公司名称：
            <?php echo $objForm->textField($model,'CompanyName');?>            
            &nbsp;&nbsp;&nbsp;&nbsp;
            法人姓名：
            <?php echo $objForm->textField($model,'FullName');?>            
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>

    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center"><?php echo $sort->link('CompanyName'); ?></td>
              <td align="center"><?php echo $sort->link('RegistrationNo') ?></td>
              <td align="center"><?php echo $sort->link('Location') ?></td>
              <td align="center"><?php echo $sort->link('RegisteredCapital') ?></td>
              <td align="center"><?php echo $sort->link('Status') ?></td>
              <td align="center"><?php echo $sort->link('Money') ?></td>
              <td align="center">操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr>
              <?php include $this->getViewFile('data'); ?>
             </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    </form>
  </div>
  

  <script type="text/javascript">

  </script>

</body>
</html>