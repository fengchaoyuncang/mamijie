<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">
<style type="text/css">
	.detail-view img{
		display: block;
		position: relative;
		width:200px;
		height: 200px;
	}
/*	.quan img{
		width:auto;
		height: auto;
	}*/
	.quan img{
		z-index: 99999;
		width:800px;
		height: 800px;
	}
	.quan{
		position: absolute;
		top:20px;
		left:120px;
	}
	.detail-view tr td{
		display: block;
		position: relative;
	}
</style>
  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>
    

	<div style="margin-left:100px;">
	<div style="font-size:20px;margin:30px auto">入驻详情</div>
	<?php  $homeUrl = ConfigModel::model()->getConfig('sitefileurl'); ?>
	<?php  $Imgs = MerchantsSettledImgModel::getMerchantsSettledImg($model->MerchantsSettledID);
		   $html = '';
		   foreach ($Imgs as $key => $Img) {
		   		$html .= "<img src='{$homeUrl}{$Img['Img']}'  />";
		   }
	 ?>	
	<?php $this->widget('ext.YiiBooster.widgets.TbDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'CompanyName',
			'RegistrationNo',
			'StartTime' => array('type' => 'date', 'name' => 'StartTime', ),
			'EndTime' => array('type' => 'date', 'name' => 'EndTime', ),
			'Location',
			'RegisteredCapital',
			'ScopeBusiness',
			'FullName',
			'Address',
			'MobilePhone',
			'FaxNumber',
			'CompanyUrl',
			'BrandAgent',
			'TaobaoUrl',
			'BusinessLicenseImg' => array('type'=>'image', 'name' => 'BusinessLicenseImg', 'value' => $homeUrl . $model->BusinessLicenseImg),
			'OrganizationImg' => array('type'=>'image', 'name' => 'OrganizationImg', 'value' => $homeUrl . $model->OrganizationImg),
			'TaxRegistrationImg' => array('type'=>'image', 'name' => 'TaxRegistrationImg', 'value' => $homeUrl . $model->TaxRegistrationImg),
			'PositiveIDImg' => array('type'=>'image', 'name' => 'PositiveIDImg', 'value' => $homeUrl . $model->PositiveIDImg),
			'ReverseIDImg' => array('type'=>'image', 'name' => 'ReverseIDImg', 'value' => $homeUrl . $model->ReverseIDImg),
			'TrademarkImg' => array('type'=>'image', 'name' => 'TrademarkImg', 'value' => $homeUrl . $model->TrademarkImg),
			'BrandAuthorizationImg' => array('type'=>'image', 'name' => 'BrandAuthorizationImg', 'value' => $homeUrl . $model->BrandAuthorizationImg),
			'Certification3cImg' => array('type'=>'image', 'name' => 'Certification3cImg', 'value' => $homeUrl . $model->Certification3cImg),
			'HeadName',
			'HeadPosition',
			'HeadCardID',
			'HeadMobilePhone',
			'HeadQQ',
			'HeadEmail',
			'Cause',
			'Money',
			'AddTime' => array('type' => 'date', 'name' => 'AddTime', ),
			'Status' => array('name' => 'Status', 'value' => MerchantsSettledModel::getStatusHtml($model->Status)),
			'Imgs' => array('type'=>'html', 'name' => 'Imgs', 'value' => $html),
		),
	)); ?>

	</div>

  </div>
  

  <script type="text/javascript">
  	$(".detail-view tr img").mouseover(function(){
  		$(".quan").remove();
  		var src = $(this).attr('src');
  		var html = "<div class='quan'><img src='"+src+"'/></div>";
  		$(this).parent().append(html);
  	})

  	$(document).on('mouseout',".detail-view tr img,.quan img",function(event){
  	//$(".detail-view tr img,.quan img").mouseout(function(event){
  		var src = $(this).attr('src');
	   	event = event || window.event;
	    //var target = event.fromElement;
	    var target = event.toElement;
	    console.log(src);console.log($(target).attr('src'));
	    if($(target).attr('src') != src){
	    	$(".quan").remove();
  			//$(this).parent().find('.quan').remove();
	    }
  	})


  </script>


</body>
</html>