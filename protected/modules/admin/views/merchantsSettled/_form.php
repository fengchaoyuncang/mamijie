<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>


        <?php echo $objForm->radioButtonListTable($model, 'Status', $object::getStatusHtml(),array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'Money', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'CompanyName', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'UserID', array('class' => '')); ?>
		
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>

</body>
</html>