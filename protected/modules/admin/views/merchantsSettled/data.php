<td width="50" align="center"><?php echo $rs->CompanyName; ?></td>
<td align="center"><?php echo $rs->RegistrationNo?></td>
<td align="center"><?php echo $rs->Location?></td>
<td align="center"><?php echo $rs->RegisteredCapital?></td>
<td align="center"><?php echo MerchantsSettledModel::getStatusHtml($rs->Status);?></td>
<td align="center"><?php echo $rs->Money ?></td>
<td align="center">
<?php
$op = array();
$op[] = '<a href="'.AdminBase::U("view",array('id'=>$rs->MerchantsSettledID)).'" >查看</a>';
$op[] = '<a href="'.AdminBase::U("update",array('id'=>$rs->MerchantsSettledID)).'" >修改</a>';
if(RBAC::authenticate('update')){
  $data_content_reject = "<div id='dialog_form'>拒绝理由:<textarea name='Cause'></textarea></div>"; 
  if($rs->Status == 0){//未审核
    $op[] = '<a class="J_ajax_request" href="'.AdminBase::U("update",array('id'=>$rs->MerchantsSettledID, 'Status' => 4)).'" >进入合同期</a>';
    $op[] = '<a  data-content="'.$data_content_reject.'" class="J_ajax_dialog" href="'.AdminBase::U("update",array('id'=>$rs->MerchantsSettledID, 'Status' => 2)).'" >拒绝</a>';
  }else if($rs->Status == 1){//审核通过
  	$op[] = '<a   data-content="'.$data_content_reject.'" class="J_ajax_dialog" href="'.AdminBase::U("update",array('id'=>$rs->MerchantsSettledID, 'Status' => 2)).'" >拒绝</a>';
  }else if($rs->Status == 2){//审核被拒绝
    $op[] = '<a class="J_ajax_request" href="'.AdminBase::U("update",array('id'=>$rs->MerchantsSettledID, 'Status' => 4)).'" >进入合同期</a>';
  }else if($rs->Status == 4){//进入合同期
  	$data_content = "<div id='dialog_form'>入驻金额:<input type='text' name='Money' /></div>";
    $op[] = '<a data-content="'.$data_content.'" class="J_ajax_dialog" href="'.AdminBase::U("updateMoney",array('id'=>$rs->MerchantsSettledID, 'Status' => 5)).'" >进入付款期</a>';
    $op[] = '<a   data-content="'.$data_content_reject.'" class="J_ajax_dialog" href="'.AdminBase::U("update",array('id'=>$rs->MerchantsSettledID, 'Status' => 2)).'" >拒绝</a>';
  }else if($rs->Status == 5){//进入付款期
    $op[] = '<a class="J_ajax_request" href="'.AdminBase::U("update",array('id'=>$rs->MerchantsSettledID, 'Status' => 1)).'" >通过</a>';
    $op[] = '<a   data-content="'.$data_content_reject.'" class="J_ajax_dialog" href="'.AdminBase::U("update",array('id'=>$rs->MerchantsSettledID, 'Status' => 2)).'" >拒绝</a>';
  }
}

echo implode('|', $op);
?> 
</td>