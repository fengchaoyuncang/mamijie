<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('index'))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            文件类型：
            <?php echo $objForm->textField($model,'Type');?>            
            &nbsp;&nbsp;&nbsp;&nbsp;
            文件扩展名：<?php echo $objForm->textField($model,'Ext');?>        &nbsp;&nbsp;&nbsp;&nbsp;

            上传时间：
            <input type="text" value="<?php echo $add_start_time; ?>" name="add_start_time" class="J_date input length_2" placeholder="选择起始时间">
            -
            <input type="text" name="add_end_time" value="<?php echo $add_end_time; ?>" class="J_date input length_2" placeholder="选择结束时间">
            </br></br>


            <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>


    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
    <div class="mb10"> <button class="J_ajax_submit_btn" data-action="<?php echo $this->createUrl('deleteAll');  ?>" >删除</button></div>      
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              
              <td width="10%" align="center"><input type="checkbox" class="checkall" name="checkall"/><?php echo $sort->link('AttachmentID') ?></td>
              <td align="center"><?php echo $sort->link('Name') ?></td>
              <td align="center"><?php echo $sort->link('Path') ?></td>
              <td align="center"><?php echo $sort->link('Path') ?></td>
              <td align="center"><?php echo $sort->link('Size') ?></td>
              <td align="center"><?php echo $sort->link('Ext') ?></td>
              <td align="center"><?php echo $sort->link('Type') ?></td>
              <td align="center"><?php echo $sort->link('AddTime') ?></td>
              <td align="center"><?php echo $sort->link('UploadIP') ?></td>
              <td align="center">操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr data-id="<?php echo $model->AttachmentID ?>" >
                <?php include $this->getViewFile('data'); ?>          
             </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    </form>
  </div>


    <script type="text/javascript">
      //全选
      $(document).on('click','.checkall',function(){
      //$(".checkall").click(function(){
        var obj = $(this);
        var isCheck = obj[0].checked;
        var length = $('.check').length;
        for (var i = 0; i < length; i++) {
          $('.check').eq(i)[0].checked = isCheck;
        };
      })
    </script>

</body>
</html>