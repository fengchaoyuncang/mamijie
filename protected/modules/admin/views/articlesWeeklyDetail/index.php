<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('index'))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            &nbsp;&nbsp;&nbsp;&nbsp;
            周刊ID：<?php echo $objForm->textField($model,'WeeklyID'); ?>&nbsp;&nbsp;&nbsp;&nbsp;
            <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>

    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <div class="mb10"> <a href="<?php echo $this->createUrl('create', array('WeeklyID' => $model->WeeklyID)) ?>">创建</a></div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="" align="center"><?php echo $model->getAttributeLabel('WeeklyDetailID'); ?></td>
              <td width="" align="left"><?php echo $model->getAttributeLabel('WeeklyID'); ?></td>             
              <td align="center"><?php echo $model->getAttributeLabel('Content'); ?></td>            
              <td align="center"><?php echo $model->getAttributeLabel('Title'); ?></td>           
              <td align="center"><?php echo $model->getAttributeLabel('AddTime'); ?></td>           
              <td align="center"><?php echo $model->getAttributeLabel('Sorting'); ?></td>        
              <td align="center">操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
              <tr>
                  <?php include $this->getViewFile('data'); ?>          
              </tr>
             <?php } ?>

          </tbody>
        </table>
        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>  
       
      </div>
    </form>
  </div>

</body>
</html>