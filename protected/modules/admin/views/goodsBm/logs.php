<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">
<style>
.col-left{float:left}
.col-auto{overflow:hidden;_zoom:1;_float:left;}
.title-1{border-bottom:1px solid #eee; padding-left:5px}
.f14{font-size: 14px}
.lh28{line-height: 28px}
.blue,.blue a{color:#004499}
.gray4,a.gray4{color:#999;}
.lh22{line-height: 22px}
.lh25{line-height: 25px}
</style>
  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl(ACTION_NAME))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            <!-- 状态，操作人，上架时间  报名时间  栏目  关键字  -->

            状态：
            <?php echo $objForm->dropDownList($model,'Status',GoodsBmModel::getStatusHtml(),array('empty' => '全部')); ?>
            &nbsp;&nbsp;
            审核人：
            <?php echo $objForm->dropDownList($model,'AdminID',AdminUser::model()->getUserListName(),array('empty' => '全部')); ?>
            &nbsp;&nbsp;
            审核时间：
            <input type="text" name="add_start_time" class="J_date input length_2" placeholder="选择起始时间">
            -
            <input type="text" name="add_end_time" class="J_date input length_2" placeholder="选择结束时间">
            &nbsp;&nbsp;
            商品ID：
            <?php echo $objForm->textField($model,'ProductID', array('class' => 'input length_4', 'placeholder' => "请搜索")); ?>
            &nbsp;&nbsp;
            商品报名ID：
            <?php echo $objForm->textField($model,'GoodsBmID', array('class' => 'input length_4', 'placeholder' => "请搜索")); ?>
            &nbsp;&nbsp;
             <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>


    
    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center"><?php echo $sort->link('OperationID') ?></td>
              <td align="center"><?php echo $sort->link('GoodsBmID') ?></td>
              <td align="center"><?php echo $sort->link('ProductID') ?></td>
              <td align="center">商品报名信息</td>
              <td align="center"><?php echo $sort->link('OldStatus') ?></td>
              <td align="center"><?php echo $sort->link('Status') ?></td>
              <td align="center"><?php echo $sort->link('AddTime') ?></td>
              <td align="center">审核人</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr>
                <?php include $this->getViewFile('data_logs'); ?>            
              </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    </form>
  </div>

</body>
</html>