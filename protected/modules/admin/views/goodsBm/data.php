<td  width="50" align="center"><?php echo $rs->GoodsBmID; ?></td>
<td  width="100" align="center"><?php echo $rs->ProductID; ?></td>


<!-- //详细信息 -->
<td width="" align="left">
	<div class="col-left mr10" style="width:146px; height:112px">
		<a href="<?php echo $rs->DetailUrl ?>" target="_blank">
			<img src="<?php echo $rs->Image ?>" width="146" height="112" style="border:1px solid #eee" align="left">
		</a>
	</div>

	<div class="col-auto">
      <h2 class="title-1 f14 lh28 mb6 blue">
      	<a href="<?php echo $rs->DetailUrl ?>" target="_blank"><?php echo $rs->GoodsName ?></a>
        <a width="600" height="1200" style="color: #999" class="J_dialog" href="<?php  echo $this->createUrl('quickUpdate', array('id' => $rs->GoodsBmID)) ?>">(快速编辑)</a>
      </h2>

      <p class="gray4 lh25">
      原价：<font color="#FF0000"><?php echo $rs->Pprice ?></font>， 
      活动价：<font color="#FF0000"><?php echo $rs->PromoPrice ?></font>，
      销量：<font color="#FF0000"><?php echo $rs->Sales ?></font>，
      旺旺：<b><a href="http://amos.im.alisoft.com/msg.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid=<?php echo $rs->Nick ?>" target="_blank"><?php echo $rs->Nick ?><img src="http://amos.im.alisoft.com/online.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid=<?php echo $rs->Nick ?>" alt=""></a></b>
      店铺类型：<font color="#FF0000"><?php echo GoodsBmModel::getItemTypeHtml($rs->ItemType) ?></font>
      </p>

      <p class="gray4 lh25">
      报名时间：<font color="#000000"><?php echo date('Y-m-d', $rs->AddTime) ?></font>，
      报名栏目：<font color="#0033FF"><?php $d = CategoryModel::getDataDetail($rs->CatID);echo $d['Title']  ?></font>
      </p>


      <p class="gray4 lh25">
      上架时间：<font color="#FF33FF"><?php echo date('Y-m-d H:i:s', $rs->StartTime) ?></font> ，
      下架时间：<font color="#FF33FF"><?php echo date('Y-m-d H:i:s', $rs->EndTime) ?></font>
      </p>	

      <p class="gray4 lh25">
      模式：<font color="#FF0000"><?php echo GoodsBmModel::getPatternHtml($rs->Pattern) ?></font>
      是否交付：<font color="#FF33FF"><?php echo GoodsBmModel::getIsPayHtml($rs->IsPay) ?></font>
      </p>	

	</div>
</td>

<td align="center">
	<?php 
		if($rs->Status == 0){
			echo "分配：";
			echo Form::select(AdminUser::model()->getUserListName(), $rs->AdminID, "data-id='{$rs->GoodsBmID}' class='feipei' name='AdminID'",'未分配');
			echo "</br>";
		}
	 ?>
	<?php  echo  $rs->getStatusOperationHtml(); ?>

</td>


<td width="200" align="center">
	<?php  
	    $op = array();
	    if($rs->Status == 1){ //审核通过
	    	$op[] = '<span>已审核商品不能进行操作 </span>';
	    	$op[] = '</br><a class="J_ajax_request" href="'.$this->createUrl("release", array("id" => $rs->GoodsBmID)).'">重新发布到商品管理</a>';
	    }else{
		    if(RBAC::authenticate('update')){
		      $op[] = '<a class="" href="'.AdminBase::U("update",array('id'=>$rs->GoodsBmID)).'" >编辑</a>';
		    } 
		    if(RBAC::authenticate('delete')){
		      $op[] = '<a class="J_ajax_del" href="'.AdminBase::U("delete",array('id'=>$rs->GoodsBmID)).'" >删除</a>';
		    }
		    if(RBAC::authenticate('notice')){
		      $content = "<span style='margin-right:20px;'>信息内容</span> <textarea style='height:50px;width:400px;' type='text' name='notice' cols='60' rows='3'>亲，您的商品({$rs->ProductID})已经进入初审阶段，请到“报名查询”页面查询商品审核进展（查询后有审核人员旺旺），并联系洽谈。</textarea>";

		      $op[] = '<a style="color:red" data-title="信息通知" data-content="'.$content.'" class="J_ajax_dialog" href="'.AdminBase::U("notice",array('id'=>$rs->GoodsBmID)).'" >信息通知</a>';
		    }
	    }         
	    $op = implode('| ', $op);
	    echo $op;
	?>   
</td>



