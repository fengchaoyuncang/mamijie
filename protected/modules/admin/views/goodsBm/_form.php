<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>

          <tr>
            <th width="80">URL/ID</th>
            <td>
              <?php echo $objForm->textField($model, 'DetailUrl', array('class' => 'input length_6', 'disabled' => 'disabled')); ?>
              <button class="btn J_ajax_getiteminfo">获取最新信息</button>
            </td>
          </tr>

          <?php echo $objForm->textFieldTable($model, 'GoodsName', array('class' => 'input length_6')); ?>

          <?php echo $objForm->textFieldTable($model, 'Image', array('class' => 'input length_6')); ?>
          
          <!-- 图片展示 -->
          <tr>
            <th></th>
            <td>
<div class="product_img">
  <img class="current" width="160" height="160" src="<?php echo $model->Image ?>">
</div>              
            </td>
          </tr>


          <tr>
            <th>商品ID</th>
            <td><span><?php echo $model->ProductID  ?></span></td>
          </tr>
          <tr>
            <th>原价</th>
            <td><span><?php echo $model->Pprice  ?></span></td>
          </tr>

          <?php echo $objForm->dropDownListTable($model, 'Pattern', GoodsBmModel::getPatternHtml()); ?>
          <?php echo $objForm->textFieldTable($model, 'PromoPrice', array('class' => 'input length_6')); ?>


          <?php echo $objForm->textFieldTable($model, 'Sales', array('class' => 'input length_6')); ?>


          <?php echo $objForm->radioButtonListTable($model, 'Status', GoodsBmModel::getStatusHtml()); ?>

          <?php echo $objForm->textFieldTable($model, 'Sorting'); ?>
          <?php echo $objForm->textFieldTable($model, 'Money'); ?>
          <?php echo str_replace('amp;', '', $objForm->dropDownListTable($model, 'CatID', CategoryModel::getListTree())); ?>
          <?php //echo $objForm->dropDownListTable($model, 'ModuleID', ModuleModel::getList()); ?>          
          <?php echo $objForm->textFieldTable($model, 'Sorting'); ?>     

<tr>
  <th>上架时间</th>
  <td><input class="J_date date" placeholder="上架时间" name="GoodsBmModel[StartTime]" id="GoodsBmModel_StartTime" type="text" value="<?php echo date('Y-m-d', $model->StartTime); ?>">
  </td>
</tr>

<tr>
  <th>下架时间</th>
  <td><input class="J_date date" placeholder="下架时间" name="GoodsBmModel[EndTime]" id="GoodsBmModel_EndTime" type="text" value="<?php echo date('Y-m-d', $model->EndTime); ?>">
  </td>
</tr>

     
          <?php echo $objForm->textFieldTable($model, 'PlanSale'); ?>         
          <?php echo $objForm->textFieldTable($model, 'IsPay'); ?>          
          <?php echo $objForm->textAreaTable($model, 'Excuse'); ?> 
          <?php echo $objForm->textAreaTable($model, 'Brief'); ?>
          <?php echo $objForm->checkBoxTable($model, 'IsFreeShipping'); ?>                       
  
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>

  <script type="text/javascript">
    $(".J_ajax_getiteminfo").click(function(e){
      e.preventDefault();
      var url = $("#GoodsBmModel_DetailUrl").val();
      $.post('/front/public/taoData', {url:url}, function(data){
        if(data.status){
            //图片
            var imgstr = '';
            if (data.images) {
                $.each(data.images, function(i, v) {
                  imgstr += '<img  width="160" height="160"  src=' + v + ' />';
                });
                $('div.product_img').html(imgstr);
                product_img();
            }
            $("#GoodsBmModel_Sales").val(data.sales);          
        }
      }, 'json');      
    })

    function product_img() {
        $('div.product_img img').click(function() {
            $('div.product_img img').removeClass('current');
            $(this).addClass('current');
            $('#GoodsBmModel_Image').val(this.src);
        });
    }

  </script>



</body>
</html>