<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>

          <tr>
            <th width="80">URL/ID</th>
            <td>
              <?php echo $objForm->textField($model, 'DetailUrl', array('class' => 'input length_6', 'disabled' => 'disabled')); ?>
              <button class="btn J_ajax_getiteminfo">获取最新信息</button>
            </td>
          </tr>

          <?php echo $objForm->textFieldTable($model, 'GoodsName', array('class' => 'input length_6')); ?>

          <?php echo $objForm->textFieldTable($model, 'Image', array('class' => 'input length_6')); ?>
          
          <!-- 图片展示 -->
          <tr>
            <th></th>
            <td>
<div class="product_img">
  <img class="current" width="160" height="160" src="<?php echo $model->Image ?>">
</div>              
            </td>
          </tr>


          <tr>
            <th>商品ID</th>
            <td><span><?php echo $model->ProductID  ?></span></td>
          </tr>
          <tr>
            <th>原价</th>
            <td><span><?php echo $model->Pprice  ?></span></td>
          </tr>


          <?php echo $objForm->dropDownListTable($model, 'Pattern', GoodsBmModel::getPatternHtml()); ?>

          <?php echo $objForm->textFieldTable($model, 'PromoPrice', array('class' => 'input length_6')); ?>

          <?php echo $objForm->textFieldTable($model, 'Sales', array('class' => 'input length_6')); ?>


          <?php echo $objForm->radioButtonListTable($model, 'Status', GoodsBmModel::getStatusHtml()); ?>

          <?php echo $objForm->textFieldTable($model, 'Sorting'); ?>
          <?php echo $objForm->textFieldTable($model, 'Money'); ?>
          <?php echo str_replace('amp;', '', $objForm->dropDownListTable($model, 'CatID', CategoryModel::getListTree())); ?>
          <?php //echo $objForm->dropDownListTable($model, 'ModuleID', ModuleModel::getList()); ?>          
          <?php echo $objForm->textFieldTable($model, 'Sorting'); ?>     

<tr>
  <th>上架时间</th>
  <td><input class="J_date date" placeholder="上架时间" name="GoodsBmModel[StartTime]" id="GoodsBmModel_StartTime" type="text" value="<?php echo date('Y-m-d', $model->StartTime); ?>">
  </td>
</tr>

<tr>
  <th>下架时间</th>
  <td><input class="J_date date" placeholder="下架时间" name="GoodsBmModel[EndTime]" id="GoodsBmModel_EndTime" type="text" value="<?php echo date('Y-m-d', $model->EndTime); ?>">
  </td>
</tr>

     
          <?php echo $objForm->textFieldTable($model, 'PlanSale'); ?>         
          <?php echo $objForm->textFieldTable($model, 'IsPay'); ?>          
          <?php echo $objForm->textAreaTable($model, 'Excuse'); ?> 
          <?php echo $objForm->textAreaTable($model, 'Brief'); ?>
          <?php echo $objForm->checkBoxTable($model, 'IsFreeShipping'); ?>                       
  
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_submit" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>

  <script type="text/javascript">
    $(".J_ajax_getiteminfo").click(function(e){
      e.preventDefault();
      var url = $("#GoodsBmModel_DetailUrl").val();
      $.post('/front/public/taoData', {url:url}, function(data){
        if(data.status){
            //图片
            var imgstr = '';
            if (data.images) {
                $.each(data.images, function(i, v) {
                  imgstr += '<img  width="160" height="160"  src=' + v + ' />';
                });
                $('div.product_img').html(imgstr);
                product_img();
            }
            $("#GoodsBmModel_Sales").val(data.sales);          
        }
      }, 'json');      
    })

    function product_img() {
        $('div.product_img img').click(function() {
            $('div.product_img img').removeClass('current');
            $(this).addClass('current');
            $('#GoodsBmModel_Image').val(this.src);
        });
    }

  </script>

  <script type="text/javascript">
      Wind.use('ajaxForm', 'artDialog', function () {

          if ($.browser.msie) {
              //ie8及以下，表单中只有一个可见的input:text时，会整个页面会跳转提交
              ajaxForm_list.on('submit', function (e) {
                  //表单中只有一个可见的input:text时，enter提交无效
                  e.preventDefault();
              });
          }
          $(document).on('click', 'button.J_submit', function(e){
          //$('button.J_ajax_submit_btn').on('click', function (e) {
              e.preventDefault();
              var btn = $(this),
                  form = btn.parents('form.J_ajaxForm');

              //批量操作 判断选项
              if (btn.data('subcheck')) {
                  btn.parent().find('span').remove();
                  if (form.find('input.J_check:checked').length) {
                      var msg = btn.data('msg');
                      if (msg) {
                          art.dialog({
                              id: 'warning',
                              icon: 'warning',
                              content: btn.data('msg'),
                              cancelVal: '关闭',
                              cancel: function () {
                                  btn.data('subcheck', false);
                                  btn.click();
                              }
                          });
                      } else {
                          btn.data('subcheck', false);
                          btn.click();
                      }

                  } else {
                      $('<span class="tips_error">请至少选择一项</span>').appendTo(btn.parent()).fadeIn('fast');
                  }
                  return false;
              }

              //ie处理placeholder提交问题
              if ($.browser.msie) {
                  form.find('[placeholder]').each(function () {
                      var input = $(this);
                      if (input.val() == input.attr('placeholder')) {
                          input.val('');
                      }
                  });
              }

              form.ajaxSubmit({
                  url: btn.data('action') ? btn.data('action') : form.attr('action'), //按钮上是否自定义提交地址(多按钮情况)
                  dataType: 'json',
                  beforeSubmit: function (arr, $form, options) {
                      var text = btn.text();

                      //按钮文案、状态修改
                      btn.text(text + '中...').prop('disabled', true).addClass('disabled');
                  },
                  success: function (data, statusText, xhr, $form) {
                      var text = btn.text();
                      
                      //按钮文案、状态修改
                      btn.removeClass('disabled').text(text.replace('中...', '')).parent().find('span').remove();

                      if (data.state === 'success') {
                        $(parent.window.document).find("tr[data-id='<?php echo $model->GoodsBmID ?>']").eq(0).html(data.content);
                        var list = parent.window.art.dialog.list;
                        for (var i in list) {
                          list[i].close();
                        };                                                      
                              
                      } else if (data.state === 'fail') {
                          $('<span class="tips_error">' + data.info + '</span>').appendTo(btn.parent()).fadeIn('fast');
                          btn.removeProp('disabled').removeClass('disabled');
                      }
                  }
              });
          });

      });
        
  </script>

</body>
</html>

