<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">
<style>
.col-left{float:left}
.col-auto{overflow:hidden;_zoom:1;_float:left;}
.title-1{border-bottom:1px solid #eee; padding-left:5px}
.f14{font-size: 14px}
.lh28{line-height: 28px}
.blue,.blue a{color:#004499}
.gray4,a.gray4{color:#999;}
.lh22{line-height: 22px}
.lh25{line-height: 25px}
</style>
  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl(ACTION_NAME))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            <!-- 状态，操作人，上架时间  报名时间  栏目  关键字  -->

            状态：
            <?php echo $objForm->dropDownList($model,'Status',GoodsBmModel::getStatusHtml(),array('empty' => '全部')); ?>
            &nbsp;&nbsp;
            操作人：
            <?php echo $objForm->dropDownList($model,'AdminID',AdminUser::model()->getUserListName(),array('empty' => '全部')); ?>
            &nbsp;&nbsp;
            上架时间：
            <?php echo $objForm->textField($model, 'StartTime', array('class' => 'J_date input length_2', 'placeholder' => '选择起始时间')); ?>
            -
            <?php echo $objForm->textField($model, 'EndTime', array('class' => 'J_date input length_2', 'placeholder' => '选择结束时间')); ?>
            &nbsp;&nbsp;
            报名时间：
            <input type="text" value="<?php echo $add_start_time; ?>" name="add_start_time" class="J_date input length_2" placeholder="选择起始时间">
            -
            <input type="text" name="add_end_time" value="<?php echo $add_end_time; ?>" class="J_date input length_2" placeholder="选择结束时间">
            </br></br>
            栏目：
            <?php echo str_replace('amp;', '', $objForm->dropDownList($model,'CatID',CategoryModel::getListTree(),array('empty' => '全部'))); ?>
            &nbsp;&nbsp;
            品牌团：
            <?php echo $objForm->dropDownList($model,'UserBannerID',MemberBrandModel::getAll(),array('empty' => '全部')); ?>
            &nbsp;&nbsp;
            
            关键字:
            <?php echo Form::select(array(1=>'商品ID',2=>'商品名称',3=>'商品旺量',4=>'报名ID'),$key,'name="key"') ?>
            <input type="text" class="input length_2" id="keyword" name="keyword" style="width:200px;" value="<?php echo $keyword ?>" placeholder="请输入搜索内容...">            

             <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>

    <?php  if(ACTION_NAME == 'check'){  ?>
    <div class="mb10">
          <a class="J_ajax_request" href="<?php echo $this->createUrl('getgoodsbm') ?>"><b><font color="#FF0000">点击获取新的需要审核的宝贝数据</font></b></a>
          (当前待审核总数：<font color="#FF0000"><?php echo $dcount; ?></font>)
    </div>
    <?php } ?>
    
    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center"><?php echo $sort->link('GoodsBmID') ?></td>
              <td align="center"><?php echo $sort->link('ProductID') ?></td>
              <td align="center">商品报名信息</td>
              <td align="center"><?php echo $sort->link('Status') ?></td>
              <td align="center">管理操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr data-id="<?php $rs->GoodsBmID ?>">
                <?php include $this->getViewFile('data'); ?>            
              </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    </form>
  </div>


<script type="text/javascript">
  Wind.use('artDialog','iframeTools', function () {  
    $(document).on('change', '.feipei', function(){
      var obj_this = $(this);
        $.post('<?php echo $this->createUrl("fenpei") ?>', {id:$(this).data('id'),AdminID:$(this).val()}, function(data){
              if (data.state === 'success') {
                  if (data.referer) {
                      location.href = data.referer;
                  } else {
                      if(typeof data.info == 'undefined'){
                          art.dialog.alert('分配成功');
                          obj_this.parents('tr').eq(0).html(data.content);
                      }else{
                          art.dialog.alert(data.info);
                      }                           
                  }
              } else if (data.state === 'fail') {
                  art.dialog.alert(data.info);
              }        
        }, 'json');        


    });
  });
</script>

</body>
</html>