<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>




<?php
/* @var $this GoodsBmModelController */
/* @var $model GoodsBmModel */

	//加面包
	$this->breadcrumbs = array(
		'用户中心' => array('user/ab'),
		'用户登录' => array('user/ab'),
	);

  if(!empty($this->breadcrumbs)){
    $this->widget('zii.widgets.CBreadcrumbs', array( 
        'homeLink'=>'<a href="/">首页</a>', //根的是什么
        'links'=>$this->breadcrumbs,   
        'separator'=>'&gt;',  //分隔符
    ));      
  }
?>

<h1>详细信息</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'GoodsBmID',
		'GoodsName',
		'City',
		'CreditScore',
		'DetailUrl',
		'Image',
		'ItemType',
		'Location',
		'Nick',
		'Pprice',
		'ProductID',
		'PromoPrice',
		'Province',
		'Sales',
		'AddTime',
		'Status',
		'UpdateTime',
		'Sorting',
		'UserID',
		'UserName',
		'AdminID',
		'Money',
		'CatID',
//		'ModuleID',
		'StartTime',
		'EndTime',
		'SpecialID',
		'PlanSale',
		'IsPay',
		'Excuse',
		'IsMail',
	),
)); ?>


  </div>

</body>
</html>