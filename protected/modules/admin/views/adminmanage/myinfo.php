<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
   <?php include $this->getViewFile('/common/Nav') ?>
   <form class="J_ajaxForm" action="<?php echo AdminBase::U("myinfo") ?>" method="post" id="myform">
   <input type="hidden" name="admin_id" value="<?php echo $info['admin_id'] ?>"/>
   <div class="h_a">基本属性</div>
   <div class="table_full">
   <table width="100%" class="table_form contentWrap">
        <tbody>
          <tr>
            <th width="80">用户名</th>
            <td><input type="test" name="admin_name" class="input" id="admin_name" value="<?php echo $info['admin_name'] ?>" disabled>
              <span class="gray">请输入用户名</span></td>
          </tr>
          <tr>
            <th>E-mail</th>
            <td><input type="text" name="email" value="<?php echo $info['email']?>" class="input" id="email" size="30">
              <span class="gray">请输入E-mail</span></td>
          </tr>
          <tr>
            <th>真实姓名</th>
            <td><input type="text" name="real_name" value="<?php echo $info['real_name']?>" class="input" id="real_name"></td>
          </tr>
          <tr>
            <th>旺旺</th>
            <td><input type="text" name="wangwang" value="<?php echo $info['wangwang']?>" class="input" id="wangwang"></td>
          </tr>
        </tbody>
      </table>
   </div>
   <div class="btn_wrap">
      <div class="btn_wrap_pd">             
        <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">修改</button>
      </div>
    </div>
    </form>
</div>

</body>
</html>