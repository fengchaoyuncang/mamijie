<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>
        <?php  $d = $objForm->dropDownListTable($model, 'ParentID', CategoryModel::getListTree(), array('empty' => '≡ 作为一级栏目 ≡'), array('th' => array('width' => 100))); 
               echo str_replace('amp;', '', $d);
        ?>

        <?php echo $objForm->textFieldTable($model, 'Title', array('class' => 'input length_6'), array('th' => array('width' => 100))); ?>
        <?php echo $objForm->textAreaTable($model, 'Brief', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'Sorting'); ?>
        <?php echo $objForm->textFieldTable($model, 'AppImg', array('class' => 'input length_6')); ?>
        <?php echo $objForm->checkBoxTable($model, 'IsHot'); ?>
        <?php echo $objForm->radioButtonListTable($model, 'Status', CategoryModel::getStatusHtml()); ?>
        <?php echo $objForm->textFieldTable($model, 'TitleSeo', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'KeywordsSeo', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'DescriptionSeo', array('class' => '')); ?>
							
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>

</body>
</html>