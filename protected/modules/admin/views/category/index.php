<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>



    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="5%" align="center"><?php echo $model->getAttributeLabel('CatID'); ?></td>
              <td width="20%" align="left"><?php echo $model->getAttributeLabel('Title'); ?></td>             
              <td width="30%" align="center"><?php echo $model->getAttributeLabel('AppImg'); ?></td>            
              <td align="center"><?php echo $model->getAttributeLabel('AddTime'); ?></td>           
              <td align="center"><?php echo $model->getAttributeLabel('Sorting'); ?></td>          
              <td align="center"><?php echo $model->getAttributeLabel('Status'); ?></td>
              <td align="center">操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
              <tr>
                  <?php include $this->getViewFile('data'); ?>          
              </tr>
             <?php } ?>

          </tbody>
        </table>

       
      </div>
    </form>
  </div>

</body>
</html>