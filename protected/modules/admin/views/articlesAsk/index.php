<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('index'))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            标题：
            <?php echo $objForm->textField($model,'Title');?>            
            &nbsp;&nbsp;&nbsp;&nbsp;
            栏目：<?php echo str_replace('amp;', '', $objForm->dropDownList($model,'CatID',CategoryArticlesAskModel::getListTree(),array('empty' => '全部'))); 

            ?>&nbsp;&nbsp;&nbsp;&nbsp;
            <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>



    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center"><?php echo $model->getAttributeLabel('ArticlesID'); ?></td>
              <td width="40%" align="left"><?php echo $model->getAttributeLabel('Title'); ?></td>             
              <td align="center"><?php echo $model->getAttributeLabel('AddTime'); ?></td>           
              <td align="center"><?php echo $model->getAttributeLabel('Sorting'); ?></td>          
              <td align="center"><?php echo $model->getAttributeLabel('IsShow'); ?></td>
              <td align="center">操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
              <tr>
                  <?php include $this->getViewFile('data'); ?>          
              </tr>
             <?php } ?>

          </tbody>
        </table>

       
      </div>
    </form>
  </div>

</body>
</html>