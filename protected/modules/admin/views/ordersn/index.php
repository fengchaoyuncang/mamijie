<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('index'))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            关键字：
            <?php echo Form::select(array(1=>'订单号',2=>'支付宝交易号',3=>'用户ID',4=>'用户名'),$key,'name="key"') ?>
            <input type="text" class="input length_2" id="keyword" name="keyword" style="width:200px;" value="<?php echo $keyword ?>" placeholder="请输入搜索内容...">
            &nbsp;&nbsp;&nbsp;&nbsp;
            是否付款：<?php echo $objForm->dropDownList($model,'Status',OrdersnModel::getStatusHtml(),array('empty' => '全部')); ?>&nbsp;&nbsp;&nbsp;&nbsp;
            类型：<?php echo $objForm->dropDownList($model,'Type',OrdersnModel::getTypeHtml(),array('empty' => '全部')); ?>&nbsp;&nbsp;&nbsp;&nbsp;
            <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>

    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="" align="center"><?php echo $model->getAttributeLabel('OrdersnID'); ?></td>
              <td width="" align="center"><?php echo $model->getAttributeLabel('Type'); ?></td>             
              <td align="center"><?php echo $model->getAttributeLabel('OrderNo'); ?></td>   
              <td align="center"><?php echo $model->getAttributeLabel('AddTime'); ?></td>                          
              <td align="center"><?php echo $model->getAttributeLabel('AlipayNo'); ?></td>                  
              <td align="center"><?php echo $model->getAttributeLabel('Status'); ?></td>                  
              <td align="center"><?php echo $model->getAttributeLabel('Remarks'); ?></td>                  
              <td align="center"><?php echo $model->getAttributeLabel('Money'); ?></td>                  
              <td align="center"><?php echo $model->getAttributeLabel('KeyID'); ?></td>                          
              <td align="center"><?php echo $model->getAttributeLabel('UserName'); ?></td>            
              <td align="center">操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
              <tr>
                  <?php include $this->getViewFile('data'); ?>          
              </tr>
             <?php } ?>

          </tbody>
        </table>
        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div> 
       
      </div>
    </form>
  </div>

</body>
</html>