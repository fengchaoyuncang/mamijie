
<?php 

/*	//加面包
	$this->breadcrumbs = array(
		'用户中心' => array('user/ab'),
		'用户登录' => array('user/ab'),
	);

  if(!empty($this->breadcrumbs)){
    $this->widget('zii.widgets.CBreadcrumbs', array( 
        'homeLink'=>'<a href="/">首页</a>', //根的是什么
        'links'=>$this->breadcrumbs,   
        'separator'=>'&gt;',  //分隔符
    ));      
  }*/



?>

<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

<h1>订单详情</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'OrdersnID',
		'Type' => array('name' => 'Type', 'value' => OrdersnModel::getTypeHtml($model->Type)),
		'OrderNo',
		'AddTime' => array('type' => 'date', 'name' => 'AddTime', ),
		'AlipayNo',
		'UserID',
		'UserName',
		'OrderTitle',
		'Status' => array('name' => 'Status', 'value' => OrdersnModel::getStatusHtml($model->Status)),
		'Remarks',
		'KeyID',
	),
)); ?>

</div>
</body>
