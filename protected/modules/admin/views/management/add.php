<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
   <?php include $this->getViewFile('/common/Nav') ?>
   <form class="J_ajaxForm" action="<?php echo AdminBase::U("add") ?>" method="post" id="myform">
   <div class="h_a">基本属性</div>
   <div class="table_full">
   <table width="100%" class="table_form contentWrap">
        <tbody>
          <tr>
            <th width="80">用户名</th>
            <td><input type="test" name="admin_name" class="input" id="admin_name" value="">
              <span class="gray">请输入用户名</span></td>
          </tr>
          <tr>
            <th>新密码</th>
            <td><input type="password" name="password" class="input" id="password" value="">
              <span class="gray">请输入密码</span></td>
          </tr>
          <tr>
            <th>确认密码</th>
            <td><input type="password" name="confirm_password" class="input" id="confirm_password" value="">
              <span class="gray">请输入确认密码</span></td>
          </tr>
          <tr>
            <th>E-mail</th>
            <td><input type="text" name="email" value="" class="input" id="email" size="30">
              <span class="gray">请输入E-mail</span></td>
          </tr>
          <tr>
            <th>真实姓名</th>
            <td><input type="text" name="real_name" value="" class="input" id="real_name"></td>
          </tr>
          <tr>
            <th>旺旺</th>
            <td><input type="text" name="wangwang" value="" class="input" id="wangwang"></td>
          </tr>
          <tr>
            <th>所属角色</th>
            <td><?php echo Form::select(Role::model()->getSelectRoleList(),3,'name="potency"') ?></td>
          </tr>
          <tr>
          <th>状态</td>
          <td><?php echo Form::radio(array(1 => '启用',0 => '禁用'),1,'name="status"'); ?></td>
        </tr>
        </tbody>
      </table>
   </div>
   <div class="btn_wrap1">
      <div class="btn_wrap_pd">             
        <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">添加</button>
      </div>
    </div>
    </form>
</div>
</body>
</html>