<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
   <?php include $this->getViewFile('/common/Nav') ?>
   <div class="table_list">
   <table width="100%" cellspacing="0">
        <thead>
          <tr>
            <td>序号</td>
            <td align="left" >用户名</td>
            <td >真实姓名</td>
            <td align="left" >所属角色</td>
            <td align="left" >最后登录时间</td>
            <td align="left" >E-mail</td>
            <td align="center">管理操作</td>
          </tr>
        </thead>
        <tbody>
        <?php
		foreach($list as $rs){
		?>
          <tr>
            <td><?php echo $rs->admin_id;?></td>
            <td><?php echo $rs->admin_name;?></td>
            <td><?php echo $rs->real_name?></td>
            <td><?php echo AdminUser::model()->userGroupsLabel($rs->potency);?></td>
            <td>
            <?php
			if(empty($rs->last_login)){
				echo "该用户还没登陆过";
			}else{
				echo date('Y-m-d H:i:s',$rs->last_login);
			}
			?>
            </td>
            <td><?php echo $rs->email?></td>
            <td align="center">
            <a href="<?php echo AdminBase::U("editor",array('admin_id'=>$rs->admin_id)) ?>">修改</a> | 
            <a class="J_ajax_del" href="<?php echo AdminBase::U("delete",array('admin_id'=>$rs->admin_id)) ?>">删除</a>
            </td>
          </tr>
         <?php
		}
		 ?>
        </tbody>
      </table>
   </div>
</div>
</body>
</html>