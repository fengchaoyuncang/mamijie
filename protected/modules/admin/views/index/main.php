<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 -<?php echo Yii::app()->name; ?></title>
<link href="<?php echo yii::app()->theme->baseUrl; ?>/assets/statics/css/admin_style.css" rel="stylesheet" />
<link href="<?php echo yii::app()->theme->baseUrl; ?>/assets/statics/js/artDialog/skins/default.css" rel="stylesheet" />
<?php include $this->getViewFile('/common/Js'); ?>
</head>
<body>
<div class="wrap">
  <div id="home_toptip"></div>
  <h2 class="h_a">系统信息</h2>
  <div class="home_info">
    <ul>
      <li> <em>操作系统</em> <span><?php echo PHP_OS;?></span> </li>
      <li> <em>运行环境</em> <span><?php echo  $_SERVER["SERVER_SOFTWARE"];?></span> </li>
      <li> <em>PHP运行方式</em> <span><?php echo php_sapi_name();?></span> </li>
      <li> <em>上传附件限制</em> <span><?php echo ini_get('upload_max_filesize');?></span> </li>
      <li> <em>执行时间限制</em> <span><?php echo ini_get('max_execution_time');?>秒</span> </li>
      <li> <em>剩余空间</em> <span><?php echo round((@disk_free_space(".") / (1024 * 1024)), 2) . 'M';?></span> </li>
    </ul>
  </div>
</div>
</body>
</html>