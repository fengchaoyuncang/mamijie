<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>

          <tr>
            <th width="80">URL/ID</th>
            <td>
              <?php echo $objForm->textField($model, 'DetailUrl', array('class' => 'input length_6')); ?>
              <button class="btn J_ajax_getiteminfo">获取最新信息</button>
            </td>
          </tr>


          <?php echo $objForm->textFieldTable($model, 'GoodsName', array('class' => 'input length_6')); ?>         

          <!-- 总共有哪些标签，然后我这个现在目前有哪些标签 -->
          <tr>
            <th>标签管理</th>
            <td>
<div class="label_box" style="">
  <ul id="tag_label" class="label">
<!--     <li id="li_0">
      <a href="javascript:;" onclick="taglist(0);">a</a>
      <img src="<?php echo yii::app()->theme->baseUrl ?>/assets/statics/images/admin/label_03.png" class="label-pic" onclick="deletes(0);">
      <input type="hidden" name="tags[]" value="a">
    </li> -->
  </ul>
</div>

              <span>
                新增标签：
                 <input type="text" value="" name="add_tags" class="input length_2" id="add_tags">
                 <input type="button" id="addtags" class="btn  mr10 addtags" value="添加">
               </span>
              
              <?php //echo $objForm->checkBoxList($model, 'Tag', TagModel::getFieldList('Title')); ?>  
            </td>
          </tr>

          <script type="text/javascript">
            //新增标签
            $("#addtags").click(function(){
              var value = $("#add_tags").val();
              if(value != ''){
                var html = '<li>';
                html += '<a href="javascript:;" >'+value+'</a>';
                html += '<img src="<?php echo yii::app()->theme->baseUrl ?>/assets/statics/images/admin/label_03.png" class="delete_tags label-pic";">';
                html += '<input type="hidden" name="GoodsModel[Tag_add][]" value="'+value+'">';
                html += '</li>';
                $("#tag_label").append(html);
              }
            })

            $(document).on('click', '.delete_tags', function(){
              $(this).parent().remove();
            })
          </script> 






            
          <?php echo $objForm->textFieldTable($model, 'Image', array('class' => 'input length_6')); ?>
          
          <!-- 图片展示 -->
          <tr>
            <th></th>
            <td>
<div class="product_img">
  <?php  if(!empty($model->Image)){ ?>
  <img class="current" width="160" height="160" src="<?php echo $model->Image ?>">
  <?php  } ?>
</div>              
            </td>
          </tr>

          <?php echo $objForm->textFieldTable($model, 'TaobaoUrl', array('class' => 'input length_6')); ?>
          <?php echo $objForm->textFieldTable($model, 'AppTaobaoUrl', array('class' => 'input length_6')); ?>
          <?php echo $objForm->dropDownListTable($model, 'ItemType', GoodsModel::getItemTypeHtml()); ?> 
          <?php echo $objForm->textFieldTable($model, 'Nick', array('class' => 'input length_6')); ?>
          <?php echo $objForm->textFieldTable($model, 'Pprice', array('class' => 'input length_2')); ?>
          <?php echo $objForm->textFieldTable($model, 'ProductID', array('class' => 'input length_2')); ?>
          <?php echo $objForm->textFieldTable($model, 'PromoPrice', array('class' => 'input length_2')); ?>
          <?php echo $objForm->textFieldTable($model, 'Sales', array('class' => 'input length_1')); ?>
          <?php echo $objForm->textFieldTable($model, 'Sorting', array('class' => 'input length_1')); ?>
          <?php echo $objForm->radioButtonListTable($model, 'Status', GoodsModel::getStatusHtml()); ?>
          <?php echo str_replace('amp;', '', $objForm->dropDownListTable($model, 'CatID', CategoryModel::getListTree(), array('empty' => '请选择...'))); ?>
            
          <?php echo $objForm->textAreaTable($model, 'Brief'); ?>
          <?php echo $objForm->checkBoxTable($model, 'IsFreeShipping'); ?>
          <?php echo $objForm->checkBoxTable($model, 'IsHot'); ?>
          <?php echo $objForm->checkBoxTable($model, 'IsPreferential'); ?>
          <?php echo $objForm->dropDownListTable($model, 'UserBannerID', MemberBrandModel::getAll(), array('empty' => '普通')); ?>

          <?php echo $objForm->dropDownListTable($model, 'SpecialID', GoodsSpecialModel::getFieldList('Name'), array('empty' => '无专场')); ?>
          
          <tr>
            <th>开始时间</th>
            <td><input class="J_date date input" placeholder="开始时间" name="GoodsModel[StartTime]" id="GoodsModel_StartTime" type="text" value="<?php echo date('Y-m-d H:i:s', $model->StartTime); ?>"></td>
          </tr>
          <tr>
            <th>结束时间</th>
            <td><input class="J_date date input" placeholder="开始时间" name="GoodsModel[EndTime]" id="GoodsModel_EndTime" type="text" value="<?php echo date('Y-m-d H:i:s', $model->EndTime); ?>"></td>
          </tr>

                        
  
          <tr>
            <th>附加属性</th>
            <td><span><?php echo $objForm->labelEx( $model, 'IsIndex' ) ?>：</span><?php echo $objForm->checkBox($model, 'IsIndex'); ?> </td>
          </tr>


              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>

  <script type="text/javascript">
    $(".J_ajax_getiteminfo").click(function(e){
      e.preventDefault();
      var url = $("#GoodsModel_DetailUrl").val();
      $.post('/front/public/taoData', {url:url}, function(data){
        console.log(data);
        if(data.status){
            //图片
            var imgstr = '';
            if (data.images) {
                $.each(data.images, function(i, v) {
                  if(i == 0 && '<?php echo ACTION_NAME ?>' == 'create'){
                    $("#GoodsModel_Image").val(v);
                  }
                  imgstr += '<img  width="160" height="160"  src=' + v + ' />';
                });
                $('div.product_img').html(imgstr);
                product_img();
            }
            $("#GoodsModel_DetailUrl").val(data.detail_url);
            $("#GoodsModel_GoodsName").val(data.title);
            $("#GoodsModel_ItemType").val(data.item_type);
            $("#GoodsModel_Nick").val(data.nick); 
            $("#GoodsModel_Pprice").val(data.p_price); 
            $("#GoodsModel_ProductID").val(data.product_id); 
            $("#GoodsModel_PromoPrice").val(data.promo_price); 
            $("#GoodsModel_Sales").val(data.sales); 
        }
      }, 'json');      
    })

    function product_img() {
        $('div.product_img img').click(function() {
            $('div.product_img img').removeClass('current');
            $(this).addClass('current');
            $('#GoodsModel_Image').val(this.src);
        });
    }

  </script>



</body>
</html>