<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">
<style>
.col-left{float:left}
.col-auto{overflow:hidden;_zoom:1;_float:left;}
.title-1{border-bottom:1px solid #eee; padding-left:5px}
.f14{font-size: 14px}
.lh28{line-height: 28px}
.blue,.blue a{color:#004499}
.gray4,a.gray4{color:#999;}
.lh22{line-height: 22px}
.lh25{line-height: 25px}
</style>
  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl(ACTION_NAME))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            <!-- 状态，操作人，上架时间  报名时间  栏目  关键字  -->
            状态：
            <?php  $d = GoodsModel::getStatusHtml();$d[9] = '正在显示';  ?>
            <?php echo $objForm->dropDownList($model,'Status',$d,array('empty' => '全部')); ?>
            &nbsp;&nbsp;
            上架时间：
            <input type="text" value="<?php echo $start_StartTime; ?>" name="start_StartTime" class="J_date input length_2" placeholder="选择起始时间">
            -
            <input type="text" name="end_StartTime" value="<?php echo $end_StartTime; ?>" class="J_date input length_2" placeholder="选择结束时间">
            &nbsp;&nbsp;
            下架时间：
            <input type="text" value="<?php echo $start_EndTime; ?>" name="start_EndTime" class="J_date input length_2" placeholder="选择起始时间">
            -
            <input type="text" name="end_EndTime" value="<?php echo $end_EndTime; ?>" class="J_date input length_2" placeholder="选择结束时间">
            &nbsp;&nbsp;
</br></br>


            首页推荐
            <?php echo $objForm->checkBox($model,'IsIndex', array('separator'=>'')); ?>

            特惠
            <?php echo $objForm->checkBox($model,'IsPreferential', array('separator'=>'')); ?>

            &nbsp;&nbsp;
            报名商品
            <?php echo $objForm->checkBox($model,'IsGather', array('separator'=>'')); ?>


            栏目：
            <?php echo str_replace('amp;', '', $objForm->dropDownList($model,'CatID',CategoryModel::getListTree(),array('empty' => '全部'))); ?>

            &nbsp;&nbsp;
            店铺类型：
            <?php echo $objForm->dropDownList($model,'ItemType',GoodsModel::getItemTypeHtml(),array('empty' => '全部')); ?>
            &nbsp;&nbsp;
          </br></br>
            品牌团：
            <?php echo $objForm->dropDownList($model,'UserBannerID',MemberBrandModel::getAll(),array('empty' => '全部')); ?>
            &nbsp;&nbsp;

            专场：
          <?php echo $objForm->dropDownList($model, 'SpecialID', GoodsSpecialModel::getFieldList('Name'), array('empty' => '全部')); ?>

            &nbsp;&nbsp;
            关键字:
            <?php echo Form::select(array(1=>'商品ID',2=>'商品名称',3=>'商品旺量',4=>'报名ID'),$key,'name="key"') ?>
            <input type="text" class="input length_2" id="keyword" name="keyword" style="width:200px;" value="<?php echo $keyword ?>" placeholder="请输入搜索内容...">            

             <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>


    
    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录 </div>
    <form name="myform" action="" method="post" class="J_ajaxForm form">
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm'), array('class' => 'J_ajaxForm form')); ?>  
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center"><?php echo $sort->link('GoodsID') ?></td>
              <td align="center"><?php echo $sort->link('ProductID') ?></td>
              <td align="center">商品报名信息</td>
              <td align="center"><?php echo $sort->link('CatID') ?></td>
              <td align="center"><?php echo $sort->link('Sorting') ?></td>
              <td align="center">管理操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <?php  $rsBm = GoodsBmModel::model()->findByPk($rs->GoodsBmID);   ?>
             <tr data-id="<?php echo $rs->GoodsID ?>" >
                <?php include $this->getViewFile('data'); ?>            
              </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    <?php $this->endWidget(); ?>
  </div>




  <script type="text/javascript">


  var FORM_VALUE = '';


    $(function(){
      $(document).on('change', ".form [data-gid]", function(){
      //$(".form [data-gid]").change(function(){
        var obj_this = $(this);
        var id = obj_this.data('gid');
        if($(this).attr('type') == 'checkbox'){
          if($(this)[0].checked == true){
            var value = 1;
          }else{
            var value = 0;
          }
        }else{
          var value = obj_this.val();
        }
        var name = obj_this.attr('name');
        var obj = {};
        obj[name] = value;
        obj.id = id;
        
        $.post('<?php echo $this->createUrl("updateOne")  ?>', obj, function(data){
          if(data.status){
            art.dialog.tips('修改成功！', 1.5);
            obj_this.parents('tr').eq(0).html(data.content);                     
          }else{
            art.dialog.tips(data.info, 3);
            obj_this.val(FORM_VALUE);
          }
        }, 'json');
      })

      $(".form [data-gid]").focus(function(){
        FORM_VALUE = $(this).val();
        //console.log(FORM_VALUE);
      })
    })
  </script>

  <script type="text/javascript">
    $(function(){
      $(".jiesuan").click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        var dialog = art.dialog({id: 'servicefees',title: false});
        $.post(href,{},function(data){
          console.log(data);
          dialog.content(data.content);
          dialog.button([{
            name:'确定',
            callback:function(){
              var num = $("[name='RealSale']").val();
              var is_complete = $("[name='is_complete']:checked").val();

              $.post(href,{num:num,is_complete:is_complete},function(data){
                alert(data.info);
                if(data.status){
                  dialog.close();
                }
                return true;
              },'json');
              return false;
            },
            focus: true
          },{name:'取消'}]);
        },'json');
      })
    })
  </script>

</body>
</html>