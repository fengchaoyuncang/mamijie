<?php  if(empty($objForm)){
	$objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl(ACTION_NAME)));
	$ISFORM = 1;
}   
?>
<td  width="50" align="center"><?php echo $rs->GoodsID; ?></td>
<td  width="100" align="center"><?php echo $rs->ProductID; ?></td>


<!-- //详细信息 -->
<td width="" align="left">
	<div class="col-left mr10" style="width:146px; height:112px">
		<a href="<?php echo $rs->DetailUrl ?>" target="_blank">
			<img src="<?php echo $rs->Image ?>" width="146" height="112" style="border:1px solid #eee" align="left">
		</a>
	</div>

	<div class="col-auto">
      <h2 class="title-1 f14 lh28 mb6 blue">
      	<?php echo $objForm->textField($rs, 'GoodsName', array('data-gid'=> $rs->GoodsID, 'class' => 'input length_6')); ?>
        <a width="600" height="1200" href="<?php  echo $this->createUrl('quickUpdate', array('id' => $rs->GoodsID)) ?>" style="color: #999" class="J_dialog" >(快速编辑)</a>
      </h2>
      </br>
      <p class="gray4 lh25">
      原价：<font color="#FF0000"><?php echo $rs->Pprice ?></font>， 
      活动价：<?php echo $objForm->textField($rs, 'PromoPrice', array('data-gid'=> $rs->GoodsID, 'class' => 'input length_1')); ?>，
      销量：<?php echo $objForm->textField($rs, 'Sales', array('data-gid'=> $rs->GoodsID, 'class' => 'input length_1')); ?>，
      旺旺：<b><a href="http://amos.im.alisoft.com/msg.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid=<?php echo $rs->Nick ?>" target="_blank"><?php echo $rs->Nick ?><img src="http://amos.im.alisoft.com/online.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid=<?php echo $rs->Nick ?>" alt=""></a></b>
      店铺类型：<?php echo $objForm->dropDownList($rs,'ItemType',GoodsModel::getItemTypeHtml(), array('data-gid'=> $rs->GoodsID)); ?>

	  </p>
      <p class="gray4 lh25">
    	
	  首页推荐：<?php echo $objForm->checkBox($rs, 'IsIndex', array('data-gid'=> $rs->GoodsID)); ?>
	  是否售罄：<?php echo $objForm->checkBox($rs, 'IsSoldout', array('data-gid'=> $rs->GoodsID)); ?>
	  是否特惠：<?php echo $objForm->checkBox($rs, 'IsPreferential', array('data-gid'=> $rs->GoodsID)); ?>
      </p>

      <p class="gray4 lh25">
    	
      上架时间：<input data-gid="<?php echo $rs->GoodsID; ?>" class="input lenght_2" name="GoodsModel[StartTime]" id="GoodsModel_StartTime" type="text" value="<?php echo date('Y-m-d H:i:s', $rs->StartTime); ?>">，
      下架时间：<input data-gid="<?php echo $rs->GoodsID; ?>" class="input lenght_2" name="GoodsModel[EndTime]" id="GoodsModel_EndTime" type="text" value="<?php echo date('Y-m-d H:i:s', $rs->EndTime); ?>">，
      </p>
       <?php  if($rs->Status == 5){
       	echo '<p style="color:red" class="gray4 lh25">下架商品</p>';
       } ?>


	</div>
</td>

<td align="center">
	<?php echo str_replace('amp;', '', $objForm->dropDownList($rs, 'CatID', CategoryModel::getListTree(), array('data-gid'=> $rs->GoodsID))); ?>
</td>

<td align="center">
	<?php echo $objForm->textField($rs, 'Sorting', array('data-gid'=> $rs->GoodsID, 'class' => 'input length_1')); ?>
</td>


<td width="200" align="center">
	<?php  
		$op = array();

		//没有结算的
		if(RBAC::authenticate('jiesuan') && $rsBm->IsAccounts == 0){

			$op[] = '<a class="jiesuan" width="400" height="400" href="'.AdminBase::U("jiesuan",array('id'=>$rs->GoodsID)).'" >结算</a>';
		}

		if(RBAC::authenticate('update')){
			$op[] = '<a href="'.AdminBase::U("update",array('id'=>$rs->GoodsID)).'" >编辑</a>';
		}
		if(RBAC::authenticate('delete') && $rs->Status == 1 && TIME_TIME < $rs->EndTime){
			$op[] = '<a class="J_ajax_request" data-msg="确定要下架吗？" href="'.AdminBase::U("delete",array('id'=>$rs->GoodsID)).'" >下架</a>';
		}
		if(RBAC::authenticate('shangjia') && $rs->Status != 1 && TIME_TIME < $rs->EndTime){
			$op[] = '<a class="J_ajax_request" data-msg="确定要上架吗？" href="'.AdminBase::U("shangjia",array('id'=>$rs->GoodsID)).'" >上架</a>';
		}
		if(RBAC::authenticate('activity') && $rs->GoodsBmID && $rs->Status == 1){
			$op[] = '<a class="J_ajax_request" data-msg="确定要取消活动吗？取消活动不影响商家正常报名。" href="'.AdminBase::U("activity",array('id'=>$rs->GoodsID)).'" >取消活动</a>';
		}
		if(RBAC::authenticate('goodsbm/notice') && $rs->GoodsBmID){
	      $content = "<div id='dialog_form'><span style='margin-right:20px;'>信息内容</span> <textarea style='height:50px;width:400px;' type='text' name='notice' cols='60' rows='3'>亲，您的商品({$rs->ProductID})已经上架，请到“报名查询”页面查询商品（查询后有审核人员旺旺），并联系洽谈。</textarea></div> ";

	      $op[] = '<a style="color:red" data-title="信息通知" data-content="'.$content.'" class="J_ajax_dialog" href="'.$this->createUrl("/admin/goodsBm/notice",array('id'=>$rs->GoodsBmID)).'" >信息通知</a>';
	   	}
        
	    $op = implode('| ', $op);
	    echo $op;
	?>   
</td>

<?php  if(!empty($ISFORM)){
	$this->endWidget();
	
}   
?>

