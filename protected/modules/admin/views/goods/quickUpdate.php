<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
 
     

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>



       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>

          <tr>
            <th width="80">URL/ID</th>
            <td>
              <?php echo $objForm->textField($model, 'DetailUrl', array('class' => 'input length_6')); ?>
              <button class="btn J_ajax_getiteminfo">获取最新信息</button>
            </td>
          </tr>

          <?php echo $objForm->textFieldTable($model, 'TaobaoUrl', array('class' => 'input length_6')); ?> 
          <?php echo $objForm->textFieldTable($model, 'AppTaobaoUrl', array('class' => 'input length_6')); ?>       

          <?php echo $objForm->textFieldTable($model, 'GoodsName', array('class' => 'input length_6')); ?>


          <!-- 总共有哪些标签，然后我这个现在目前有哪些标签 -->
          <tr>
            <th>标签管理</th>
            <td>
<div class="label_box" style="">
  <ul id="tag_label" class="label">
<!--     <li id="li_0">
      <a href="javascript:;" onclick="taglist(0);">a</a>
      <img src="<?php echo yii::app()->theme->baseUrl ?>/assets/statics/images/admin/label_03.png" class="label-pic" onclick="deletes(0);">
      <input type="hidden" name="tags[]" value="a">
    </li> -->
  </ul>
</div>

              <span>
                新增标签：
                 <input type="text" value="" name="add_tags" class="input length_2" id="add_tags">
                 <input type="button" id="addtags" class="btn  mr10 addtags" value="添加">
               </span>
              
              <?php //echo $objForm->checkBoxList($model, 'Tag', TagModel::getFieldList('Title')); ?>  
            </td>
          </tr>

          <script type="text/javascript">
            //新增标签
            $("#addtags").click(function(){
              var value = $("#add_tags").val();
              if(value != ''){
                var html = '<li>';
                html += '<a href="javascript:;" >'+value+'</a>';
                html += '<img src="<?php echo yii::app()->theme->baseUrl ?>/assets/statics/images/admin/label_03.png" class="delete_tags label-pic";">';
                html += '<input type="hidden" name="GoodsModel[Tag_add][]" value="'+value+'">';
                html += '</li>';
                $("#tag_label").append(html);
              }
            })

            $(document).on('click', '.delete_tags', function(){
              $(this).parent().remove();
            })
          </script> 





          <?php echo $objForm->textFieldTable($model, 'Image', array('class' => 'input length_6')); ?>
          
          <!-- 图片展示 -->
          <tr>
            <th></th>
            <td>
<div class="product_img">
  <?php  if(!empty($model->Image)){ ?>
  <img class="current" width="160" height="160" src="<?php echo $model->Image ?>">
  <?php  } ?>
</div>              
            </td>
          </tr>





          <tr>
            <th>店铺类型：</th>
            <td>
          <?php echo $objForm->dropDownList($model, 'ItemType', GoodsModel::getItemTypeHtml()); ?> 
          <am>旺旺：</am><?php echo $objForm->textField($model, 'Nick', array('class' => 'input length_2')); ?>
          <am>原价：</am><?php echo $objForm->textField($model, 'Pprice', array('class' => 'input length_2')); ?>


            </td>
          </tr>    


          <tr>
            <th>商品ID：</th>
            <td>
          <?php echo $objForm->textField($model, 'ProductID', array('class' => 'input length_2')); ?>
         <am>折扣价：</am> <?php echo $objForm->textField($model, 'PromoPrice', array('class' => 'input length_1')); ?>
<am>排序：</am><?php echo $objForm->textField($model, 'Sorting', array('class' => 'input length_1')); ?>
<?php echo $objForm->labelEx( $model, 'IsIndex' ) ?>：</span><?php echo $objForm->checkBox($model, 'IsIndex'); ?>
            </td>
          </tr>   




          <tr>
            <th>销量：</th>
            <td>
          <?php echo $objForm->textField($model, 'Sales', array('class' => 'input length_1')); ?>
          
          <am>商品状态：</am><?php echo $objForm->dropDownList($model, 'Status', GoodsModel::getStatusHtml(), array('separator' => "&nbsp")); ?>
          <am>分类：</am><?php echo str_replace('amp;', '', $objForm->dropDownList($model, 'CatID', CategoryModel::getListTree())); ?>

            </td>
          </tr> 



          <?php echo $objForm->dropDownListTable($model, 'SpecialID', GoodsSpecialModel::getFieldList('Name'), array('empty' => '无专场')); ?>



              


          <tr>
            <th>时间：</th>
            <td>
              <input class="J_datetime date input length_2" placeholder="开始时间" name="GoodsModel[StartTime]" id="GoodsModel_StartTime" type="text" value="<?php echo date('Y-m-d H:i:s', $model->StartTime); ?>">
              -<input class="J_datetime date input length_2" placeholder="结束时间" name="GoodsModel[EndTime]" id="GoodsModel_EndTime" type="text" value="<?php echo date('Y-m-d H:i:s', $model->EndTime); ?>">

            </td>
          </tr>




                       
  
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_submit" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>


  <script type="text/javascript">
    $(".J_ajax_getiteminfo").click(function(e){
      e.preventDefault();
      var url = $("#GoodsModel_DetailUrl").val();
      $.post('/front/public/taoData', {url:url}, function(data){
        console.log(data);
        if(data.status){
            //图片
            var imgstr = '';
            if (data.images) {
                $.each(data.images, function(i, v) {
                  if(i == 0 && '<?php echo ACTION_NAME ?>' == 'create'){
                    $("#GoodsModel_Image").val(v);
                  }
                  imgstr += '<img  width="160" height="160"  src=' + v + ' />';
                });
                $('div.product_img').html(imgstr);
                product_img();
            }
            $("#GoodsModel_DetailUrl").val(data.detail_url);
            $("#GoodsModel_GoodsName").val(data.title);
            $("#GoodsModel_ItemType").val(data.item_type);
            $("#GoodsModel_Nick").val(data.nick); 
            $("#GoodsModel_Pprice").val(data.p_price); 
            $("#GoodsModel_ProductID").val(data.product_id); 
            $("#GoodsModel_PromoPrice").val(data.promo_price); 
            $("#GoodsModel_Sales").val(data.sales);           
        }
      }, 'json');      
    })

    function product_img() {
        $('div.product_img img').click(function() {
            $('div.product_img img').removeClass('current');
            $(this).addClass('current');
            $('#GoodsModel_Image').val(this.src);
        });
    }

  </script>


  <script type="text/javascript">
      Wind.use('ajaxForm', 'artDialog', function () {

          if ($.browser.msie) {
              //ie8及以下，表单中只有一个可见的input:text时，会整个页面会跳转提交
              ajaxForm_list.on('submit', function (e) {
                  //表单中只有一个可见的input:text时，enter提交无效
                  e.preventDefault();
              });
          }
          $(document).on('click', 'button.J_submit', function(e){
          //$('button.J_ajax_submit_btn').on('click', function (e) {
              e.preventDefault();
              var btn = $(this),
                  form = btn.parents('form.J_ajaxForm');

              //批量操作 判断选项
              if (btn.data('subcheck')) {
                  btn.parent().find('span').remove();
                  if (form.find('input.J_check:checked').length) {
                      var msg = btn.data('msg');
                      if (msg) {
                          art.dialog({
                              id: 'warning',
                              icon: 'warning',
                              content: btn.data('msg'),
                              cancelVal: '关闭',
                              cancel: function () {
                                  btn.data('subcheck', false);
                                  btn.click();
                              }
                          });
                      } else {
                          btn.data('subcheck', false);
                          btn.click();
                      }

                  } else {
                      $('<span class="tips_error">请至少选择一项</span>').appendTo(btn.parent()).fadeIn('fast');
                  }
                  return false;
              }

              //ie处理placeholder提交问题
              if ($.browser.msie) {
                  form.find('[placeholder]').each(function () {
                      var input = $(this);
                      if (input.val() == input.attr('placeholder')) {
                          input.val('');
                      }
                  });
              }

              form.ajaxSubmit({
                  url: btn.data('action') ? btn.data('action') : form.attr('action'), //按钮上是否自定义提交地址(多按钮情况)
                  dataType: 'json',
                  beforeSubmit: function (arr, $form, options) {
                      var text = btn.text();

                      //按钮文案、状态修改
                      btn.text(text + '中...').prop('disabled', true).addClass('disabled');
                  },
                  success: function (data, statusText, xhr, $form) {
                      var text = btn.text();
                      
                      //按钮文案、状态修改
                      btn.removeClass('disabled').text(text.replace('中...', '')).parent().find('span').remove();

                      if (data.state === 'success') {
                        
                        $(parent.window.document).find("tr[data-id='<?php echo $model->GoodsID ?>']").eq(0).html(data.content);
                        var list = parent.window.art.dialog.list;
                        for (var i in list) {
                          list[i].close();
                        };                                                      
                              
                      } else if (data.state === 'fail') {
                          $('<span class="tips_error">' + data.info + '</span>').appendTo(btn.parent()).fadeIn('fast');
                          btn.removeProp('disabled').removeClass('disabled');
                      }
                  }
              });
          });

      });
        
  </script>



</body>
</html>