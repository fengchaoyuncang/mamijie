<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('guestData'))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
              时间：
              <input type="text" value="<?php echo $time; ?>" name="time" class="J_date input length_2" placeholder="选择时间">    

              <button class="btn">搜索</button>       
          </div>
      </div>
    <?php $this->endWidget(); ?>

    <!-- 列表 -->
    <!-- <div class="mb10"> 点击总数(<?php  //echo $arrResult['click']; ?>)，Ip总数(<?php  //echo $arrResult['ip']; ?>)</div> -->
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="" align="center">名称</td>
              <td width="" align="left">点击量</td>             
              <td align="center">Ip访问量</td>            
            </tr>
          </thead>
          <tbody>

             <?php foreach($result as $url => $rs){ ?>
              <tr>
                  <?php include $this->getViewFile('data'); ?>          
              </tr>
             <?php } ?>

          </tbody>
        </table>
 
       
      </div>
    </form>
  </div>

</body>
</html>