<?php include $this->getViewFile('/common/Head') ?>
<body>
<div class="wrap J_check_wrap">
  <?php include $this->getViewFile('/common/Nav') ?>
  <div class="h_a">说明</div>
  <div class="prompt_text">
    <ul>
      <li>只支持csv文件上传！</li>
    </ul>
  </div>
  <div class="h_a"> 数据导入</div>
  <form name="myform" action="<?php echo AdminBase::U("import") ?>" method="post" class="J_ajaxForm" enctype="multipart/form-data">
    <div class="table_full">
      <table width="100%" class="table_form">
        <tr>
          <th width="120">来源：</th>
          <td><label><input name="source" type="radio" value="1">优站</label> <label><input name="source" type="radio" value="0" checked>官网</label></td>
        </tr>
        <tr>
          <th>csv文件：</th>
          <td><input type="file" name="file" value="" />
            只支持.csv文件上传</td>
        </tr>
      </table>
    </div>
    <div class="">
      <button type="submit" class="btn btn_submit  mr10">提交</button>
    </div>
  </form>
</div>
</body>
</html>