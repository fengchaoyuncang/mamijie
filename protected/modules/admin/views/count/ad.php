<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl(ACTION_NAME))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
        时间：
        <input type="text" name="start_time" class="input length_2 J_date" value="<?php echo $start_time?>" placeholder="选择起始时间">
        -
        <input type="text" class="input length_2 J_date" name="end_time" value="<?php echo $end_time?>" placeholder="选择结束时间">
        广告标识：<?php echo $objForm->dropDownList($model,'AdMark', AdModel::getListName(), array('empty' => '全部')); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;

            <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>


    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center"><?php echo $sort->link('AdMark') ?></td>
              <td align="center"><?php echo $sort->link('AddTime') ?></td>
              <td align="center"><?php echo $sort->link('View') ?></td>
              <td align="center"><?php echo $sort->link('UserView') ?></td>
              <td align="center"><?php echo $sort->link('TouristView') ?></td>
              <td align="center"><?php echo $sort->link('Gview') ?></td>
              <td align="center"><?php echo $sort->link('Ip') ?></td>              
              <td align="center"><?php echo $sort->link('Uv') ?></td>
              <td align="center">时间</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr>
                <td width="50" align="center"><?php echo $rs->AdMark; ?></td>
                <td align="center"><?php echo date('Y-m-d',$rs->AddTime)?></td>
                <td align="center"><?php echo $rs->View?></td>
                <td align="center"><?php echo $rs->UserView?></td>
                <td align="center"><?php echo $rs->TouristView?></td>
                <td align="center"><?php echo $rs->Gview?></td>
                <td align="center"><?php echo $rs->Ip?></td>
                <td align="center"><?php echo $rs->Uv?></td>
                <td align="center"><?php echo $rs->Year . '年' . $rs->Month . '月' . $rs->Day . '日' ?></td>

            
              </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    </form>
  </div>

</body>
</html>