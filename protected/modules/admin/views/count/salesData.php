<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl(ACTION_NAME))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
          操作人：
          <?php echo $objForm->dropDownList($model,'AdminID',AdminUser::model()->getUserListName(),array('empty' => '全部')); ?>
          &nbsp;&nbsp;
          关键字：
          <?php echo Form::select(array(1=>'商品名',2=>'商品ID',3=>'商品主键ID',4=>'订单号',5=>'报名ID'),$key,'name="key"') ?>
          <input type="text" class="input length_2" id="keyword" name="keyword" style="width:200px;" value="<?php echo $keyword ?>" placeholder="请输入搜索内容...">
          &nbsp;&nbsp;&nbsp;&nbsp;



        时间：
        <input type="text" name="start_time" class="input length_2 J_date" value="<?php echo $start_time?>" placeholder="选择起始时间">
        -
        <input type="text" class="input length_2 J_date" name="end_time" value="<?php echo $end_time?>" placeholder="选择结束时间">

            <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>




    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('deleteGoods'))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">


        时间：
        <input type="text" name="start_time" class="input length_2 J_date" value="" placeholder="选择起始时间">
        -
        <input type="text" class="input length_2 J_date" name="end_time" value="" placeholder="选择结束时间">

            <button class="btn">删除</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>







    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <div class="mb10"> 金额 ( <b><?php echo $price_num; ?></b> ) 元</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center"><?php echo $sort->link('SalesID') ?></td>
              <td align="center"><?php echo $sort->link('TCreateTime') ?></td>
              <td align="center"><?php echo $sort->link('TGoodsName') ?></td>
              <td align="center"><?php echo $sort->link('TGoodsNumber') ?></td>
              <td align="center"><?php echo $sort->link('TPrice') ?></td>
              <td align="center"><?php echo $sort->link('TProductID') ?></td>
              <td align="center"><?php echo $sort->link('TBili') ?></td>              
              <td align="center"><?php echo $sort->link('TOrderNumber') ?></td>         
              <td align="center"><?php echo $sort->link('GoodsID') ?></td>         
              <td align="center"><?php echo $sort->link('GoodsBmID') ?></td>         
              <td align="center"><?php echo $sort->link('AdminID') ?></td>        
              <td align="center"><?php echo $sort->link('Source') ?></td>         
              <td align="center"><?php echo $sort->link('PromoPrice') ?></td>                       
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr>
                <td width="50" align="center"><?php echo $rs->SalesID; ?></td>
                <td align="center"><?php echo date('Y-m-d',$rs->TCreateTime)?></td>
                <td align="center"><?php echo $rs->TGoodsName?></td>
                <td align="center"><?php echo $rs->TGoodsNumber?></td>
                <td align="center"><?php echo $rs->TPrice?></td>
                <td align="center"><?php echo $rs->TProductID?></td>
                <td align="center"><?php echo $rs->TBili?></td>
                <td align="center"><?php echo $rs->TOrderNumber?></td>
                <td align="center"><?php echo $rs->GoodsID?></td>
                <td align="center"><?php echo $rs->GoodsBmID?></td>
                <td align="center"><?php echo $rs->AdminID?></td>
                <td align="center"><?php echo SalesDataModel::getSourceHtml($rs->Source);?></td>
                <td align="center"><?php echo $rs->PromoPrice?></td>

            
              </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    </form>
  </div>

</body>
</html>