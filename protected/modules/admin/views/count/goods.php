<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">
<style>
.col-left{float:left}
.col-auto{overflow:hidden;_zoom:1;_float:left;}
.title-1{border-bottom:1px solid #eee; padding-left:5px}
.f14{font-size: 14px}
.lh28{line-height: 28px}
.blue,.blue a{color:#004499}
.gray4,a.gray4{color:#999;}
.lh22{line-height: 22px}
.lh25{line-height: 25px}
</style>
  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl(ACTION_NAME))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            <!-- 状态，操作人，上架时间  报名时间  栏目  关键字  -->

            操作人：
            <?php echo $objForm->dropDownList($model,'AdminID',AdminUser::model()->getUserListName(),array('empty' => '全部')); ?>
            &nbsp;&nbsp;
            上架时间：
            <?php echo $objForm->textField($model, 'StartTime', array('class' => 'J_date input length_2', 'placeholder' => '选择起始时间')); ?>
            -
            <?php echo $objForm->textField($model, 'EndTime', array('class' => 'J_date input length_2', 'placeholder' => '选择结束时间')); ?>
            &nbsp;&nbsp;
            记录添加时间：
            <input type="text" value="<?php echo $add_start_time; ?>" name="add_start_time" class="J_date input length_2" placeholder="选择起始时间">
            -
            <input type="text" name="add_end_time" value="<?php echo $add_end_time; ?>" class="J_date input length_2" placeholder="选择结束时间">
            </br></br>
            统计访问时间：
            <input type="text" value="<?php echo $count_start_time; ?>" name="count_start_time" class="J_date input length_2" placeholder="选择起始时间">
            -
            <input type="text" name="count_end_time" value="<?php echo $count_end_time; ?>" class="J_date input length_2" placeholder="选择结束时间">
            </br></br>
            栏目：
            <?php echo str_replace('amp;', '', $objForm->dropDownList($model,'CatID',CategoryModel::getListTree(),array('empty' => '全部'))); ?>
            &nbsp;&nbsp;
            商品ID:
            <?php echo $objForm->textField($model, 'ProductID'); ?>
            商品主键ID:
            <?php echo $objForm->textField($model, 'GoodsID'); ?>
         

             <button class="btn">搜索</button>
          </div>
      </div>

    <?php $this->endWidget(); ?>


    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('goodsOperate'))); ?>

      <div class="search_type cc mb10">
           <div class="mb10">

            统计：
            <input class="J_date input length_2" name="countTime" placeholder="选择时间"/>
             <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>


    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录,总单：<?php echo $dangSum; ?>,总额：<?php echo $zongSales; ?></div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="5%" align="center"><?php echo $sort->link('ProductID') ?></td>
              <td width="5%" align="center"><?php echo $sort->link('GoodsID') ?></td>
              <td width="40%" align="center">报名信息</td>
              <td align="center"><?php echo $sort->link('AdminID') ?></td>
              <td align="center"><?php echo $sort->link('View') ?></td>
              <td align="center"><?php echo $sort->link('UserView') ?></td>
              <td align="center"><?php echo $sort->link('TouristView') ?></td>
              <td align="center"><?php echo $sort->link('Deal') ?></td>              
              <td align="center"><?php echo $sort->link('MmjDeal') ?></td>           
              <td align="center"><?php echo $sort->link('UDeal') ?></td>           
              <td align="center"><?php echo $sort->link('Sales') ?></td>         
              <td align="center"><?php echo $sort->link('AddTime') ?></td>
              <td align="center">时间</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr>
                <td align="center"><?php echo $rs->ProductID?></td>
                <td align="center"><?php echo $rs->GoodsID?></td>


                <?php   $GoodsModel = GoodsModel::model()->findByPk($rs->GoodsID);  ?>
                <td width="" align="left">
                  <div class="col-left mr10" style="width:146px; height:112px">
                    <a href="<?php echo $GoodsModel->DetailUrl ?>" target="_blank">
                      <img src="<?php echo $GoodsModel->Image ?>" width="146" height="112" style="border:1px solid #eee" align="left">
                    </a>
                  </div>

                  <div class="col-auto">
                      <h2 class="title-1 f14 lh28 mb6 blue">
                        <a href="<?php echo $GoodsModel->DetailUrl ?>" target="_blank"><?php echo $GoodsModel->GoodsName ?></a>
                      </h2>

                      <p class="gray4 lh25">
                      原价：<font color="#FF0000"><?php echo $GoodsModel->Pprice ?></font>， 
                      活动价：<font color="#FF0000"><?php echo $GoodsModel->PromoPrice ?></font>，
                      销量：<font color="#FF0000"><?php echo $GoodsModel->Sales ?></font>，
                      旺旺：<b><a href="http://amos.im.alisoft.com/msg.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid=<?php echo $GoodsModel->Nick ?>" target="_blank"><?php echo $GoodsModel->Nick ?><img src="http://amos.im.alisoft.com/online.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid=<?php echo $GoodsModel->Nick ?>" alt=""></a></b>
                      店铺类型：<font color="#FF0000"><?php echo GoodsBmModel::getItemTypeHtml($GoodsModel->ItemType) ?></font>
                      </p>

                      <p class="gray4 lh25">
                      报名时间：<font color="#000000"><?php echo date('Y-m-d', $GoodsModel->AddTime) ?></font>，
                      报名栏目：<font color="#0033FF"><?php $d = CategoryModel::getDataDetail($GoodsModel->CatID);echo $d['Title']  ?></font>
                      </p>


                      <p class="gray4 lh25">
                      上架时间：<font color="#FF33FF"><?php echo date('Y-m-d H:i:s', $GoodsModel->StartTime) ?></font> ，
                      下架时间：<font color="#FF33FF"><?php echo date('Y-m-d H:i:s', $GoodsModel->EndTime) ?></font>
                      </p>                                                   
                  </div>
                </td>








                <td width="50" align="center"><?php echo $rs->AdminID; ?></td>
                <td align="center"><?php echo $rs->View?></td>
                <td align="center"><?php echo $rs->UserView?></td>
                <td align="center"><?php echo $rs->TouristView?></td>
                <td align="center"><?php echo $rs->Deal?></td>
                <td align="center"><?php echo $rs->MmjDeal?></td>
                <td align="center"><?php echo $rs->UDeal?></td>
                <td align="center"><?php echo $rs->Sales?></td>
                <td align="center"><?php echo date('Y-m-d',$rs->AddTime)?></td>
                <td align="center"><?php echo $rs->Year . '年' . $rs->Month . '月' . $rs->Day . '日' ?></td>

            
              </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    </form>
  </div>

</body>
</html>