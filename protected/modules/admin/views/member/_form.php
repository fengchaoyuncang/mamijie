<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>
        <?php echo $objForm->textFieldTable($model, 'Email', array('class' => 'input length_6'), array('th' => array('width' => 100))); ?>


        <?php echo $objForm->textFieldTable($model, 'Phone'); ?>
        <?php echo $objForm->textFieldTable($model, 'UserName'); ?>

        <?php echo $objForm->textFieldTable($model, 'Password', array(), array('htmlOptions' => array('td' => ' 留空则不修改密码'))); ?>

        <?php echo $objForm->textFieldTable($model, 'WangWang'); ?>

        <?php echo $objForm->textFieldTable($model, 'Nickname'); ?>

        <?php echo $objForm->textFieldTable($model, 'Score'); ?>

        <?php echo $objForm->dropDownListTable($model, 'Type', $object::getTypeHtml()); ?>
        <?php echo $objForm->dropDownListTable($model, 'Status', $object::getStatusHtml()); ?>

                      
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>


</body>
</html>