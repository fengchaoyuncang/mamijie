<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">

  <div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 搜索 -->
    <div class="h_a">查找</div>
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm','method' => 'get', 'action' => $this->createUrl('index'))); ?>
      <div class="search_type cc mb10">
           <div class="mb10">
            注册时间：
            <input type="text" value="<?php echo $add_start_time; ?>" name="add_start_time" class="J_date input length_2" placeholder="选择起始时间">
            -
            <input type="text" value="<?php echo $add_end_time; ?>" name="add_end_time" class="J_date input length_2" placeholder="选择结束时间">
            &nbsp;&nbsp;

            登录时间：
            <input type="text" value="<?php echo $add_login_time; ?>" name="add_login_time" class="J_date input length_2" placeholder="选择起始时间">
            -
            <input type="text" value="<?php echo $end_login_time; ?>" name="end_login_time" class="J_date input length_2" placeholder="选择结束时间">
            &nbsp;&nbsp; 

            关键字：
            <?php echo Form::select(array(1=>'会员名',2=>'邮箱',3=>'手机',4=>'会员ID',5=>'旺旺'),$key,'name="key"') ?>
            <input type="text" class="input length_2" id="keyword" name="keyword" style="width:200px;" value="<?php echo $keyword ?>" placeholder="请输入搜索内容...">                       
            
            <button class="btn">搜索</button>
          </div>
      </div>
    <?php $this->endWidget(); ?>


    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录</div>
    <form name="myform" action="" method="post" class="J_ajaxForm">
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center"><?php echo $sort->link('UserID') ?></td>
              <td align="center"><?php echo $sort->link('UserName') ?></td>
              <td align="center"><?php echo $sort->link('Email') ?></td>
              <td align="center"><?php echo $sort->link('Phone') ?></td>
              <td align="center"><?php echo $sort->link('Type') ?></td>
              <td align="center"><?php echo $sort->link('AddTime') ?></td>
              <td align="center"><?php echo $sort->link('Status') ?></td>
              <td align="center"><?php echo $sort->link('LatLoginTime') ?></td>
              <td align="center">操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr data-id="<?php echo $rs->UserID ?>" >
                <?php include $this->getViewFile('data'); ?>          
             </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    </form>
  </div>

</body>
</html>