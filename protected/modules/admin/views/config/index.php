<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
    <?php include $this->getViewFile('/common/Nav') ?>
    <div class="h_a">站点配置</div>
    <div class="table_full">
    <form method='post'   id="myform" class="J_ajaxForm"  action="<?php echo AdminBase::U("index") ?>">
      <table cellpadding=0 cellspacing=0 width="100%" class="table_form" >
      
       <tr>
	     <th width="140">站点名称:</th>
	     <td><input type="text" class="input"  name="sitename" value="<?php echo $info['sitename']?>" size="40"></td>
      </tr>
      <tr>
	     <th width="140">网站访问地址:</th>
	     <td><input type="text" class="input"  name="siteurl" value="<?php echo $info['siteurl']?>" size="40"> </td>
      </tr>
      <tr>
	     <th width="140">附件访问域名:</th>
	     <td><input type="text" class="input"  name="sitefileurl" value="<?php echo $info['sitefileurl']?>" size="40"> 例如:http://file.1zw.com/</td>
      </tr>
      <tr>
	     <th width="140">appkey:</th>
	     <td><input type="text" class="input"  name="appkey" value="<?php echo $info['appkey']?>" size="40"> </td>
      </tr>
      <tr>
	     <th width="140">secretkey:</th>
	     <td><input type="text" class="input"  name="secretkey" value="<?php echo $info['secretkey']?>" size="40"> </td>
      </tr>

      <tr>
	     <th width="140">报名商家等级:</th>
	     <td><input type="text" class="input length_1"  name="lowgrade" value="<?php echo $info['lowgrade']?>" size="40"><span class="gray"> 级以上</span> </td>
      </tr>
      <tr>
       <th width="140">C店销量低于多少不能报名:</th>
       <td><input type="text" class="input length_1"  name="lowsales" value="<?php echo $info['lowsales']?>" size="40"><span class="gray"> 以下</span> </td>
      </tr>

      <tr>
	     <th width="140">审核通过商品:</th>
	     <td><input type="text" class="input length_1"  name="baoming_ok" value="<?php echo $info['baoming_ok']?>" size="40"><span class="gray"> 天后才能重新报名</span></td>
      </tr>
      <tr>
	     <th width="140">审核不通过商品:</th>
	     <td><input type="text" class="input length_1"  name="baoming_no" value="<?php echo $info['baoming_no']?>" size="40"><span class="gray"> 天后才能重新报名</span></td>
      </tr>

      </table>
      <div class="btn_wrap">
      <div class="btn_wrap_pd">             
        <button class="btn btn_submit mr10 J_ajax_submit_btn " type="submit">提交</button>
      </div>
    </div>
    </form>
    </div>
</div>

</body>
</html>