<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
  <?php include $this->getViewFile('/common/Nav') ?>
  <div class="h_a">搜索</div>
  <form method="post" action="<?php echo AdminBase::U("index") ?>">
  <div class="search_type cc mb10">
    <div class="mb10"> <span class="mr20">
    搜索类型：
    <select class="select_2" name="status" style="width:70px;">
        <option value='' <?php echo $_GET['status'] == ''?'selected':''?>>不限</option>
                <option value="0" <?php echo $_GET['status'] == '0'?'selected':''?>>error</option>
                <option value="1" <?php echo $_GET['status'] == '1'?'selected':''?>>success</option>
      </select>
      用户ID：<input type="text" class="input length_2" name="uid" size='10' value="<?php echo $_GET['uid'] ?>" placeholder="用户ID">
      IP：<input type="text" class="input length_2" name="ip" size='20' value="<?php echo $_GET['ip'] ?>" placeholder="IP">
      时间：
      <input type="text" name="start_time" class="input length_2 J_date" value="<?php echo $_GET['start_time'] ?>" placeholder="起始时间">
      -
      <input type="text" name="end_time" class="input length_2 J_date" value="<?php echo $_GET['end_time'] ?>" placeholder="结束时间">
      <button class="btn">搜索</button>
      </span> </div>
  </div>
  </form>
    <div class="table_list">
      <table width="100%" cellspacing="0">
        <thead>
          <tr>
            <td align="center" width="30">ID</td>
            <td align="center" width="50" >用户</td>
            <td align="center" width="60">状态</td>
            <td>说明</td>
            <td>GET</td>
            <td align="center" width="150">时间</td>
            <td align="center" width="120">IP</td>
          </tr>
        </thead>
        <tbody>
          <?php
		  foreach($data as $rs){
		  ?>
            <tr>
              <td align="center"><?php echo $rs->id ?></td>
              <td align="center"><?php echo AdminUser::model()->getByUidFindName($rs->uid) ?></td>
              <td align="center"><?php echo $rs->status ? 'success' :'error' ?></td>
              <td><?php echo $rs->info ?></td>
              <td><?php echo $rs->get ?></td>
              <td align="center"><?php echo date('Y-m-d H:i:s',$rs->time) ?></td>
              <td align="center"><?php echo $rs->ip ?></td>
            </tr>
          <?php
		  }
		  ?>
        </tbody>
      </table>
      <div class="p10">
        <div class="pages"> <?php echo $Page ?> </div>
      </div>
    </div>
</div>
</body>
</html>