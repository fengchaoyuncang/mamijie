<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>
        <?php  $d = $objForm->dropDownListTable($model, 'ParentID', CategoryArticlesAskModel::getListTree(), array('empty' => '≡ 作为一级栏目 ≡'), array('th' => array('width' => 100))); 
               echo str_replace('amp;', '', $d);
        ?>

        <?php echo $objForm->textFieldTable($model, 'Title', array('class' => 'input length_6'), array('th' => array('width' => 100))); ?>
        <?php echo $objForm->textAreaTable($model, 'Brief', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'Sorting'); ?>
        <?php echo $objForm->radioButtonListTable($model, 'Status', CategoryArticlesAskModel::getStatusHtml()); ?>
        <?php echo $objForm->checkBoxTable($model, 'IsHot', array('class' => '')); ?>
        <?php echo $objForm->checkBoxTable($model, 'IsRecommend', array('class' => '')); ?>
        <?php echo $objForm->checkBoxTable($model, 'IsNew', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'TitleSeo', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'KeywordsSeo', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'DescriptionSeo', array('class' => '')); ?>


        <?php echo $objForm->radioButtonListTable($model, 'IsAppIndex', array('0' => '否', '1' => '是'), array('class' => '')); ?>


        <?php echo $objForm->textFieldTable($model, 'AppImg', array('name' => 'AppImg','class' => 'length_6')); ?>
    <?php if(!empty($model->AppImg)){  
        $html = '<div class="div_img"><img  src="'.ConfigModel::model()->getConfig('sitefileurl') . $model->AppImg.'"><input type="hidden" value="'.$model->AppImg.'" name="CategoryArticlesAskModel[AppImg]"><div data-id="CategoryArticlesAskModel_AppImg" data-filename="" data-path="'.$model->AppImg.'" class="uploader-close update"></div></div>';
      }else{
        $html = '';
      }
    ?>
    <?php echo $objForm->fileFieldTable($model, 'AppImg', array('class' => 'length_1', 'style' => 'width:65px'), array('htmlOptions' => array('td' => $html))); ?>






        <?php echo $objForm->textFieldTable($model, 'AppDesc', array('class' => '')); ?>



							
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>



<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl ?>/assets/js/plupload-2.1.2/js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl ?>/assets/js/plupload-2.1.2/js/common.js"></script>
<script type="text/javascript">


  /*定义图片处理的一些全局变量 路径等*/
  var HostUrl = '<?php echo ConfigModel::model()->getConfig('sitefileurl');   ?>/';
  var savePath = 'advertising/';
  var deleteUrl = '<?php echo $this->createUrl("/front/public/deleteImg") ?>';
  var uploadUrl = '<?php echo $this->createUrl("/front/public/upload") ?>';  /*上传图片URL*/
  var flash_swf_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.swf';;
  var silverlight_xap_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.xap';


  saveImg('CategoryArticlesAskModel_AppImg', 'CategoryArticlesAskModel[AppImg]', savePath, false);

</script>



</body>
</html>