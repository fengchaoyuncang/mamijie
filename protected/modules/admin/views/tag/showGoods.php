<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">
<style>
.col-left{float:left}
.col-auto{overflow:hidden;_zoom:1;_float:left;}
.title-1{border-bottom:1px solid #eee; padding-left:5px}
.f14{font-size: 14px}
.lh28{line-height: 28px}
.blue,.blue a{color:#004499}
.gray4,a.gray4{color:#999;}
.lh22{line-height: 22px}
.lh25{line-height: 25px}
</style>
  <div class="wrap J_check_wrap">
  

    
    <!-- 列表 -->
    <div class="mb10"> 共有 ( <b><?php echo $count; ?></b> ) 条记录 </div>
    <form name="myform" action="" method="post" class="J_ajaxForm form">
    <?php $objForm = $this->beginWidget('CActiveForm', array('id' => 'searchForm'), array('class' => 'J_ajaxForm form')); ?>  
      <div class="table_list">
        <table width="100%" cellspacing="0">
          <thead>
            <tr>
              <td width="10%" align="center">商品主键ID</td>
              <td align="center">商品ID</td>
              <td align="center">图片</td>
              <td align="center">标题</td>
              <td align="center">价格</td>
              <td align="center">操作</td>
            </tr>
          </thead>
          <tbody>

             <?php foreach($data as $rs){ ?>
             <tr data-id="<?php echo $rs->GoodsModel->GoodsID ?>" >
                <td  align="center"><?php echo $rs->GoodsModel->GoodsID; ?></td>
                <td  align="center"><?php echo $rs->GoodsModel->ProductID; ?></td> 
                <td  align="center"><img style="width:120px;height:120px;" src="<?php echo $rs->GoodsModel->Image ?>"></td>
                <td  width="40%" align="center"><?php echo $rs->GoodsModel->GoodsName; ?></td> 
                <td  align="center"><?php echo $rs->GoodsModel->Pprice; ?></td>

                <td width="200" align="center">
                  <?php  
                    $op = array();

                    if(RBAC::authenticate('deleteGoods')){
                      $op[] = '<a class="J_ajax_request" data-msg="确定要删除吗？" href="'.AdminBase::U("deleteGoods",array('id'=>$rs->TagGoodsID)).'" >删除</a>';
                    }
                        
                      $op = implode('| ', $op);
                      echo $op;
                  ?>   
                </td>



              </tr>
             <?php } ?>

          </tbody>
        </table>

        <!-- 分页 -->
        <div class="p10">
          <div class="pages"><?php echo $Page;?></div>
        </div>        
      </div>
    <?php $this->endWidget(); ?>
  </div>

</body>
</html>