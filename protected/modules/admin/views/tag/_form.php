<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>





     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>

        <?php echo $objForm->textFieldTable($model, 'Title', array('class' => 'input length_6'), array('th' => array('width' => 100))); ?>
        <?php echo $objForm->textFieldTable($model, 'Sorting'); ?>
        <?php echo $objForm->radioButtonListTable($model, 'Status', TagModel::getStatusHtml()); ?>
        <?php echo $objForm->radioButtonListTable($model, 'IsHot', TagModel::getIshotHtml()); ?>

				<?php //echo $objForm->radioButtonListTable($model, 'IsHot', TagModel::getIsHotHtml()); ?>			
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>

</body>
</html>