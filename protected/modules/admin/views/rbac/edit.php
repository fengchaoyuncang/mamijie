<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
  <?php include $this->getViewFile('/common/Nav') ?>
  <div class="h_a">角色信息</div>
  <form class="J_ajaxForm" action="<?php echo AdminBase::U("edit") ?>" method="post" id="myform">
  <div class="table_full">
      <table width="100%">
        <tr>
          <th width="100">角色名称</th>
          <td><input type="text" name="name" value="<?php echo $data['name']?>" class="input" id="rolename"></input></td>
        </tr>
        <tr>
          <th>角色描述</th>
          <td><textarea name="remark" rows="2" cols="20" id="remark" class="inputtext" style="height:100px;width:500px;"><?php echo $data['remark']?></textarea></td>
        </tr>
        <tr>
          <th>是否启用</th>
          <td><input type="radio" name="status" value="1"  <?php echo $data['status'] == 1 ?'checked':''?>>启用<label>  &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="status" value="0" <?php echo $data['status'] == 0 ?'checked':''?>>禁止</label></td>
        </tr>
      </table>
  </div>
  <div class="">
      <div class="btn_wrap_pd">             
        <input type="hidden" name="id" value="<?php echo $data['id']?>">
        <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">修改</button>
      </div>
    </div>
    </form>
</div>
<script src="<?php echo yii::app()->theme->baseUrl; ?>/assets/statics/js/common.js?v"></script>
</body>
</html>