<?php include $this->getViewFile('/common/Head') ?>
<body>
<div class="wrap J_check_wrap">
  <?php include $this->getViewFile('/common/Nav') ?>
  <div class="table_list">
    <table width="100%" cellspacing="0">
      <thead>
        <tr>
          <td width="20">ID</td>
          <td width="200"  align="left" >角色名称</td>
          <td align="left" >角色描述</td>
          <td width="50"  align="center" >状态</td>
          <td width="200" align="center">管理操作</td>
        </tr>
      </thead>
      <tbody>
        <?php
		foreach($data as $vo){
		?>
        <tr>
          <td><?php echo $vo->id; ?></td>
          <td><?php echo $vo->name; ?></td>
          <td ><?php echo $vo->remark; ?></td>
          <td align="center">
          <?php
		  if($vo->status){
			  echo '<font color="red">√</font>';
		  }else{
			  echo '<font color="red">╳</font>';
		  }
		  ?>
          </td>
          <td  class="text-c"  align="center">
          <?php
		  if($vo->id == 1){
		  ?>
          <font color="#cccccc">权限分配</font> | <a href="<?php echo AdminBase::U("list",array('id'=>$vo->id)) ?>">成员管理</a> | <font color="#cccccc">修改</font> | <font color="#cccccc">删除</font>
          <?php
		  }else{
		  ?>
          <a href="<?php echo AdminBase::U("authorize",array('id'=>$vo->id)) ?>">权限设置</a> | 
          <a href="<?php echo AdminBase::U("list",array('id'=>$vo->id)) ?>">成员管理</a> | 
          <a href="<?php echo AdminBase::U("edit",array('id'=>$vo->id)) ?>">修改</a> | 
          <a class="J_ajax_del" href="<?php echo AdminBase::U("delete",array('id'=>$vo->id)) ?>">删除</a>
          <?php
		  }
		  ?>
          </td>
        </tr>
        <?php
		}
		?>
      </tbody>
    </table>
  </div>
</div>

</body>
</html>