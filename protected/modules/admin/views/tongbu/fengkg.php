<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
<div class="wrap ">
  <?php include $this->getViewFile('/common/Nav') ?>
  <div class="h_a">温馨提示</div>
  <div class="prompt_text">
    <p>最好是有选择性的同步，如果全部同步，当信息量比较大的时候同步时间会比较久！</p>
  </div>
  <div class="h_a">批量同步</div>
  <div class="table_full">
  <table width="100%" cellspacing="0">
      <form action="<?php echo AdminBase::U(ACTION_NAME) ?>" method="post" name="myform">
        <tbody  height="200" class="nHover td-line">
          <tr>
            <th width="100">上架时间范围</th>
            <td><input type="text" name="start_time" class="input length_2 J_date" value="<?php echo date('Y-m-d') ?>" placeholder="选择起始时间"> - <input type="text" class="input length_2 J_date" name="end_time" value="<?php echo date('Y-m-d',time()+86400) ?>" placeholder="选择结束时间"> <em>不选时同步全部商品</em></td>
          </tr>
          <tr>
            <th width="100">指定商品ID导入</th>
            <td><input type="text" name="product_id" class="input length_3" value="" placeholder="商品ID"></td>
          </tr>
          <tr>
            <th></th>
            <td>
            <input type="hidden" name="act" value="start">
            <input type="button" name="dosubmit1" value=" 开始同步 " class="btn" onClick="myform.submit();">
            <input type="hidden" name="one" value="0">
            <input type="button" name="dosubmit1" value=" 一键同步 " class="btn" onClick="$('input[name=\'one\']').val('1');myform.submit();">
            </td>
          </tr>
        </tbody>
      </form>
    </table>
  </div>
</div>
</body>
</html>