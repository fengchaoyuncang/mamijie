<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>
        <?php echo $objForm->textFieldTable($model, 'Title', array('class' => 'input length_6'), array('th' => array('width' => 100))); ?>


        <?php echo $objForm->textFieldTable($model, 'Sorting'); ?>
        <?php echo $objForm->textFieldTable($model, 'Image', array('name' => 'Image')); ?>

        <?php echo $objForm->dropDownListTable($model, 'Status', AdvertisingModel::getStatusHtml()); ?>



    <?php if(!empty($model->Image)){  
        $html = '<div class="div_img"><img  src="'.ConfigModel::model()->getConfig('sitefileurl') . $model->Image.'"><input type="hidden" value="'.$model->Image.'" name="AdvertisingModel[Image]"><div data-id="AdvertisingModel_Image" data-filename="" data-path="'.$model->Image.'" class="uploader-close update"></div></div>';
      }else{
        $html = '';
      }
    ?>

    <?php echo $objForm->fileFieldTable($model, 'Image', array('class' => 'length_1', 'style' => 'width:65px'), array('htmlOptions' => array('td' => $html))); ?>

        <?php echo $objForm->textFieldTable($model, 'Url', array('class' => 'input length_6')); ?>

        <?php echo $objForm->dropDownListTable($model, 'Type', AdvertisingModel::getTypeHtml()); ?>   
        							
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>



<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl ?>/assets/js/plupload-2.1.2/js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl ?>/assets/js/plupload-2.1.2/js/common.js"></script>



<script type="text/javascript">


  /*定义图片处理的一些全局变量 路径等*/
  var HostUrl = '<?php echo ConfigModel::model()->getConfig('sitefileurl');   ?>/';
  var savePath = 'advertising/';
  var deleteUrl = '<?php echo $this->createUrl("/front/public/deleteImg") ?>';
  var uploadUrl = '<?php echo $this->createUrl("/front/public/upload") ?>';  /*上传图片URL*/
  var flash_swf_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.swf';;
  var silverlight_xap_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.xap';


  saveImg('AdvertisingModel_Image', 'AdvertisingModel[Image]', savePath, false);

</script>

</body>
</html>