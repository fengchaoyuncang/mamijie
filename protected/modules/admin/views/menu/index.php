<?php include $this->getViewFile('/common/Head') ?>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
  <?php include $this->getViewFile('/common/Nav') ?>
  <form class="J_ajaxForm" action="<?php echo AdminBase::U("index") ?>" method="post">
    <div class="table_list">
      <table width="100%">
        <colgroup>
        <col width="80">
        <col width="100">
        <col>
        <col width="80">
        <col width="200">
        </colgroup>
        <thead>
          <tr>
            <td>排序</td>
            <td>ID</td>
            <td>菜单英文名称</td>
            <td align="center">状态</td>
            <td align="center">管理操作</td>
          </tr>
        </thead>
        <?php echo $categorys?>
      </table>
    </div>
    <div class="btn_wrap">
      <div class="btn_wrap_pd">             
        <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">排序</button>
      </div>
    </div>
  </form>
</div>

</body>
</html>