<?php include $this->getViewFile('/common/Head'); ?>
<body class="J_scroll_fixed">
  <div class="wrap J_check_wrap">
     <?php include $this->getViewFile('/common/Nav') ?>

     <!-- 表单 -->
      <?php $objForm = $this->beginWidget('CActiveFormTabel', array('id' => 'SubmitForm', 'htmlOptions' => array('class' => 'J_ajaxForm'))); ?>

       <div class="h_a">基本属性</div>

       <div class="table_full">
         <table width="100%" class="table_form contentWrap">
              <tbody>
        <?php  $d = $objForm->dropDownListTable($model, 'CatID', CategoryArticleModel::getListTree(), array(),array('th' => array('width' => 100))); 
               echo str_replace('amp;', '', $d);
        ?>

        <?php echo $objForm->textFieldTable($model, 'Title', array('class' => 'input length_6'), array('th' => array('width' => 100))); ?>


        <?php //echo $objForm->textAreaTable($model, 'Contnet', array('class' => '')); ?>

        <tr id="edit" ><!-- 使用百度编辑器来完成 -->
            <th>内容</th>
            <td>
                <script type="text/plain" id="Contnet" name="Contnet"><?php echo $model->Contnet ?></script>
                <?php
                echo Form::editor('Contnet', 'full', 1, 1, '', 10, 300, 1);
                ?>
            </td>
        </tr>



        <?php echo $objForm->textFieldTable($model, 'Sorting', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'TitleSeo', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'KeywordsSeo', array('class' => '')); ?>
        <?php echo $objForm->textFieldTable($model, 'DescriptionSeo', array('class' => '')); ?>
        <?php echo $objForm->radioButtonListTable($model, 'IsShow', ArticlesModel::getIsShowHtml(), array('class' => '')); ?>

		
              </tbody>
         </table>
       </div>


       <!-- 确定按钮 -->
       <div class="btn_wrap">
          <div class="btn_wrap_pd">
            <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">确认</button>
          </div>
       </div>

      <?php $this->endWidget(); ?>
  </div>

</body>
</html>