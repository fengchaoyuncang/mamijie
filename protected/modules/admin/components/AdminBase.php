<?php

/**
 * 后台控制器基类
 * File Name：AdminBase.php
 * File Encoding：UTF-8
 * File New Time：2014-4-29 8:59:10
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class AdminBase extends YzwController {

    //不使用布局
    public $layout = false;
    //当前登录用户uid
    public static $uid = 0;
    //当前登录用户会员名称
    public static $username = NULL;
    //当前登录会员详细信息
    public static $userInfo = array();
    //模板输出变量
    protected $tVar = array(
        //默认提示跳转时间
        'waitSecond' => 2000,
    );

    //初始化
    public function init() {
        parent::init();

        //权限检查/验证其登录
        $this->competence();
    }

    /**
     * RBAC权限检测
     * @param type $action
     * @return boolean
     */
    public function beforeAction($action) {
        parent::beforeAction($action);          
        //RBAC
        if (!RBAC::AccessDecision(GROUP_NAME)) {
            $this->error('对不起，您没有该项操作的权限！');
        }

       return true;  
    }
    /**
     * 验证登录
     * @return boolean
     */
    private function competence() {
        //检查是否登录
        self::$uid = Yii::app()->passport->isLogin();
        if (empty(self::$uid)) {
            //一些特殊的控制器，不用要求登录
            if (in_array(strtolower($this->getId()), explode(',', NOT_AUTH_CONTROLLER))) {
                return true;
            }
            //跳转到登录页面
            $this->redirect(array('public/login'));
            return false;
        }
        //获取当前登录用户信息
        self::$userInfo = Yii::app()->passport->getLocalAdminUser(self::$uid);
        if (empty(self::$userInfo)) {
            Yii::app()->passport->logoutLocal();
            return false;
        }
        //是否锁定
        if (!self::$userInfo['status']) {
            Yii::app()->passport->logoutLocal();
            $this->error('您的帐号已经被锁定！', self::U('public/login'));
            return false;
        }
        self::$username = self::$userInfo['admin_name'];
        return self::$userInfo;
    }

    /**
     * 操作错误跳转的快捷方法
     * @access protected
     * @param string $message 错误信息
     * @param string $jumpUrl 页面跳转地址
     * @param mixed $ajax 是否为Ajax方式 当数字时指定跳转时间
     * @return void
     */
    protected function error($message = '', $jumpUrl = '', $ajax = false) {
        $this->dispatchJump($message, 0, $jumpUrl, $ajax);
    }

    /**
     * 操作成功跳转的快捷方法
     * @access protected
     * @param string $message 提示信息
     * @param string $jumpUrl 页面跳转地址
     * @param mixed $ajax 是否为Ajax方式 当数字时指定跳转时间
     * @return void
     */
    protected function success($message = '', $jumpUrl = '', $ajax = false) {
        $this->dispatchJump($message, 1, $jumpUrl, $ajax);
    }

    /**
     * 默认跳转操作 支持错误导向和正确跳转
     * 调用模板显示 默认为common目录下面的success页面
     * 提示页面为可配置 支持模板标签
     * @param string $message 提示信息
     * @param Boolean $status 状态
     * @param string $jumpUrl 页面跳转地址
     * @param mixed $ajax 是否为Ajax方式 当数字时指定跳转时间
     * @access private
     * @return void
     */
    protected function dispatchJump($message, $status = 1, $jumpUrl = '', $ajax = false) {
        //记录日志
        AdminOperationLog::model()->record($message, $status);
        // AJAX提交
        if (true === $ajax || Yii::app()->request->isAjaxRequest) {
            $data = is_array($ajax) ? $ajax : array();
            $data['info'] = $message;
            $data['status'] = $status;
            $data['referer'] = $data['url'] = $jumpUrl;
            $this->ajaxReturn($data);
        }
        //如果为数字，表示跳转停顿时间
        if (is_int($ajax)) {
            $this->assign('waitSecond', $ajax);
        }
        //页面跳转地址
        if (!empty($jumpUrl)) {
            $this->assign('jumpUrl', $jumpUrl);
        }
        // 提示标题
        $this->assign('msgTitle', $status ? '操作成功！' : '操作失败！');
        $this->assign('status', $status);   // 状态
        if ($status) { //发送成功信息
            $this->assign('message', $message); // 提示信息
            // 成功操作后默认停留1秒
            if (!isset($this->tVar['waitSecond'])) {
                $this->assign('waitSecond', '1');
            }
            // 默认操作成功自动返回操作前页面
            if (!isset($this->tVar['jumpUrl'])) {
                $this->assign("jumpUrl", Yii::app()->request->getUrlReferrer());
            }
            $this->render('/common/success');
        } else {
            $this->assign('error', $message); // 提示信息
            //发生错误时候默认停留3秒
            if (!isset($this->tVar['waitSecond'])) {
                $this->assign('waitSecond', '3');
            }
            // 默认发生错误的话自动返回上页
            if (!isset($this->tVar['jumpUrl'])) {
                $this->assign("jumpUrl", Yii::app()->request->getUrlReferrer());
                //$this->assign('jumpUrl', "javascript:history.back(-1);");
            }
            $this->render('/common/error');
            // 中止执行  避免出错后继续执行
            exit;
        }
    }

    /**
     * 验证码验证
     * @param type $verifyCode 验证码
     * @return boolean 正确true，不正确false
     */
    protected function validateVerifyCode($verifyCode) {
        if (empty($verifyCode)) {
            return false;
        }
        //验证验证码输入
        $valid = $this->createAction('captcha')->validate($verifyCode, false);
        return $valid ? true : false;
    }


    /**
     * 基础Add简洁方法
     * @param type $model  模型对象或者模型名
     * @param type $url
     */
    protected function baseCreate($strModel, $url = '') {

        if(is_object($strModel)){
            $objModel = $strModel;
            $objModel->setScenario('admin');
            $strModel = get_class($objModel);
        }else{
            $objModel = new $strModel('admin'); 
            $objModel->resetAttributes();           
        }



        $this->assign('model', $objModel);
        $this->assign('object', $strModel);    
        if (IS_POST) {
            $objModel->attributes = CMap::mergeArray($_GET,$_POST); 
            if(isset($_POST[$strModel])){
                $objModel->attributes = $_POST[$strModel];                
            }
            
            if ($objModel->save()) {
                $this->success('添加成功！', $url);
            } else {
                $error = $objModel->getOneError();
                $this->error($error ? $error : '添加失败！');
            }
        } else {
            $this->render();
        }
    }

    /**
     * 基础信息编辑
     * 如果修改成功后需要返回数据这一条的后台数据，$is_content设置为true，
     * 且需要找到data.php这个HTML页面，并做为info参数
     * 且要修改的数据必须是post过来的数据。
     * @param model $model 模型对象或者模型名
     * @param type $url
     */
    protected function baseUpdate($strModel, $url = '', $is_content = false, $viewdata = 'data') {
        if(is_object($strModel)){
            $objModel = $strModel;
            $strModel = get_class($objModel);
        }else{
            $objTemporary = new $strModel();//时间Model
            $id = Yii::app()->request->getParam($objTemporary->getTableSchema()->primaryKey);
            if(!$id){
                $id = Yii::app()->request->getParam('id');
            }
            $objModel = $objTemporary->findByPk($id);
        }
        $this->assign('object', $strModel);        
        if (empty($objModel)) {
            $this->error('该信息不存在！');
        }
        if (IS_POST) {
            $objModel->setScenario('admin');
            $objModel->attributes = CMap::mergeArray($_GET,$_POST); 
            if(isset($_POST[$strModel])){
                $objModel->attributes = $_POST[$strModel];                
            }        

            if ($objModel->save()) {
                if($is_content){
                    $content = $this->renderPartial($viewdata, array('rs' => $objModel,'object' => $strModel), true);
                    $arr = array(
                        'status' => 1,
                        'content' => $content,
                    );
                    $this->ajaxReturn($arr);
                }
                $this->success('修改成功！', $url);
            } else {
                $error = $objModel->getOneError();
                $this->error($error ? $error : '修改失败！');
            }
        } else {
            $this->assign('model', $objModel);
            $this->render();
        }
    }

    /**
     * 基本信息删除
     * @param model $model 模型对象或者模型名
     * @param type $url
     */
    protected function baseDelete($strModel, $url = 'index') {
        $objTemporary = new $strModel();//时间Model
        $id = Yii::app()->request->getParam($objTemporary->getTableSchema()->primaryKey);
        if(!$id){
            $id = Yii::app()->request->getParam('id');
        }
        //查询信息
        $objModel = $objTemporary->findByPk($id);
        if (empty($objModel)) {
            $this->error('该信息不存在！');
        }
        if ($objModel->delete()) {
            $this->success('删除成功！', self::U($url));
        } else {
            $this->error('删除失败！');
        }
    }
    /**
     * 更多功能的基本的列表控制器，扩展的控制器可参数这个
     * @param  [type]  $model  [description]
     * @param  array   $params 如果需要添加额外的一些条件，比如排序等可用这个参数，array("uid=2", 'order' => 'id asc');
     * @param  integer $limit  [description]
     * @return [type]          [description]
     */
    protected function baseIndex($model, $params = array(), $limit = 20, $render = ''){

        if(is_object($model)){
            $objModel = $model;
            $model = get_class($model);
        }else{
            $objModel = new $model('admin');
            $objModel->unsetAttributes();
        }

        $this->assign('object', $model);    
        
        if(isset($_GET[$model]))
        {
            $objModel->attributes = $_GET[$model];
        }
        if(method_exists($objModel, 'createSearchCriteria')){
            $objCriteria = $objModel->createSearchCriteria();
        }else{
            $objCriteria = new CDbCriteria();
        }
        if($params){
            foreach($params as $key => $value){
                if(is_numeric($key)){
                   $objCriteria->addCondition($value); 
                }else{
                    $objCriteria->$key = $value;
                }
            }
        }
        //分页
        $pageId = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        //信息总数
        $count = $objModel->count($objCriteria);
        //分页
        $page = self::page($count, $limit, $pageId,'',$objCriteria);

        //排序
        
        $objSort = new CSort($model);
        $primaryKey = $objModel->getTableSchema()->primaryKey;
        $objSort->defaultOrder = $primaryKey . ' DESC';
        $objSort->applyOrder($objCriteria);

        $data = $objModel->findAll($objCriteria);
        $this->assign($_GET);
        $this->assign("Page", $page->show());
        $this->assign('data', $data);
        $this->assign('sort', $objSort);
        $this->assign('model', $objModel);
        $strModel = get_class($objModel);
        $this->assign('object', $strModel);  
        $this->assign('count', $count);   
        $this->render($render);      
    }






}
