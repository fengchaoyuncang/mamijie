<?php

/**
 * File Name：PassportAdminService.php
 * File Encoding：UTF-8
 * File New Time：2014-4-29 9:22:06
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class PassportAdminService extends CApplicationComponent implements ArrayAccess {

    //存储用户uid的Key
    const adminUidKey = 'admin_user_id';
    //超级管理员角色id
    const administratorRoleId = 1;

    //错误密码
    public $error = NULL;

    /**
     * 获取当前登录用户角色ID
     * @return boolean
     */
    public function getRoleid() {
        if ($this->isLogin()) {
            return AdminBase::$userInfo['potency'];
        }
        return false;
    }

    /**
     * 获取超级管理员角色id
     * @return type
     */
    public function getAdministratorRoleId() {
        return self::administratorRoleId;
    }

    /**
     * 获取当前登录用户UID
     * @return type
     */
    public function getUid() {
        return $this->isLogin();
    }

    /**
     * 获取当前登录用户信息
     * @return type
     */
    public function getUserInfo() {
        return AdminBase::$userInfo;
    }

    /**
     * 检查是否已经登录，有登录返回登录userid
     * @return boolean
     */
    public function isLogin() {
        $userId = Encrypt::authcode(Yii::app()->session->get(self::adminUidKey), 'DECODE');
        if (!empty($userId)) {
            return (int) $userId;
        }
        return false;
    }

    /**
     * 登录后台并进行身份验证
     * @param type $identifier 验证条件，直接uid，用户名
     * @param type $password 明文密码
     * @return boolean
     */
    public function loginAdmin($identifier, $password) {
        if (empty($identifier) || empty($password)) {
            $this->error = '帐号或者密码为空！';
            return false;
        }
        //进行登录验证
        $userInfo = $this->getLocalAdminUser($identifier, $password);
        if (empty($userInfo)) {
            return false;
        }
        return $this->registerLogin($userInfo);
    }

    /**
     * 检查当前用户是否超级管理员
     * @param type $authId 用户ID
     * @return boolean
     */
    public function isAdministrator($authId = null) {
        $userId = $authId ? $authId : $this->isLogin();
        if (empty($userId)) {
            return false;
        }
        $userInfo = $this->getUserInfo();
        if (empty($userInfo)) {
            $userInfo = $this->getLocalAdminUser((int) $userId);
        }
        if ($userInfo['potency'] == self::administratorRoleId) {
            return true;
        }
        return false;
    }

    /**
     * 注册用户登录状态
     * @param array $user 用户基本信息
     * @param type $is_remeber_me 有效期，秒
     * @return boolean
     */
    public function registerLogin(array $user, $is_remeber_me = 604800) {
        if (empty($user) || !is_array($user)) {
            return false;
        }
        //安全检测
/*        if ($user['potency'] != self::administratorRoleId) {
            //检查是否在上班时间段
            $time = time();
            if ($time > strtotime(date('Y-m-d 08:20:00')) && $time < strtotime(date('Y-m-d 18:40:00'))) {
                
            } else {
                $this->error = '普通管理员，只能在上班时间段登录！';
                return false;
            }
        }*/
        //设置uid到session
        Yii::app()->session->add(self::adminUidKey, Encrypt::authcode($user['admin_id'], ''));
        //更新登录时间啥的
        AdminUser::model()->loginSaveInfo($user['admin_id']);
        //缓存访问权限
        //RBAC::saveAccessList();
        return true;
    }

    /**
     * 注销登录状态
     * @return boolean
     */
    public function logoutLocal() {
        Yii::app()->session->clear();
        return true;
    }

    /**
     * 根据提示符(username)和未加密的密码(密码为空时不参与验证)获取本地用户信息
     * @param type $identifier 为数字时，表示uid，其他为用户名
     * @param type $password 明文密码
     * @return boolean 成功返回用户信息array()，否则返回布尔值false
     */
    public function getLocalAdminUser($identifier, $password = null) {
        if (empty($identifier)) {
            $this->error = '帐号不能为空！';
            return false;
        }
        //获取用户信息
        $userInfo = AdminUser::model()->getUserInfo($identifier);
        if (empty($userInfo)) {
            $this->error = '该用户不存在，或者帐号密码错误，请重新登录！';
            return false;
        }
        //如果密码不为空，且判断密码是否错误
        if (!empty($password) && AdminUser::model()->hashPassword($password) != $userInfo['password']) {
            $this->error = '该用户不存在，或者帐号密码错误，请重新登录！';
            return false;
        }
        return $userInfo;
    }

    public function offsetExists($index) {
        return isset(AdminBase::$userInfo[$index]);
    }

    public function offsetGet($index) {
        return AdminBase::$userInfo[$index];
    }

    public function offsetSet($index, $newvalue) {
        //AdminBase::$userInfo[$index] = $newvalue;
    }

    public function offsetUnset($index) {
        //unset(AdminBase::$userInfo[$index]);
    }

}
