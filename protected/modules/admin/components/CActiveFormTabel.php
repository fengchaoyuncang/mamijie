<?php

class CActiveFormTabel extends CActiveForm{
	public function textFieldTable($model,$attribute,$htmlOptions=array(), $attr_data = array())
	{
		$htmlOptions['placeholder'] = $model->getAttributeLabel($attribute);	
		$input = parent::textField($model,$attribute,$htmlOptions);
		return $this->getTabel($input, $model, $attribute, $attr_data);
	}
	public function radioButtonListTable($model,$attribute,$data,$htmlOptions=array(), $attr_data = array())
	{
		$input = parent::radioButtonList($model,$attribute,$data,$htmlOptions);
		return $this->getTabel($input, $model, $attribute, $attr_data);
	}
	public function checkBoxListTable($model,$attribute,$data,$htmlOptions=array(), $attr_data = array())
	{
		$input = parent::checkBoxList($model,$attribute,$data,$htmlOptions);
		return $this->getTabel($input, $model, $attribute, $attr_data);
	}
	public function checkBoxTable($model,$attribute,$htmlOptions=array(), $attr_data = array())
	{
		$input = parent::checkBox($model,$attribute,$htmlOptions);
		return $this->getTabel($input, $model, $attribute, $attr_data);
	}
	public function dropDownListTable($model,$attribute,$data,$htmlOptions=array(), $attr_data = array())
	{
		$input = parent::dropDownList($model,$attribute,$data,$htmlOptions);
		return $this->getTabel($input, $model, $attribute, $attr_data);
	}
	public function textAreaTable($model,$attribute,$htmlOptions=array(), $attr_data = array())
	{
		$input = parent::textArea($model,$attribute,$htmlOptions);
		return $this->getTabel($input, $model, $attribute, $attr_data);
	}
	public function passwordFieldTable($model,$attribute,$htmlOptions=array(), $attr_data = array())
	{
		$input = parent::passwordField($model,$attribute,$htmlOptions);
		return $this->getTabel($input, $model, $attribute, $attr_data);
	}
	public function fileFieldTable($model,$attribute,$htmlOptions=array(), $attr_data = array())
	{
		$input = parent::fileField($model,$attribute,$htmlOptions);
		return $this->getTabel($input, $model, $attribute, $attr_data);
	}

	/**
	 * 把数据组合成tabel的形式
	 * @param  [type] $content   [description]
	 * @param  [type] $model     [description]
	 * @param  [type] $attribute [description]
	 * @param  array  $attr_data 格式：array('tr' => array('style' => 'abcd', 'width' => 'cc'), 'th' => array());为其加各属性
	 * 如果有带htmlOptions 属性则  键值 th的值就是th标签后面跟着的内容。
	 * @return [type]            [description]
	 */
	public function getTabel($content, $model, $attribute, $attr_data = array()){
		if(isset($attr_data['htmlOptions'])){
			$htmlOptions = $attr_data['htmlOptions'];
			unset($attr_data['htmlOptions']);
		}else{
			$htmlOptions = array();
		}
		$data_tr = $data_th = $data_td = '';
		foreach($attr_data as $attr => $datas){
			$html = '';
			foreach($datas as $key => $value){
				$html .= ' ' . $key . '=' . '"'.$value.'"';
			}
			switch ($attr) {
				case 'tr':
					$data_tr = $html;
					break;
				case 'th':
					$data_th = $html;
					break;
				case 'td':
					$data_td = $html;
					break;				
				default:
					# code...
					break;
			}
		}
		$html = "<tr {$data_tr}>";
		$html .= "<th {$data_th}>";
		$html .= $model->getAttributeLabel($attribute);
		isset($htmlOptions['th']) ? $html .= $htmlOptions['th'] : '';
		$html .= "</th>";
		$html .= "<td {$data_td}>";
		$html .= $content;
		isset($htmlOptions['td']) ? $html .= $htmlOptions['td'] : '';
		$html .= "</td>";
		isset($htmlOptions['tr']) ? $html .= $htmlOptions['tr'] : '';
		$html .= "</tr>";
		return $html;
	}	
}