<?php

class SsoServer extends YzwController{
	public $key = ''; //获取key
	public $data = '';//解密后数据
	public function init(){
		parent::init();
		if(empty($_POST['data'])){
			$this->error('参数错误');
		}

		$this->data = Encrypt::authcode($_POST['data'], 'DECODE', $this->key);
		$this->data = json_decode($this->data, true);
		if(empty($this->data)){
			$this->error('找不到数据');
		}
	}

    protected function error($message = '', $jumpUrl = '', $ajax = true) {
        $this->dispatchJump($message, 0, $jumpUrl, $ajax);
    }
    protected function success($message = '', $jumpUrl = '', $ajax = true) {
        $this->dispatchJump($message, 0, $jumpUrl, $ajax);
    }


    /**
     * 成功返回数据
     * @param type $return
     */
    protected function returnData($return = '') {
        $data = array();
        $data['status'] = true;
        if (!empty($return)) {
            $data['data'] = $return;
        }
        $this->ajaxReturn($data);
    }
}