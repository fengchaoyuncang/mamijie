<?php
//同步登录，注册，修改资料等。。。。。
class IndexController extends SseServer {
	//登录
	public function actionLogin(){
        if(empty($this->data['UserName'])){
            $this->error('用户名不能为空');
        }
        if(empty($this->data['Password'])){
            $this->error('密码不能为空');
        }
		$username = $this->data['UserName'];
		$password = $this->data['Password'];
		//登录成功
		if(yii::app()->user->login($username, $password)){
			$this->success('登录成功');
		}else{
			$this->error(yii::app()->user->getError());
		}
	}

    //获取用户信息
    public function actionGetuser() {
        $model = MembersModel::model();
        if (empty($this->data['UserName']) || empty($this->data['UserID'])) {
            $this->error('用户名或用户ID不能为空！');
        }
        if (!empty($this->data['userID']) {
            $data = $model->findByPk($this->data['userID']);
        } else {
            $data = $model->find($model->where(array('UserName' => $this->data['username'])));
        }
        if (empty($data)) {
            $this->error('该用户不存在！');
        }
        $data->Password = '';
        $this->returnData($data->attributes);
    }
    //注册
    public function actionRegister(){
        $model = MemberModel::model();
        $model->setScenario('front');          
        $model->setIsNewRecord(true);
        $model->attributes = $this->data;
        if($model->save()){
        	$this->returnData($model->UserID);
        }else{
        	$this->error($model->getOneError());
        }       	
    }
    //编辑用户信息
    public function actionEdit(){
        $model = MemberModel::model()->findByPk($this->data['UserID']);
        if($model){
        	$model->attributes = $this->data;
        	if($model->save()){
        		$model->success('修改成功');
        	}else{
        		$this->error($model->getOneError());
        	}
        }else{
        	$this->error('找不到记录');
        }
    }
}