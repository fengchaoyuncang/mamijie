<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/huodong/chun.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
	$(function(){
		$('.chun_miaosha ul li,.floor ul li').hover(function(){
			$(this).addClass('active');
		},function(){
			$(this).removeClass('active');
		})		
	})
</script>
<script type="text/javascript">
$(document).ready(function(){
	jQuery.scrollto = function(scrolldom,scrolltime) {
		$(scrolldom).click( function(){ 
			var scrolltodom = $(this).attr("date-scroll");
			$(this).addClass("thisscroll").siblings().removeClass("thisscroll");
			$('html,body').animate({
				scrollTop:$(scrolltodom).offset().top},scrolltime
			);
			return false;
		}); 
		
	};
    $.scrollto("#chun_nav a",600);
});
</script>
<div class="chun_banner" id="chun_banner"></div>
<div class="chun_box">
	<div class="chun_miaosha" id="chun1">
		<div class="chun_miaoshaTitle">
			<div class="chun_sloleft">焕新节，妈咪街送辣妈萌宝最好的礼物</div>
			<div class="chun_sloRight">NEW SEASON</div>
		</div>
		<?php $goods = $goods1['data']; include $this->getViewFile('goods_miaosha');  ?>

	</div>

	<div class="floor" id="chun2">
		<div class="floor_title">
			<div class="floor_theme">明星宝贝</div>
			<div class="floor_t1">“春日秀”看过来</div>
		</div>
		<div class="floor_box">
			<?php $goods = $goods2['data']; include $this->getViewFile('goods');  ?>
		</div>
	</div>

	<div class="floor" id="chun3">
		<div class="floor_title">
			<div class="floor_theme">摩登女郎</div>
			<div class="floor_t1">时髦先行站，时尚的追随者</div>
		</div>
		<div class="floor_box">
			<?php $goods = $goods3['data']; include $this->getViewFile('goods');  ?>
		</div>
	</div>

	<div class="floor" id="chun4">
		<div class="floor_title">
			<div class="floor_theme">呵护宝宝</div>
			<div class="floor_t1">更贴心，成长更出色</div>
		</div>
		<div class="floor_box">
			<?php $goods = $goods4['data']; include $this->getViewFile('goods');  ?>
		</div>
	</div>
	<div class="floor" id="chun5">
		<div class="floor_title">
			<div class="floor_theme">美食每克</div>
			<div class="floor_t1">嘴懂女人心，做一个吃货挺好</div>
		</div>
		<div class="floor_box">
			<?php $goods = $goods5['data']; include $this->getViewFile('goods');  ?>
		</div>
	</div>
	<div class="floor" id="chun6">
		<div class="floor_title">
			<div class="floor_theme">窝居格调</div>
			<div class="floor_t1">创意源于生活</div>
		</div>
		<div class="floor_box">
			<?php $goods = $goods6['data']; include $this->getViewFile('goods');  ?>
		</div>
	</div>
</div>
<div class="chun_nav" id="chun_nav">
	<ul>
		<li><a id="flo1" href="#" date-scroll="#chun1">特价秒杀</a></li>
		<li><a id="flo2" href="#" date-scroll="#chun2">明星宝贝</a></li>
		<li><a id="flo3" href="#" date-scroll="#chun3">摩登女郎</a></li>
		<li><a id="flo4" href="#" date-scroll="#chun4">呵护宝宝</a></li>
		<li><a id="flo5" href="#" date-scroll="#chun5">美食每克</a></li>
		<li><a id="flo6" href="#" date-scroll="#chun6">窝居格调</a></li>
		<li class="chun_top"><a href="#"  date-scroll="#chun_banner">TOP</a></li>
	</ul>
</div>
