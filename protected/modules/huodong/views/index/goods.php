<ul>
  <?php $num = 0; foreach($goods as $arrGoodsData){ $num++; ?>

  <?php  

  	$class = '';
  	if(($num%4) == 0){
  		$class .= ' last';
  	}
  	if($arrGoodsData['IsSoldout']){
  		$class .= ' off';
  	}

  	if($arrGoodsData['StartTime'] > TIME_TIME){
  		$url = "javascript:void(0)";
  		$target = '';
  	}else{
  		$url = $this->createUrl('/front/goods/detail', array('gid' => $arrGoodsData['GoodsID']));
  		$target = 'target="_blank"';
  	}

  ?>

  <li class="<?php  echo $class; ?>">	

		<a <?php  echo $target; ?> class="floor_img" href="<?php echo $url ?>" title="<?php  echo $arrGoodsData['GoodsName']  ?>"> <img width="250" height="250" src="<?php  echo $arrGoodsData['Image']  ?>" alt="<?php  echo $arrGoodsData['GoodsName']  ?>"> </a>
					<div class="floor_name">
						<a <?php  echo $target; ?> href="<?php echo $url ?>"><?php  echo $arrGoodsData['GoodsName']  ?></a>
					</div>

					<div class="floor_price1">原价: <strong><?php  echo $arrGoodsData['Pprice']  ?></strong>元</div>
					<div class="floor_price2">焕新价: <strong><?php  echo $arrGoodsData['PromoPrice']  ?></strong>元</div>
					<div class="floor_btn"><a <?php  echo $target; ?> href="<?php echo $url ?>"></a></div>
					


					<?php if($arrGoodsData['StartTime'] > TIME_TIME){  ?>
						<div class="chun_zt">
							<div class="chun_shadow9"></div>
							<div class="chun_img9"></div>
						</div>

					<?php  } ?>

  	</li>

  <?php }  ?>  
</ul>
             	   

