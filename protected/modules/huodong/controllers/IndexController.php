<?php

//活动
class IndexController extends FrontBaseC {

    public function actionIndex(){
        $this->title = '3月绽放换新季,换新节特价秒杀活动-妈咪街';
        $this->keywords = '';
        $this->description = '妈咪街3月绽放换新季,换新节特价秒杀活动专场,限时限量';
        $goods1 = GoodsModel::model()->getVariedGoods(array('SpecialID' => 1, 'IsPreferential' => 1,'page' => '','pagesize' => 40));
        $goods2 = GoodsModel::model()->getVariedGoods(array('SpecialID' => 1, 'cid' => 53,'page' => '','pagesize' => 40));
        $goods3 = GoodsModel::model()->getVariedGoods(array('SpecialID' => 1, 'cid' => 64,'page' => '','pagesize' => 40));
        $goods4 = GoodsModel::model()->getVariedGoods(array('SpecialID' => 1, 'cid' => 52,'page' => '','pagesize' => 40));
        $goods5 = GoodsModel::model()->getVariedGoods(array('SpecialID' => 1, 'cid' => 69,'page' => '','pagesize' => 40));
        $goods6 = GoodsModel::model()->getVariedGoods(array('SpecialID' => 1, 'cid' => 70,'page' => '','pagesize' => 40));
        $this->assign(array(
        	'goods1' => $goods1,
        	'goods2' => $goods2,
        	'goods3' => $goods3,
        	'goods4' => $goods4,
        	'goods5' => $goods5,
        	'goods6' => $goods6,
        ));
        $this->render();     
    }
}
