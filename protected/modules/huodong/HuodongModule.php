<?php

class HuodongModule extends CWebModule {

    public $defaultController = 'index';

    /**
     * import classes
     */
    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'huodong.models.*',
            'huodong.components.*',
        ));
        //当前模块
        defined('GROUP_NAME') or define('GROUP_NAME', $this->getId());
        //手机访问跳转
/*        if (!UrlClass::mobileHost()) {
            //是否强制pc访问
            $name = 'phone_pc';
            $isPc = Yii::app()->request->getParam('client');
            if (!empty($isPc) && $isPc == 'pc') {
                $cookie = new CHttpCookie($name, 1);
                $cookie->expire = time() + 604800;
                Yii::app()->request->cookies[$name] = $cookie;
            } else {
                $cookie = Yii::app()->request->getCookies();
                $isPc = isset($cookie[$name]) ? true : false;
            }
            $mobile = Mobile::getInstance();
            if ($mobile->is_mobile_request() && empty($isPc)) {
                header("Location: http://m.1zw.com/");
                exit;
            };
        }*/
    }

    /**
     * @param CController $controller
     * @param CAction $action
     * @return bool
     */
    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else {
            return false;
        }
    }

}
