<?php

class PublicController extends FrontBaseC{

    public function actionCeshi(){
        $this->layout = false;
        $this->render('ceshi');
    }

    //ajax获取商品
    public function actionAjaxGood(){

        $ajaxpage = Yii::app()->request->getParam('ajaxpage'); //ajax的时候的分页，一页有20        
        $page = Yii::app()->request->getParam('page');//大的分页 一页有两百
        $gooddata = Yii::app()->request->getParam('gooddata');//获取哪个地址


        empty($ajaxpage) ? $ajaxpage = 1 : '';
        empty($page) ? $page = 1 : '';
        empty($gooddata) ? $gooddata = '//layouts/index/good' : '';
        //然后算出真正的我要找第几页的产品  200/20
        $page = ($page - 1)*($this->numpage/$this->ajaxonepage) + $ajaxpage;

        $goods = GoodsModel::model()->getVariedGoods(array('page' => $page));
        if(empty($goods['data'])){
            if($ajaxpage == 1){
                $return['status'] = 1;
                $return['content'] = $this->renderPartial('//layouts/index/nogood','', true);
                $this->ajaxReturn($return);           
            }else{
                $goods['status'] = 0;
                $goods['content'] = false;
            }
        }else{           
            $goods['status'] = 1;            
            $goods['content'] = $this->renderPartial($gooddata, array('goods' => $goods), true);
            unset($goods['data']); 
        }
        $this->ajaxReturn($goods);
    }

    //ajax获取总数
    public function actionAjaxGoodCount(){
        $route = Yii::app()->request->getParam('route');


        !empty($_POST['sort']) ? $_GET['#'] = $_POST['sort'] : '';
        $_GET = CMap::mergeArray($_GET,$_POST);
        unset($_GET['route']);unset($_GET['sort']);unset($_GET['ajaxpage']);unset($_GET['gooddata']);//去除一些会影响分页的get数据



        $pagesize = Yii::app()->request->getParam('pagesize');
        $pagesize = empty($pagesize) ? $this->numpage : $pagesize;
        $goods = GoodsModel::model()->getVariedGoods(array('gettype' => 1,'pagesize' => $pagesize));
        if(empty($goods['count'])){
            $goods['status'] = 0;
            $goods['content'] = false;
        }else{           
            $goods['status'] = 1;
            $goods['content'] = $this->renderPartial('//layouts/index/page', array('page' => $goods['page'],'route' => $route), true);
            unset($goods['page']); 
        }
        $this->ajaxReturn($goods);
    }

    public function actionAjaxIsLogin(){
        $this->ajaxReturn(array('status' => 1, 'data' => yii::app()->user->isLogin()));
    }

	public function actionTaoData(){
        $url = Yii::app()->request->getParam('url');
        if (empty($url)) {
            $this->ajaxReturn(array('status' => 0));
        }

        $data = TaobaoApi::getDataUrl($url);
        
        if (empty($data)) {
            $data = array('status' => 0);
        } else {
            $data['status'] = 1;
        }
        $this->ajaxReturn($data);		
	}

    /**
     * 上传图片
     * @return [type] [description]
     */
    public function actionUpload(){
        //上传后返回其路径。
        UploadFile::getInstance()->upload($_POST['path'], $_POST['name']);
        $arrFile = UploadFile::getInstance()->getArrSuccess();
        if(!empty($arrFile)){
            echo $arrFile['file']['filename'];  
        }   
    }
    /**
     * 删除图片
     * @return [type] [description]
     */
    public function actionDeleteImg(){
        if(empty($_POST['filename'])){
            $this->error('参数有误');
        }
        UploadFile::deleteImg($_POST['filename']);      
    }
    /**
     * 错误页面
     * @return [type] [description]
     */
    public function actionError(){
        $redis = RedisCluster::getInstance()->getRedis();
        $arr = array(
            'Get' => json_encode($_GET),
            'Post' => json_encode($_POST),
            'Url' => Yii::app()->request->hostInfo . Yii::app()->request->getUrl(),
            'UserID' => yii::app()->user->id,
            'Browser' => GuestInfo::getBrowser(),
            'Ip' => GuestDataModel::model()->getForwardedForIp(),
        );
        RedisCluster::getInstance()->push('public_error', $arr);
        $this->render();
    }

    //意见
    public function actionOpinion(){
        $objModel = new OpinionModel;

        if(isset($_POST['OpinionModel']))
        {
            //赋值
            $objModel->attributes = $_POST['OpinionModel'];
        
            if($objModel->save())
            {           
                $this->success('提交成功');     
            }else
            {
                $error = $objModel->getOneError();
                $this->error($error ? $error : '提交失败');         
            }
        }

        $this->render('opinion', array('model' => $objModel));          
    }

    //网站地图
    //产品链接，其它链接，等
    //其它链接
    public function actionSitemap(){
        $page = Yii::app()->request->getParam('page');
        empty($page) ? $page = 1 : '';
        $this->assign('page',$page);
        if($page == 1){

            $category = CategoryModel::getList();
            $this->assign('category', $category);


            $goodsData = GoodsModel::model()->getVariedGoods(array('page' => '', 'pagesize' => '1000','gettype' => 2), false, true, 3600);
            $this->assign('goodsData', $goodsData);

            $goodsTomorrowData = GoodsModel::model()->getVariedGoods(array('settime' => 1,'page' => '','pagesize' => 300));
            $this->assign('goodsTomorrowData', $goodsTomorrowData);


            //玩转1折网
            $data = array(
                array(
                    'url' => $this->createUrl('/front/index/index'),
                    'title' => '妈咪街',
                ),
                array(
                    'url' => $this->createUrl('/user/merchantsSettled/introduction'),
                    'title' => '入驻妈咪街',
                ),
                array(
                    'url' => $this->createUrl('/user/merchantsSettled/create'),
                    'title' => '申请入驻',
                ),
                array(
                    'url' => $this->createUrl('/user/baoming/create'),
                    'title' => '报名商品',
                ),
            );
            $this->assign('wangzhuang', $data);

            //系统文章页面
            $data = ArticlesModel::getCatData(7);
            $this->assign('articlesSystem', $data);            
        }

        //把文章的链接和问答的链接弄出来.
        $asklist = ArticlesAskModel::model()->getVaried(array('pagesize' => 400,'gettype' => 2));
        $articlelist = ArticlesNewModel::model()->getVaried(array('pagesize' => 400,'gettype' => 2));
        if($articlelist['count'] > $asklist['count']){
            $objpage = $articlelist['page'];
        }else{
            $objpage = $asklist['page'];
        }
        $this->assign('objpage',$objpage);
        $this->assign('asklist',$asklist);
        $this->assign('articlelist',$articlelist);




        $this->render();
    }





    public function actionSitemapXml(){
        $this->layout = false;
        $html = '<?xml version="1.0" encoding="UTF-8"?>';
        $html .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';


        $html .= "\r\n"; 
   

        //玩转1折网
        $datas = array(
            $this->createUrl('/front/index/index'),
        );
        foreach ($datas as $key => $value) {
            $html .= "<url>";
            $html .= "\r\n";
            $html .= "<loc>";
            $html .= "\r\n";
            $html .= "http://www.mamijie.com/" . $value;  
            $html .= "\r\n";  
            $html .= "</loc>";
            $html .= "\r\n";
            $html .= "<priority>";
            $html .= "\r\n";
            $html .= "1.0"; 
            $html .= "\r\n";           
            $html .= "</priority>";
            $html .= "\r\n";
            $html .= "</url>";
            $html .= "\r\n";
        }




        $category = CategoryModel::getList();
        foreach ($category as $key => $value) {
            $html .= "<url>";
            $html .= "\r\n";
            $html .= "<loc>";
            $html .= "\r\n";
            $html .= "http://www.mamijie.com/" . $this->createUrl('/front/index/good', array('cid' => $value['CatID']));  
            $html .= "\r\n";  
            $html .= "</loc>";
            $html .= "\r\n";
            $html .= "<priority>";
            $html .= "\r\n";
            $html .= "0.6"; 
            $html .= "\r\n";           
            $html .= "</priority>";
            $html .= "\r\n";
            $html .= "</url>";
            $html .= "\r\n";
        }

        $goodsData = GoodsModel::model()->getVariedGoods(array('page' => '', 'pagesize' => '1000','gettype' => 2), false, true, 3600);
        foreach ($goodsData['data'] as $key => $value) {
            $html .= "<url>";
            $html .= "\r\n";
            $html .= "<loc>";
            $html .= "\r\n";
            $html .= "http://www.mamijie.com/" . $this->createUrl('/front/goods/detail', array('gid' => $value['GoodsID']));  
            $html .= "\r\n";  
            $html .= "</loc>";
            $html .= "\r\n";
            $html .= "<priority>";
            $html .= "\r\n";
            $html .= "0.6"; 
            $html .= "\r\n";           
            $html .= "</priority>";
            $html .= "\r\n";
            $html .= "</url>";
            $html .= "\r\n";
        }

        $goodsTomorrowData = GoodsModel::model()->getVariedGoods(array('settime' => 1,'page' => '','pagesize' => 300));
        foreach ($goodsTomorrowData['data'] as $key => $value) {
            $html .= "<url>";
            $html .= "\r\n";
            $html .= "<loc>";
            $html .= "\r\n";
            $html .= "http://www.mamijie.com/" . $this->createUrl('/front/goods/detail', array('gid' => $value['GoodsID']));
            $html .= "\r\n";    
            $html .= "</loc>";
            $html .= "\r\n";
            $html .= "<priority>";
            $html .= "\r\n";
            $html .= "0.6";  
            $html .= "\r\n";          
            $html .= "</priority>";
            $html .= "\r\n";
            $html .= "</url>";
            $html .= "\r\n";
        }


        //系统文章页面
        $data = ArticlesModel::getCatData(7);
        foreach ($data as $keys => $values) {
            foreach ($values['data']['data'] as $key => $value) {
                $html .= "<url>";
            $html .= "\r\n";
                $html .= "<loc>";
            $html .= "\r\n";
                $html .= "http://www.mamijie.com/" . $this->createUrl('/baike/articles/index', array('id' => $value['ArticlesID']));    
            $html .= "\r\n";
                $html .= "</loc>";
            $html .= "\r\n";
                $html .= "<priority>";
            $html .= "\r\n";
                $html .= "0.6";     
            $html .= "\r\n";       
                $html .= "</priority>";
            $html .= "\r\n";
                $html .= "</url>";
            $html .= "\r\n";
            }
        }


        $asklist = ArticlesAskModel::model()->getVaried(array('pagesize' => 400,'gettype' => 2));
        foreach ($asklist['data'] as $keys => $value) {
                $html .= "<url>";
            $html .= "\r\n";
                $html .= "<loc>";
            $html .= "\r\n";
                $html .= "http://www.mamijie.com/" . $this->createUrl('/baike/index/askDetail', array('id' => $value['ArticlesID']));    
            $html .= "\r\n";
                $html .= "</loc>";
            $html .= "\r\n";
                $html .= "<priority>";
            $html .= "\r\n";
                $html .= "0.6";     
            $html .= "\r\n";       
                $html .= "</priority>";
            $html .= "\r\n";
                $html .= "</url>";
            $html .= "\r\n";
        }


        $articlelist = ArticlesNewModel::model()->getVaried(array('pagesize' => 400,'gettype' => 2));
        foreach ($asklist['data'] as $keys => $value) {
                $html .= "<url>";
            $html .= "\r\n";
                $html .= "<loc>";
            $html .= "\r\n";
                $html .= "http://www.mamijie.com/" . $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID']));    
            $html .= "\r\n";
                $html .= "</loc>";
            $html .= "\r\n";
                $html .= "<priority>";
            $html .= "\r\n";
                $html .= "0.6";     
            $html .= "\r\n";       
                $html .= "</priority>";
            $html .= "\r\n";
                $html .= "</url>";
            $html .= "\r\n";
        }


        $html .= '</urlset>';

        file_put_contents('sitemap.xml', $html);
        exit;
    }


    //获取用户这个月的签到记录
    public function actionGetsign(){
        if(!$uid = yii::app()->user->isLogin()){
            $this->ajaxReturn(array('status' => false, 'content' => '未登录'));
        }
        if(is_array($uid)){
            $this->ajaxReturn(array('status' => false, 'content' => '未绑定登录'));             
        }
        $data = MemberSignModel::model()->getSignData();

        $this->ajaxReturn(array('status' => 1, 'content' => $data));
    }

    //用户签到动作
    public function actionSign(){
        if($score = MemberSignModel::model()->sign()){
            $this->ajaxReturn(array('status' => 1, 'content' => '签到成功', 'score' => $score));
        }else{
            $this->ajaxReturn(array('status' => 0, 'content' => MemberSignModel::model()->getOneError()));
        }
    }

} 