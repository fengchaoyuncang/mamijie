<?php

//网站前台
class IndexController extends FrontBaseC {

    public function init(){
        parent::init();
        $this->assign($_GET);
    }

    public function actionIndex(){
         if(Yii::app()->request->getParam('page') >= 2){
            $_GET['#'] = 'sort';
         }

        //获取两层的分类
         $category = CategoryModel::getListMenu(3);
         $this->assign('category',$category);
         //获取广告类型为1与2的广告图
         $ad[1] = AdvertisingModel::getListType(7);
         $ad[2] = AdvertisingModel::getListType(8);
         $this->assign('ad',$ad);
         //获取热门的商品8款
         $goods_hot = GoodsModel::model()->getVariedGoods(array('pagesize' => 8,'page' => ''));
         $this->assign('goods',$goods_hot);
         $this->render();     
    }



    public function actionGood(){
        $cid = Yii::app()->request->getParam('cid');
        //找出这个分类的面包所有的父类来。
        $mianbao = CategoryModel::getMiaobaoAddTong($cid);
        if(!empty($mianbao['data'])){
            $this->title = $mianbao['data']['TitleSeo'];
            $this->keywords = $mianbao['data']['KeywordsSeo'];
            $this->description = $mianbao['data']['DescriptionSeo'];
            unset($mianbao['data']);
        }

        $this->assign('mianbao',$mianbao);
        $this->render();
    }
 

    /**
     * 明日预告
     * @return [type] [description]
     */
    public function actionTomorrow() {
        $_GET['settime'] = 1;
        $this->render('good');
    }
    /**
     * 19.9专场
     * @return [type] [description]
     */
    public function actionData199() {
        $_GET['eid'] = 5;
        $this->render('good');
    }
    /**
     * 19.9专场
     * @return [type] [description]
     */
    public function actionNew() {
        $_GET['settime'] = 2;//每日新品
        $this->render('good');
    }
}
