<?php

class GoodsController extends FrontBaseC {
    //商品内页
    public function actionDetail() {
        $good_id = (int) $_GET['gid'];
        $goods = GoodsModel::getOne($good_id);
        if(!$goods){
            $goods = GoodsBakModel::getOne($good_id);
        }
 

        //判断是否蜘蛛
        if($goods){
            if (!UrlClass::getrobot()) {
                if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] && $goods['ItemType'] < 10) {
                    $domain = GuestDataModel::getDomain($_SERVER['HTTP_REFERER']);
                    //以下来源域名不跳
                    if (!in_array($domain['RootDomain'], array('baidu.com', 'so.com', 'soso.com', 'sogou.com', 'google.com', 'bing.com', 'yahoo.com', '360.cn', '2345.com', 'hao123.com', 'duba.com', 'uc123.com', 'qq.com'))) {
                        if (empty($goods['TaobaoUrl'])) {
                            $this->redirect($goods['DetailUrl']);
                        } else {
                            $this->redirect($goods['TaobaoUrl']);
                        }
                    }
                }
            }
        }



        $this->layout = 'theme.views.layouts.common.layout.index';   
        $this->title = "【{$goods['GoodsName']}】{$goods['GoodsName']}价格和图片-妈咪街母婴用品";
        $this->keywords = "{$goods['GoodsName']}，{$goods['GoodsName']}价格，{$goods['GoodsName']}图片";
        $this->description = "妈咪街特卖{$goods['GoodsName']}，欢迎来妈咪街母婴用品网站了解购买{$goods['GoodsName']}";


        

        if ($goods) {
            $datas = GoodsModel::model()->getVariedGoods(array('cid' => $goods['CatID'], 'page' => '', 'pagesize' => 20));  
            $this->assign('datas', $datas); 


            $goods['Spec'] = explode("\n", $goods['Spec']);
            $this->assign('goods', $goods);
            $this->render();
        }else{
            $this->render('noGoodShow');                            
        }
    }

    //品牌团
    public function actionBanner(){
        //所有的品牌
        $arrData = MemberBrandModel::getList();

        $this->assign('banner',$arrData);
        //用户请求的page
        $page = Yii::app()->request->getParam('page');
        $page = empty($page) ? 1: $page;//第几页                  
        $datas = GoodsModel::model()->getBannerGoods($page, 20, $arrData);
        $this->assign('data', $datas); 
        $this->render();
    }


    //品牌团详细页面
    public function actionBannerDetail(){
        $bid = (int)$_GET['bid'];
        $banner = MemberBrandModel::getDetail($bid);
        if(!$banner){
            $this->error('访问出错');
        }
        $this->title = $banner['TitleSeo'];
        $this->keywords = $banner['KeywordsSeo'];
        $this->description = $banner['DescriptionSeo'];
        $data = GoodsModel::model()->getVariedGoods(array('gettype' => 2,'pagesize' => 80));
        $this->assign('data', $data);
        $this->assign('banner', $banner);
        $this->render();
    }

    //品牌团介绍
    public function actionBannerIntroduction(){
        $this->render();
    }
    public function actionCeshi(){
        return false;
        $a = new Memcache;
        $a->addServer('127.0.0.1', 11211);
        $a->set('aa', array(12,3,4));
        print_r($a->get('aa'));exit;


        $redis = new Redis();
        $redis->pconnect('127.0.0.1', 6379);
        echo $redis->exists('abcd');exit;
        echo $redis->rPop('abcd');exit;
        echo $redis->exists('abcd');
        $redis->del('abcd');
        echo $redis->exists('abcd');
    }

}
