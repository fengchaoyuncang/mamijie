<?php

/**
 * File Name：AlipayController.php
 * File Encoding：UTF-8
 * File New Time：2014-6-4 13:24:48
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class AlipayController extends FrontBaseC {

    public function actionNotify() {
        Yii::import('ext.alipay.*');
        $alipay = new Alipay();
        $alipay->alipayNotify();
    }

    public function actionReturn() {
        Yii::import('ext.alipay.*');
        $alipay = new Alipay();
        if($this->getCookie('pay_return_url'))
        {
            $url = $this->getCookie('pay_return_url');
            $this->removeCookie('pay_return_url');
        }else
        {
            $url = '/';
        }
        if ($alipay->alipayReturn() === true) {
            $this->success('充值成功',$url);
        } else {
            $this->error('支付结果确认失败！',$url);
        }
    }
}
