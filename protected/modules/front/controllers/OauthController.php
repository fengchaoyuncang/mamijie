<?php

//网站前台
class OauthController extends FrontBaseC {
    public function actionQq(){
        if(yii::app()->user->isLogin()){//已经登录了
            $this->redirect($this->createUrl('/front/index/index'));
        }

        include(yii::app()->basePath . '/extensions/Login/qq/qqConnectAPI.php');
        $qc = new QC();
        $qc->qq_login();        
    }

    public function actionQqcallback(){
        include(yii::app()->basePath . '/extensions/Login/qq/qqConnectAPI.php');
        $qc = new QC();
        $token = $qc->qq_callback();
        $openid = $qc->get_openid();

        $model = MemberOauthModel::saveData($token,$openid);
        yii::app()->user->oauth($model);
        $this->redirect(self::U('/user/user/login'));

        //$arr = $qc->get_user_info();  
        //print_r($arr);exit;               
    }
}
