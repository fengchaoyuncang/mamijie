<?php

/**
 * File Name：GoodsController.php
 * File Encoding：UTF-8
 * File New Time：2014-9-24 10:02:16
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class GoodsController extends AppBaseC {


    //商品内页
    public function actionDetail() {
        $good_id = (int) $_GET['gid'];
        $goods = GoodsModel::getOne($good_id);
        if(!$goods){
            $goods = GoodsBakModel::getOne($good_id);
        }        
        if (empty($goods['AppTaobaoUrl'])) {
            if (empty($goods['TaobaoUrl'])) {
                $this->redirect($goods['DetailUrl']);
            } else {
                $this->redirect($goods['TaobaoUrl']);
            }            
        } else {
            $this->redirect($goods['AppTaobaoUrl']);
        }
    }


    public function actionSkip() {
        $good_id = (int) $_GET['gid'];
        $GoodsModel = new GoodsModel();
        $good = $GoodsModel->getGoodBYGoodId($good_id);
        $pid = 'mm_48515914_6072913_25800213';
        if ($this->platform == 'Android') {
            $pid = 'mm_48515914_6072913_25800213';
        } else if ($this->platform == 'iOS') {
            $pid = 'mm_48515914_6072913_25800210';
        }
        if (!empty($good)) {
            $this->layout = false;
            $this->render('show', array(
                'product_id' => $good->product_id,
                'pid' => $pid,
            ));
            return true;
        }
    }

}
