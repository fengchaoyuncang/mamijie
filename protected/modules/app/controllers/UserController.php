<?php

/**
 * 用户相关
 * File Name：UserController.php
 * File Encoding：UTF-8
 * File New Time：2015-1-20 13:13:54
 * Author：张锦辉
 * Mailbox：www.008zjh@163.com
 */
class UserController extends AppUserBaseC {

    //会员资料
    public function actionInfo() {
        $data = Yii::app()->request->getParam('data'); //支持修改的数据
        if (!empty($data)) {
            $data = json_decode($data, true);
        }
        //根据id获取资料
        $model = MemberModel::model()->findByPk($this->UserID);
        if (!$model) {
            $this->ajaxReturn(array('status' => 0, 'info' => "未找到该用户资料"));
        }
        //选取应该输出的资料类型
        $return = array(
            'UserName' => $this->userInfo['UserName'] ? $this->userInfo['UserName'] : "", //用户名
            'Phone' => $this->userInfo['Phone'] ? $this->userInfo['Phone'] : "", //手机号
            'Email' => $this->userInfo['Email'] ? $this->userInfo['Email'] : "", //电子邮箱
            'Gender' => $this->userInfo['Gender'], //性别1男2女
            'Birthday' => $this->userInfo['Birthday'],
        );
        if (!empty($data) && is_array($data)) {
            $rules = array(
                array('Email', 'required', 'message' => '邮箱不能为空！'),
                array('Email', 'unique', 'message' => '您输入的邮件地址已被使用！'),
                array('UserName,Email,Gender', 'safe')
            );
            $model->setValidators($rules);
            $model->attributes = array(
                'Birthday' => $data['Birthday'],
                'Email' => trim($data['Email']),
                'Gender' => trim($data['Gender']),
            );
            if ($model->validate() && $model->save()) {
                $this->getUserInfo(false);
                $this->ajaxReturn(array('status' => 1, 'info' => "修改成功", 'return' => $return));
            }
            $this->ajaxReturn(array('status' => 0, 'info' => $model->getOneError() ? $model->getOneError() : "修改失败", 'error_code' => 913));
        }
        $this->ajaxReturn(array('status' => 1, 'return' => $return, 'info' => "获取成功"));
    }

    //修改密码
    public function actionChangepassword() {
        $receive = Yii::app()->request->getParam('data');
        //解密
        switch ($this->Platform) {
            case 'iOS':
                $receive = AppEncrypt::ios(urldecode($receive), 'DECODE');
                break;
            case 'Android':
            default:
                $receive = AppEncrypt::android($receive, 'DECODE');
                break;
        }
        //解析json
        if (!empty($receive)) {
            $data = json_decode($receive, true);
        }
        if (!is_array($data)) {
            $this->ajaxReturn(array('status' => 0, 'info' => "数据格式不对!"));
        }
        $model = MemberModel::model();
        $modelobj = $model->findByPk($this->UserID);
        //用户数据
        if (empty($modelobj)) {
            $this->ajaxReturn(array('status' => 0, 'info' => "未找到该用户数据!", 'error_code' => 300));
        }
        //验证密码
        if ($model->encrypt($data['oldpassword']) !== $modelobj->Password) {
            $this->ajaxReturn(array('status' => 0, 'info' => "原密码错误,请重新输入!", 'error_code' => 500));
        }
        $modelobj->Password = trim($data['newpassword']);
        //修改密码
        if ($modelobj->save() && $modelobj->validate()) {
            $this->ajaxReturn(array('status' => 1, 'info' => "修改成功!"));
        } else {
            $this->ajaxReturn(array('status' => 0, 'info' => "修改失败!"));
        }
    }

    //绑定手机号
    public function actionChangephone() {
        $model = MemberModel::model();
        $phone = Yii::app()->request->getParam('Phone');
        $code = Yii::app()->request->getParam('PhoneCode');
        if (!preg_match('/^1[34578][0-9]{9}$/', trim($phone))) {
            $this->ajaxReturn(array('status' => 0, 'info' => "请输入正确的手机号!"));
        }
        if (!SnsVerification::getInstance()->verification($code, $phone, 'changephone')) {
            $this->ajaxReturn(array('status' => 0, 'info' => SnsVerification::getInstance()->getError() ? SnsVerification::getInstance()->getError() : "验证码错误!"));
        }
        if ($model->changePhone($this->UserID, $phone)) {
            $this->getUserInfo(false);
            $this->ajaxReturn(array('status' => 1, 'info' => "修改成功"));
        }
        $this->ajaxReturn(array('status' => 0, 'info' => $model->getOneError() ? $model->getOneError() : "修改失败", 'error_code' => 918));
    }
    

}
