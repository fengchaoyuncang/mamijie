<?php

/*
 * APP公共控制器
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PublicController extends AppBaseC {


    //关联登录
    public function actionRelationLogin(){
/*        $this->data = array(
            'openid' => '5B4CCD70384DBC59DE3D5A156276C7CF',
            'token' => 'B140AAF096089114B966C29A5BF77B69',
            'type' => 1,
            'username' => 'leiqianghuaffsdf',
            'emailphone' => '13124083838',
            //'password' => '631222',
        );*/
        empty($this->data['emailphone']) ? $this->data['emailphone'] = '' : '';
        empty($this->data['password']) ? $this->data['password'] = '' : '';
        $model = MemberOauthModel::saveData($this->data['token'],$this->data['openid'],$this->data['type']);

        if(!$memberModel = $model->relationLogin($this->data['username'], $this->data['emailphone'], $this->data['password'])){//关联登录  //返回memberModel
           $this->ajaxReturn(array('status' => 0, 'info' => $model->getOneError()));
        }
        $return = MembersAccessModel::model()->login($memberModel->UserName, NULL, $this->Platform);         
        $this->ajaxReturn(array('status' => 1, 'return' => $return));        
    }

    public function actionOauthqq(){
        $model = MemberOauthModel::saveData($this->data['token'],$this->data['openid'],$this->data['type']);
        if($model->UserID){
            $return = MembersAccessModel::model()->login($model->UserID,null,$this->Platform);
        }
        if (!empty($return)) {
            $this->ajaxReturn(array('status' => 1, 'return' => $return));
        } else {
            $this->ajaxReturn(array('status' => 0, 'info' => '该用户未关联用户'));
        }                
    }


    //用户登录
    public function actionLogin() {

        $model = MembersAccessModel::model();
        $return = $model->login($this->data['UserName'], $this->data['Password'], $this->Platform);
        if (!empty($return)) {
            $this->ajaxReturn(array('status' => 1, 'return' => $return));
        } else {
            $this->ajaxReturn(array('status' => 0, 'info' => $model->getOneError() ? $model->getOneError() : "账号或密码错误！"));
        }
    }

    //手机端注册
    public function actionReg() {
        $model = new RegisterForm();
        $this->data['UserName'] = urldecode($this->data['UserName']);
        $this->data['Repassword'] = $this->data['Password'];
        $this->data['Remember'] = 1;
        if (!SnsVerification::getInstance()->verification($this->data['PhoneCode'], $this->data['EmailPhone'], 'mobilePhone')) {
            $this->ajaxReturn(array('status' => 0, 'info' => SnsVerification::getInstance()->getError() ? SnsVerification::getInstance()->getError() : "验证码错误!"));
        }
        $model->attributes = $this->data;
        if ($model->validate()) {
            if ($model->register()) {
                $return = MembersAccessModel::model()->login(trim($this->data['UserName']));
                $this->ajaxReturn(array('status' => 1, 'info' => '注册成功!', 'return' => $return));
            }
        } else {
            SnsVerification::getInstance()->recovery($this->data['EmailPhone'], 'mobilePhone');
            $error = $model->getErrors();
            $field = array_keys($error);
            $key = current($field);
            $errors = $error[$key][0];
            $this->ajaxReturn(array('status' => 0, 'info' => $errors ? $errors : '注册失败'));
        }
    }

    //发送手机验证码
    public function actionSendcode() {
        $moblie = Yii::app()->request->getParam('Phone');
        $type = (int) Yii::app()->request->getParam('Type'); //短信类型  
        $sendtype = array(1 => 'mobilePhone', 2 => 'changephone', 3 => 'findbyphone');
        switch ($this->Platform) {
            case 'iOS':
                $moblie = AppEncrypt::ios($moblie, 'DECODE');
                break;
            case 'Android':
            default:
                $moblie = AppEncrypt::android($moblie, 'DECODE');
                break;
        }
        if (empty($moblie)) {
            $this->ajaxReturn(array('status' => 0, 'info' => "手机号不能为空!"));
        } else {
            $moblie = json_decode($moblie, true);
        }
        $key = md5(Tool::getForwardedForIp() . $moblie);
        if (Yii::app()->cache->get($key)) {
            $this->ajaxReturn(array('status' => 0, 'info' => "请在60秒后在发送",));
        }
//        if (defined('YII_DEBUG') && YII_DEBUG == false) {
//            $rKey = md5($moblie);
//            $redisKey = (int) RedisCluster::getInstance()->get($rKey);
//            if ($redisKey >= 5) {
//                $this->error('该手机号今日已达最大发送数！');
//            }
//        }
        if (SnsVerification::getInstance()->send($moblie, $sendtype[$type] ? $sendtype[$type] : 'mobilePhone')) {
//            if (defined('YII_DEBUG') && YII_DEBUG == false) {
//                $redisKey = (int) $redisKey + 1;
//                $time = strtotime(date('Y-m-d 23:59:59')) - time();
//                RedisCluster::getInstance()->set($rKey, $redisKey, $time);
//            }
            Yii::app()->cache->set($key, 1, 60);
            $this->ajaxReturn(array('status' => 1, 'info' => "短信验证码已经发送！",));
        } else {
            $this->ajaxReturn(array('status' => 0, 'info' => SnsVerification::getInstance()->getError() ? SnsVerification::getInstance()->getError() : "验证码发送失败！",));
        }
    }

    //找回密码
    public function actionFindpassword() {
        if (empty($this->data['Phone'])) {
            $this->ajaxReturn(array('status' => 0, 'info' => "信息为空", 'error_code' => 200));
        }
        $model = MemberModel::model();
        //检测账号真实性
        $modelObj = $model->find($model->where(array('Phone' => trim($this->data['Phone']))));
        if (!$modelObj) {
            $this->ajaxReturn(array('status' => 0, 'info' => "对不起,您没有注册过该手机号", 'error_code' => 701));
        }
        // 验证验证码真实性
        if (!SnsVerification::getInstance()->verification($this->data['PhoneCode'], $this->data['Phone'], 'findbyphone')) {
            $this->ajaxReturn(array('status' => 0, 'info' => SnsVerification::getInstance()->getError() ? SnsVerification::getInstance()->getError() : '验证失败', 'error_code' => 702));
        }
        if (empty($this->data['NewPassword'])) {
            $this->ajaxReturn(array('status' => 0, 'info' => "请输入密码!"));
        }
        //修改密码
        $modelObj->Password = trim($this->data['NewPassword']);
        if ($modelObj->save() && $modelObj->validate()) {
            $this->ajaxReturn(array('status' => 1, 'info' => "修改成功!"));
        }
        $this->ajaxReturn(array('status' => 0, 'info' => "修改失败"));
    }


    public function actionAgreement(){
        $this->layout = false;
        $this->render();
    }

}
