<?php
//百科页面
class BaikeController extends FrontBaseC {
	//百科首页
	public function actionIndex(){
		$arrResult = array();

		//banner
		$arrResult['ad'] = AdvertisingModel::getListType(9);

		//一热门文章
		$data = ArticlesNewModel::model()->getVaried(array('hot' => 1, 'page' => '', 'pagesize' => 1, 'AppImg' => 1));
		$arrResult['hotarticle'] = $data['data'][0];
		unset($data);


		//热门的分类
		$arrResult['hotaskcategory'] = CategoryArticlesNewModel::getAppHot();
		$arrResult['hotarticlecategory'] = CategoryArticlesAskModel::getAppHot();
		//文章与分类
		

		$d = ArticlesAskModel::model()->getVaried(array('IsAppIndex' => 1, 'page' => '', 'pagesize' => 3));
		$ds = ArticlesNewModel::model()->getVaried(array('IsAppIndex' => 1, 'page' => '', 'pagesize' => 3));
		$arrResult['ask'] = $d['data'];
		$arrResult['article'] = $ds['data'];

		$data['data'] = $arrResult;
		unset($arrResult);
		$data['status'] = true;


		$this->ajaxReturn($data);  
	}



	public function actionAskList(){
		$data = ArticlesAskModel::model()->getVaried();		
		$data['status'] = true;
		$this->ajaxReturn($data);
	}

	public function actionArticleList(){
		$data = ArticlesNewModel::model()->getVaried();		
		$data['status'] = true;
		$this->ajaxReturn($data);
	}



	public function actionAskDetail($id){

		$this->layout = false;
		$model = $this->loadAskModel($id);
		$this->assign('model',$model);
		$this->render();
	}

	public function actionArticleDetail($id){
		$this->layout = false;
		$model = $this->loadModel($id);
		$this->assign('model',$model);
		$this->render();
	}



	//周刊
	public function actionWeekly(){
		$id = Yii::app()->request->getParam('id');
		$model = $this->loadWeeklyModel($id);

		//此周刊类型的所有数据
		$weeklyType = ArticlesWeeklyModel::getList($model->Type);	
		$this->assign('weeklyType', $weeklyType);
		$data = $model->attributes;
		$data['data'] = ArticlesWeeklyDetailModel::getListWeekly($data['WeeklyID']);
		$this->assign('data', $data);
		$this->title = $model->TitleSeo;
		$this->keywords = $model->KeywordsSeo;
		$this->description = $model->DescriptionSeo;
		$this->render();
	}



	//ajax获取文章
	public function actionAjaxArticle(){
		//育儿知识第一块
		$cid = Yii::app()->request->getParam('id');
		if(!$cid){
			$this->error('请求出错');
		}
		$datacate['ask'] = $datacate['imgarticle'] = $datacate['article'] = array();
		$datacate['ask'] = ArticlesAskModel::model()->getVaried(array('cid' => $cid, 'page' => '', 'pagesize' => 9));
		$datacate['imgarticle'] = ArticlesNewModel::model()->getVaried(array('cid' => $cid, 'page' => '', 'pagesize' => 4, 'IsImg' => true));
		$datacate['article'] = ArticlesNewModel::model()->getVaried(array('cid' => $cid, 'page' => '', 'pagesize' => 5, 'IsImg' => false));

		$content = $this->renderPartial('data', array('datacate' => $datacate), true);

		$this->ajaxReturn(array('content' => $content, 'status' => 1));
	}

	//百科具体文章内容页面
	public function actionView($id)
	{
		$d = GoodsModel::model()->getVariedGoods(array('page' => '', 'pagesize' => 1));
		$this->assign('arrGodsData', $d['data'][0]);
		$model = $this->loadModel($id);
		$this->title = $model->TitleSeo;
		$this->keywords = $model->KeywordsSeo;
		$this->description = $model->DescriptionSeo;
		$this->render('view',array(
			'model'=>$model,
		));
	}



	public function loadWeeklyModel($id)
	{
		$model= ArticlesWeeklyModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}	

	public function loadAskModel($id)
	{
		$model= ArticlesAskModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}	
	public function loadModel($id)
	{
		$model= ArticlesNewModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	public function actionAjaxHelp(){
		$id = Yii::app()->request->getParam('id');
		$model = $this->loadAskModel($id);
		$help = Yii::app()->request->getParam('help');		
		if($help == 0){
			$model->NoHelp = $model->NoHelp + 1;
		}else{
			$model->Help = $model->Help + 1;
		}
		$model->save(false);
		$this->ajaxReturn(array('status' => 1, 'NoHelp' => $model->NoHelp, 'Help' => $model->Help, 'info' => '处理成功'));
	}
}