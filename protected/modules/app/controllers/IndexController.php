<?php

class IndexController extends AppBaseC {

    public function actionIndexNew(){
        $datas = array_values(AdvertisingModel::getListType(5));
        $arrorder = array(
            array('name' => '默认','order' => ''),
            array('name' => '销量','order' => 1),
            array('name' => '折扣','order' => 5),
            array('name' => '价格','order' => 2),
        );        

        $arr = array('order' => $arrorder, 'status' => 1, 'ad' => $datas);
        echo json_encode($arr);  
         
    }

    
    public function actionIndex(){
        $datas = array_values(AdvertisingModel::getListType(5));

        $d = array_values(MemberBrandModel::getList());

        $arrDatas = array(
            array('ModuleID' => 51,'Title' => '萌宝宝'),
            array('ModuleID' => 60,'Title' => '好妈咪'),
            array('ModuleID' => 68,'Title' => '生活馆'),
        );

        $arr = array('banner' => $datas, 'status' => 1, 'module' => $arrDatas,'brand' => $d);
        echo json_encode($arr);  
         
    }


    public function actionBanner(){

        //所有的品牌
        $arrData = MemberBrandModel::getList();

        //用户请求的page
        $page = Yii::app()->request->getParam('page');
        $page = empty($page) ? 1: $page;//第几页 

        $pagesize = Yii::app()->request->getParam('pagesize');
        $pagesize = empty($pagesize) ? 20: $pagesize;//第几页 

        $pagebanner = Yii::app()->request->getParam('pagebanner');
        $pagebanner = empty($pagebanner) ? '': $pagebanner;//第几页  


        $datas = GoodsModel::model()->getBannerGoods($page, $pagesize, $arrData, $pagebanner);
        if(isset($datas['page'])){
            unset($datas['page']);
        }
        $d['count'] = $datas['count'];
        $d['status'] = 1;
        $d['data'] = array();
        foreach ($datas['data'] as $bid => $value) {
            $banner = MemberBrandModel::getDetail($bid);
            if(!empty($banner['AppImg'])){
                $banner['AppImg'] = Yii::app()->params->fileUrl . $banner['AppImg'];          
            }
            if($pagebanner){
                $banner['count'] = $value['count'];
                $banner['data'] = $value['data'];
            }
            $d['data'][] = $banner;
        }
       
        echo json_encode($d);       
    }

    public function actionBannerDetail(){
        $bid = Yii::app()->request->getParam('bid');
        $banner = MemberBrandModel::getDetail($bid);
        if(!empty($banner['AppImg'])){
            $banner['AppImg'] = Yii::app()->params->fileUrl . $banner['AppImg'];            
        }
        echo json_encode($banner);        
    }
    //轮播图
    public function actionCarousel(){
        $datas = array_values(AdvertisingModel::getListType(5));
        echo json_encode($datas);
    }
    //获取所有的分类
    public function actionAllCategory(){
        $arrSignMenu = CategoryModel::getListMenu(3);
        $arrSignMenu = array_values($arrSignMenu);
        foreach ($arrSignMenu as $key => $value) {
            $arrSignMenu[$key]['data'] = array_values($arrSignMenu[$key]['data']);
            if(empty($arrSignMenu[$key]['data'][0]['data'])){
                $dd = $arrSignMenu[$key]['data'];
                unset($arrSignMenu[$key]['data']);
                $arrSignMenu[$key]['data'][0] = $arrSignMenu[$key];
                $arrSignMenu[$key]['data'][0]['data'] = $dd;
                unset($dd);
            }
            foreach ($arrSignMenu[$key]['data'] as $k => $v) {
                $arrSignMenu[$key]['data'][$k]['data'] = array_values($arrSignMenu[$key]['data'][$k]['data']);
            }
        }

       // print_r($arrSignMenu);exit;
        $a['status'] = 1;
        $a['data'] = $arrSignMenu;
        echo json_encode($a);
    }
    //获取分类
    public function actionCategoryForCid(){
        $cid = Yii::app()->request->getParam('cid');
        echo json_encode(array_values(CategoryModel::getArrChildidDetail($cid)));
    }
    //排序
    public function actionGetOrder(){
        $arr = array(
            array('name' => '销量','order' => 1),
            array('name' => '价格从小到大','order' => 2),
            array('name' => '价格从大到小','order' => 3),
            array('name' => '折扣','order' => 6),
            array('name' => '新品','order' => 9),
        );
        echo json_encode($arr);
        exit;
    }
    public function actionGetModule(){
        echo json_encode(array());
        exit;
    }

    public function actionGetGoods(){
        $pagesize = Yii::app()->request->getParam('pagesize');
        $pagesize = empty($pagesize) ? 20: $pagesize;//第几页          
        $datas = GoodsModel::model()->getVariedGoods(array('pagesize' => $pagesize,'gettype' => 2), false, true, 600);
        if($datas['page']){
            unset($datas['page']);
        }
        $datas['status'] = 1;
        echo json_encode($datas);                  
    }


    public function actionGetTomorrowGoods(){
        $pagesize = Yii::app()->request->getParam('pagesize');
        $page = Yii::app()->request->getParam('page');
        $order = Yii::app()->request->getParam('order');
        $pagesize = empty($pagesize) ? 20: $pagesize;//第几页  
        $page = empty($page) ? 1: $page;//第几页          
        $datas = GoodsModel::getTomorrow($page, $pagesize, $order);
        if($datas['page']){
            unset($datas['page']);
        }
        $datas['status'] = 1;
        echo json_encode($datas);          
    }
}
