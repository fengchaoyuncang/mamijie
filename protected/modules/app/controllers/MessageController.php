<?php

/**
 * File Name：MessageController.php
 * File Encoding：UTF-8
 * File New Time：2014-12-2 15:49:28
 * Author：张锦辉
 * Mailbox：www.008zjh@163.com
 */
class MessageController extends AppUserBaseC {

    //站内消息列表
    public function actionList() {
        $limit = Yii::app()->request->getParam('limit'); //页大小
        $page = Yii::app()->request->getParam('page'); //分页
        $model = MessageModel::model();
        $where = array(
            'order' => 'IsRead asc,AddTime desc',
            'UserID' => $this->UserID,
        ); //条件
        $criteria = $model->where($where);
        $count = $model->count($criteria);
        $pager = self::page($count, $limit, $page);        //分页
        $criteria->limit = $pager->listRows;
        $criteria->offset = $pager->firstRow;
        $dataList = $model->findAll($criteria);
        if (empty($dataList)) {
            $this->ajaxReturn(array('status' => 1, 'info' => '信息为空', 'return' => array(), 'count' => 0));
        }
        //打开次页面表示已经阅读
        $data = $nids = array();
        foreach ($dataList as $k => $rs) {
            $data[$k]['MessageID'] = $rs->MessageID;
            $data[$k]['UserID'] = $rs->UserID;
            $data[$k]['Type'] = $rs->Type;
            $data[$k]['Title'] = $rs->Title;
            $data[$k]['IsRead'] = $rs->IsRead;
            $data[$k]['Remark'] = $rs->Remark; //补充内容
            $data[$k]['SendUserID'] = $rs->SendUserID;
            $data[$k]['AddTime'] = $rs->AddTime;
            $data[$k]['Content'] = strip_tags($rs->Content);
            $data[$k]['Image'] = $rs->Image;
            if ($rs['IsRead']) {
                continue;
            }
            $nids[$rs['MessageID']] = $rs['MessageID'];
        }
        //标记为已读信息
        if (!empty($nids)) {
            $model->updateList($nids, $this->UserID);
        }
        $return = array(
            'status' => 1,
            'info' => '读取消息成功',
            'count' => $count,
            'return' => $data
        );
        $this->ajaxReturn($return);
    }

    //是否有新消息
    public function actionIsNew() {
        $model = new MessageModel();
        $count = (int) $model->getNoReadCount($this->UserID);
        if (empty($count)) {
            $this->ajaxReturn(array('status' => 1, 'info' => '您没有未读消息!', 'count' => 0));
        } else {
            $this->ajaxReturn(array('status' => 1, 'info' => '您有' . $count . '条未读消息!', 'count' => $count));
        }
    }

    //删除消息
    public function actionDelete() {
        $model = new MessageModel();
        $nid = Yii::app()->request->getParam('MessageIDs');
        if (is_int($nid)) {
            $list = array(0 => $nid);
        } else {
            $list = json_decode($nid, true);
        }
        $str = BaseModel::getStrIn($list);
        $Criteria = BaseModel::getC(array('UserID' => $this->UserID, 'MessageID' => array('sign' => 'in', 'data' => $str)));
        if ($model->deleteAll($Criteria)) {
            $this->ajaxReturn(array('status' => 1, 'info' => '删除成功!'));
        } else {
            $this->ajaxReturn(array('status' => 0, 'info' => '删除失败!', 'error_code' => 609));
        }
    }

}
