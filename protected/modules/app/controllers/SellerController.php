<?php

/**
 * 卖家相关控制器
 * File Name：SellerController.php
 * File Encoding：UTF-8
 * File New Time：2015-1-21 9:20:51
 * Author：张锦辉
 * Mailbox：www.008zjh@163.com
 */
class SellerController extends AppUserBaseC {

    const wait = 0; //待审核
    const pass = 1; //审核通过
    const refuse = 2; //审核不通过  
    const mail = 3; //邮寄样品
    const cancel = 4; //取消报名
    const going = 5; //审核中

    //报名管理

    public function actionBmGoods() {
        $label = (int) Yii::app()->request->getParam('label'); //获取当前条件下的报名产品
        $all = Yii::app()->request->getParam('all'); //获取该用户全部报名产品信息
        $limit = Yii::app()->request->getParam('limit'); //页大小
        $page = Yii::app()->request->getParam('page'); //分页
        $GoodsBmModel = GoodsBmModel::model();
        $model = new CategoryModel(); //分类
        $where = array(
            'UserID' => $this->UserID,
            'order' => 'GoodsBmID desc',
        );
        switch ($label) {
            case self::wait://待审核
                $where['Status'] = 0;
                $where['AdminID'] = 0;
                break;
            case self::pass://审核通过
                $where['Status'] = 1;
                break;
            case self::refuse://审核不通过
                $where['Status'] = 2;
                break;
            case self::mail://邮寄样品中
                $where['Status'] = 3;
                break;
            case self::cancel://取消报名
                $where['Status'] = 4;
                break;
            case self::going://审核中
                $where['Status'] = 0;
                $where['AdminID'] = array('GT', 0);
                break;
            default :
                break;
        }
        if ($all >= 1) {//获取全部的报名产品
            $where = array(
                'UserID' => $this->UserID,
                'order' => 'GoodsBmID desc',
            );
        }
        $criteria = $GoodsBmModel->where($where);
        $count = $GoodsBmModel->count($criteria);
        $pager = self::page($count, $limit, $page); //分页
        $criteria->limit = $pager->listRows;
        $criteria->offset = $pager->firstRow;
        $data = $GoodsBmModel->findAll($criteria);
        if (empty($data)) {
            $data = array();
        }
        $return = array();
        foreach ($data as $key => $rs) {//数据整理
            $return[$key]['GoodsBmID'] = $rs->GoodsBmID;
            $return[$key]['GoodsName'] = $rs->GoodsName; //商品名称
            $return[$key]['ProductID'] = $rs->ProductID;
            $return[$key]['AddTime'] = $rs->AddTime; //添加时间
            $return[$key]['Image'] = $rs->Image; //图片
            $return[$key]['DetailUrl'] = $rs->DetailUrl;
            $return[$key]['PromoPrice'] = $rs->PromoPrice; //活动价格
            $return[$key]['StartTime'] = $rs->StartTime; //上架
            $return[$key]['EndTime'] = $rs->EndTime; //下架
            $return[$key]['AdminID'] = $rs->AdminID; //管理员ID
            $return[$key]['IsMail'] = $rs->IsMail; //邮寄样品 0未邮寄 显示填写快递单号
            $return[$key]['IsFreeShipping'] = $rs->IsFreeShipping; //是否包邮
            $return[$key]['CatName'] = $model->getDataDetail($rs->CatID, 'Title') ? $model->getDataDetail($rs->CatID, 'Title') : "";
            $return[$key]['Excuse'] = $rs->Excuse; //审核不通过原因
            $return[$key]['Status'] = $rs->Status; //审核状态  
        }
        if ($all >= 1) {
            $allData = $return;
            unset($return);
        }
        $this->ajaxReturn(array('status' => 1, 'return' => $return ? $return : array(), 'info' => "获取成功", 'alldata' => $allData ? $allData : array(), 'total' => $count));
    }

    //取消报名
    public function actionCancel() {
        $goods_id = Yii::app()->request->getParam('GoodsBmID');
        $model = GoodsBmModel::model();
        $data = $model->find($model->where(array('GoodsBmID' => $goods_id, 'UserID' => $this->UserID)));
        if (empty($data)) {
            //记录错误信息
            $this->ajaxReturn(array('status' => 0, 'info' => "该商品已进入审核,请确认!", 'error_code' => 806));
        } elseif ($data->Status == 0 && $data->AdminID == 0) {
            $data->Status = 4;
            $data->save(false, array('Status'));
            $this->ajaxReturn(array('status' => 1, 'info' => "取消报名成功!"));
        } else {
            $this->ajaxReturn(array('status' => 0, 'info' => "当前状态不能取消报名!"));
        }
    }

}
