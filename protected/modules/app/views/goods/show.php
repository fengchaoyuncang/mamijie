<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?> 
<div  style=" display:none">
    <input type="hidden" id="time_ico" value="0">
    <input type="hidden" id="product_id" value="<?php echo $product_id; ?>">
    <input type="hidden" id="skip" value="0">
    <a data-type="0" biz-itemid="<?php echo $product_id; ?>" data-tmpl="290x380" data-tmplid="5" data-rd="2" data-style="2" data-border="1">跳转中...</a>
</div>
<script type="text/javascript">
    (function(win, doc) {
        var s = doc.createElement("script"), h = doc.getElementsByTagName("head")[0];
        if (!win.alimamatk_show) {
            s.charset = "gbk";
            s.async = true;
            s.src = "http://a.alimama.cn/tkapi.js";
            h.insertBefore(s, h.firstChild);
        }
        ;
        var o = {
            pid: "<?php echo $pid; ?>", /*推广单元ID，用于区分不同的推广渠道*/
            appkey: "", /*通过TOP平台申请的appkey，设置后引导成交会关联appkey*/
            unid: ""/*自定义统计字段*/
        };
        win.alimamatk_onload = win.alimamatk_onload || [];
        win.alimamatk_onload.push(o);
    })(window, document);


    $(document).ready(function() {
        window.setInterval("skip_taobao()", 50);
    });

    function skip_taobao() {
        var is_skip = $('#skip').val();
        if (is_skip === "0") {
            if ($('#writeable_iframe_0').contents().find('a').length > 0) {
                var href = $('#writeable_iframe_0').contents().find('a').attr('href');
                window.location.href = href;
                $('#skip').val('1');
            }
            var time = parseInt($('#time_ico').val());
            if (time > 25) {
                var product_id = $('#product_id').val();
                window.location.href = 'http://item.taobao.com/item.htm?id=' + product_id;
                $('#skip').val('1');
            } else {
                $('#time_ico').val(time + 1);
            }
        }

    }
</script>