<?php

/**
 * 商品模型
 */
class AppGoodsModel extends GoodsModel {

    //缓存时间
    private $cacheTime = 300;

    /**
     * 淘宝   API
     * @param type $num
     * @param type $cache
     * @param type $order
     * @return type
     */
    public function getTBGoods($num = 20, $cache = true, $order = "default") {
        $time = time();
        $offset = (int) $_GET['offset'];
        $key = "AppGoodsModel_getTBGoods_{$offset}_{$num}";
        if ($return = $this->modelSetCache($key, $cache)) {
            return $return;
        }
        $max_start_time = strtotime(date('Y-m-d', $time)) + 86400;
        $where = array(
            'item_type' => array('LT', 10),
            'level' => 1,
            'status' => 1,
            'start_time' => array('LT', $max_start_time),
            'end_time' => array('GT', $time),
        );
        $order_str = "start_time DESC ,listorder DESC, sales DESC";
        $model = GoodsModel::model();
        $data = $model->getGoodsList($where, $num, $offset, $order_str);
        $return['data'] = $this->AppgetGoodList($data[0]);
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        return $return;
    }

    /**
     * 获取各栏目下的商品数据  API
     * @param type $num
     * @param type $cache
     * @param type $order
     * @return type
     */
    public function getAppCatGoods($num = 20, $cache = true, $order = "default") {
        $time = time();
        $offset = (int) $_GET['offset'];
        $cid = (int) $_GET['cid'];
        $key = "AppGoodsModel_getAppCatGoods_{$cid}_{$num}_{$offset}";
        if ($return = $this->modelSetCache($key, $cache)) {
            return $return;
        }
        $max_start_time = strtotime(date('Y-m-d', $time)) + 86400;
        $where = array(
            'item_type' => array('LT', 10),
            //'level' => 1,
            'status' => 1,
            'start_time' => array('LT', $max_start_time),
            'end_time' => array('GT', $time),
        );
        $order_str = "start_time DESC ,listorder DESC, sales DESC";
        if ($cid > 0) {
            $where['cat_id'] = $cid;
            if ($cid == 100) {
                $where['cat_id'] = array('IN', array(4, 7));
            } elseif ($cid == 101 || $cid == 8 || $cid == 10) {
                $where['cat_id'] = array('IN', array(5, 9));
            }
        }
        $model = GoodsModel::model();
        $data = $model->getGoodsList($where, $num, $offset, $order_str);
        $return['data'] = $this->AppgetGoodList($data[0]);
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        return $return;
    }

    /**
     * 获取栏目列表
     * @return type
     */
    public function getCatlist() {
        $model = CategoryModel::model();
        $modelCat = $model->where(array('is_app' => 1, 'order' => 'cat_id asc'));
        $modelCatname = $model->findAll($modelCat);
        $catname = array();
        foreach ($modelCatname as $p => $u) {
            $catname[$p]['cid'] = $u->cat_id;
            $catname[$p]['cat_name'] = $u->cat_name;
        }
        $cat['cid'] = "0";
        $cat['cat_name'] = "全部";
        array_unshift($catname, $cat);
        $catname[4] = array(
            'cid' => 100,
            'cat_name' => '鞋包配饰'
        );
        $catname[5] = array(
            'cid' => 101,
            'cat_name' => '家居百货'
        );
        $catname[6] = array(
            'cid' => 6,
            'cat_name' => '美食特产'
        );

        return $catname;
    }

    /**
     * 上期热卖栏下商品app 接口
     * @param type $num
     * @param type $cache
     * @param type $order
     * @return type
     */
    public function getAppNextSales($num = 20, $cache = true, $order = "default") {
        $time = time();
        $offset = (int) $_GET['offset'];
        $key = "AppGoodsModel_getAppNextSales_{$num}_{$offset}";
        if ($return = $this->modelSetCache($key, $cache)) {
            return $return;
        }
        $max_end_time = $time + 86400 * 1;
        $where = array(
            'item_type' => array('LT', 10),
            'status' => 1,
            'start_time' => array(array('GT', $time), array('LT', $max_end_time), 'AND'),
        );
        $order_str = "start_time DESC ,listorder DESC, sales DESC";
        $model = GoodsModel::model();
        $data = $model->getGoodsList($where, $num, $offset, $order_str);
        // print_r($data);exit;
        $return['data'] = $this->AppgetGoodList($data[0]);
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        return $return;
    }

    /**
     * 秋冬新款数据  APP 接口
     * @param type $num
     * @param type $cache
     * @param type $order
     * @return type
     */
    public function getAppWinterNewGoods($num = 20, $cache = true, $order = "default") {
        $time = time();
        $offset = (int) $_GET['offset'];
        $key = "AppGoodsModel_getAppWinterNewGoods_{$num}_{$offset}";
        if ($return = $this->modelSetCache($key, $cache)) {
            return $return;
        }
        $max_start_time = strtotime(date('Y-m-d', $time)) + 86400;
        $where = array(
            'item_type' => array('LT', 10),
            //'level' => 1,
            'status' => 1,
            'is_best' => 1,
            'start_time' => array('LT', $max_start_time),
            'end_time' => array('GT', $time),
        );
        $order_str = "start_time DESC ,listorder DESC, sales DESC";
        $model = GoodsModel::model();
        $data = $model->getGoodsList($where, $num, $offset, $order_str);
        //print_r($data);exit;
        $return['data'] = $this->AppgetGoodList($data[0]);
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        return $return;
    }

    /**
     * 天猫专场接口
     * @param type $num
     * @param type $cache缓存
     * @param type $order
     * @return type
     */
    public function getAppTianMaoGoods($num = 20, $cache = true, $order = "default") {
        $time = time();
        $offset = (int) $_GET['offset'];
        $key = "AppGoodsModel_getAppTianMaoGoods_{$num}_{$offset}";
        if ($return = $this->modelSetCache($key, $cache)) {
            return $return;
        }
        $max_start_time = strtotime(date('Y-m-d', $time)) + 86400;
        $where = array(
            'item_type' => 1,
            'level' => 1,
            'status' => 1,
            'start_time' => array('LT', $max_start_time),
            'end_time' => array('GT', $time),
        );
        $order_str = "start_time DESC ,listorder DESC, sales DESC";
        $model = GoodsModel::model();
        $data = $model->getGoodsList($where, $num, $offset, $order_str);
        //print_r($data);exit;
        $return['data'] = $this->AppgetGoodList($data[0]);
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        return $return;
    }

    /**
     * 首页展示 API
     * @param type $num一页展示的条数
     * @param type $cache 缓存
     * @param type $order
     * @return type
     */
    public function getAppHomeGoods($num = 20, $cache = true, $order = "default") {
        $time = time();
        $offset = (int) $_GET['offset'];
        $key = "AppGoodsModel_getAppHomeGoods_{$num}_{$offset}";
        if ($return = $this->modelSetCache($key, $cache)) {
            return $return;
        }
        $max_start_time = strtotime(date('Y-m-d', $time)) + 86400;
        $where = array(
            'item_type' => array('LT', 10),
            'level' => 1,
            'status' => 1,
            'start_time' => array('LT', $max_start_time),
            'end_time' => array('GT', $time),
        );
        $order_str = "start_time DESC ,listorder DESC, sales DESC";
        $model = GoodsModel::model();
        $data = $model->getGoodsList($where, $num, $offset, $order_str);
        $return['data'] = $this->AppgetGoodList($data[0]);
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        return $return;
    }

    /**
     * 京东 API
     * @param type $cache
     * @return type
     */
    public function getAppJdGoods($num = 20, $cache = true, $order = "default") {
        $time = time();
        $offset = (int) $_GET['offset'];
        //缓存，参数会变化的需要加入$key中
        $key = "AppGoodsModel_getAppJDGoods_{$num}_{$offset}";
        if ($return = $this->modelSetCache($key, $cache)) {
            return $return;
        }
        $max_start_time = strtotime(date('Y-m-d', $time)) + 86400;
        $where = array(
            'item_type' => 11,
            'status' => 1,
            'start_time' => array('LT', $max_start_time),
            'end_time' => array('GT', $time),
        );
        $model = GoodsModel::model();
        $data = $model->getGoodsList($where, $num, $offset, $order_str);
        $return['data'] = $this->AppgetGoodList($data[0]);
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        return $return;
    }

    /**
     * 9.9专区 api
     * @param type $num  显示的商品条数
     * @param type $cache  缓存
     * @param type $order
     * @return type
     */
    public function getAppJKJGoods($num = 20, $cache = true, $order = "default") {
        $time = time();
        $offset = (int) $_GET['offset'];
        $key = "AppGoodsModel_getAppJKJGoods_{$num}_{$offset}";
        if ($return = $this->modelSetCache($key)) {
            return $return;
        }
        $max_start_time = strtotime(date('Y-m-d', $time)) + 86400;
        $where = array(
            'item_type' => array('LT', 10),
            'level' => 1,
            'status' => 1,
            'start_time' => array('LT', $max_start_time),
            'end_time' => array('GT', $time),
            'promo_price' => array('LT', 10),
        );
        $model = GoodsModel::model();
        $data = $model->getGoodsList($where, $num, $offset, $order_str);
        $return['data'] = $this->AppgetGoodList($data[0]);
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        return $return;
    }

    /**
     * 今日特惠 API 
     * @param type $num  显示的商品条目
     * @param type $cache
     * @param type $order
     * @return $return
     */
    public function getHomeGoods($num = 20, $cache = true, $order = "default") {
        $time = time();
        $offset = (int) $_GET['offset'];
        $key = "AppGoodsModel_getHomeGoods_{$num}_{$offset}_{$order}";
        if ($return = $this->modelSetCache($key, $cache)) {
            return $return;
        }
        $max_start_time = strtotime(date('Y-m-d', $time)) + 86400;
        $where = array(
            'item_type' => array('LT', 10),
            'level' => 1,
            'status' => 1,
            'start_time' => array('LT', $max_start_time),
            'end_time' => array('GT', $time),
        );
        $model = GoodsModel::model();
        $data = $model->getGoodsList($where, $num, $offset, $order);
        $return['data'] = $this->AppgetGoodList($data[0]);
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        return $return;
    }

    /**
     * 积分兑换商品展示
     * @return type
     */
    public function getExchangeGoods($num = 20) {
        $time = time();
        $offset = (int) $_GET['offset'];
        $key = "AppGoodsModel_getExchangeGoods_{$num}_{$offset}_";
        if ($return = $this->modelSetCache($key, $cache)) {
            return $return;
        }
        $model = RedeemModel::model();
        $where = array(
            'status' => RedeemModel::STATUS_APPROVE,
            'start_time' => array('ELT', $time),
            'end_time' => array(array('EGT', $time), array('EQ', 0), 'OR'),
        );
        $data = $model->getExchangeGoodsList($where, $num, $offset);
        $return['data'] = $data[0];
        $return['total'] = $data[1]->itemCount;
        Yii::app()->cache->set($key, $return, $this->cacheTime);
        if (!empty($return)) {
            return $return;
        }
    }

    /**
     * 数据处理
     * @param array $dataList 数据
     * @return array 处理后的数组
     */
    protected function appgetGoodList($dataList) {
        if (empty($dataList)) {
            return false;
        }
        $return = array();
        foreach ($dataList as $i => $good) {
            $return[$i]['title'] = $good['goods_name'];
            $return[$i]['goods_id'] = $good['goods_id'];
            $return[$i]['goods_brief'] = $good['goods_name'];
            $return[$i]['promop'] = $good['promo_price'];
            $return[$i]['img'] = self::convertImg($good['item_type'], $good['product_img']);
            $return[$i]['orginp'] = $good['p_price'];
            $return[$i]['sales'] = "销量:" . $good['sales'] . "件";
            $return[$i]['url'] = self::convertUrl($good);
            $return[$i]['discount'] = self::discount($good['promo_price'], $good['p_price']);
        }
        return $return;
    }

    protected static function discount($promo_price, $p_price) {
        $discount = "";
        if (floatval($p_price) > 0) {
            $discount = round($promo_price / $p_price, 2) * 10;
            $discount = $discount . "折";
        }
        return $discount;
    }

    protected static function convertUrl($good) {
        if ($good['item_type'] < 10) {
            if (empty($good['click_url'])) {
                //return 'http://m.1zw.com/index/show/gid/' . $good_id . '.html';
                return $good['detail_url'];
            } else {
                return $good['click_url'];
            }
        }
        return $good['detail_url'];
    }

    protected static function convertImg($item_type, $img) {
        $img_url = "";
        if ($item_type < 10) {
            $small_img_ico = strrpos($img, ".jpg_");
            $small_pngimg_ico = strrpos($img, ".png_");
            $small_gifimg_ico = strrpos($img, ".gif_");
            if ($small_img_ico) {
                $img = substr($img, 0, $small_img_ico + 4);
            } else if ($small_pngimg_ico) {//处理png的
                $img = substr($img, 0, $small_pngimg_ico + 4);
            } else if ($small_gifimg_ico) {
                $img = substr($img, 0, $small_gifimg_ico + 4);
            }
            $img_url = trim($img) . "_230x230Q100.jpg";
        }
        if ($item_type == 11) {
            $small_img_ico = strrpos($img, ".jpg!");
            if ($small_img_ico) {
                $img = substr($img, 0, $small_img_ico + 4);
            }

            $img_url = trim($img) . "!q100.jpg";
        }

        return $img_url;
    }

}
