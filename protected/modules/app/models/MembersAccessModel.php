<?php

//APP授权模型
class MembersAccessModel extends BaseModel {

    //Token生命周期,ExpiredTime为过期时间
    protected $ExpiredTime = 2678400; //-个月

    public function tableName() {
        return '{{members_access}}';
    }

    public function rules() {
        return array(
            array('UserID', 'required', 'message' => '用户ID不能为空！'),
            array('Token,CreateTime,ExpiredTime,Platform,Type,DeviceToken,UserName', 'safe'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    //数据保存前操作
    protected function beforeSave() {
        if ($this->isNewRecord) {
            $time = time();
            $this->CreateTime = $time;
            $this->ExpiredTime = $time + $this->ExpiredTime;
            $this->Token = $this->createToken($this->UserID);
        }
        return true;
    }

    //数据保存后回调
    protected function afterSave() {
        
    }

    /**
     * app用户端登录验证
     * @param type $username 用户名或者邮箱
     * @param type $pass 有明文密码会进行密码验证
     * @return boolean
     */
    public function login($username, $pass = NULL, $platform = 'Android', $device = '') {
        if (empty($username)) {
            return false;
        }
        $model = MemberModel::model();
        //判断登录方式
        if (preg_match('/^1[34578][0-9]{9}$/', trim($username))) {
            $where = array('Phone' => $username);
        } elseif (preg_match('/^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/', $username)) {
            $where = array('Email' => $username);
        } else {
            $where = array('UserName' => $username);
        }
        $userInfo = $model->find($model->where($where));
        if(is_numeric($username) && !$userInfo){
            $userInfo = $model->findByPk($username);            
        }
        if (empty($userInfo)) {            
           $userInfo = $model->find($model->where(array('UserName' => $username)));
           if(empty($userInfo)){
               return false;
           }
        }
        //未激活账户拒绝登录
        if ($userInfo->Status == 0) {
            $this->addError('Status', "该账号已经被系统锁定!");
            return false;
        }
        //密码验证通过
        if (!is_null($pass) && $model->encrypt($pass) !== $userInfo->Password) {
            $this->addError('Password', "密码错误请重新输入");
            return false;
        }
        //删除旧的授权记录
        $this->deleteAll($this->where(array('UserID' => $userInfo->UserID)));
        $userInfo->LatLoginTime = time();
        $userInfo->LastLoginIP = GuestInfo::getIp();
        $userInfo->save();
        //创建授权
        $this->UserID = $userInfo->UserID;
        $this->Type = $userInfo->Type;
        $this->UserName = $userInfo->UserName;
        $this->DeviceToken = trim($device);
        $this->Platform = trim($platform);
        $this->setIsNewRecord(true);
        if ($this->validate() && $this->insert()) {
            return ($this->attributes);
        }
        return false;
    }

    /**
     * 获取token
     * @param type $uid
     * @return string
     */
    public function getToken($uid) {
        if (empty($uid)) {
            return false;
        }
        $info = $this->findByPk($uid);
        if (empty($info)) {
            return false;
        }
        return $info->Token;
    }

    /**
     * 根据token获取用户uid
     * @param type $token
     * @return boolean
     */
    public function getUid($token) {
        if (empty($token)) {
            $this->addError('Token', '请先登录!');
            return false;
        }
        $info = $this->find($this->where(array('Token' => $token)));
        if (empty($info)) {
            $this->addError('Token', '请先登录!');
            return false;
        }
        //过期检查
        if ($this->verificationToken($token) !== true) {
            $this->addError('ExpiredTime', '您的账号太久没有登录,请重新登录');
            return false;
        } else {
            return $info->UserID;
        }
    }

    /**
     * 生成token
     * @param type $uid
     * @return type
     */
    public function createToken($uid) {
        return md5(Encrypt::authcode($uid, ''));
    }

    /**
     * 验证token是否有效
     * @param type $token token值
     * @return boolean
     */
    public function verificationToken($token) {
        if (empty($token)) {
            return false;
        }
        $info = $this->find($this->where(array('Token' => $token)));
        if (empty($info)) {
            return false;
        }
        if ($info->ExpiredTime < time()) {
            $info->delete();
            return false;
        } else {
            return true;
        }
    }

    /**
     * 退出登录
     * @param type $uid
     * @return boolean
     */
    public function logout($uid) {
        if (empty($uid)) {
            return false;
        }
        return $this->deleteAll($this->where(array('UserID' => $uid)));
    }

}
