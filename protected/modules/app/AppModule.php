<?php

class AppModule extends CWebModule {

    //默认控制器
    public $defaultController = 'Index';

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'app.models.*',
            'app.components.*',
        ));
        //当前模块
        defined('GROUP_NAME') or define('GROUP_NAME', $this->getId());
        //判断app模块是不是指定域名访问
        $parse_url = parse_url(Yii::app()->request->hostInfo);
        if (!in_array($parse_url['host'], array('appapi.mamijie.com', 'appdev.mamijie.com')) && YII_DEBUG == false) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        //载入模块配置，暂时不清楚是否可行
        Yii::app()->configure(require($this->basePath . '/config/config.php'));
        
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }

}
