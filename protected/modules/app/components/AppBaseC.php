<?php

//手机
class AppBaseC extends YzwController {

    public $assetsName = 'app';
    //接口请求平台类型
    public $Platform = 'Android';
    //接口请求客户端版本
    public $Version = 0;

    public $data = '';//解密完了后的数据

    public function init() {
        //平台  iOS|Android
        $platform = Yii::app()->request->getParam('Platform');
        if (!empty($platform)) {
            $this->Platform = $platform;
        }
        //版本
        $version = Yii::app()->request->getParam('Version');
        if (!empty($version)) {
            $this->Version = $version;
        }

        $sign = Yii::app()->request->getParam('sign');
        //解密
        if($sign){
            switch ($this->Platform) {
                case 'iOS':
                    $sign = AppEncrypt::ios($sign, 'DECODE');
                    break;
                case 'Android':
                default:
                    $sign = AppEncrypt::android($sign, 'DECODE');
                    break;
            }
            if (empty($sign)) {
                $this->ajaxReturn(array('status' => 0, 'info' => '用户信息为空或解析失败！'));
            } else {
                $this->data = json_decode($sign, true);
            }            
        }
        parent::init();
    }

    /**
     * Ajax方式返回数据到客户端
     * @access protected
     * @param mixed $data 要返回的数据
     * @param String $type AJAX返回数据格式
     * @return void
     */
    protected function ajaxReturn($data, $type = 'JSON') {
        $data['status'] = $data['status'] ? 1 : 0;
        switch (strtoupper($type)) {
            case 'JSON' :
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:text/html; charset=utf-8');
                exit(json_encode($data));
            case 'XML' :
                // 返回xml格式数据
                header('Content-Type:text/xml; charset=utf-8');
                exit(xml_encode($data));
            case 'JSONP':
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                $handler = isset($_GET['callback']) ? $_GET['callback'] : 'callback';
                exit($handler . '(' . json_encode($data) . ');');
            case 'EVAL' :
                // 返回可执行的js脚本
                header('Content-Type:text/html; charset=utf-8');
                exit($data);
            default :
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:text/html; charset=utf-8');
                exit(json_encode($data));
        }
    }

}
