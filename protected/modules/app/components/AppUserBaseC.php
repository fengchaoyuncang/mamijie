<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AppUserBaseC extends AppBaseC {

    protected $cacheTime =300; //缓存时间
    protected $UserID = 0;
    protected $userInfo = array(); //会员资料 
    protected $Type = 0; //会员类型

    public function init() {
        parent::init();
        $token = Yii::app()->request->getParam('Token');
        if (empty($token)) {
            $this->ajaxReturn(array('status' => 0, 'info' => '请先登录！', 'error_code' => 405));
        }
        $model = MembersAccessModel::model();
        $info = $model->getUid(trim($token));
        if (!$info) {
            $this->ajaxReturn(array('status' => 0, 'info' => $model->getOneError() ? $model->getOneError() : "请先登录!", 'error_code' => 406));
        }
        $this->UserID = $info;
        if ($this->UserID <= 0) {
            $this->ajaxReturn(array('status' => 0, 'info' => '请先登录！', 'error_code' => 406));
        }
        //验证通过后处理 
        $this->getUserInfo();
        if ($this->userInfo['Status'] == 0) {
            $this->ajaxReturn(array('status' => 0, 'info' => '对不起,你的账号被锁定!', 'error_code' => 408));
        }
    }

    /**
     * 获取用户信息
     * @param type $isCache true使用缓存，false删除缓存
     * @return boolean
     */
    protected function getUserInfo($isCache = true) {
        $key = 'AppUserBaseC_getUserInfo_mmj' . $this->UserID;
        if ($isCache === false) {
            Yii::app()->cache->delete($key);
        }
        $userInfo = Yii::app()->cache->get($key);
       
        if (empty($userInfo)) {
            $userInfoObj = MemberModel::model()->findByPk($this->UserID);
            $userInfo = $userInfoObj->attributes;
            Yii::app()->cache->set($key, $userInfo, $this->cacheTime);
        }
        
        if (empty($userInfo)) {
            return false;
        }
        $this->userInfo = $userInfo;
        $this->Type = $userInfo['Type'];
        return $userInfo;
    }

}
