<?php
/**
 * 记录相关的数据
 */
class GuestController extends YzwController{
	//把分析的数据添加进来
	public function actionIndex(){

		$cache = yii::app()->cache->get('qw');
		$cache = empty($cache) ? 1 : $cache + 1;
		yii::app()->cache->set('qw', $cache, 36000);
		echo $cache;
		exit;



        set_time_limit(0);
        $cache = yii::app()->cache->get('GuestController_actionIndex');
        if(!empty($cache)){
        	echo '正在锁定';
        	exit;
        }else{
        	yii::app()->cache->set('GuestController_actionIndex', true, 60);
        }
        $intCount = 200;
        for($i = 1;$i<=$intCount;$i++){

			$arrData = RedisCluster::getInstance()->pop('guest_record');
            if (empty($arrData)) {
		        yii::app()->cache->delete('GuestController_actionIndex');
		        echo '执行完,没有记录</br>'; 
		        return;               
            }else{
	            //设置存储到哪张表里面去
	            GuestDataModel::$strTableName = '{{guest_data'.$arrData['Year'].$arrData['Month'].$arrData['Day'].'}}';
	            $objModel = new GuestDataModel();	
	            $objModel->attributes = $arrData;
	            $objModel->save(false);
	            echo "第{$i}次成功添加</br>";   
            }         	        	
        }
        yii::app()->cache->delete('GuestController_actionIndex');
        echo '执行完成</br>';
        exit;
	}

	/**
	 * 统计今天到现在广告相关数据
	 * @return [type] [description]
	 */
	public function actionCountAd(){
		set_time_limit(0);
		//统计到现在这个时间的数据
		CountAdModel::saveDataOneDate(TIME_TIME);		
	}

	/**
	 * 统计昨天广告
	 * @return [type] [description]
	 */
	public function actionCountYesterdayAd(){
		set_time_limit(0);
		//获取昨天时间戳
		$intTimeYesterday = strtotime('yesterday') + 1;
		//统计昨天的数据
		CountAdModel::saveDataOneDate($intTimeYesterday);			
	}

	//统计今天至现在的来源信息
	public function actionCountReferer(){
		set_time_limit(0);
		CountRefererModel::saveDataOneDate(TIME_TIME);
	}

	//统计昨天来源信息
	public function actionCountYesterdayReferer(){
		set_time_limit(0);
		$intTimeYesterday = strtotime('yesterday') + 1;
		CountRefererModel::saveDataOneDate($intTimeYesterday);
	}


	//统计今天至现在的商品
	public function actionCountGoods(){
		set_time_limit(0);
		$key_lock = 'GuestController_actionCountGoods_lock';
		$key = 'GuestController_actionCountGoods_goods'.date('d',TIME_TIME);
		if(RedisCluster::getInstance()->get($key_lock)){
			echo '还有任务在执行';exit;
		}else{
			RedisCluster::getInstance()->set($key_lock,true,300);
		}

		$id = RedisCluster::getInstance()->get($key);
		$id = CountGoodsModel::saveDataOneDate(TIME_TIME,$id);
		if($id == 0){
			RedisCluster::getInstance()->remove($key);
		}else{
			RedisCluster::getInstance()->set($key,$id);			
		}
		RedisCluster::getInstance()->remove($key_lock);
	}

	//统计昨天来源信息
	public function actionCountYesterdayGoods(){


		set_time_limit(0);
		$intTimeYesterday = strtotime('yesterday') + 1;
		$key_lock = 'GuestController_actionCountYesterdayGoods_lock';
		$key = 'GuestController_actionCountYesterdayGoods_goods'.date('d',$intTimeYesterday);
		if(RedisCluster::getInstance()->get($key_lock)){
			echo '还有任务在执行';exit;
		}else{
			RedisCluster::getInstance()->set($key_lock,true,300);
		}

		$id = RedisCluster::getInstance()->get($key);
		$id = CountGoodsModel::saveDataOneDate($intTimeYesterday,$id);
		RedisCluster::getInstance()->set($key,$id,3600);
		RedisCluster::getInstance()->remove($key_lock);
	}	

}