<?php
/**
 * 记录相关的数据
 */
class PublicController extends YzwController{
	//错误页面
	public function actionError(){
		set_time_limit(0);
		for ($i=0; $i < 50; $i++) { 
			$datas = RedisCluster::getInstance()->pop('public_error');
			if(!$datas){
				break;
			}
			$model = new ErrorLogModel();
			$model->attributes = $datas;
			$model->save(false);
		}

	}


    public function actionSlow(){
        set_time_limit(0);
        $cache = yii::app()->cache->get('WarningController_actionSlow');
        if(!empty($cache)){
            echo '正在锁定';
            exit;
        }else{
            yii::app()->cache->set('WarningController_actionSlow', true, 60);
        }
        $intCount = 50;
        for($i = 1;$i<=$intCount;$i++){

            $arrData = RedisCluster::getInstance()->pop('slow_record');
            if (empty($arrData)) {
                yii::app()->cache->delete('WarningController_actionSlow');
                echo '执行完,没有记录</br>'; 
                return;               
            }else{
                $objModel = new SlowModel();   
                $objModel->attributes = $arrData;
                $objModel->save(false);
                echo "第{$i}次成功添加</br>";   
            }                       
        }
        yii::app()->cache->delete('WarningController_actionSlow');
        echo '执行完成</br>';
        exit;        
    }


    /**
     * 临时发邮件的
     * @return [type] [description]
     */
    public function actionSendEmail(){
        set_time_limit(0);
        $cache = yii::app()->cache->get('PublicController_actionSendEmail');
        if(!empty($cache)){
            echo '正在锁定';
            exit;
        }else{
            yii::app()->cache->set('PublicController_actionSendEmail', true, 120);
        }
        $mail = Yii::createComponent('application.extensions.mailer.EMailer');
        $redis = RedisCluster::getInstance();        
        $key = 'pisendemail';
        $title = '妈咪街招商';
        $content = '<div><br></div><div><includetail><div><div><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp;<font face="黑体">&nbsp; &nbsp;<font size="5" color="#ff99cc"><b>2015年-紧跟"妈咪街"号母婴电商航母</b></font></font></p><p class="MsoNormal"><font size="5" color="#ff99cc" face="黑体"><b>&nbsp; &nbsp; 开启你的淘金之旅</b></font></p><p class="MsoNormal"><font size="5" color="#ff99cc"><b><br></b></font></p><p class="MsoNormal"><b style="color: rgb(255, 0, 0); font-family: 宋体; font-size: 14pt; line-height: 1.5;">&nbsp; &nbsp; &nbsp;</b><b style="font-family: 宋体; line-height: 1.5;"><font color="#ff99cc" size="4">对不起，妈咪街来晚了！</font></b></p><p class="MsoNormal"><font color="#ff99cc" size="4"><span style="font-family: 宋体; line-height: 1.5;"><span id="_editor_bookmark_start_4" style="display: none; line-height: 0px;">‍</span></span><span style="font-family: 宋体;">&nbsp; &nbsp;&nbsp; &nbsp;据我们的用户流量数据统计库显示，1折网总用户中</span><span style="font-family: 宋体;">年轻的妈咪群体</span><span style="font-family: 宋体;">占据相当大的比重</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><font color="#ff99cc"><font size="4"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp; 现在，我们宣布：专业化的时尚妈咪购物商城——妈咪街将于</span><span style="font-family: 宋体;">1月10日正式</span><span style="font-family: 宋体;">上线！！！</span></font><span style="mso-spacerun:\'yes\'; font-family:宋体; font-size:10.5000pt; mso-font-kerning:1.0000pt; "><o:p></o:p></span></font></p><p class="MsoNormal"><span style="mso-spacerun:\'yes\'; font-family:宋体; font-size:10.5000pt; mso-font-kerning:1.0000pt; ">&nbsp;</span></p><p class="MsoNormal"><span style="mso-spacerun:\'yes\'; font-family:宋体; font-size:10.5000pt; mso-font-kerning:1.0000pt; ">&nbsp; &nbsp; &nbsp; <font color="#808080">&nbsp; </font></span><font size="4" color="#808080"><span style="font-family: 宋体;">妈咪街招商启动啦~ </span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp; 妈咪街需要你：母婴产品！品质保证！</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp; 我们有责任，有义务，为妈咪们找那家“对的店”！</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><font size="4"><span style="font-family: 宋体;"><font color="#808080">&nbsp; &nbsp; &nbsp; 一群时尚而有品味的妈咪们期待看到你店里的好产品！</font></span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><span style="font-family: 宋体;"><font size="4">&nbsp;</font></span><span style="mso-spacerun:\'yes\'; font-family:宋体; font-size:10.5000pt; mso-font-kerning:1.0000pt; "><o:p></o:p></span></p><p class="MsoNormal"><font face="宋体" style="font-size: 10.5pt;">&nbsp; &nbsp; &nbsp; &nbsp; </font><b><font face="黑体" size="5" color="#ff99cc">妈咪街首期招商的福利在这里，</font></b></p><p class="MsoNormal"><b><font face="黑体" size="5" color="#ff99cc">&nbsp; &nbsp; 看过来看过来 </font></b><span style="font-family: 宋体; line-height: 1.5;"><b><font color="#ff99cc">～</font></b><span id="_editor_bookmark_start_5" style="display: none; line-height: 0px;">‍</span></span></p><p class="MsoNormal"><b><font face="黑体" size="5" color="#ff99cc"><br></font></b></p><p class="MsoNormal" style="margin-left:18.0000pt; text-indent:-18.0000pt; mso-list:l0 level1 lfo1; "><span style="font-family:Calibri; font-weight:bold; font-size:10.5000pt; mso-font-kerning:1.0000pt; ">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><font size="4" color="#808080"><span style="font-family: Calibri; font-weight: bold;"> &nbsp;1. &nbsp; &nbsp;</span><span style="font-family: 宋体; font-weight: bold;">0<font face="宋体">流量成本 ，先成交后付款</font></span><span style="font-family: Calibri; font-weight: bold;">&nbsp;</span><span style="font-family: 宋体; font-weight: bold;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 现在起，入驻商城并报名活动的商家将由妈咪街特别推送至各大</span><span style="font-family: 宋体; font-weight: bold;">流量</span><span style="font-family: 宋体;">板块，占据更多免费流量资源。</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 不用你花费任何推广费用，即享妈咪街成熟的流量用户。</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 抢先入驻，被收藏被购买被妈咪们看中并成为你的死忠粉的几率将大大提升！！！</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp;</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; text-indent:-18.0000pt; mso-list:l0 level1 lfo1; "><font size="4" color="#808080"><span style="font-family: 宋体; font-weight: bold;">&nbsp; &nbsp; &nbsp; 2. </span><span style="font-family: 宋体; font-weight: bold;">0成本入驻 ，0风险</span><span style="font-family: 宋体; font-weight: bold;">&nbsp;</span><span style="font-family: 宋体; font-weight: bold;">&nbsp;</span><span style="font-family: 宋体; font-weight: bold;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 现在起，截至3月30日全场免技术服务费</span><span style="font-family: 宋体;">，更惊喜的是</span><span style="font-family: 宋体;">入驻商城即享全年8折技术服务费。</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp; &nbsp; 商家入驻妈咪街无需缴交入驻费用，只需缴交保证金。保证金用于保证商家按照妈咪街的规则进行</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; "><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp; &nbsp; 销售活动，我们必须保证所有入驻妈咪街的优质商家的利益，保证 所有妈咪们的合法权益，并且商</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 家退出妈咪街时可申请</span><span style="font-family: 宋体; font-weight: bold;">退还</span><span style="font-family: 宋体;">，运营完全</span><span style="font-family: 宋体; font-weight: bold;">０风险</span><span style="font-family: 宋体;">！让您的入驻毫无后顾之忧！</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp;</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; text-indent:-18.0000pt; mso-list:l0 level1 lfo1; "><font size="4" color="#808080"><span style="font-family: 宋体; font-weight: bold;">&nbsp; &nbsp; &nbsp; 3. </span><span style="font-family: 宋体; font-weight: bold;">先入为主</span><span style="font-family: 宋体; font-weight: bold;">&nbsp;，</span><span style="font-family: 宋体; font-weight: bold;">占据好位置</span><span style="font-family: 宋体; font-weight: bold;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 偷偷告诉你：前期入驻门槛较低哦～　门槛低　收益高　这样的好事　</span><span style="font-family: 宋体;">表怪我　现在才告诉你～</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 视觉经济下，好位置的重要性就不多说啦~</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp;</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; text-indent:-18.0000pt; mso-list:l0 level1 lfo1; "><font size="4" color="#808080"><span style="font-family: Calibri; font-weight: bold;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;4. &nbsp; &nbsp;</span><span style="font-family: 宋体; font-weight: bold;">时尚母婴精准定位，用户黏度高</span><span style="font-family: Calibri; font-weight: bold;">&nbsp;</span><span style="font-family: 宋体; font-weight: bold;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp; &nbsp; 主打时尚母婴市场，定位精准，粘度高招商类目：女装、食品、用品、化妆品、玩具、书籍、童装、</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 童鞋、营养品等</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp; &nbsp; 你店里有的，都是妈咪们需要的，谁让最大的购物消费群体就是她们呢，而她们又恰恰在我们这里，</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 我们就是有这样的优势，就是这么任性~ 　</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp;</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; text-indent:-18.0000pt; mso-list:l0 level1 lfo1; "><font size="4" color="#808080"><span style="font-family: 宋体; font-weight: bold;">&nbsp; &nbsp; &nbsp; 5. </span><span style="font-family: 宋体; font-weight: bold;">15天结清货款，资金周转快</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="text-indent:21.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 妈咪街入驻商家在平台上所有交易额，采用１５天结清模式，绝不压款，提升商家资金周转速度。</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp; &nbsp;钱的事儿，绝对严肃起来！</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp;</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; text-indent:-18.0000pt; mso-list:l0 level1 lfo1; "><font size="4" color="#808080"><span style="font-family: 宋体; font-weight: bold;">&nbsp; &nbsp; &nbsp; 6. </span><span style="font-family: 宋体; font-weight: bold;">年5000万广告及返利支持</span><span style="font-family: 宋体;">&nbsp;</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="text-indent:21.0000pt; mso-char-indent-count:2.0000; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 我们推广渠道有哪些呢？</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="text-indent:21.0000pt; mso-char-indent-count:2.0000; "><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp; &nbsp; 商城入口遍及各大浏览器和网站首页！你若在360搜索、百度引擎、好123，毒霸网址大全、2345</font></span></p><p class="MsoNormal" style="text-indent:21.0000pt; mso-char-indent-count:2.0000; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 网址导航、金山购物、360购物、一淘金山购物等等看见我们的商城入口那就是常态！</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="text-indent:21.0000pt; mso-char-indent-count:2.0000; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 年度广告及推广费用预算超3000万</span><span style="font-family: 宋体;">！</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="text-indent:21.0000pt; mso-char-indent-count:2.0000; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 日UV20万以上精准目标客群看见你的商品！</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="text-indent:21.0000pt; mso-char-indent-count:2.0000; "><span style="font-family: Calibri;"><font size="4" color="#808080">&nbsp;</font></span></p><p class="MsoNormal" style="text-indent:21.0000pt; mso-char-indent-count:2.0000; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 我们的促销频率是怎样的呢？</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="text-indent:21.0000pt; mso-char-indent-count:2.0000; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 每日、每周、每季接连不断的促销活动+多种促销活动方式+大力度的节假日促销返利</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="text-indent:21.0000pt; mso-char-indent-count:2.0000; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; 渠道</span><span style="font-family: 宋体;">，平台，流量用户，我们通通都给你~&nbsp; </span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp;</font></span></p><p class="MsoNormal" style="margin-left:18.0000pt; text-indent:-18.0000pt; mso-list:l0 level1 lfo1; "><font size="4" color="#808080"><span style="font-family: 宋体; font-weight: bold;">&nbsp; &nbsp; &nbsp; &nbsp;7. </span><span style="font-family: 宋体; font-weight: bold;">专业化运营服务团队</span><span style="font-family: 宋体;">&nbsp;</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp;一折网原班人马倾力打造，这样的实力，你值得拥有~ </span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp;为了满足妈咪街的时尚母婴定位，重金聘请</span><span style="font-family: 宋体; font-weight: bold;">时尚专业人员</span><span style="font-family: 宋体;">、</span><span style="font-family: 宋体; font-weight: bold;">母婴顾问</span><span style="font-family: 宋体;">１００余名。</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp;其中来自时尚杂志、广告公司的</span><span style="font-family: 宋体; font-weight: bold;">资深编辑、设计、品牌推广</span><span style="font-family: 宋体;">人员高达50名。</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal" style="margin-left:18.0000pt; "><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp;为树立母婴时尚新风向标全力出击。</span><span style="font-family: 宋体;">&nbsp;</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><span style="font-family: 宋体;"><font size="4" color="#808080">&nbsp;</font></span></p><p class="MsoNormal"><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp; &nbsp; 妈咪街，值得你入驻。</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp; &nbsp; 猛戳商家群号：424071021　马上加入　</span><span style="font-family: 宋体;">1</span><span style="font-family: 宋体;">折网已有的成熟用户群</span><span style="font-family: 宋体;">&nbsp;</span><span style="font-family: 宋体;">千万妈咪在这里等你～</span><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp; &nbsp; 我们的网址： </span><a href="http://www.mamijie.com"><span class="15" style="font-family: 宋体;">www.mamijie.com</span></a><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><font size="4" color="#808080"><span style="font-family: 宋体;">&nbsp; &nbsp; &nbsp; &nbsp; 1折网首页也可以直接进入活动报名地址：</span><a href="http://www.mamijie.com/user/baoming/create.html"><span class="15" style="font-family: 宋体;">http://www.mamijie.com/user/baoming/create.html</span></a><span style="font-family: 宋体;"><o:p></o:p></span></font></p><p class="MsoNormal"><font color="#808080"><span style="font-family: 宋体;"><font size="4">&nbsp; &nbsp; &nbsp; &nbsp; 品牌入驻地址：</font></span><span class="15" style="font-family: 宋体;"><a href="http://www.mamijie.com/user/merchantsSettled/introduction.html"><font size="4">http://www.mamijie.com/user/merchantsSettled/introduction.html</font><span id="_editor_bookmark_start_0" style="display: none; line-height: 0px;">‍</span></a></span><span style="mso-spacerun:\'yes\'; font-family:宋体; font-size:10.5000pt; mso-font-kerning:0.0000pt; "><o:p></o:p></span></font></p><p class="MsoNormal"><br></p><p class="MsoNormal">&nbsp; <font color="#00ffff">&nbsp;</font><u><font color="#00ffff">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</font><font color="#ff6600">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</font><font color="#ff00ff">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </font><font color="#00ff00">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</font><font color="#3366ff">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</font><font color="#ff6600">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</font></u></p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <font size="4">&nbsp; &nbsp; &nbsp;<font face="宋体"> &nbsp;<font color="#808080"> </font></font></font><b><font face="宋体"><font size="4"><font color="#808080">全国服务电话 </font>&nbsp;<font color="#ff99cc"> </font></font><font size="5" color="#ff99cc">4006-398-090 </font></font></b></p></div></div></includetail></div>';
        for ($i=0; $i < 30; $i++) {
            echo "</br>"; 
            $email = $redis->pop($key);
            echo $email;
            echo '-';
            //$email = str_replace('\\', '', str_replace('"', '', $email));
            if ($mail->sendEmail($email, $title, $content)) {
                echo 'ok';
            }else{
                $redis->push('pisendemail', $email);
                echo 'no';
            }          
        }
        yii::app()->cache->delete('PublicController_actionSendEmail');
    }

    public function actionCeshi(){

        set_time_limit(0);
/*        $datas = MemberModel::model()->findAll();
        foreach ($datas as $key => $data) {
            if($data->Status == 0){
                $data->Status = 1;
            }else if($data->Email && $data->Phone){
                $data->Status = 4;
            }else if($data->Email){
                $data->Status = 2;
            }else if($data->Phone){
                $data->Status = 3;
            }
            $data->save(false, array('Status'));
        }

        echo 'ok';exit;*/

        echo time();
        echo '</br>';
        $mail = Yii::createComponent('application.extensions.mailer.EMailer');
        $title = 'sdsd';
        $content = 'sdsdsdsfagasdfasdsd';
        $email = '385612297@qq.com';

        if ($mail->sendEmail($email, $title, $content)) {
            echo 'ok';
        }else{
            echo 'no';
        }         
    }

}