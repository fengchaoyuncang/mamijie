<?php

/**
 * 预警
 * File Name：WarningController.php
 * File Encoding：UTF-8
 * File New Time：2014-10-20 17:09:43
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class WarningController extends YzwController {

    public function actionIndex() {
        header("Content-type: text/html; charset=utf-8");
        echo "检测开始：";
        //redis 监测
        RedisCluster::getInstance()->push('warning_demo', array('demo'));
        $warning = RedisCluster::getInstance()->pop('warning_demo');
        if (empty($warning)) {
            $notice = "系统监测到1zw Redis可能已经当机，请尽快检查。";
            Sns::getInstance()->mobile('15392421692')->data($notice)->send();
            Sns::getInstance()->mobile('15859228363')->data($notice)->send();
        } else {
            echo "<br/>Redis检测正常";
        }
        Yii::app()->cache->set('warning_demo', 1, 3);
        $warning = Yii::app()->cache->get('warning_demo');
        if (empty($warning)) {
            $notice = "系统监测到1zw Memcache可能已经当机，请尽快检查。";
            Sns::getInstance()->mobile('15392421692')->data($notice)->send();
            Sns::getInstance()->mobile('15859228363')->data($notice)->send();
        } else {
            echo "<br/>Memcache检测正常";
        }
    }

    //获取memcached的信息
    public function actionMemcache(){
        print_r(yii::app()->cache->getMemCache()->getExtendedStats());exit;
    }

    public function actionRedis(){
        $redis = RedisCluster::getInstance();
        print_r($redis);exit;
    }

}
