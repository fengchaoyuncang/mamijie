<?php

/**
 * 跟金额有关的计划任务
 */
class MoneyController extends YzwController {
	//找到结算后三天的商品，解冻此商品
	//现在的时间 - 记录的时间 >= 3天的时间 符合条件
	// 记录的时间 <  现在的时间 - 3天的时间
	public function actionGoodsBm(){
		set_time_limit(0);
		if(!RedisCluster::getInstance()->isConnect()){
			return false;
		}
		$key = "MoneyController_actionGoodsBm";		
		$cache = yii::app()->cache->get($key);
		if($cache){
			exit('正在锁定');
		}else{
			yii::app()->cache->set($key, 1, 60);
		}


		$id = RedisCluster::getInstance()->get('cron_MoneyController_actionGoodsBm_goodsID');
		if(empty($id)){
			$id = RedisCluster::getInstance()->set('cron_MoneyController_actionGoodsBm_goodsID',0);
		}

		$datas = GoodsBmModel::model()->findAll(BaseModel::getC(array('order'=>'GoodsBmID asc','IsAccounts' => 1, 'PlanAccounts' => 0, 'GoodsBmID' => array('sign' => '>', 'data' => $id), 'AccountsTime' => array('sign' => '<', 'data' => TIME_TIME-86400*3))));
		if(!$datas){
			$id = RedisCluster::getInstance()->set('cron_MoneyController_actionGoodsBm_goodsID',0);
		}
		//每个循环的去结算
		//验证 需要有报名ID，金额等条件OK
		//扣了的钱已经扣了。
		foreach ($datas as $key => $GoodsBmModel) {
			try {
				$userID = $GoodsBmModel->UserID;
				MemberMoneyChangeModel::getLock($userID);
				//这个商品总共冻结了多少钱
				$money = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'RelationID' => $GoodsBmModel->GoodsBmID, 'Type' => 31));

				$transaction=Yii::app()->db->beginTransaction();
				//先解冻这部份钱，再从总额扣除这部份钱		
	            $data = array(
	                'UserID' => $userID,
	                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
	                'Money' => -$money,
	                'Type' => 31,
	                'ChangeType' => 2,
	                'RelationID' => $GoodsBmModel->GoodsBmID,//商品报名ID
	                'AddTime' => TIME_TIME,
	                'NumOnly' => $userID . '_31_' . $GoodsBmModel->GoodsBmID .  '_'. TIME_TIME,
	                'Remarks' => '计算解冻',
	            );
                if(!MemberMoneyChangeModel::saveData($data)){
                    throw new Exception("金额添加异常");                    
                }

	            $GoodsBmModel->PlanAccounts = 1;
	            $GoodsBmModel->PlanAccountsTime = time();
	            $GoodsBmModel->save(false);

				$transaction->commit();	
				MemberMoneyChangeModel::releaseLock($userID);

				

			} catch (Exception $e) {
		        if(!empty($transaction)){
		          $transaction->rollBack();
		        }
		        if(defined('USER_MONEY_LOCK')){
		        	MemberMoneyChangeModel::releaseLock($userID);
		        }
		        echo $GoodsBmModel->GoodsBmID . 'ok';	        
				continue; 	
			}
		}
		$id = RedisCluster::getInstance()->set('cron_MoneyController_actionGoodsBm_goodsID',$GoodsBmModel->GoodsBmID);

        //请求删除
        yii::app()->cache->delete($key);

	}

	//保证金  当用户申请需要退保证金的时候给用户提
	public function actionBaoZhengJing(){
		//什么时候退还保证金//需要申请，，，
	}

}
