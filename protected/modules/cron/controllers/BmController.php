<?php

//计划任务，发送报名审核结果，邮件和短信通知
class BmController extends YzwController {

    public function actionIndex() {
        set_time_limit(0);
        $AdminUser = Yii::createComponent('application.modules.admin.models.AdminUser');
        $data = array();
        //从redis读取20条,并且组织内容
        for ($i=0; $i < 20; $i++) { 
                $info = RedisCluster::getInstance()->pop('snsNotice');
                print_r($info);
                if (empty($info) || !is_array($info)) {
                    break;
                }
                //根据报名表id，再去核对
                $bmInfo = GoodsBmModel::model()->findByPk($info['GoodsBmID']);
                if (empty($bmInfo) || $bmInfo['UserID'] != $info['UserID']) {
                    continue;
                }
                $adminInfo = $AdminUser->getUserInfo((int) $info['AdminID']);//审核人员
                if ($info['Status'] == 1) {//审核通过
                    $notice = '您报名的商品(' . $info['ProductID'] . ')已成功排期，时间为 ' . date('Y-m-d', $info['StartTime']) . ' 至 ' . date('Y-m-d', $info['EndTime']) . '，请在活动前一天 17:00 前按照活动要求进行设置。';
                    $emailBody = "恭喜亲通过了【妈咪街】活动！<br/>
    &nbsp;&nbsp;你的宝贝将" . date('Y-m-d H:i:s', $info['StartTime']) . "上线展示，将" . date('Y-m-d H:i:s', $info['EndTime']) . "下线。 <br/>
    &nbsp;&nbsp;活动期间：<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;1、不得更改标题，活动价格和无故下架，logo在宝贝描述内置顶并不许添加其他活动的logo<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;2、不能私自添加邮费，单件不发货。更改活动价格和无故下架 ，活动结束后一个月不得低于活动价格出售<br/>
    &nbsp;&nbsp;请掌柜们注意：<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;违反以上规则，一经发现立即下架，永久拉黑处理，永不合作，请商家慎重！<br/>
    &nbsp;&nbsp;提醒：<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;1:最近发现部分商家在预告后恶意修改成区间价格，如发现进行下架拉黑处理！<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;2:商家在上活动期间收到非活动方发出的自称活动人员的信息，要求加QQ的，请不要相信。<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;3:有问题直接与活动主办方联系.咨询审核旺旺!<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;4:最终活动解释权归主办方所有。<br/>
    预告请到妈咪街官网 www.mamijie.com 查看！谢谢各位商家的支持！<br/>
    活动期间违规者将不再通知修改，直接进行删除处理！！请各位商家自觉点！";
                } else if ($info['Status'] == 2) {//审核不通过
                    $notice = '您报名的商品(' . $info['ProductID'] . ') 审核不通过，原因：' . $info['Excuse'] . '。';
                    $emailBody = "
    通知：<br/>
    &nbsp;&nbsp;您于 " . date('Y--m-d H:i:s', $info['AddTime']) . " 在妈咪街报名的活动，商品&nbsp;<b>{$info['GoodsName']}</b>&nbsp;审核不通过，原因：<br/>
    &nbsp;&nbsp;" . $info['Excuse'] . "<br/>
    如有问题，请及时联系旺旺：<a href=\"http://amos.im.alisoft.com/msg.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid={$adminInfo['wangwang']}\" target=\"_blank\">{$adminInfo['wangwang']}<img src=\"http://amos.im.alisoft.com/online.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid={$adminInfo['wangwang']}\" ></a>。";
                } else if ($info['Status'] == 3) {//邮寄样品
                    $notice = '您报名的商品(' . $info['ProductID'] . ') ，等待邮寄样品进行进一步审核，邮寄地址：福建省 厦门市 集美区 杏林湾商务营运中心2号楼24层厦门易折网络科技有限公司（最好发顺丰，节省时间）(' . $adminInfo['wangwang'] . ' 收) 180-3015-1020';
                    $emailBody = "
    通知：<br/>
    &nbsp;&nbsp;您于 " . date('Y--m-d H:i:s', $info['AddTime']) . " 在妈咪街报名的活动，商品&nbsp;<b>{$info['GoodsName']}</b>&nbsp;已通过初审，等待邮寄样品进行进一步审核。<br/>
    &nbsp;&nbsp;邮寄地址：福建省 厦门市 集美区 杏林湾商务营运中心2号楼24层厦门易折网络科技有限公司（最好发顺丰，节省时间）({$adminInfo['wangwang']} 收) 180-3015-1020 <br/>
    " . ($info['Excuse'] ? "&nbsp;&nbsp;审核建议：{$info['Excuse']} <br/>" : "") . "
    如有问题，请及时联系旺旺：<a href=\"http://amos.im.alisoft.com/msg.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid={$adminInfo['wangwang']}\" target=\"_blank\">{$adminInfo['wangwang']}<img src=\"http://amos.im.alisoft.com/online.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid={$adminInfo['wangwang']}\" ></a>。";
                }








                //短信
                Sns::getInstance()->send($info['Phone'], $notice);
                echo "<br/>短信发送成功...";
                //Sns::getInstance()->mobile('15392421692')->data($notice.$info['phone'])->send();
                //邮件
                try {
                    $mail = Yii::createComponent('application.extensions.mailer.EMailer');
                    $status = $mail->sendEmail($info['Email'], '您报名的商品(' . $info['ProductID'] . ') 审核结果提醒', $emailBody);
                    echo $status ? "<br/>邮件发送成功..." : "<br/>邮件发送失败...";
                    //$mail->sendEmail('2428552416@qq.com', '您报名的商品(' . $info['ProductID'] . ') 审核结果提醒' . $info['email'], $emailBody);
                } catch (Exception $exc) {
                    
                }
                //站内消息通知
                $info['title'] = $info['GoodsName'] . ' 审核结果通知！';
                $status = MessageModel::model()->saveData(array('Type' => 1, 'UserID' => $info['UserID'], 'Image' => $info['Image'], 'Title' => $info['title'], 'Content' => $emailBody));
                echo $status ? "<br/>站内消息发送成功..." : "<br/>站内消息发送失败...";

                unset($mail, $info);
                //暂停1秒执行
                sleep(1);            
        }
    }
}
