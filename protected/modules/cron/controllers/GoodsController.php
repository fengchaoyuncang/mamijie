<?php
/**
 * 记录相关的数据
 */
class GoodsController extends YzwController{
	//收藏的相关的数据更新。更新商品表的浏览次数
	//当这个商品已经下架了或者过期了则自动的把这个redis记录给删除。
	public function actionConnect(){
		set_time_limit(0);
		$redis = RedisCluster::getInstance()->getRedis();
		$key = 'membercollect_hash';
		$datas = $redis->hKeys($key);
		foreach ($datas as $data) {
			if($redis->hGet($key,$data) === 0){
				$value = explode('_', $data);
				$uid = $value[0];
				$goodsID = $value[1];
				$model = GoodsModel::getOne($goodsID);
				$arr = array(
					'GoodsID' => $goodsID,
					'GoodsName' => $model->GoodsName,
					'Pprice' => $model->Pprice,
					'Image' => $model->Image,
					'UserID' => $uid,
				);
				$collectModel = new MemberCollectModel;
				$collectModel->attributes = $arr;
				$collectModel->save();
				$redis->hSet($key,$data, 1);
			}	
		}		
	}
}