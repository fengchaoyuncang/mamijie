<?php

/**
 * 前台会员通行证
 * File Name：PassportAdminService.php
 * File Encoding：UTF-8
 * File New Time：2014-4-29 9:22:06
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class UserPassport extends CApplicationComponent implements ArrayAccess {

    //存储用户uid的Key
    const key = 'user_id';
    const oauth_key = 'oauth_user_id';

    //错误密码
    protected $error = NULL;
    //用户信息
    protected static $userInfo = array();
    //返回地址
    public $returnUrl = '/user/member/index.html';

    public function __get($name) {
        if (isset(self::$userInfo[$name])) {
            return self::$userInfo[$name];
        } else {
            $userInfo = $this->getInfo();
            if (!empty($userInfo)) {
                return $userInfo[$name];
            }
            return NULL;
        }
    }

    /**
     * 获取错误信息
     * @return type
     */
    public function getError() {
        return $this->error;
    }

    /**
     * 获取当前登录用户资料  加关联登录
     * 如果获取不到则获取其关联登录的信息
     * @return array 
     */
    public function getInfo() {
        if (empty(self::$userInfo)) {
            $uid = $this->getCookieUid();
            self::$userInfo = $this->getLocalUser((int) $uid);
            if (!empty(self::$userInfo)) {
                self::$userInfo['id'] = self::$userInfo['UserID'];
                self::$userInfo['name'] = self::$userInfo['UserName'];
            } else {
                $cookie = Yii::app()->request->getCookies();
                unset($cookie[self::key]);
            }
            //不管怎么样然后我还要看一下是否需要获取关联的登录            
            if($OauthID = $this->isOauth()){
                //获取新的东西
                $model = MemberOauthModel::model()->findByPk($OauthID);
                $oatthUserInfo = $model->get_user_info();
                self::$userInfo['oatthUserInfo'] = $oatthUserInfo;
                self::$userInfo['OauthID'] = $OauthID;
                self::$userInfo['id'] = self::$userInfo['uid'] = 0;
            }
        }
        return !empty(self::$userInfo) ? self::$userInfo : false;
    }

    /**
     * 获取cookie中的id
     * @return boolean
     */
    public function getCookieUid() {
        $cookie = Yii::app()->request->getCookies();
        if (!isset($cookie[self::key])) {
            return false;
        }
        $ciphertext = $cookie[self::key]->value;
        if (empty($ciphertext)) {
            return false;
        }
        $uid = Encrypt::authcode($ciphertext);
        if (empty($uid)) {
            return false;
        }
        return $uid;
    }

    /**
     * 检查是否已经登录，有登录返回登录uid
     * 如果返回一个数组说明是已用QQ关联登录了。。
     * @return boolean
     */
    public function isLogin() {
        //获取cookie中的用户id
        $uid = $this->getCookieUid();
        if (empty($uid) || (int) $uid < 1) {
            if($id = $this->isOauth()){
                //如果缓回一个数据
                return array('id' => $id);
            }
            return false;
        }
        return $uid;
    }

    /**
     * 登录后台并进行身份验证
     * @param type $identifier 验证条件，直接uid，用户名
     * @param type $password 明文密码
     * @param type $is_remeber_me 记录时间
     * @return boolean
     */
    public function login($identifier, $password, $is_remeber_me = 2592000) {
        if (empty($identifier) || empty($password)) {
            $this->error = '帐号或者密码为空！';
            return false;
        }
        //进行登录验证
        $userInfo = $this->getLocalUser($identifier, $password);
        if (empty($userInfo)) {
            return false;
        }
       if (empty($userInfo['Status'])) {
           $this->error = '你的账号被锁定';
           return false;
       }

        //增加登录送积分
        //明文密码
        $userInfo['expresslyPassword'] = $password;
        return $this->registerLogin($userInfo, $is_remeber_me);
    }

    /**
     * 注册用户登录状态
     * @param array $user 用户基本信息
     * @param type $is_remeber_me 有效期，秒
     * @return boolean
     */
    public function registerLogin($user, $is_remeber_me = 604800) {
        if (is_object($user) && $user instanceof MemberModel) {
            $user = $user->attributes;
        }

        if (empty($user)) {
            return false;
        }
        //更新最后登录时间
        $model = MemberModel::model()->findByPk($user['UserID']);
        $model->LastLoginIP = Tool::getForwardedForIp();
        $model->LatLoginTime = time();
        $model->save(false, array('LastLoginIP', 'LatLoginTime'));
        //保持登录状态
        $cookie = new CHttpCookie(self::key, Encrypt::authcode($user['UserID'], ''));
        //设置有效期
        $cookie->expire = time() + (int) $is_remeber_me;
        Yii::app()->request->cookies[self::key] = $cookie;
        if(GROUP_NAME != 'admin'){
            //生成登录记录
            $data = array(
                'UserID' => $user['UserID'],
                'Type' => 1,            
            );
            MemberOperationModel::saveData($data);            
        }



        return true;
    }

    /**
     * 注销登录状态
     * @return boolean
     */
    public function logout() {
        //生成登录记录
        if(is_array($this->isLogin())){
            Yii::app()->request->getCookies()->remove(self::oauth_key);
            return true;            
        }
        $data = array(
            'UserID' => yii::app()->user->id,
            'Type' => 2,            
        );
        MemberOperationModel::saveData($data);        
        Yii::app()->request->getCookies()->remove(self::key);
        if($this->isOauth()){
            Yii::app()->request->getCookies()->remove(self::oauth_key);
        }
        return true;
    }

    /**
     * 根据提示符(username)和未加密的密码(密码为空时不参与验证)获取本地用户信息
     * @param type $identifier 为数字时，表示uid，其他为用户名
     * @param type $password 明文密码
     * @return boolean 成功返回用户信息array()，否则返回布尔值false
     */
    public function getLocalUser($identifier, $password = null) {
        if (empty($identifier)) {
            return false;
        }
        $model = MemberModel::model();
        $userInfo = $model->getUserInfo($identifier);
        if (empty($userInfo)) {
            $this->error = '该用户不存在！';
            return false;
        }
        //密码验证
        if (!empty($password)) {
            if (MemberModel::encrypt($password) != $userInfo['Password']) {
                $this->error = '帐号密码错误！';
                return false;
            }
        } 
        //去除敏感
        unset($userInfo['Password']);
        return $userInfo;
    }

    public function offsetExists($index) {
        return isset(self::$userInfo[$index]);
    }

    public function offsetGet($index) {
        return self::$userInfo[$index];
    }

    public function offsetSet($index, $newvalue) {
        
    }

    public function offsetUnset($index) {
        
    }
    //临时存储的东西
    public function getFlash($key = 'gweasdadsdfweas'){
        $mixData = Yii::app()->session[$key];
        Yii::app()->session->remove($key);
        return $mixData;
    }

    public function setFlash($value, $key = 'gweasdadsdfweas'){
        Yii::app()->session->add($key, $value);
    }

    public function hasFlash($key = 'gweasdadsdfweas'){
        return Yii::app()->session[$key];
    }

    //是否绑定登录
    //如果绑定登录，则返回数据库中其主键，
    //如果没有绑定登录则返回false
    public function isOauth(){
        $cookie = Yii::app()->request->getCookies();
        if (!isset($cookie[self::oauth_key])) {
            return false;
        }
        $ciphertext = $cookie[self::oauth_key]->value;
        if (empty($ciphertext)) {
            return false;
        }
        $OauthID = Encrypt::authcode($ciphertext);
        if (empty($OauthID)) {
            return false;
        }
        return $OauthID;
    }


    //绑定登录,,  现在数据已经创建了
    //然后接着就是看是否需要
    //登录进来，看是否OK。。。。。。。
    //$oauthModel为此Model
    public function oauth($oauthModel){
        if($oauthModel->UserID){//如果绑定登录有绑定QQ号的话则为其登录,变成登录状态
            $userinfo = $this->getLocalUser($oauthModel->UserID);
            $this->registerLogin($userinfo);
        }
        //为自己保存数据
        $cookie = new CHttpCookie(self::oauth_key, Encrypt::authcode($oauthModel->OauthID, ''));
        //设置有效期
        $cookie->expire = time() + 86400;
        Yii::app()->request->cookies[self::oauth_key] = $cookie;        
    }

}
