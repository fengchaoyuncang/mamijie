<?php

/**
 * 数据同步
 * File Name：Tongbu.php
 * File Encoding：UTF-8
 * File New Time：2014-6-6 11:06:25
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class Tongbu {

    private $data = '';

    /**
     * 链接同步服务
     * @staticvar null $handier
     * @return \TaoAPI
     */
    static public function getInstance() {
        static $handier = NULL;
        if (empty($handier)) {
            $handier = new Tongbu();
        }
        return $handier;
    }

    /**
     * 设置数据
     * @param type $data
     * @return \Tongbu
     */
    public function data($data) {
        if ($this->isDimensional($data) !== false) {
            $data = array($data);
        }
        $this->data = $data;
        return $this;
    }

    //疯狂购
    public function fengkg($data = '', $url = 'http://94258.uz.taobao.com/view/front/sync.php') {
        if (empty($data) && !is_array($data)) {
            if (empty($this->data)) {
                return false;
            }
            $data = $this->data;
            $this->data = '';
        }
        if ($this->isDimensional($data) !== false) {
            $data = array($data);
        }
        $file = '';
        foreach ($data as $val) {
            foreach ($val as $k => $v) {
                $val[$k] = iconv("UTF-8", "GB2312//IGNORE", $v);
            }
            if($val['Status'] == 5 || $val['Status'] == 4){
                $val['Status'] = 0;
            }else{
                $val['Status'] = 1;
            }
            $val['commission_rate'] = 0.05;
            $val['nick_add'] = 0;
            $val['state'] = '';
            $val['city'] = '';
            $val['is_stock'] = 0;
            $val['stock'] = 0;
            $val['freeshipping'] = 1;
            $val['is_vip'] = 0;
            $val['ModuleID'] = 0;
            $file .= urlencode($val['ProductID']) . "--wd--";
            $file .= urlencode(0) . "--wd--";
            $file .= urlencode($val['GoodsName']) . "--wd--";
            $file .= urlencode($val['Brief']) . "--wd--";
            $file .= urlencode($val['ItemType']) . "--wd--";
            $file .= urlencode($val['IsPreferential']) . "--wd--";
            $file .= urlencode($val['IsNew']) . "--wd--";
            $file .= urlencode($val['IsHot']) . "--wd--";
            $file .= urlencode(1) . "--wd--";//在首页显示
            $file .= urlencode($val['DetailUrl']) . "--wd--";
            $file .= urlencode($val['Image']) . "--wd--";
            $file .= urlencode($val['Image']) . "--wd--";
            $file .= urlencode($val['Image']) . "--wd--";
            $file .= urlencode($val['GAddTime']) . "--wd--";
            $file .= urlencode($val['Status']) . "--wd--";
            $file .= urlencode($val['Sorting']) . "--wd--";
            $file .= urlencode($val['CatID']) . "--wd--";
            $file .= urlencode($val['ModuleID']) . "--wd--";
            $file .= urlencode($val['Pprice']) . "--wd--";
            $file .= urlencode($val['commission_rate']) . "--wd--";
            $file .= urlencode($val['PromoPrice']) . "--wd--";
            $file .= urlencode($val['StartTime']) . "--wd--";
            $file .= urlencode($val['EndTime']) . "--wd--";
            $file .= urlencode($val['Nick']) . "--wd--";
            $file .= urlencode($val['nick_add']) . "--wd--";
            $file .= urlencode($val['state']) . "--wd--";
            $file .= urlencode($val['city']) . "--wd--";
            $file .= urlencode($val['is_stock']) . "--wd--";
            $file .= urlencode($val['stock']) . "--wd--";
            $file .= urlencode($val['freeshipping']) . "--wd--";
            $file .= urlencode($val['is_vip']) . "--wd--";
            $file .= urlencode($val['Sales']) . "--wd--";
            $file .= "--le--";
        }
        $post_data = array(
            "password" => "121khksdhaiu001823123kjahsdkjaoiyeiqddjsjhga",
            "result" => $file,
        );
        foreach ($post_data as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        //开发模式不同步
        if (defined('YII_DEBUG') && YII_DEBUG) {
            return true;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($post_data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        //不显示
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        //echo $result;exit;
    }

    protected function isDimensional($data) {
        if (is_array($data)) {
            return false;
        }
        return is_array(current($data)) ? true : false;
    }

}
