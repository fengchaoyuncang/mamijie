<?php

class Sns extends CComponent{
	public static $client = null;
	protected $error = null;
	private static $handier = NULL;
	private $parameters = array(
		'userId'=>'J02251',
		'password'=>'327906',
		'pszMobis'=>'',
		'pszMsg'=>'',
		'iMobiCount'=>1,
		'pszSubPort'=>1,		
	);

	public function init(){
		if(empty(self::$client)){
			self::$client=new nusoap_client('http://61.145.229.29:9003/MWGate/wmgw.asmx?wsdl');	
			self::$client->soap_defencoding='gb2312';
			self::$client->decode_utf8=true;			
		}		
	}
	public function getError(){
		return $this->error;
	}
	private function __construct(){
		$this->init();
	}
	//单例
	public static function getInstance(){
		if (empty(self::$handier)) {
			require_once(yii::app()->basePath . '/extensions/nusoap/lib/nusoap.php');
			self::$handier = new self();
		}

		return self::$handier;	
	}
	/**
	 * 发送内容
	 * @param  [type] $phone   [description]
	 * @param  [type] $content [description]
	 * @return [type]          [description]
	 */
	public function send($phone, $content){
		$this->parameters['pszMsg'] = $content;
		$this->parameters['pszMobis'] = $phone;
		$aryResult=self::$client->call('MongateCsSpSendSmsNew',$this->parameters);
		if($err = self::$client->getError()){
			$this->error = $err;
			return false;
		}else{
			//记录发送的数据
			$data = array(
				'Phone' => $phone,
				'Content' => $content,
				'UserID' => yii::app()->user->id,
			);
			PhoneDataModel::saveData($data);
			return true;
		}	
	}
}