<?php

/**
 * 获取访客基本信息
 * File Name：GuestInfo.php
 * File Encoding：UTF-8
 * File New Time：2014-8-11 9:59:22
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class GuestInfo {

    /**
     * @name 获取浏览器名称
     * @return String
     */
    static public function getBrowser() {
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $br = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/MSIE/i', $br)) {
                $br = 'MSIE';
            } elseif (preg_match('/Firefox/i', $br)) {
                $br = 'Firefox';
            } elseif (preg_match('/Chrome/i', $br)) {
                $br = 'Chrome';
            } elseif (preg_match('/Safari/i', $br)) {
                $br = 'Safari';
            } elseif (preg_match('/Opera/i', $br)) {
                $br = 'Opera';
            } else {
                $br = 'Other';
            }
        }
        return $br;
    }

    /**
     * @name 获取IP地址
     * @return String
     */
    static public function getIp() {
        if (getenv("HTTP_CLIENT_IP")) {
            $ip = getenv("HTTP_CLIENT_IP");
        } elseif (getenv("HTTP_X_FORWARDED_FOR")) {
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        } elseif (getenv("REMOTE_ADDR")) {
            $ip = getenv("REMOTE_ADDR");
        } else {
            $ip = "127.0.0.1";
        }
        return $ip;
    }

    /**
     * @name 获取字符类型
     * @return String
     */
    static public function getLang() {
        if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
            $lang = substr($lang, 0, 5);
            if (preg_match("/zh-cn/i", $lang)) {
                $lang = "简体中文";
            } elseif (preg_match("/zh/i", $lang)) {
                $lang = "繁体中文";
            } else {
                $lang = "English";
            }
            return $lang;
        }
    }

    /**
     * @name 获取操作系统名称
     * @return String
     */
    static public function getOs() {
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $OS = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/win/i', $OS)) {
                $OS = 'Windows';
            } elseif (preg_match('/mac/i', $OS)) {
                $OS = 'MAC';
            } elseif (preg_match('/linux/i', $OS)) {
                $OS = 'Linux';
            } elseif (preg_match('/unix/i', $OS)) {
                $OS = 'Unix';
            } elseif (preg_match('/bsd/i', $OS)) {
                $OS = 'BSD';
            } else {
                $OS = '未知';
            }
            return $OS;
        }
    }

    /**
     * @name 获取访客物理地址
     * @return String
     */
    static public function getAddr() {
        $ip = self::Ip();
        if ($ip == "127.0.0.1") {
            $add = "未知地址";
        } else {
            //根据新浪api接口获取
            if ($ipadd = @file_get_contents('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=' .
                            $ip)) {
                $ipadd = json_decode($ipadd);
                if (!is_object($ipadd) or $ipadd->ret == '-1') {
                    $add = '未知地址';
                } elseif (is_object($ipadd) and $ipadd->ret <> '-1') {
                    $add = $ipadd->province . $ipadd->isp;
                } else {
                    $add = '未知地址';
                }
            } else {
                $add = '未知地址';
            }
        }
        return $add;
    }

    /**
     * @name 获取访客来源地址
     * @return String
     */
    static public function getReferer() {
        $Urs = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : '';
        return $Urs;
    }

    /**
     * @name 获取访客GET查询
     * @return String
     */
    static public function getQuery() {
        if (isset($_SERVER["QUERY_STRING"])) {
            $uquery = addslashes($_SERVER["QUERY_STRING"]);
        } else {
            $uquery = '';
        }
        return $uquery;
    }

    /**
     * @name 获取访客终端综合信息
     * @return String
     */
    static public function getAgent() {
        if (isset($_SERVER["HTTP_USER_AGENT"])) {
            $uagent = addslashes($_SERVER["HTTP_USER_AGENT"]);
        } else {
            $uagent = "未知终端";
        }
        return $uagent;
    }

}
