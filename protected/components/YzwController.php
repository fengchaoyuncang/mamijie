<?php

/**
 * 妈咪街项目基础控制器基类
 */
class YzwController extends CController {

    public $title = '【妈咪街官网】母婴之家,母婴用品第一站,妈咪街做最好的母婴用品网站！';
    public $keywords = '妈咪街,妈咪街官网,母婴之家,母婴用品网站,母婴用品,母婴之家官网';
    public $description = '妈咪街(mamijie.com)专业的母婴用品网上商城,母婴电商领导者.每天9点精选上百款母婴用品,1元秒杀、1折抢购等独家特价商品,做最好的母婴用品网站.';

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = false;
    //模板输出变量
    protected $tVar = array(
        //默认提示跳转时间
        'waitSecond' => 2000,
    );

    //初始
    public function init() {
        defined('IS_POST') or define('IS_POST', Yii::app()->request->isPostRequest);
        defined('IS_AJAX') or define('IS_AJAX', Yii::app()->request->isAjaxRequest);
        //当前模块名字
        if(!is_object($this->getModule())){
            throw new CHttpException(404,'The requested page does not exist.');
        }
        defined('GROUP_NAME') or define('GROUP_NAME', $this->getModule()->getId());        
        //当前控制器名
        defined('CONTROLLER_NAME') or define('CONTROLLER_NAME', $this->getId());
        //当前时间戳 年，月，日
        defined('TIME_TIME') or define('TIME_TIME', time());
        defined('TIME_YEAR') or define('TIME_YEAR', date('Y', TIME_TIME));
        defined('TIME_MONTH') or define('TIME_MONTH', date('m', TIME_TIME));        
        defined('TIME_DAY') or define('TIME_DAY', date('d', TIME_TIME)); 

        //获取毫秒数，用于看一下哪个页面访问慢
        $time = explode (" ", microtime());  
        $time = $time[1] . ($time[0] * 1000);  
        $time2 = explode ( ".", $time );  
        $time = $time2[0];
        defined('TIME_HAOMIAO') or define('TIME_HAOMIAO', $time);  


        parent::init();
    }

    protected function beforeAction($action){
        //方法名
        defined('ACTION_NAME') or define('ACTION_NAME', $action->getId());
        Yii::app()->onEndRequest = function($event)
        {
            if(GROUP_NAME != 'admin'){
                $time = explode (" ", microtime());  
                $time = $time[1] . ($time[0] * 1000);  
                $time2 = explode ( ".", $time );  
                $time = $time2[0];
                $time = $time - TIME_HAOMIAO;
                if($time >= 400  && $time < 999999){
                    SlowModel::record($time);
                }
            }
            if(defined('USER_MONEY_LOCK')){
                MemberMoneyChangeModel::releaseLock(USER_MONEY_LOCK);
            }            
        };         
        return parent::beforeAction($action);     
    }

    /**
     * 渲染显示模板页面 $this->render(); 自动侦测
     * @param string $view 要呈现的视图的名称
     * @param array $data 分配到模板中快捷使用的变量
     * @param boolean $return 是否不直接输出返回结果
     */
    public function render($view = '', $data = null, $return = false) {
        //为空时尝试自动定位
        if (empty($view)) {
            $view = Yii::app()->getController()->getAction()->id;
        }
        //分配变量到模板
        if (!empty($data) && is_array($data)) {
            $this->assign($data);
        }
        if ($return) {
            parent::render($view, $this->tVar, $return);
        } else {
            return parent::render($view, $this->tVar, $return);
        }
    }

    /**
     * 模板变量赋值
     * @access public
     * @param mixed $name
     * @param mixed $value
     */
    public function assign($name, $value = '') {
        if (is_array($name)) {
            $this->tVar = array_merge($this->tVar, $name);
        } else {
            $this->tVar[$name] = $value;
        }
        return $this;
    }

    /**
     * 生成地址
     * @param type $route 路由 格式：[模块/控制器/]action
     * @param type $params 参数
     * @param type $ampersand
     * @return type
     */
    public static function U($route, $params = array(), $ampersand = '&') {
        if (!empty($params) && is_string($params)) {
            parse_str($params, $params);
        }
        $routeArr = explode('/', $route);
        if (count($routeArr) >= 3) {
            return Yii::app()->createUrl($route, $params, $ampersand);
        } else {
            return Yii::app()->getController()->createUrl($route, $params, $ampersand);
        }
    }

    /**
     * Ajax方式返回数据到客户端
     * @access protected
     * @param mixed $data 要返回的数据
     * @param String $type AJAX返回数据格式
     * @return void
     */
    protected function ajaxReturn($data, $type = 'JSON') {
        !isset($data['status']) ? $data['status'] = true : '';
        $data['state'] = $data['status'] ? "success" : "fail";
        switch (strtoupper($type)) {
            case 'JSON' :
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:text/html; charset=utf-8');
                exit(json_encode($data));
            case 'XML' :
                // 返回xml格式数据
                header('Content-Type:text/xml; charset=utf-8');
                exit(xml_encode($data));
            case 'JSONP':
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                $handler = isset($_GET['callback']) ? $_GET['callback'] : 'callback';
                exit($handler . '(' . json_encode($data) . ');');
            case 'EVAL' :
                // 返回可执行的js脚本
                header('Content-Type:text/html; charset=utf-8');
                exit($data);
            default :
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:text/html; charset=utf-8');
                exit(json_encode($data));
        }
    }

    /**
     * 操作错误跳转的快捷方法
     * @access protected
     * @param string $message 错误信息
     * @param string $jumpUrl 页面跳转地址
     * @param mixed $ajax 是否为Ajax方式 当数字时指定跳转时间
     * @return void
     */
    protected function error($message = '', $jumpUrl = '', $ajax = false) {
        empty($ajax) ? $ajax = IS_AJAX : '';
        $this->layout = 'theme.views.layouts.frontNoContent';
        $this->dispatchJump($message, 0, $jumpUrl, $ajax);
    }

    /**
     * 操作成功跳转的快捷方法
     * @access protected
     * @param string $message 提示信息
     * @param string $jumpUrl 页面跳转地址
     * @param mixed $ajax 是否为Ajax方式 当数字时指定跳转时间
     * @return void
     */
    protected function success($message = '', $jumpUrl = '', $ajax = false) {
        empty($ajax) ? $ajax = IS_AJAX : '';
        $this->layout = 'theme.views.layouts.frontNoContent';
        $this->dispatchJump($message, 1, $jumpUrl, $ajax);
    }

    /**
     * 默认跳转操作 支持错误导向和正确跳转
     * 调用模板显示 默认为common目录下面的success页面
     * 提示页面为可配置 支持模板标签
     * @param string $message 提示信息
     * @param Boolean $status 状态
     * @param string $jumpUrl 页面跳转地址
     * @param mixed $ajax 是否为Ajax方式 当数字时指定跳转时间
     * @access private
     * @return void
     */
    protected function dispatchJump($message, $status = 1, $jumpUrl = '', $ajax = false) {
        // AJAX提交
        if (true === $ajax || Yii::app()->request->isAjaxRequest) {
            $data = is_array($ajax) ? $ajax : array();
            $data['info'] = $message;
            $data['status'] = $status;
            $data['referer'] = $data['url'] = $jumpUrl;
            $this->ajaxReturn($data);
        }

        //如果为数字，表示跳转停顿时间
        if (is_int($ajax)) {
            $this->assign('waitSecond', $ajax);
        }
        //页面跳转地址
        if (!empty($jumpUrl)) {
            $this->assign('jumpUrl', $jumpUrl);
        }
        // 提示标题
        $this->assign('msgTitle', $status ? '操作成功！' : '操作失败！');
        $this->assign('status', $status);   // 状态
        if ($status) { //发送成功信息
            $this->assign('message', $message); // 提示信息
            // 成功操作后默认停留1秒
            if (!isset($this->tVar['waitSecond'])) {
                $this->assign('waitSecond', '1');
            }
            // 默认操作成功自动返回操作前页面
            if (!isset($this->tVar['jumpUrl'])) {
                $this->assign("jumpUrl", Yii::app()->request->getUrlReferrer());
            }
            $this->render('//common/success');
        } else {
            $this->assign('error', $message); // 提示信息
            //发生错误时候默认停留3秒
            if (!isset($this->tVar['waitSecond'])) {
                $this->assign('waitSecond', '3');
            }
            // 默认发生错误的话自动返回上页
            if (!isset($this->tVar['jumpUrl'])) {
                $this->assign("jumpUrl", Yii::app()->request->getUrlReferrer());
                //$this->assign('jumpUrl', str_replace(Yii::app()->request->hostInfo,"",GuestInfo::getReferer()));
                //$this->assign('jumpUrl', $this->createUrl('/front/index/index'));
            }
            $this->render('//common/error');
            // 中止执行  避免出错后继续执行
            exit;
        }
    }

    /**
     * session快捷操作
     * @param type $key session name
     * @param type $value 为空获取session，有值增加。null，删除session
     * @return type
     */
    protected function session($key, $value = '') {
        if ('' == $value) {
            //获取seesion
            return Yii::app()->session[$key];
        } else if (is_null($value)) {
            //删除session
            Yii::app()->session->remove($key);
        } else {
            //设置session
            Yii::app()->session->add($key, $value);
        }
    }

    /**
     * 设置Cookie
     * @param string $name Cookie名
     * @param string $value Cookie 值
     * @param int $expire Cookie有效期，不写为当前会话结束，单位秒
     */
    protected function setCookie($name, $value, $expire = 0) {
        if (empty($name) || empty($value)) {
            return false;
        }
        $cookie = new CHttpCookie($name, $value);
        //设置有效期
        if ($expire) {
            $cookie->expire = time() + (int) $expire;
        }
        Yii::app()->request->cookies[$name] = $cookie;
    }

    /**
     * 获取Cookie
     * @param type $name Cookie名
     * @return string Cookie值
     */
    protected function getCookie($name) {
        if (empty($name)) {
            return '';
        }
        $cookie = Yii::app()->request->getCookies();
        if ($cookie[$name]) {
            return $cookie[$name]->value;
        }
        return '';
    }

    /**
     * 删除Cookie
     * @param type $name Cookie名
     * @return boolean
     */
    protected function removeCookie($name) {
        if (empty($name)) {
            return false;
        }
        Yii::app()->request->getCookies()->remove($name);
        return true;
    }

    /**
     * 分页处理
     * @staticvar array $_pageCache 静态变量
     * @param type $total 信息总数
     * @param type $size 每页数量
     * @param type $number 当前分页号（页码）
     * @param type $config 配置，会覆盖默认设置
     * @return \Page|array
     */
    public static function page($total, $size = 0, $number = 0, $config = array(), $objCriteria = '') {
        //配置
        $defaultConfig = array(
            //当前分页号
            'number' => $number,
            //接收分页号参数的标识符
            'param' => 'page',
            //分页规则
            'rule' => '',
            //是否启用自定义规则
            'isrule' => false,
            //分页模板
            'tpl' => '',
            //分页具体可控制配置参数默认配置
            'tplconfig' => array('listlong' => 6, 'listsidelong' => 2, "first" => "首页", "last" => "尾页", "prev" => "上一页", "next" => "下一页", "list" => "*", "disabledclass" => ""),
        );
        //分页具体可控制配置参数
        $cfg = array(
            //每次显示几个分页导航链接
            'listlong' => 6,
            //分页链接列表首尾导航页码数量，默认为2，html 参数中有”{liststart}”或”{listend}”时才有效
            'listsidelong' => 2,
            //分页链接列表
            'list' => '*',
            //当前页码的CSS样式名称，默认为”current”
            'currentclass' => 'current',
            //第一页链接的HTML代码，默认为 ”«”，即显示为 «
            'first' => '&laquo;',
            //上一页链接的HTML代码，默认为”‹”,即显示为 ‹
            'prev' => '&#8249;',
            //下一页链接的HTML代码，默认为”›”,即显示为 ›
            'next' => '&#8250;',
            //最后一页链接的HTML代码，默认为”»”,即显示为 »
            'last' => '&raquo;',
            //被省略的页码链接显示为，默认为”…”
            'more' => '...',
            //当处于首尾页时不可用链接的CSS样式名称，默认为”disabled”
            'disabledclass' => 'disabled',
            //页面跳转方式，默认为”input”文本框，可设置为”select”下拉菜单
            'jump' => '',
            //页面跳转文本框或下拉菜单的附加内部代码
            'jumpplus' => '',
            //跳转时要执行的javascript代码，用*代表页码，可用于Ajax分页
            'jumpaction' => '',
            //当跳转方式为下拉菜单时最多同时显示的页码数量，0为全部显示，默认为50
            'jumplong' => 50,
        );
        //覆盖配置
        if (!empty($config) && is_array($config)) {
            $defaultConfig = array_merge($defaultConfig, $config);
        }
        //每页显示信息数量
        $defaultConfig['size'] = $size ? $size : 10;
        //把默认配置选项设置到tplconfig
        foreach ($cfg as $key => $value) {
            if (isset($defaultConfig[$key])) {
                $defaultConfig['tplconfig'][$key] = isset($defaultConfig[$key]) ? $defaultConfig[$key] : $value;
            }
        }
        //是否启用自定义规则，规则是一个数组，index和list。不启用的情况下，直接以当前$_GET的参数组成地址
        if ($defaultConfig['isrule'] && empty($defaultConfig['rule'])) {
            //通过全局参数获取分页规则
            $URLRULE = $GLOBALS['URLRULE'] ? $GLOBALS['URLRULE'] : '';
            $PageLink = array();
            if (!is_array($URLRULE)) {
                $URLRULE = explode("~", $URLRULE);
            }
            $PageLink['index'] = $URLRULE['index'] ? $URLRULE['index'] : $URLRULE[0];
            $PageLink['list'] = $URLRULE['list'] ? $URLRULE['list'] : $URLRULE[1];
            $defaultConfig['rule'] = $PageLink;
        } else if ($defaultConfig['isrule'] && !is_array($defaultConfig['rule'])) {
            $URLRULE = explode('|', $defaultConfig['rule']);
            $PageLink = array();
            $PageLink['index'] = $URLRULE[0];
            $PageLink['list'] = $URLRULE[1];
            $defaultConfig['rule'] = $PageLink;
        }
        $Page = new Page($total, $defaultConfig['size'], $defaultConfig['number'], $defaultConfig['list'], $defaultConfig['param'], $defaultConfig['rule'], $defaultConfig['isrule']);
        $Page->SetPager('default', $defaultConfig['tpl'], $defaultConfig['tplconfig']);

        if($objCriteria){
            $objCriteria->limit = $Page->listRows;
            $objCriteria->offset = $Page->firstRow;            
        }

        return $Page;
    }

}
