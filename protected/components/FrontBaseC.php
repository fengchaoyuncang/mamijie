<?php

/**
 * 前台控制器
 */
class FrontBaseC extends YzwController {

    public $layout = 'theme.views.layouts.common.layout.index';   
    public $menus = array();//菜单
    public $head_menus = array();      //顶部菜单 
    public $oldmenus = array();      //顶部菜单 

    public $ajaxonepage = 20;
    public $numpage = 200;


    public function init() {
        parent::init();
    }

    protected function beforeAction($action) {     
        parent::beforeAction($action);
        //导航
        $this->menus = array(
            array('label' => '<strong>首页</strong><small>Home</small>',     'url' => array('/front/index/index'), 'itemOptions' => array('class' => 'first')),
            array('label' => '<strong>今日特价</strong><small>Sale</small>',     'url' => array('/front/index/new')),
            //array('label' => '<strong>宝宝专区</strong><small>Baby</small>',     'url' => array('/front/index/onsale', 'cid' => '1,22')),
            //array('label' => '<strong>19.9专区</strong><small>19.9</small>',     'url' => array('/front/index/data199')),
            array('label' => '<strong>品牌团</strong><small>Group purchase</small>',     'url' => array('/front/goods/banner')),
            array('label' => '<strong>明日预告</strong><small>Tomorrow</small>',     'url' => array('/front/index/tomorrow')),
        );

        $this->oldmenus = array(
            array('label' => '首页',     'url' => array('/front/index/index')),
            array('label' => '品牌团',     'url' => array('/front/goods/banner')),

            array('label' => '妈咪百科',     'url' => array('/baike/index/index')),

            array('label' => '商家入驻',     'url' => array('/user/merchantsSettled/introduction'), 'linkOptions' => array('style' => 'position:absolute;right:80px', 'target' => "_blank")),
            array('label' => '商家报名',     'url' => array('/user/baoming/create'), 'linkOptions' => array('style' => 'position:absolute;right:0px', 'target' => "_blank")),

        );

        $this->setSEO();

        return true;
    }

    public function setSEO(){

        if(ACTION_NAME == 'new' && CONTROLLER_NAME == 'index' && GROUP_NAME == 'front'){
            $this->title = '【今日特价】精选各种母婴用品,1元秒杀、1折抢购每天准时开抢 - 妈咪街母婴用品';
            $this->keywords = '今日特价,特价,秒杀,1元秒杀';
            $this->description = '妈咪街今日特价汇集精选全网最低的母婴用品,1元秒杀、1折抢购,每天十点开抢,找特价,秒杀上妈咪街就对了.';            
            return;
        }

        if(ACTION_NAME == 'tomorrow'  && GROUP_NAME == 'front'){
            $this->title = '【明日预告】1元秒杀、1折抢购、打折促销早知道-妈咪街母婴用品';
            $this->keywords = '1元秒杀,1折抢购,打折促销';
            $this->description = '妈咪街明日预告为您提供1元秒杀,1折抢购,打折促销等各种精选母婴用品,让您开抢早知道!';            
            return;
        }

        if(CONTROLLER_NAME == 'merchantsSettled'){
            $this->title = '【商家入驻|报名入口】-妈咪街母婴用品';
            $this->description = '妈咪街商家入驻,妈咪街报名入口';            
            return;
        }
        if(CONTROLLER_NAME == 'baoming'){
            $this->title = '【商家活动报名入口】-妈咪街母婴用品';
            $this->description = '妈咪街活动报名入口';            
            return;
        }
        if(GROUP_NAME == 'baike' && CONTROLLER_NAME == 'index' && ACTION_NAME == 'index'){
            $this->title = '孕妇知识,母婴问答,母婴知识,母婴知识大全,育儿百科知识大全,让您更懂母婴 - 妈咪街';
            $this->keywords = '孕妇知识,母婴知识,母婴问答,懂母婴,育儿百科知识大全';
            $this->description = '妈咪街母婴知识,母婴问答专栏,欢迎孕妈咪在这里能找到你想要的母婴知识,妈咪街让您更懂母婴育儿知识.';            
            return;
        }
        if(CONTROLLER_NAME == 'public' && ACTION_NAME == 'sitemap'){
            $this->title = '网站地图-妈咪街母婴用品';
            $this->description = '妈咪街网站地图,妈咪街母婴用品网站地图';            
            return;
        }
        if(CONTROLLER_NAME == 'goods' && ACTION_NAME == 'banner'){
            $this->title = '品牌商城,品牌街,品牌特卖-妈咪街母婴用品';
            $this->keywords = '品牌商城,品牌街,品牌特卖';
            $this->description = '妈咪街母婴品牌商城,提供各类母婴品牌特卖商品,每天更新,还有各种抢购活动.';            
            return;
        }

    }

}
