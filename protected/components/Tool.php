<?PHP
/**
 * 一些工具类
 */
class Tool{
    /**
     * 获取阿里云通过SLB负载均衡取得客户端IP
     * @return String ip地址
     */
    public static function getForwardedForIp() {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
            $ip = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = trim($ip[0]);
            return $ip ? $ip : '127.0.0.1';
        }
        return Yii::app()->request->userHostAddress;
    }
    //通过秒数计算时间
    public static function getDataTime($time){
        $maio = $time%60;
        if($maio){
           $time = $time-$maio+60; 
        }
        $result = '';
        if($time < 60 && $time > 0){
            return $time.'秒';
        }else if($time >= 60  && $time < 3600){
            $a = (int)($time/60);
            $b = $time - $a*60;
            return "<span style='font-size:12px' class='datatime'>{$a}</span>".'分';
        }else if($time >= 3600 && $time < 86400){
            $a = (int)($time/3600);//时
            $b = (int)(($time-(3600*$a))/60);//分
            $c = $time - 3600*$a - 60*$b;//秒
            return "<span  style='font-size:12px' class='datatime'>{$a}</span>".'时'."<span  style='font-size:12px' class='datatime'>{$b}</span>".'分';
        }else if($time >= 86400){
            $a = (int)($time/86400);//天
            $b = (int)(($time-(86400*$a))/3600);//时
            $c = (int)(($time-(86400*$a + $b*3600))/60);//分
            $d = ($time-(86400*$a + $b*3600 + $c*60));//秒
            return "<span  style='font-size:12px' class='datatime'>{$a}</span>".'天'."<span  style='font-size:12px' class='datatime'>{$b}</span>".'时' . "<span  style='font-size:12px' class='datatime'>{$c}</span>" . '分';
        }
    }

    //按时，，分，，秒的方式展示
    public static function getDataTimearrsfm($time){
        $arrResult = array();
        if($time < 60){
            $arrResult[0] = $arrResult[1] = '00';
            if($time < 10){
                $arrResult[2] = '0' . $time;
            }else{
                $arrResult[2] = $time;
            }
        }else if($time >= 60  && $time < 3600){
            $arrResult[0] = '00';
            $a = (int)($time/60);
            $b = $time%60;

            if($a < 10){
                $arrResult[1] = "0{$a}";
            }else{
                $arrResult[1] = $a;
            }

            if($b < 10){
                $arrResult[2] = "0{$b}";
            }else{
                $arrResult[2] = $b;
            }
        }else if($time >= 3600){
            $a = (int)($time/3600);//时
            $b = (int)(($time - $a*3600)/60);//分
            $c = $time%60;
            if($a < 10){
                $arrResult[0] = "0{$a}";
            }else{
                $arrResult[0] = $a;
            }
            if($b < 10){
                $arrResult[1] = "0{$b}";
            }else{
                $arrResult[1] = $b;
            }

            if($c < 10){
                $arrResult[2] = "0{$c}";
            }else{
                $arrResult[2] = $c;
            }
        }
        return $arrResult;
    }

    //按时 两位，，分两位，秒两位的方式展示
    public static function getDataTimearr($time){
        $arrResult = array();
        if($time < 60){
            $arrResult[0] = $arrResult[1] = $arrResult[2] = $arrResult[3] = 0;
            if($time < 10){
                $arrResult[4] = 0;
                $arrResult[5] = $time;
            }else{
                $arrResult[4] = (int)($time/10) + 0;
                $arrResult[5] = $time%10;                     
            }
        }else if($time >= 60  && $time < 3600){
            $arrResult[0] = $arrResult[1] = 0;
            $a = (int)($time/60);
            $b = $time%60;

            if($a < 10){
                $arrResult[2] = 0;
                $arrResult[3] = $a;
            }else{
                $arrResult[2] = (int)($a/10) + 0;
                $arrResult[3] = $a%10;                      
            }

            if($b < 10){
                $arrResult[4] = 0;
                $arrResult[5] = $b;
            }else{
                $arrResult[4] = (int)($b/10) + 0;
                $arrResult[5] = $b%10;                  
            }
        }else if($time >= 3600){
            $a = (int)($time/3600);//时
            $b = (int)(($time - $a*3600)/60);//分
            $c = $time%60;
            if($a < 10){
                $arrResult[0] = 0;
                $arrResult[1] = $a;
            }else{
                $arrResult[0] = (int)($a/10);
                $arrResult[1] = $a%10;                 
            }
            if($b < 10){
                $arrResult[2] = 0;
                $arrResult[3] = $b;
            }else{
                $arrResult[2] = (int)($b/10) + 0;
                $arrResult[3] = $b%10;   
            }

            if($c < 10){
                $arrResult[4] = 0;
                $arrResult[5] = $c;
            }else{
                $arrResult[4] = (int)($c/10) + 0;
                $arrResult[5] = $c%10;                    
            }
        }
        return $arrResult;
    }    	
}

?>