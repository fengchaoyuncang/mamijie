<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UrlClass
 *
 * @author Administrator
 */
class UrlClass {

    public static function returnUrl($filter) {
        if (isset($_SERVER['HTTP_REFERER']) && !stristr($_SERVER['HTTP_REFERER'], $filter)) {
            Yii::app()->user->returnUrl = $_SERVER['HTTP_REFERER'];
        }
        if (isset($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'], "user/user/verify")) {
            Yii::app()->user->returnUrl = Yii::app()->createUrl("user/info/index");
        }
        if (isset($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'], "user/user/findpasswordverify")) {
            Yii::app()->user->returnUrl = Yii::app()->createUrl("user/info/index");
        }
    }

    public static function getrobot() {
        if (!defined('IS_ROBOT')) {
            $kw_spiders = 'Bot|Crawl|Spider|slurp|sohu-search|lycos|robozilla|360Spider';
            $kw_browsers = 'MSIE|Netscape|Opera|Konqueror|Mozilla|360User-agent';
            if (!strpos($_SERVER['HTTP_USER_AGENT'], 'http://') && preg_match("/($kw_browsers)/i", $_SERVER['HTTP_USER_AGENT'])) {
                define('IS_ROBOT', FALSE);
            } elseif (preg_match("/($kw_spiders)/i", $_SERVER['HTTP_USER_AGENT'])) {
                define('IS_ROBOT', TRUE);
            } else {
                define('IS_ROBOT', FALSE);
            }
        }
        return IS_ROBOT;
    }

    public static function mobileHost() {
        if (isset(Yii::app()->request->hostInfo) && !stristr(Yii::app()->request->hostInfo, "m.1zw.com")) {
            return false;
        }
        return true;
    }

}
