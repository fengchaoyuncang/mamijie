<?php

class Callback{
    public $data = array();
    public static $CallbackClass = array(
        '1' => '',//充值回调
        '2' => 'BaoZhengJing',//支付保证金回调
        '3' => 'Ruzhu',//支付保证金回调
        '4' => 'Goods',//支付商品
    );

    /**
     * 链接服务
     * @staticvar null $handier
     * @return \RedisCluster
     */
    static public function getInstance($type = '') {
        static $handier = NULL;
        if (empty($handier)) {
            if(empty(self::$CallbackClass[$type])){
                $handier = new Callback();                
            }else{
                Yii::import('ext.alipay.Callback.*');
                $class = self::$CallbackClass[$type];
                if (class_exists($class)) {
                    $handier = new $class();
                }else{
                    $handier = new Callback();       
                }                
            }
        }
        return $handier;
    }    
    public function data($data){
        $this->data = CMap::mergeArray($this->data,$data); 
        return $this;
    }

    public function run(){
        return true;
    }
}
