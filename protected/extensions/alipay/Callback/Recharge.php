<?php

/**
 * 用户充值
 * File Recharge.php
 * File Encoding：UTF-8
 */
class Recharge extends Callback {

    public function run() {
        //print_r($this->data);exit;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $memberModel = MembersModel::model()->lockTable($this->data['uid']);
            $memberModel->gold += $this->data['money'];
            $memberModel->save(false, array('gold'));
            $transaction->commit();
            unset($transaction);
            $data = array(
                'uid' => $this->data['uid'],
                'type' => 2,
                'note' => $this->data['order_sn'],
                'money' => $this->data['money'],
                'usermoney' => $memberModel->gold,
            );
            MembersMoneyActionModel::model()->addActionLog($data);            
        } catch (Exception $e) {
            $transaction->rollBack();
        }
                               // print_r($memberModel);exit;
    }

}
