<?php

/**
 * @use:
 * $TBInfoByAPI = Yii::createComponent('application.extensions.taoAPI.TBInfoByAPI');
 * $TBInfoByAPI->getGoodBMInfo("37873404097");
 */
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'TaoAPI.php';

class TBInfoByAPI {

    private static $_tao_api;

    public function __construct() {
        if (self::$_tao_api == NULL) {
            self::$_tao_api = new TaoAPI();
        }
    }

    /**
     * 
     * @param init $product_id : taobao good id
     * @return array
     * 
     */
    public function getGoodBMInfo($product_id) {

        // $fields='detail_url,title,nick,location,item_img,price,pic_url,sales,credit_score';
        self::$_tao_api->method = 'taobao.item.get';
        self::$_tao_api->fields = 'detail_url,title,nick,location,item_img,price,pic_url';
        self::$_tao_api->num_iid = $product_id;
        $TaoapiItem = self::$_tao_api->Send('get', 'xml')->getArrayData();
        return $TaoapiItem;
    }

    public function sellerCreditScore($product_id) {

        self::$_tao_api->method = 'taobao.taobaoke.items.detail.get';
        self::$_tao_api->fields = 'seller_credit_score';
        self::$_tao_api->num_iid = $product_id;
        $TaoapiItem = self::$_tao_api->Send('get', 'xml')->getArrayData();
        if (isset($TaoapiItem['taobaoke_item_details']['taobaoke_item_detail']['seller_credit_score'])) {
            $credit_score = $TaoapiItem['taobaoke_item_details']['taobaoke_item_detail']['seller_credit_score'];
        } else {
            $credit_score = 6;
        }
        return $credit_score;
    }

}
