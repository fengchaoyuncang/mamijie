<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaoAPICache
 *
 * @author Administrator
 */
class TaoAPICache {

    private $_CachePath = './ApiCache/';
    private $_IndexCachePath = './IndexCache/';
    private $_cachetime = false;
    private $_method = "";
    public function setMethod ($method)
    {
        $this->_method = $method;
    }
    /**
     * @return Taoapi_Cache
     */
    public function setCacheTime ($time)
    {
        $this->_cachetime = intval($time);
        return $this;
    }
    /**
     * @return Taoapi_Cache
     */
    public function setCachePath ($CachePath)
    {
        $this->_CachePath = substr($CachePath, - 1, 1) == '/' ? $CachePath : $CachePath . '/';
        return $this;
    }
    public function saveCacheData ($id, $result)
    {
        $idkey = substr($id,0,2);
        if ($this->_cachetime) {
            if (! is_dir($this->_CachePath)) {
                mkdir($this->_CachePath);
            }
            if (! is_dir($this->_CachePath . $this->_method)) {
                mkdir($this->_CachePath . $this->_method);
            }
            if (! is_dir($this->_CachePath . $this->_method.'/'.$idkey)) {
                mkdir($this->_CachePath . $this->_method.'/'.$idkey);
            }
           $filepath = $this->_CachePath . $this->_method.'/'.$idkey;
            if (is_dir($filepath)) {
                $filename = $filepath . '/' . $id . '.cache';
                @file_put_contents($filename, $result);
            }
        }
    }
	public function saveIndexCacheData ($catid, $result)
    {
        if (! is_dir($this->_IndexCachePath)) {
			mkdir($this->_IndexCachePath);
		}
		$filepath = $this->_IndexCachePath;
		if (is_dir($filepath)) {
			$filename = $filepath . $catid . '.cache';
			@file_put_contents($filename, $result);
		}
    }
    public function clearCache ($id = null)
    {
        if ($id) {
            $filename = $this->_CachePath . $this->_method . '/' . $id . '.cache';
            unlink($filename);
        } else {
            $dir = $this->_CachePath . $this->_method . '/';
            if (is_dir($dir)) {
                if ($dh = opendir($dir)) {
                    while (($file = readdir($dh)) !== false) {
                        if (is_dir($dir . $file)) {
                            continue;
                        }
                        unlink($dir . $file);
                    }
                    closedir($dh);
                }
            }
        }
    }
    public function getCacheData ($id)
    {
        $idkey = substr($id,0,2);
        $filename = $this->_CachePath . $this->_method . '/' . $idkey . '/' . $id . '.cache';
        if ($this->_cachetime) {
            if (file_exists($filename)) {
                $filetime = date('U', filemtime($filename));
                $cachetime = $this->_cachetime * 60 * 60;
                if ($this->_cachetime != 0 && (time() - $filetime) > $cachetime) {
					unlink($filename);
                    return false;
                }
                return @file_get_contents($filename);
            }
        }
        return false;
    }
	public function getIndexCacheData ($catid)
    {
        $filename = $this->_IndexCachePath . $catid . '.cache';
		
        if (file_exists($filename)) {
			return @file_get_contents($filename);
		}
        return false;
    }
}
