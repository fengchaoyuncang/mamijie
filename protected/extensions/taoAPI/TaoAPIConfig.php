<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaoAPIConfig
 *
 * @author Administrator
 */
class TaoAPIConfig {
     private $_Config;
    /**
     * @var  Taoapi_Config
     */
    private static $_init;
    /**
     * @return Taoapi_Config
     */
    public static function Init ()
    {
        if (! self::$_init) {
            self::$_init = new TaoAPIConfig();
        }
        return self::$_init;
    }
    /**
     * @return Taoapi_Config
     */
    public function setTestMode ($test = true)
    {
        if ($test) {
            $this->_Config['Url'] = 'http://gw.api.tbsandbox.com/router/rest';
        } else {
            $this->_Config['Url'] = 'http://gw.api.taobao.com/router/rest';
        }
        return $this;
    }
    public function getAppURL ()
    {
        return $this->_Config['Url'];
    }
    /**
     * @return Taoapi_Config
     */
    public function setVersion ($version,$signmode = 'md5')
    {
        $this->_Config['version'] = array('version'=>$version,'signmode'=>$signmode);
        return $this;
    }
    public function getVersion ()
    {
       return !empty($this->_Config['version']) ? $this->_Config['version'] : array('version'=>1,'signmode'=>'md5');
    }
    public function getAppKey ()
    {
        return ! empty($this->_Config['appKey']) ? $this->_Config['appKey'] : '';
    }
    public function getAppSecret ()
    {
        return ! empty($this->_Config['appSecret']) ? $this->_Config['appSecret'] : '';
    }
    /**
     * @return Taoapi_Config
     */
    public function setAppKey ($key)
    {
        $this->_Config['appKey'] = $key;
        return $this;
    }
    /**
     * @return Taoapi_Config
     */
    public function setAppSecret ($Secret)
    {
        $this->_Config['appSecret'] = $Secret;
       
        return $this;
    }
}
