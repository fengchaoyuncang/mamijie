<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * use: $TaobaoDetail = Yii::createComponent('application.extensions.taoAPI.TaobaoDetail',$product_id);
 */

/**
 * Description of TaobaoDetail
 *
 * @author Administrator
 */
class TaobaoDetail {
    private $product_id;
    private $item_type;
    private $good;

            

    public function __construct($product_id)
    { 

        $this->product_id = $product_id;

    }
    
    private function getItemType(){
       $this->good = Goods::model()->find('product_id = :pid', array(':pid'=>$this->product_id));
       $this->item_type = $this->good->item_type;
       
    }
    
    public function getItemPage(){
        $this->getItemType();
        $shop = $this->item_type ==1?"tmallcom":"taobaocom";
        $url = "http://{$_SERVER['HTTP_HOST']}/{$shop}?id=".$this->product_id;
        $ch = curl_init();
        // 设置 url
        curl_setopt($ch, CURLOPT_URL, $url);
        // 设置浏览器的特定header
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "User-Agent: {Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0}",
                            "Accept: {text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8}",
                "Accept-Language: {zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3}",
                            "Cookie:{cq=ccp%3D1; cna=a7suCzOmSTECAXgg9iCf4AtX; t=671b2069c7e8ac444da66d664a397a5f; tracknick=%5Cu4F0D%5Cu6653%5Cu8F8901; _tb_token_=nDiU1vCuzFd0; cookie2=c54709ffbe04a5ccb80283c34d6b00fa; pnm_cku822=128WsMPac%2FFS4KgNn%2BYfhzduo4U2NC0zh9cAS4%3D%7CWUCLjKhqr873bOIFQcMecSw%3D%7CWMEKRlV%2B3D9a6XWaidNWNQOSWXwaXugvQHzhxALh%7CX0YLbX78NUR2b2DHoxnIqZENQqR35TBZbfQ5vooI0b6GHZA3U1kr%7CXkdILogCr878ZK9I%2B%2FE3QjAD3lFJJaAZRA%3D%3D%7CXUeMwMR2s%2BTUQk8IPP5TNgWfUjQwonccMCxihTa0fRYgtjgfa4j6%7CXMYK7F8liOvH3hMUpzXkiaU%2FJw%3D%3D}",
            ));
        // 页面内容我们并不需要
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        // 只需返回HTTP header
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // 返回结果，而不是输出它
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        ob_start();
        curl_exec($ch);
        $strHtml = ob_get_contents();
        ob_end_clean();
        curl_close($ch);
        return $strHtml;
    }
    
    public function descFromPage(){
        
        $str_html = $this->getItemPage();
        $des_url = null;
        
        if(strlen($str_html)>5000){
            $output = preg_match_all('/http:\/\/dsc.taobaocdn.com\/i.*?(".*?)/is',$str_html,$out);
            $des_url = str_replace("\"","",$out[0][0]);
        }
        $desc = $this->descFromUrl($des_url);
        if($desc){
             return $desc;
         }else{
             return "暂时没有描述";
         }
    }
    
    public function descFromUrl($url){
        $desc = null;
        if($url){
             $strHtml=@file_get_contents($url);
             $strHtml1=str_replace("var desc='","",$strHtml);
             $desc = str_replace("';","",$strHtml1);  
         }
         if($desc){
             return $desc;
         }else{
             return false;
         }
    }
    
    public function updateGoodDesc(){
       $desc =  $this->descFromPage();
       $this->good->desc = $desc;
       return $this->good->update();
    }
}
