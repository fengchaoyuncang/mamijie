<?php

/**
 * EMailer class file.
 *
 * @author MetaYii
 * @version 2.2
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2009 MetaYii
 *
 * Copyright (C) 2009 MetaYii.
 *
 * 	This program is free software: you can redistribute it and/or modify
 * 	it under the terms of the GNU Lesser General Public License as published by
 * 	the Free Software Foundation, either version 2.1 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU Lesser General Public License for more details.
 *
 * 	You should have received a copy of the GNU Lesser General Public License
 * 	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For third party licenses and copyrights, please see phpmailer/LICENSE
 *
 */
/**
 * Include the the PHPMailer class.
 */
//require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'phpmailer' . DIRECTORY_SEPARATOR . 'class.phpmailer.php');
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'PHPMailerNew' . DIRECTORY_SEPARATOR . 'PHPMailerAutoload.php');
/**
 * EMailer is a simple wrapper for the PHPMailer library.
 * @see http://phpmailer.codeworxtech.com/index.php?pg=phpmailer
 *
 * @author MetaYii
 * @package application.extensions.emailer 
 * @since 1.0
 */
class EMailer {

    private $_myMailer;

    //***************************************************************************
    // Initialization
    //***************************************************************************

    /**
     * Init method for the application component mode.
     */
    public function init() {
        
    }

    /**
     * Constructor. Here the instance of PHPMailer is created.
     */
    public function __construct() {
        $this->_myMailer = new PHPMailer();
    }




    public function sendEmail($to, $subject, $body, $form_name = "妈咪街", $altBody = null) {


        //$this->_myMailer->SMTPDebug = 3;                               // Enable verbose debug output

        $this->_myMailer->isSMTP();                                      // Set mailer to use SMTP
        $this->_myMailer->Host = 'smtp.qiye.163.com';  // Specify main and backup SMTP servers
        $this->_myMailer->SMTPAuth = true;                               // Enable SMTP authentication

        $k = array_rand(Yii::app()->params->smtp['account']);
        $this->_myMailer->Username = Yii::app()->params->smtp['account'][$k]['Username']; 
        $this->_myMailer->Password = Yii::app()->params->smtp['account'][$k]['Password']; // SMTP password

        $this->_myMailer->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $this->_myMailer->Port = 25;                                    // TCP port to connect to

        $this->_myMailer->From = Yii::app()->params->smtp['account'][$k]['From'];
        $this->_myMailer->FromName = $form_name;
        //$this->_myMailer->addAddress($to, 'Joe User');     // Add a recipient
        $this->_myMailer->addAddress($to);     // Add a recipient
/*        $this->_myMailer->addAddress('ellen@example.com');               // Name is optional
        $this->_myMailer->addReplyTo('info@example.com', 'Information');
        $this->_myMailer->addCC('cc@example.com');
        $this->_myMailer->addBCC('bcc@example.com');*/

/*        $this->_myMailer->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        $this->_myMailer->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name*/
        $this->_myMailer->isHTML(true);                                  // Set email format to HTML

        $this->_myMailer->Subject = $subject;
        $this->_myMailer->Body    = $body;
        //$this->_myMailer->AltBody = 'This is the body in plain text for non-HTML mail clients';

        return $this->_myMailer->send();
/*        if(!$this->_myMailer->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $this->_myMailer->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }*/

    }

}
