<?php
/* @var $this GoodsBmModelController */
/* @var $model GoodsBmModel */

$this->breadcrumbs=array(
	'Goods Bm Models'=>array('index'),
	$model->GoodsBmID,
);

$this->menu=array(
	array('label'=>'List GoodsBmModel', 'url'=>array('index')),
	array('label'=>'Create GoodsBmModel', 'url'=>array('create')),
	array('label'=>'Update GoodsBmModel', 'url'=>array('update', 'id'=>$model->GoodsBmID)),
	array('label'=>'Delete GoodsBmModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->GoodsBmID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GoodsBmModel', 'url'=>array('admin')),
);
?>

<h1>View GoodsBmModel #<?php echo $model->GoodsBmID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'GoodsBmID',
		'GoodsName',
		'City',
		'CreditScore',
		'DetailUrl',
		'Image',
		'ItemType',
		'Location',
		'Nick',
		'Pprice',
		'ProductID',
		'PromoPrice',
		'Province',
		'Sales',
		'AddTime',
		'Status',
		'UpdateTime',
		'Sorting',
		'UserID',
		'UserName',
		'AdminID',
		'Money',
		'CatID',
//		'ModuleID',
		'StartTime',
		'EndTime',
		'SpecialID',
		'PlanSale',
		'IsPay',
		'Excuse',
		'IsMail',
	),
)); ?>
