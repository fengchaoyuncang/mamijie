<?php

/**
 * This is the model class for table "{{articles_weekly}}".
 *
 * The followings are the available columns in table '{{articles_weekly}}':
 * @property integer $WeeklyID
 * @property string $Brief
 * @property integer $AddTime
 * @property integer $Week
 * @property string $Img
 */
class ArticlesWeeklyModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{articles_weekly}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Brief, AddTime, Week, Img, Type, Title,TitleSeo,KeywordsSeo,DescriptionSeo', 'safe'),
			array('WeeklyID, Brief, AddTime, Week, Img,TTitle', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'WeeklyID' => '周刊ID',
			'Brief' => '简介',
			'AddTime' => '添加时间',
			'Week' => '第几周',
			'Img' => '图片',
			'Type' => '类型',
			'Title' => '标题',
			'TitleSeo' => 'Seo标签',
			'KeywordsSeo' => 'seo关键字',
			'DescriptionSeo' => 'seo描述',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('WeeklyID',$this->WeeklyID);
		$criteria->compare('Brief',$this->Brief,true);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Week',$this->Week);
		$criteria->compare('Img',$this->Img,true);
		$criteria->compare('Title',$this->Title);
		$criteria->compare('Type',$this->Type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('WeeklyID',$this->WeeklyID);
		$criteria->compare('Brief',$this->Brief,true);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Week',$this->Week);
		$criteria->compare('Img',$this->Img,true);
		$criteria->compare('Title',$this->Title);
		$criteria->compare('Type',$this->Type);

		return $criteria;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArticlesWeeklyModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getTypeHtml($mixData = false){
		$arrData = array(
			1 => '孕期周刊',
			2 => '婴儿期育儿周刊',
			3 => '1-6岁育儿指南',
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
		}
		return true;
	}

	//获取所有的周刊
	public static function getList($type, $bloIsCache = true){
		$strKey = "ArticlesWeeklyModel_getList" . $type;
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();
        $objCriteria = BaseModel::getC(array('Type' => $type));
        $objCriteria->order = 'Week asc';
        $objModels = self::model()->findAll($objCriteria);
        foreach ($objModels as $key => $objModel) {
        	$arrCache[] = $objModel->attributes;
        }
        
        yii::app()->cache->set($strKey, $arrCache, 3600);
        return $arrCache;		
	}

}
