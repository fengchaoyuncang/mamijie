<?php

/**
 * This is the model class for table "{{tag}}".
 *
 * The followings are the available columns in table '{{tag}}':
 * @property string $TagID
 * @property string $Title
 * @property string $AddTime
 * @property integer $Status
 * @property string $Sorting
 */
class TagModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tag}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Title', 'required'),
			array('Title', 'unique'),
			array('Status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TagID, Title, AddTime, Status, Sorting, IsHot', 'safe', 'on'=>'admin'),
			array('TagID, Title, AddTime, Status, Sorting', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TagID' => '标签ID',
			'Title' => '标签名',
			'AddTime' => '添加时间',
			'Status' => '标签状态',
			'Sorting' => '排序',
			'IsHot' => '是否热门',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TagID',$this->TagID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Sorting',$this->Sorting,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TagID',$this->TagID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('IsHot',$this->IsHot);

		return $criteria;
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
		}
		return true;
	}

	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			0 => '不可用',
			1 => '可用',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TagModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	//获取所有的标签
	public static function getList($bloIsCache = true){
		$strCache = 'TagModel_getList';
		$arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }
        $arrResult = array();
        $objModels = self::model()->findAll(BaseModel::getC(array('Status' => 1, 'order' => 'Sorting DESC, AddTime DESC')));
        foreach ($objModels as $key => $objModel) {
        	$arrResult[$objModel->TagID] = $objModel->attributes;
        }
        yii::app()->cache->set($strCache, $arrResult, 3600);
        return $arrResult;
	}

	public function afterSave(){
		$this->getList(false);
	}
	//当这个标签删除了之后，我需要删除与其关联的goods_id记录
	public function afterDelete(){
		TagGoodsModel::model()->deleteAll(BaseModel::getC(array('TagID' => $this->TagID)));	
		$this->getList(false);
	}

	/**
	 * 根据某一字段来获取全部数据
	 * @param  [type] $strField [description]
	 * @return [type]           [description]
	 */
	public static function getFieldList($strField){
		$arrDatas = self::getList();
		$arrResult = array();
		foreach ($arrDatas as $TagID => $arrData) {
			$arrResult[$TagID] = $arrData[$strField];
		}

		return $arrResult;
	}

	public static function getOne($TagID, $strField = ''){
		$data = self::getList();
		if($strField){
			return $data[$TagID][$strField];
		}else{
			return $data[$TagID];
		}
	}
	public static function getIsHotHtml($mixData = false){
		$arrData = array(
			0 => '否',
			1 => '是',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

}
