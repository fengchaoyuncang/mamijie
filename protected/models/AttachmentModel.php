<?php

/**
 * This is the model class for table "{{attachment}}".
 * 附件MODEL
 * The followings are the available columns in table '{{attachment}}':
 * @property string $AttachmentID
 * @property string $Name
 * @property string $Path
 * @property string $Size
 * @property string $Ext
 * @property integer $Type
 * @property integer $UserID
 * @property integer $IsAdmin
 * @property string $AddTime
 * @property string $UploadIP
 */
class AttachmentModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{attachment}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Path, Ext,Type', 'required'),
			array('UserID, IsAdmin', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Name, Path, Size, Ext, Type, UserID, IsAdmin, AddTime, UploadIP', 'safe'),
			array('AttachmentID, Name, Path, Size, Ext, Type, UserID, IsAdmin, AddTime, UploadIP', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'AttachmentID' => '附近ID',
			'Name' => '名称',
			'Path' => '路径',
			'Size' => '大小',
			'Ext' => '扩展名',
			'Type' => '类型',
			'UserID' => '上传用户ID',
			'AddTime' => '添加时间',
			'UploadIP' => 'IP',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('AttachmentID',$this->AttachmentID,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Path',$this->Path,true);
		$criteria->compare('Size',$this->Size,true);
		$criteria->compare('Ext',$this->Ext,true);
		$criteria->compare('Type',$this->Type);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('IsAdmin',$this->IsAdmin);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('UploadIP',$this->UploadIP,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('AttachmentID',$this->AttachmentID,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Path',$this->Path,true);
		$criteria->compare('Size',$this->Size,true);
		$criteria->compare('Ext',$this->Ext,true);
		$criteria->compare('Type',$this->Type);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('IsAdmin',$this->IsAdmin);


		if(!empty($_GET['add_start_time'])){
			$startTime = strtotime($_GET['add_start_time']);
			$criteria->addCondition('AddTime >= ' . $startTime);	
		}	

		if(!empty($_GET['add_end_time'])){
			$EndTime = strtotime($_GET['add_end_time']) + 86400;
			$criteria->addCondition('AddTime < ' . $EndTime);
		}	





		$criteria->compare('UploadIP',$this->UploadIP,true);

		return $criteria;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AttachmentModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if($this->isNewRecord){
			if(GROUP_NAME == 'admin'){
				$this->UserID = AdminBase::$uid;
				$this->IsAdmin = 1;
			}else{
				$this->IsAdmin = 0;
				if(yii::app()->user->isLogin()){
					$this->UserID = yii::app()->user->id;				
				}else{
					$this->UserID = 0;
				}
			}
			$this->AddTime = time();
			$this->UploadIP = Tool::getForwardedForIp();
		}
		return true;
	}

	public function afterDelete(){
		if($this->Path){
			UploadFile::deleteImg($this->Path);
		}
	}
	/**
	 * 保存数据
	 * @return [type] [description]
	 */
	public static function saveData($arrData){
		$objModel = new AttachmentModel();
		$objModel->attributes = $arrData;
		$objModel->save(false);
		return $objModel->getPrimaryKey();
	}



	public static function getCookie(){
		$cookie = Yii::app()->request->getCookies();
		//本来就已经有值了
		if($cookie['json_baidu_edit']){
			$data = $cookie['json_baidu_edit']->value;
			return explode('@@@', $data);
		}else{//不存在此cookie
			return array();
		}
	}

	public static function setCookie($value){
		$data = self::getCookie();
		if(in_array($value, $data)){
			return $data;
		}else{
			if($data){
				$d = implode('@@@', $data);
				$d .= "@@@{$value}";				
			}else{
				$d = $value;
			}
			$cookie = new CHttpCookie('json_baidu_edit', $d);
			$cookie->expire = time() + 86400*10;
			Yii::app()->request->cookies['json_baidu_edit'] = $cookie;
			return explode('@@@', $d);
		}
	}

}
