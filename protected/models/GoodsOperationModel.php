<?php

/**
 * This is the model class for table "{{goods_operation}}".
 *
 * The followings are the available columns in table '{{goods_operation}}':
 * @property string $OperationID
 * @property string $AdminID
 * @property string $AddTime
 * @property string $OldStatus
 * @property string $Status
 * @property string $Remarks
 * @property string $GoodsBmID
 */
class GoodsOperationModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{goods_operation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('AdminID, AddTime, OldStatus, Status, Remarks, GoodsBmID, ProductID', 'safe', 'on'=>'admin'),
			array('OperationID, AdminID, AddTime, OldStatus, Status, Remarks, GoodsBmID, ProductID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'GoodsBmModel' => array(self::BELONGS_TO, 'GoodsBmModel', 'GoodsBmID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OperationID' => '操作ID',
			'AdminID' => '用户ID',
			'AddTime' => '添加时间',
			'OldStatus' => '旧的状态',
			'Status' => '变化后状态',
			'Remarks' => '备注',
			'GoodsBmID' => '报名ID',
			'ProductID' => '商品ID',
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OperationID',$this->OperationID,true);
		$criteria->compare('AdminID',$this->AdminID,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('OldStatus',$this->OldStatus,true);
		$criteria->compare('Status',$this->Status,true);
		$criteria->compare('Remarks',$this->Remarks,true);
		$criteria->compare('GoodsBmID',$this->GoodsBmID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function createSearchCriteria()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('t.OperationID',$this->OperationID,true);
		$criteria->compare('t.AdminID',$this->AdminID,true);
		$criteria->compare('t.AddTime',$this->AddTime,true);
		$criteria->compare('t.OldStatus',$this->OldStatus,true);
		$criteria->compare('t.Status',$this->Status,true);
		$criteria->compare('t.Remarks',$this->Remarks,true);
		$criteria->compare('t.GoodsBmID',$this->GoodsBmID,true);
		$criteria->compare('t.ProductID',$this->ProductID,true);

		return $criteria;
	}	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GoodsOperationModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 增加记录  
	 * @param  [type] $objOldGoodsModel 旧的model
	 * @param  [type] $objGoodsModel    新的model
	 * @return [type]                   [description]
	 */
	public static function saveData($objOldGoodsModel, $objGoodsModel){
		$objModel = new GoodsOperationModel('admin');
		$arrData = array(
			'AdminID' => yii::app()->passport->getUid(),
			'OldStatus' => $objOldGoodsModel->Status,
			'Status' => $objGoodsModel->Status,
			'Remarks' => '',
			'GoodsBmID' => $objOldGoodsModel->GoodsBmID,
			'ProductID' => $objOldGoodsModel->ProductID,
		);
		$objModel->attributes = $arrData;
		$objModel->save(false);
	}
	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = TIME_TIME;
		}
		return true;
	}
	
}
