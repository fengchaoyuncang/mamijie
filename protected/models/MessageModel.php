<?php

/**
 * This is the model class for table "{{message}}".
 * 通知消息 
 * The followings are the available columns in table '{{message}}':
 * @property integer $MessageID
 * @property string $Title
 * @property string $Content
 * @property integer $Type
 * @property integer $UserID
 * @property integer $AddTime
 * @property integer $Sorting
 * @property integer $IsRead
 * @property integer $SendUserID
 * @property string $Remark
 */
class MessageModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{message}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Title, Content', 'required'),
			array('Type, UserID, AddTime, Sorting, IsRead, SendUserID', 'numerical', 'integerOnly'=>true),
			array('Title, Content, Type, UserID, AddTime, Sorting, IsRead, SendUserID, Remark,Image', 'safe'),
			array('MessageID, Title, Content, Type, UserID, AddTime, Sorting, IsRead, SendUserID, Remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MessageID' => 'Message',
			'Title' => 'Title',
			'Content' => 'Content',
			'Type' => 'Type',
			'UserID' => 'User',
			'AddTime' => 'Add Time',
			'Sorting' => 'Sorting',
			'IsRead' => 'Is Read',
			'SendUserID' => 'Send User',
			'Remark' => 'Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MessageID',$this->MessageID);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Content',$this->Content,true);
		$criteria->compare('Type',$this->Type);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Sorting',$this->Sorting);
		$criteria->compare('IsRead',$this->IsRead);
		$criteria->compare('SendUserID',$this->SendUserID);
		$criteria->compare('Remark',$this->Remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MessageID',$this->MessageID);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Content',$this->Content,true);
		$criteria->compare('Type',$this->Type);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Sorting',$this->Sorting);
		$criteria->compare('IsRead',$this->IsRead);
		$criteria->compare('SendUserID',$this->SendUserID);
		$criteria->compare('Remark',$this->Remark,true);

		return $criteria;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MessageModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
			$this->IsRead = 0;
		}
		return true;
	}

	//根据用户ID取其所有信息
	public static function getListUser($UserID, $bloIsCache = true){
		$strCache = 'MessageModel_getListUser' . '_' . $UserID;
		$arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }
        $arrResult = array();
        $datas = self::model()->findAll(BaseModel::getC(array('UserID' => $UserID, 'order' => 'IsRead asc,Sorting desc,AddTime desc')));
        foreach ($datas as $data) {
        	$arrResult[$data->MessageID] = $data->attributes;
        }
        yii::app()->cache->set($strCache, $arrResult, 600);
        return $arrResult;
	}


	public function afterDelete(){
		self::getListUser($this->UserID);
	}

	/**
	 * 标记为已读
	 * 默认删除已经阅读的
	 * @return [type] [description]
	 */
	public static function updateList($data = array(), $UserID = ''){
		empty($UserID) ? $UserID = yii::app()->user->id : '';
		$str = BaseModel::getStrIn($data);
		$Criteria = BaseModel::getC(array('UserID' => $UserID, 'MessageID' => array('sign' => 'in', 'data' => $str)));
		self::model()->updateAll(array('IsRead' => 1), $Criteria);
		self::getListUser($UserID, false);
	}

	/**
	 * 批量删除
	 * @return [type] [description]
	 */
	public static function deleteList($data = array(), $UserID = ''){
		empty($UserID) ? $UserID = yii::app()->user->id : '';
		$str = BaseModel::getStrIn($data);
		$Criteria = BaseModel::getC(array('UserID' => $UserID, 'MessageID' => array('sign' => 'in', 'data' => $str)));
		self::model()->deleteAll($Criteria);
		self::getListUser($UserID, false);		
	}


	/**
	 * 通知用户
	 * @return [type] [description]
	 */
	public static function saveData($data){
		$model = new MessageModel();
		$model->attributes = $data;
		$model->save(false);
		return true;
	}
	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getTypeHtml($mixData = false){
		$arrData = array(
			1 => '系统通知',
			2 => '系统通知2',
			3 => '系统通知3',	
			4 => '系统通知4',				
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	/**
	 * 获取用户未读的邮件数
	 * @param  [type] $UserID [description]
	 * @return [type]         [description]
	 */
	public static function getNoReadCount($UserID = ''){
		$UserID = empty($UserID) ? yii::app()->user->id : $UserID;
		//通过缓存获取用户所有的消息
		$datas = self::getListUser($UserID);
		$count = 0;
		foreach ($datas as $key => $data) {
			if($data['IsRead'] == 0){
				$count++;
			}
		}
		return $count;
	}
}
