<?php

/**
 * This is the model class for table "{{member_thaw_apply}}".
 * 解冻申请
 * The followings are the available columns in table '{{member_thaw_apply}}':
 * @property integer $MoneyID
 * @property integer $UserID
 * @property integer $AddTime
 * @property integer $Status
 * @property string $Cacus
 * @property string $AdminID
 * @property integer $UpdateTime
 * @property integer $Type
 */
class MemberThawApplyModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{member_thaw_apply}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UserID, AddTime,Type', 'safe', 'on'=>'front'),
			array('UserID, AddTime, Status, Cacus, AdminID, UpdateTime, Type', 'safe', 'on'=>'admin'),
			array('MoneyID, UserID, AddTime, Status, Cacus, AdminID, UpdateTime, Type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MoneyID' => 'Money',
			'UserID' => 'User',
			'AddTime' => 'Add Time',
			'Status' => 'Status',
			'Cacus' => 'Cacus',
			'AdminID' => 'Admin',
			'UpdateTime' => 'Update Time',
			'Type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MoneyID',$this->MoneyID);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Cacus',$this->Cacus,true);
		$criteria->compare('AdminID',$this->AdminID,true);
		$criteria->compare('UpdateTime',$this->UpdateTime);
		$criteria->compare('Type',$this->Type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MoneyID',$this->MoneyID);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Cacus',$this->Cacus,true);
		$criteria->compare('AdminID',$this->AdminID,true);
		$criteria->compare('UpdateTime',$this->UpdateTime);
		$criteria->compare('Type',$this->Type);

		return 	$criteria;	
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MerchantsSettledMoneyModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function saveData($arr){
		$model = new MemberThawApplyModel('front');
		$model->attributes = $arr;
		return $model->save();
	}
	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = TIME_TIME;
			$this->UpdateTime = TIME_TIME;
			$this->Status = 0;
			$this->Cacus = '';
		}else{
			$this->UpdateTime = TIME_TIME;
			$this->AdminID = AdminBase::$uid;
		}
		return true;
	}
	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			0 => '未处理',
			1 => '已处理',	
			2 => '已拒绝',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}
	/**
	 * 类型
	 * @return [type] [description]
	 */
	public static function getTypeHtml($mixData = false){
		$arrData = array(
			1 => '保证金',
			2 => '入驻保证金',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	public function agree(){
		$userID = $this->UserID;
		$key = "MemberThawApplyModel_agree" . $userID;
		$cache = yii::app()->cache->get($key);
		if($cache){
			$this->addError('','请不要重复提交');
			return false;
		}else{
			yii::app()->cache->set($key, 1, 60);
		}



		//找这个用户是否还有正在上架的，没有结算的，在报名的一些商品
		try {
			if($this->Status != 0){
				throw new Exception("已经处理的不能再提交处理");
			}
			switch ($this->Type) {
				case '1':
						$d = GoodsBmModel::model()->find("UserID={$userID} and Status in(0,3)");
						if($d){
							throw new Exception("还有未审核或正在审核的商品,申请被拒绝");				
						}

						$d = GoodsBmModel::model()->find("UserID={$userID} and Status=1 and Pattern=1 and (IsAccounts=0 or PlanAccounts=0)");
						if($d){
							throw new Exception("还有未完成结算的商品");				
						}
						//最后一次结算的记录
						$d = GoodsBmModel::model()->find(array('condition' => "UserID={$userID} and AccountsTime > 0",'order' => 'AccountsTime desc'));
						
						//用户的最后一条记录
						$d_d = GoodsBmModel::model()->find(array('condition' => "UserID={$userID}",'order' => 'GoodsBmID desc'));
						
						$time =   $d_d->UpdateTime > $d->AccountsTime ? $d_d->UpdateTime : $d->AccountsTime;
						$time = $time + 86400*15;
						if(TIME_TIME <= $time){
							throw new Exception("保证金在最后一次结算或最后一次审核处理后15天才可以申请解冻");				
						}


						//上锁
						MemberMoneyChangeModel::getLock($userID);

						//获取冻结掉的保证金
						$money = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'Type' => 33));	
						if($money <= 0){
							throw new Exception("你未被冻结保证金");				
						}

						//增加记录
						$transaction=Yii::app()->db->beginTransaction();
						//先解冻这部份钱，再从总额扣除这部份钱		
			            $data = array(
			                'UserID' => $userID,
			                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
			                'Money' => -$money,
			                'Type' => 33,
			                'ChangeType' => 2,
			                'RelationID' => $userID,//商品报名ID
			                'AddTime' => TIME_TIME,
			                'Remarks' => '解冻',
			                'NumOnly' => $userID . '_33_' . $userID .  '_'. TIME_TIME,
			            );
			            if(!MemberMoneyChangeModel::saveData($data)){
			                throw new Exception("金额添加异常");                    
			            }
			            $this->Status = 1;
			            $this->save(false,array('Status'));
			            

					break;
				case '2':
						$d = MerchantsSettledModel::model()->find(BaseModel::getC(array('UserID' => $userID)));
						if(!$d){
							throw new Exception("找不到入驻信息");			
						}

						//上锁
						MemberMoneyChangeModel::getLock($userID);

						//获取冻结掉的保证金
						$money = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'Type' => 32));	
						if($money <= 0){
							throw new Exception("你未被冻结入驻保证金");				
						}

						//增加记录
						$transaction=Yii::app()->db->beginTransaction();
						//先解冻这部份钱，再从总额扣除这部份钱		
			            $data = array(
			                'UserID' => $userID,
			                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
			                'Money' => -$money,
			                'Type' => 32,
			                'ChangeType' => 2,
			                'RelationID' => $userID,//商品报名ID
			                'AddTime' => TIME_TIME,
			                'Remarks' => '解冻',
			                'NumOnly' => $userID . '_32_' . $userID .  '_'. TIME_TIME,
			            );
			            if(!MemberMoneyChangeModel::saveData($data)){
			                throw new Exception("金额添加异常");                    
			            }

			            $this->Status = 1;
			            $this->save(false,array('Status'));

					break;				
				
				default:
					throw new Exception("提交错误");		
					break;
			}

           
			$transaction->commit();	
			yii::app()->cache->delete($key);
			MemberMoneyChangeModel::releaseLock($userID);
			return true;


		} catch (Exception $e) {
	        if(!empty($transaction)){
	          $transaction->rollBack();
	        }
	        //请求删除
	        yii::app()->cache->delete($key);
	        if(defined('USER_MONEY_LOCK')){
	        	MemberMoneyChangeModel::releaseLock($userID);
	        }	        
	        $error = $e->getMessage();
	        $error = empty($error) ? '请求异常' : $error;
	        $this->addError('',$error);
	        return false;  				
		}		
	}

	public function noAgree(){
		if($this->Status != 0){
			$this->addError('','已经处理的不能再提交处理');
			return false;
		}else{
			$this->Cacus = Yii::app()->request->getParam('Cacus');
			$this->Status = 2;
			$this->save(false);
			return true;
		}
	}
}
