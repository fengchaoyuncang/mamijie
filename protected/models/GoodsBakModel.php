<?php

/**
 * This is the model class for table "{{goods}}".
 *  比商品报名表少了的字段  
 *  City CreditScore Location Province UserID UserName AdminID Money MoneyBond ServiceFee IsPay
 *  商吕表比报名表 少了若干个不紧要的字段
 * The followings are the available columns in table '{{goods}}':
 * @property string $GoodsID
 * @property string $GoodsBmID
 * @property string $DetailUrl
 * @property string $Image
 * @property integer $ItemType
 * @property string $Nick
 * @property string $Pprice
 * @property string $ProductID
 * @property string $PromoPrice
 * @property string $Sales
 * @property string $Sorting
 * @property string $CatID
 * @property string $ModuleID
 * @property string $StartTime
 * @property string $EndTime
 * @property string $SpecialID
 * @property integer $IsGather
 * @property string $TaobaoUrl
 */
class GoodsBakModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{goods_bak}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('GoodsName,DetailUrl, Image, CatID', 'required'),
			array('TaobaoUrl,AppTaobaoUrl', 'match','pattern'=>'/^http:\/\//','on'=>'admin'),
			array('TaobaoUrl', 'checkQuite','on'=>'admin'),
			array('CatID,AdminID,IsIndex', 'numerical', 'integerOnly'=>true),
			array('Brief,View,Zhekou', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.

			array('GoodsBmID, DetailUrl, Image, ItemType, Nick, Pprice, ProductID, PromoPrice, Sales, Sorting, CatID, ModuleID, StartTime, EndTime, SpecialID, IsGather, TaobaoUrl,AdminID,IsIndex,UpdateTime,IsPreferential,IsHot,IsNew,Status,IsFreeShipping,AddTime,UserBannerID,IsSoldout,AppTaobaoUrl,DetailsData,Spec', 'safe', 'on'=>'admin'),
			array('GoodsID, GoodsBmID, DetailUrl, Image, ItemType, Nick, Pprice, ProductID, PromoPrice, Sales, Sorting, CatID, ModuleID, StartTime, EndTime, SpecialID, IsGather, TaobaoUrl,IsIndex,UpdateTime,Status,IsFreeShipping', 'safe', 'on'=>'search'),

		);
	}

	public function checkQuite(){
		if($this->isNewRecord){
			if($model = self::model()->find(BaseModel::getC(array('ProductID' => $this->ProductID, 'order' => 'GAddTime DESC')))){
				if($model->EndTime > TIME_TIME && $model->Status == 1){
					$this->addError('', '系统已经存在此商品,还未下架');
				}
			}
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//self::HAS_MANY  我的这个表的主键去关联TagGoodsBakModel这个表的GoodsID这个字段  
			//self::BELONGS_TO  我的这个表的GoodsName 去关联 TagGoodsBakModel这个表的主键
			//self::HAS_ONE 我的这个表的主键去关联TagGoodsBakModel的GoodssID
			'TagGoodsBakModel' => array(self::HAS_MANY, 'TagGoodsBakModel', 'GoodsID'),
			//'ModuleGoodsBakModel' => array(self::HAS_MANY, 'ModuleGoodsBakModel', 'GoodsID'),
			//'AgeGoodsBakModel' => array(self::HAS_MANY, 'AgeGoodsBakModel', 'GoodsID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'GoodsID' => '商品主键ID',
			'GAddTime' => '添加时间',
			'GoodsName' => '商品名',
			'GoodsBmID' => '报名ID',
			'DetailUrl' => '链接地址',
			'Image' => '图片',
			'ItemType' => '店铺类型',
			'Nick' => '商家旺旺',
			'Pprice' => '原价',
			'ProductID' => '商品ID',
			'PromoPrice' => '折扣价',
			'Sales' => '销量',
			'Sorting' => '排序',
			'CatID' => '分类ID',
			'ModuleID' => '模块ID',
			'StartTime' => '开始时间',
			'EndTime' => '结束时间',
			'SpecialID' => '专场ID',
			'IsGather' => '是否采集',
			'TaobaoUrl' => '淘宝URL',
			'AppTaobaoUrl' => 'App淘宝URL',
			'IsIndex' => '首页轮播',
			'UpdateTime' => '修改时间',//状态修改时间，比如什么时候上架，下架等这状态
			'Tag' => '标签',//状态修改时间，比如什么时候上架，下架等这状态
			'Age' => '年龄',//状态修改时间，比如什么时候上架，下架等这状态
			'IsPreferential' => '是否特惠',
			'IsHot' => '热门商品',
			'IsNew' => '最新商品',
			'IsFreeShipping' => '是否免运费',
			'Brief' => '简介',
			'Module' => '版块/模块',
			'UserBannerID' => '品牌团所属',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('GoodsID',$this->GoodsID,true);
		$criteria->compare('GoodsBmID',$this->GoodsBmID,true);
		$criteria->compare('GoodsName',$this->GoodsName,true);
		$criteria->compare('DetailUrl',$this->DetailUrl,true);
		$criteria->compare('Image',$this->Image,true);
		$criteria->compare('ItemType',$this->ItemType);
		$criteria->compare('Nick',$this->Nick,true);
		$criteria->compare('Pprice',$this->Pprice,true);
		if($this->ProductID > 0){
			$criteria->compare('ProductID',$this->ProductID);
		}
		$criteria->compare('PromoPrice',$this->PromoPrice,true);
		$criteria->compare('Sales',$this->Sales,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('CatID',$this->CatID,true);
		$criteria->compare('ModuleID',$this->ModuleID,true);
		$criteria->compare('StartTime',$this->StartTime,true);
		$criteria->compare('EndTime',$this->EndTime,true);
		$criteria->compare('SpecialID',$this->SpecialID,true);
		$criteria->compare('IsGather',$this->IsGather);
		$criteria->compare('TaobaoUrl',$this->TaobaoUrl,true);
		$criteria->compare('IsIndex',$this->IsIndex);
		$criteria->compare('IsPreferential',$this->IsPreferential);
		$criteria->compare('IsHot',$this->IsHot);
		$criteria->compare('IsNew',$this->IsNew);
		$criteria->compare('Status',$this->Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function createSearchCriteria(){
		$criteria=new CDbCriteria;

		$criteria->compare('GoodsID',$this->GoodsID,true);


		$criteria->compare('GoodsBmID',$this->GoodsBmID);

		$criteria->compare('GoodsName',$this->GoodsName,true);
		$criteria->compare('DetailUrl',$this->DetailUrl,true);
		$criteria->compare('Image',$this->Image,true);
		$criteria->compare('ItemType',$this->ItemType);
		$criteria->compare('Nick',$this->Nick,true);
		$criteria->compare('Pprice',$this->Pprice,true);
		if($this->ProductID > 0){
			$criteria->compare('ProductID',$this->ProductID);
		}
		$criteria->compare('PromoPrice',$this->PromoPrice,true);
		$criteria->compare('Sales',$this->Sales,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('SpecialID',$this->SpecialID,true);

		if($this->IsGather){
			$criteria->addCondition("GoodsBmID > 0");	
		}


		$criteria->compare('TaobaoUrl',$this->TaobaoUrl,true);
		$criteria->compare('IsIndex',$this->IsIndex);
		$criteria->compare('IsPreferential',$this->IsPreferential);
		$criteria->compare('IsHot',$this->IsHot);
		$criteria->compare('IsNew',$this->IsNew);



		if($this->Status == 9){
			$criteria->compare('Status',1);
			//所有正在显示的
			$_GET['end_StartTime'] = date('Y-m-d',TIME_TIME);
			if(date('d', TIME_TIME) >= 9){
				$_GET['start_EndTime'] = date('Y-m-d',TIME_TIME+86400);
			}else{
				$_GET['start_EndTime'] = date('Y-m-d',TIME_TIME);
			}
		}else{
			$criteria->compare('Status',$this->Status);
		}
		




		$criteria->compare('UserBannerID',$this->UserBannerID);




		if(!empty($this->CatID)){
			$catids = CategoryModel::getStrChildid($this->CatID);
			//echo $catids;exit;
			$criteria->addCondition("CatID in ($catids)");	
		}


		if(!empty($_GET['start_StartTime'])){
			$time = strtotime($_GET['start_StartTime']);
			$criteria->addCondition('StartTime >= ' . $time);	
		}
		if(!empty($_GET['end_StartTime'])){
			$time = strtotime($_GET['end_StartTime']) + 86400;
			$criteria->addCondition('StartTime < ' . $time);
		}



		if(!empty($_GET['start_EndTime'])){
			$time = strtotime($_GET['start_EndTime']);
			$criteria->addCondition('EndTime >= ' . $time);	
		}
		if(!empty($_GET['end_EndTime'])){
			$time = strtotime($_GET['end_EndTime']) + 86400;
			$criteria->addCondition('EndTime < ' . $time);
		}		
		return 	$criteria;	
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GoodsBakModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function beforeSave(){



		if(GROUP_NAME !== 'admin'){
			return true;
		}		
		
		if(CONTROLLER_NAME == 'sales'){
			return true;
		}
		if(ACTION_NAME == 'updateOne'){
			if(!is_numeric($this->StartTime)){
				$this->StartTime = strtotime($this->StartTime);
			}

			if(!is_numeric($this->EndTime)){
				$this->EndTime = strtotime($this->EndTime);
			}
		}else{
			if(!is_numeric($this->StartTime)){
				$this->StartTime = strtotime(date('Y-m-d 9:00:00', strtotime($this->StartTime)));
			}

			if(!is_numeric($this->EndTime)){
				$this->EndTime = strtotime(date('Y-m-d 9:00:00', strtotime($this->EndTime)));
			}			
		}


		$objModel = self::model()->findByPk($this->GoodsID);
		if($this->Status != $objModel->Status){			
			$this->UpdateTime = TIME_TIME;	
		}


		//如果是新增的表示是自己采集的商品
		//City CreditScore Location Province UserID UserName AdminID Money MoneyBond ServiceFee IsPay
		if($this->isNewRecord){
			$this->Zhekou = $this->Pprice/$this->PromoPrice;
			$this->GAddTime = TIME_TIME;
			$this->UpdateTime = TIME_TIME;			
			//刚开始的时候初始销量等于获取的销量
		}else{
			//把修改的东西同步到商品表
			if($model = GoodsBmModel::model()->findByPk($this->GoodsBmID)){
				if($model->Status != $this->Status){
					$this->UpdateTime = TIME_TIME;
				}
				$model->setScenario('admin');          
				$model->attributes = $this->attributes;
				$model->save(false);
			}
			$oldModel = self::model()->findByPk($this->GoodsID);
			if($oldModel->Status != $this->Status){
				//如果状态已经跟原来不一样
				if($this->Status != 1){
					//已经下架了或其它的一些情况,这样的情况把关联这商品的 模块商品表  与  标签商品表的 数据删除
					TagGoodsBakModel::model()->deleteAll(BaseModel::getC(array('GoodsID' => $this->GoodsID)));
				}
			}
		}
		return true;
	}
	//getOne有的是所有的都可能会出现，包括下架的等
	//getList 只取正在上架中，所以当变化的时候需要更换两个。。。。。
	public function afterDelete(){
		self::getOne($this->GoodsID, false);
	}

	public function afterSave(){


		if(GROUP_NAME !== 'admin'){
			return true;
		}


		if(CONTROLLER_NAME == 'sales'){
			return true;
		}
		//添加成功然后就看一下是否是需要添加这标签商品
		//自己添加的标签。
		if(!empty($_POST['GoodsBakModel']['Tag'])){
			//先找出原先已经存在的，然后再对比看是否需要删除，
			$arrTag = $this->getTag();//现此商品存在的taggoods
			$arrD = array();
			foreach ($_POST['GoodsBakModel']['Tag'] as $TagID) {
				if(in_array($TagID, $arrTag)){
					//这些是已经有了的，不需要删除
					$arrD[] = $TagID;
				}else{
					TagGoodsBakModel::saveData($TagID, $this->GoodsID);
				}								
			}
			//现在则是需要删除的
			$arr = array();
			foreach ($arrTag as $TagID) {
				if(!in_array($TagID, $arrD)){
					$arr[] = $TagID;
				}
			}

			if($arr){
				TagGoodsBakModel::model()->deleteAll(BaseModel::getC(array('GoodsID' => $this->GoodsID, 'TagID' => array('sign' => 'in', 'data' => BaseModel::getStrIn($arr)))));
				TagGoodsBakModel::getList($this->GoodsID, false);
			}
		}else{
			if(IS_POST){
				$arr = $this->getTag();//现此商品存在的taggoods	
				if($arr){
					TagGoodsBakModel::model()->deleteAll(BaseModel::getC(array('GoodsID' => $this->GoodsID, 'TagID' => array('sign' => 'in', 'data' => BaseModel::getStrIn($arr)))));
					TagGoodsBakModel::getList($this->GoodsID, false);
				}	
			}				
		}

		//添加成功然后就看一下是否是需要添加这标签商品
		if(!empty($_POST['GoodsBakModel']['Tag_add'])){
			//先找出原先已经存在的，然后再对比看是否需要删除，
			//$arrTag = $this->getTag();
			foreach ($_POST['GoodsBakModel']['Tag_add'] as $key => $strTagName) {
				//先看一下是否有这个名称的标签，如果没有的话则新增
				TagGoodsBakModel::saveDataTag($strTagName, $this->GoodsID);				
			}
		}

		self::getOne($this->GoodsID, false);

	}

    //获取所有的有选的键值
    public function getTag(){
    	$arr = TagGoodsBakModel::getListField($this->GoodsID, 'TagID');
    	return $arr;
    }


	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getItemTypeHtml($mixData = false){
		$arrData = array(
			0 => '淘宝',
			1 => '天猫',
			2 => '天猫精品',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

    /**
     * 取消活动（利用用户取消报名实现）
     * @param type $object 商品ID或者商品对象
     * @return boolean
     */
    public function activity() {
        if (empty($this->GoodsBmID)) {
            yii::app()->user->setFlash('找不到报名记录ID');
            return false;
        }
        $objGoodsBmModel = GoodsBmModel::model()->findByPk($this->GoodsBmID);
        if (empty($objGoodsBmModel)) {
        	yii::app()->user->setFlash('该商品的报名信息不存在，无法取消活动！');
            return false;
        }
        $this->Status = 4;
        $this->save(false);
        return true;
    }


    public static function getTomorrow($intPage,$intPageSize,$strOrder, $bloIsCache = true, $intCacheTime = 600){
    	//获得所有的数据,包括其总数
        $strCache = "GoodsBakModel_getTomorrow_" . $intPage . '-' . $intPageSize . '' . $strOrder;

        $arrResult = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if(!($bloIsCache && $arrResult !== false)){
        	$arrResult = array();
        	$tomorrow = strtotime('tomorrow');

	    	$where = " where g.Status = 1 AND g.StartTime >= ".$tomorrow . ' ';

	        $sql_count = "select count(*) from tk_goods g " . $where;

	        $order = self::getOrderd($strOrder);

	        




			$connection=Yii::app()->db;
			$command=$connection->createCommand($sql_count);
			$intCount=$command->queryScalar();

	        $arrResult['count'] = $intCount;
	        //如果需要分页则设置分页并取分页对象
	        // && $intCount > $intPageSize
            $objPager = new CPagination($intCount);
            $objPager->setCurrentPage($intPage - 1 < 0 ? 0 : $intPage - 1);
            $objPager->pageSize = $intPageSize;
            $arrResult['page'] = $objPager; 
            $sql = " limit " . $objPager->getOffset() . ' , ' . $intPageSize;


            $sql = "select * from tk_goods g "  . $where . $sql; 

			$command=$connection->createCommand($sql);
			$arrDatas=$command->queryAll();


	        foreach ($arrDatas as $key => $arrData) {
	            //$arrResult['data'][$arrData['GoodsID']] = $arrData;
	            $arrResult['data'][] = $arrData;
	        }
	        if(empty($arrResult['data'])){
	        	$arrResult['data'] = array();
	        }

        	yii::app()->cache->set($strCache, $arrResult, $intCacheTime);
        }

        return $arrResult;   	    	
    }


    public  function getOrderd($order){
        switch ($order) {
            //销量从大到小
            case 1:
                $order = 'Sales DESC,GoodsID DESC';
                break;
            //价格从小到大
            case 2:
                $order = 'PromoPrice asc,GoodsID DESC';
                break;
            //价格从大到小
            case 3:
                $order = 'PromoPrice DESC,GoodsID DESC';
                break;
            //按人气排名
            case 4:
                $order = 'View DESC,GoodsID DESC';
                break;
            //折扣排序
            case 5:
                $order = 'Zhekou DESC,GoodsID DESC';
                break;                                                                                                                
            default:
                $order = 'StartTime desc,Sorting desc, GoodsID desc';//默认排序
                break;  
        }

        return $order;     	
    }


    /**
     * 如果标签是两个以上的组合，则不是以这种方式来获取数据，
     * 如果排序是其它的排序，也不是以这样的方式来获取
     * 根据版块，分类，标签， 专场 来取商品
     * @return [type] [description]
     */
    //                                    条件数组               是否打乱            是否缓存            缓存时间
    public function getVariedGoods($Conditions = array(), $bloIsUpset = true, $bloIsCache = true, $intCacheTime = 600){
        
    	/*各条件*/
        $intCatID = isset($Conditions['cid']) ?  $Conditions['cid'] : Yii::app()->request->getParam('cid');           //分类,可能多个一起取
        $UserBannerID = isset($Conditions['bid']) ? $Conditions['bid']: Yii::app()->request->getParam('bid');     //品牌团   
        $strTag = isset($Conditions['tid']) ? $Conditions['tid'] : Yii::app()->request->getParam('tid');           //标签   
        $intIsIndex = isset($Conditions['isindex']) ? $Conditions['isindex'] : Yii::app()->request->getParam('isindex');  //是否首页      
        $extra = isset($Conditions['eid']) ? $Conditions['eid'] : Yii::app()->request->getParam('eid');   //额外  
        $strOrder = isset($Conditions['order']) ? $Conditions['order'] : Yii::app()->request->getParam('order');   //排序 
        $hot = isset($Conditions['hot']) ? $Conditions['hot'] : Yii::app()->request->getParam('hot');           //热门 
        $IsPreferential = isset($Conditions['IsPreferential']) ? $Conditions['IsPreferential'] : Yii::app()->request->getParam('IsPreferential');           //特惠
		$settime = isset($Conditions['settime']) ? $Conditions['settime'] : Yii::app()->request->getParam('settime'); //1则为选明日预告商品2则是今日上架商品 

        $gettype = isset($Conditions['gettype']) ? $Conditions['gettype'] : Yii::app()->request->getParam('gettype');   //取数据模式
        //0或''只取数据   1只取总数+分页    2取数据与总数+分页
        if($gettype > 2){
        	//两个条件都不符合是不存在的
        	return array();
        }     

        if(isset($Conditions['page'])){
        	$intPage = $Conditions['page'];
        }else{
        	$intPage = Yii::app()->request->getParam('page');
        	empty($intPage) ? $intPage = 1 : '';
        }
        if(isset($Conditions['pagesize'])){
        	$intPageSize = $Conditions['pagesize'];
        }else{
        	$intPageSize = Yii::app()->request->getParam('pagesize');
        }
        empty($intPageSize) ? $intPageSize = 20 : '';


        $strCache = "GoodsBakModel_getVariedGoods" . $intCatID . '_' . $UserBannerID . '_' . $strTag . '_' . $intIsIndex . '_'  . $extra . '_' . $strOrder . '_' . $hot . '_' . $IsPreferential . '_' . $intPage . '_' . $intPageSize . '_' . $gettype . '_' . $tomorrow . '_' . $settime;
        /*各条件END*/
        

       
        $arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }
 
        //标签与模块的条件
        if($strTag !== ''){     //存在标签的情况下就用标签
        	$table = 'tk_goods_tag';
        }else{          //不存在模块与标签的情况下，，，直接找商品表
        	$table = 'tk_goods';
        }

		
        if($settime == 1){//明日预告
        	$tomorrow = strtotime('tomorrow');
	    	$sql = "select * from {$table}  where Status = 1 and StartTime >= ".$tomorrow;        	
        }else if($settime == 2){//今日新品
        	$startTime = TIME_TIME + 3600;
        	$today = strtotime('today');
        	$sql = "select * from {$table}  where Status = 1 and StartTime >= {$today} and StartTime <= ".$startTime." AND EndTime > ".TIME_TIME;       	
        }else{
	        /*拼SQL*/
	        if($UserBannerID){
	        	$startTime = TIME_TIME + 86400*2;
	        }else{
	        	$startTime = TIME_TIME + 3600;	        	
	        }
	        $sql = "select * from {$table}  where Status = 1 and StartTime <= ".$startTime." AND EndTime > ".TIME_TIME;        	
        }


        //N个标签
        if($strTag !== ''){
            if(is_numeric($strTag)){
                $TagID = " AND TagID = {$strTag} ";
            }else{
                $TagID = " AND TagID in({$strTag}) ";
            }
            $sql .= $TagID;
        }
       
        //分类的条件
        if($intCatID !== ''){
            $intCatID = CategoryModel::getStrChildid($intCatID);
            if(is_numeric($intCatID)){
                $sql .= " AND CatID = {$intCatID}";  
            }else{
                $sql .= " AND CatID in({$intCatID})";  
            }            
        }

        //品牌团的条件
        if($UserBannerID !== ''){
            $sql .= " AND UserBannerID = {$UserBannerID}";     
        }else{
        	$sql .= " AND UserBannerID = 0";     
        }




        //是否首页展示的条件
        if($intIsIndex !== ''){
            $sql .= " AND IsIndex = {$intIsIndex}";   
        }   


        //是否特惠
        if($IsPreferential !== ''){
            $sql .= " AND IsPreferential = {$IsPreferential}";   
        }
        //是否热门，，，，首页特推荐的几款产品
        //如果没有选，则会员专享的其它地方不会展示开来
        if($hot !== ''){
            $sql .= " AND IsHot = {$hot}";   
        }
         
        //额外的条件
        switch ($extra) {
            case 1:
                $sql .= ' and PromoPrice < 10';
                break;
            case 2:
                $sql .= ' and PromoPrice >= 10 and PromoPrice < 30 ';
                break;
            case 3:
                $sql .= ' and PromoPrice >= 30 and PromoPrice < 60 ';
                break;
            case 4:
                $sql .= ' and PromoPrice >= 60 ';
                break;
            case 5:
                $sql .= ' and PromoPrice <= 19.9 ';
                break;
        }
        /*拼SQL结束*/
        $this->getResult($gettype,$sql,$strOrder,$strCache,$intCacheTime,$bloIsUpset,$intPage,$intPageSize,$arrResult);
        return $arrResult;        

    }



	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			1 => '上架',	
			5 => '下架',						
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	public static function getOne($goodsID, $bloIsCache = true){
		if(!is_numeric($goodsID)){
			return array();
		}
		$strKey = "GoodsBakModel_getOne" . $goodsID;
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();

        $arrCache = self::model()->findByPk($goodsID);
        $arrCache = empty($arrCache) ? array() : $arrCache->attributes;

        yii::app()->cache->set($strKey, $arrCache, 1800);
        return $arrCache;	

	}


	/**
	 * [getBannerGoods description]
	 * @param  [type]  $page     展示第几页的品牌数据
	 * @param  [type]  $pagesize 每页展示多少个品牌
	 * @param  array   $arrData  所有的品牌数据
	 * @param  integer $pagegood 每个品牌展示的商品数据
	 * @return [type]            [description]
	 */
	public function getBannerGoods($page, $pagesize, $arrData = array(), $pagegood = 8){
		$arrResult_d = array();
        //查找从哪个品牌开始
        $begin = ($page-1)*$pagesize;
        $end = ($page-1)*$pagesize + $pagesize;

        $datas= self::getBannerList($begin,$end, $arrData);//获取需要展示的品牌团ID
        foreach ($datas as $value) {
        	if($pagegood){
	        	$d = $this->getVariedGoods(array('pagesize' => $pagegood, 'bid' => $value, 'page' => ''), true, true, 600);
	        	$arrResult_d[$value] =  $d; 
        	}else{
        		$arrResult_d[$value] = true;
        	}        
        }


        $arrResult['data'] = $arrResult_d;
        $arrResult['count'] = count($arrData);

        $objPager = new CPagination($arrResult['count']);
        $objPager->setCurrentPage($page - 1 < 0 ? 0 : $page - 1);
        $objPager->pageSize = $pagesize;
        $arrResult['page'] = $objPager; 

        return $arrResult;
	}



	//按分页获取需要的页面的banner
	public function getBannerList($begin, $end, $arrData = array()){
		$arrResult = array();
        //拥有商品的品牌ID
        $arrBanner = empty($arrData) ? MemberBrandModel::getList() : $arrData;
        $count = 0;
        foreach ($arrBanner as $key => $value) {
        	if($count >= $begin){
        		if($count < $end){
        			$arrResult[] = $value['BrandID'];
        		}else{
        			break;
        		}       		
        	}
        	$count++;
        }
        return $arrResult;
	}



	


}
