<?php

/**
 * This is the model class for table "{{advertising}}".
 * 广告表
 * The followings are the available columns in table '{{advertising}}':
 * @property string $AdvertisingID
 * @property string $Title
 * @property string $Sorting
 * @property string $AddTime
 * @property integer $Status
 * @property string $Image
 * @property string $Url
 * @property integer $Type
 */
class AdvertisingModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{advertising}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Title, Image', 'required'),
			array('Url', 'match', 'pattern'=>'/^http:\/\//','message'=>'{Url}必须以http://开头'),
			array('Status, Type, Sorting', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Title, Sorting, AddTime, Status, Image, Url, Type', 'safe', 'on'=>'admin'), 
			array('Title, Sorting, AddTime, Status, Image, Url, Type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'AdvertisingID' => 'ID',
			'Title' => '名称',
			'Sorting' => '排序',
			'AddTime' => '添加时间',
			'Status' => '状态',
			'Image' => '图片',
			'Url' => '链接',
			'Type' => '类型',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('AdvertisingID',$this->AdvertisingID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Image',$this->Image,true);
		$criteria->compare('Url',$this->Url,true);
		$criteria->compare('Type',$this->Type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdvertisingModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function createSearchCriteria(){
		$criteria=new CDbCriteria;

		$criteria->compare('AdvertisingID',$this->AdvertisingID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Image',$this->Image,true);
		$criteria->compare('Url',$this->Url,true);
		$criteria->compare('Type',$this->Type);
		return 	$criteria;	
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
			$this->Status = 1;
		}else{
			$oldModel = self::model()->findByPk($this->AdvertisingID);
			if($oldModel->Image != $this->Image){
				UploadFile::deleteImg($oldModel->Image);
			}
		}
		return true;
	}

	/**
	 * 获取广告类型
	 * @return [type] [description]
	 */
	public static function getTypeHtml($mixData = false){
		$arrData = array(
			1 => '首页banner图',
			2 => '首页banner图底下四张图之一',	
			3 => '首页banner图右边两张图之一',	
			4 => '登录',		
			5 => 'app轮播图',		
			6 => '文章广告',		
			7 => '新首页banner图',		
			8 => '新首页广告图1张',			
			9 => 'app百科首页',						
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			0 => '不可用',
			1 => '可用',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}
	/**
	 * 保存后更新缓存
	 * @return [type] [description]
	 */
	public function afterSave(){
		self::getListType($this->Type, false);
		return true;
	}
	/**
	 * 保存后
	 * @return [type] [description]
	 */
	public function afterDelete(){
		UploadFile::deleteImg($this->Image);
		self::getListType($this->Type, false);
		return true;
	}

	/**
	 * 根据类型来查找广告图
	 * @param  [type] $intID [description]
	 * @return [type]        [description]
	 */
	public static function getListType($intType, $bloIsCache = true){
		$strKey = "AdvertisingModel_getListType" . $intType;
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $obj = BaseModel::getC(array('Type' => $intType, 'Status' => 1));
        $obj->order = 'Sorting DESC,AddTime DESC';
        $arrCache = self::model()->findAll($obj);
        foreach ($arrCache as $key => $value) {
        	$arrCache[$key] = $value->attributes;
        }
/*        if(count($arrCache) == 1){
        	$arrCache = current($arrCache);
        }*/

        yii::app()->cache->set($strKey, $arrCache, 3600);
        return $arrCache;
	}
}
