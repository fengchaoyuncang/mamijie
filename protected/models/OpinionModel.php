<?php

/**
 * This is the model class for table "{{opinion}}".
 *
 * The followings are the available columns in table '{{opinion}}':
 * @property integer $OpinionID
 * @property string $Title
 * @property string $Content
 * @property string $Contact
 * @property integer $AddTime
 * @property integer $UserID
 */
class OpinionModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{opinion}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('AddTime, UserID, Status', 'numerical', 'integerOnly'=>true),
			array('Title, Content', 'required'),
			array('Contact', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('OpinionID, Title, Content, Contact, AddTime, UserID', 'safe', 'on'=>'search'),
		);
	}
	public function beforeSave(){
		$this->Content = CHtml::encode($this->Content);
		$this->Title = CHtml::encode($this->Title);
		$this->Contact = CHtml::encode($this->Contact);
		if($this->isNewRecord){
			$this->Status = 0;
			$this->UserID = yii::app()->user->id;
			$this->AddTime = time();
		}
		return true;
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OpinionID' => 'Opinion',
			'Title' => 'Title',
			'Content' => 'Content',
			'Contact' => 'Contact',
			'AddTime' => 'Add Time',
			'UserID' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OpinionID',$this->OpinionID);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Content',$this->Content,true);
		$criteria->compare('Contact',$this->Contact,true);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('UserID',$this->UserID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OpinionModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			0 => '未处理',
			1 => '已处理',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

}
