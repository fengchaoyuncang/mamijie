<?php

/**
 * This is the model class for table "{{member_sign}}".
 *
 * The followings are the available columns in table '{{member_sign}}':
 * @property integer $SignID
 * @property integer $UserID
 * @property integer $Type
 * @property string $Remark
 * @property integer $AddTime
 * @property integer $Num
 * @property integer $Score
 */
class MemberSignModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{member_sign}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('UserID, Type, Remark, AddTime, Num, Score', 'safe'),
			array('SignID, UserID, Type, Remark, AddTime, Num, Score', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'SignID' => 'Sign',
			'UserID' => 'User',
			'Type' => 'Type',
			'Remark' => 'Remark',
			'AddTime' => 'Add Time',
			'Num' => 'Num',
			'Score' => 'Score',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('SignID',$this->SignID);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('Type',$this->Type);
		$criteria->compare('Remark',$this->Remark,true);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Num',$this->Num);
		$criteria->compare('Score',$this->Score);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MemberSignModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
		}
		return true;
	}


	public function canSign($type = 1, $uid = '', $bloIsCache = true){

		empty($uid) ? $uid = yii::app()->user->id : '';
		if(empty($uid)){
			return false;
		}
		$strCache = 'MemberSignModel_canSign' . $type . '_' . $uid;
        $arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }



		$today = strtotime('today');
		$data = self::model()->find("UserID = {$uid} and AddTime >= {$today} and Type = {$type}");
		if($data){
			$return = true;
		}else{
			$return = false;
		}
		unset($data);
		$t = strtotime('Tomorrow');
		$t = $t - TIME_TIME - 1;
		yii::app()->cache->set($strCache, $return, $t);

		return $return;
	}	


	//获取这个月到今天的签到记录
	public function getSignData($type = 1, $uid = '', $bloIsCache = true){
		$uid == '' ? $uid = yii::app()->user->id : '';

		$strCache = 'MemberSignModel_getSignData' . $type . '_' . $uid;
        $arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

		//获取本月第一天的时间戳
		$year = date("y");
		$month = date("m");
		$time = strtotime("{$year}-$month-1");

		$result = array();
		$datas = self::model()->findAll(BaseModel::getC(array('order' => 'AddTime asc', 'UserID' => $uid, 'Type' => $type, 'AddTime' => array('sign' => '>=', 'data' => $time))));
		foreach ($datas as $key => $data) {
			$num = date('d', $data->AddTime);//签到时间戳
			if(substr($num, 0,1) == '0'){
				$num = substr($num, 1);
			}
			$result[] = $num;
		}
		$t = strtotime('Tomorrow');
		$t = $t - TIME_TIME - 1;
		yii::app()->cache->set($strCache, $result, $t);

		return $result;
	}





	//用户签到
	//type1 为PC端签到  2为手机端签到
	//签到成功则返回用户的积分总数
	public function sign($type = 1){
		try {
			if(!$uid = yii::app()->user->isLogin()){
				throw new Exception("未登录");				
			}
			if(is_array($uid)){
				throw new Exception("你未绑定账户");				
			}

			$key = "MemberSignModel_sign" . $uid . "_{$type}";		
			$cache = yii::app()->cache->get($key);
			if($cache){
				throw new Exception("请不要重复提交");				
			}else{
				yii::app()->cache->set($key, 1, 60);
			}

			if($this->canSign($type, $uid)){
				throw new Exception("你已经签到");			
			}

			//找出最后一次签到的model
			$lastModel = self::model()->find(BaseModel::getC(array('UserID' => $uid, 'Type' => $type, 'order' => 'AddTime desc')));
			if($lastModel){
				if($lastModel->AddTime < strtotime('yesterday')){//如果最后一次签到是在昨天的话则连续签到次数加1
					$num = 0;
				}else{
					$num = $lastModel->Num;
				}				
			}else{
				$num = 0;
			}

			$num++;

			$score = 5;
			$data = array(
				'UserID' => $uid,
				'Type' => $type,
				'Score' => $score,
				'Num' => $num,
			);
			$primaryKey = self::saveData($data);//签到记录保存

			//送积分
			$data = array(
				'Type' => 1,
				'Score' => $score,
				'UserID' => $uid,
				'RelationID' => $primaryKey,
			);
			MemberScoreChangeModel::saveData($data);

			//更改用户的积分
			$member = MemberModel::model()->findByPk($uid);
			$member->Score = $member->Score + $score;
			$member->save(false, array('Score'));
			$this->canSign($type,$uid,false);//更新今天已签到缓存
			$this->getSignData($type,$uid,false);//更新这个月的缓存数据

			return $member->Score;			
		} catch (Exception $e) {
			if(!empty($key)){
				yii::app()->cache->get($key);
			}			
			$this->addError('',$e->getMessage());
			return false;
		}
	}




	//保存数据
	public static function saveData($arrData){
        $model = new  self();
		$model->attributes = $arrData;
		$model->save(true);
		return $model->primaryKey;
	}

}
