<?php

/**
 * This is the model class for table "{{articles_weekly_detail}}".
 *
 * The followings are the available columns in table '{{articles_weekly_detail}}':
 * @property integer $WeeklyDetailID
 * @property integer $WeeklyID
 * @property string $Content
 * @property string $Title
 * @property string $AddTime
 * @property string $Sorting
 */
class ArticlesWeeklyDetailModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{articles_weekly_detail}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('WeeklyID', 'numerical', 'integerOnly'=>true),
			array('Title', 'length', 'max'=>255),
			array('AddTime, Sorting', 'length', 'max'=>11),
			array('Content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('WeeklyID, Content, Title, AddTime, Sorting,Subhead,TitleSeo,KeywordsSeo,DescriptionSeo', 'safe'),
			array('WeeklyDetailID, WeeklyID, Content, Title, AddTime, Sorting,Subhead', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'WeeklyDetailID' => 'ID',
			'WeeklyID' => '周刊ID',
			'Content' => '内容',
			'Title' => '标题',
			'AddTime' => '添加时间',
			'Sorting' => '排序',
			'Type' => '周刊类型',
			'Subhead' => '副标题',
			'TitleSeo' => 'Seo标签',
			'KeywordsSeo' => 'seo关键字',
			'DescriptionSeo' => 'seo描述',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('WeeklyDetailID',$this->WeeklyDetailID);
		$criteria->compare('WeeklyID',$this->WeeklyID);
		$criteria->compare('Content',$this->Content,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Sorting',$this->Sorting,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('WeeklyDetailID',$this->WeeklyDetailID);
		$criteria->compare('WeeklyID',$this->WeeklyID);
		$criteria->compare('Content',$this->Content,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		return $criteria;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArticlesWeeklyDetailModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
		}
		return true;
	}
	/**
	 * 保存后更新缓存
	 * @return [type] [description]
	 */
	public function afterSave(){
		self::getListWeekly($this->WeeklyID, false);
		return true;
	}
	/**
	 * 保存后
	 * @return [type] [description]
	 */
	public function afterDelete(){
		self::getListWeekly($this->WeeklyID, false);
		return true;
	}
	/**
	 * 根据周刊ID来获取
	 * @param  [type] $intID [description]
	 * @return [type]        [description]
	 */
	public static function getListWeekly($Weekly, $bloIsCache = true){
		$strKey = "ArticlesWeeklyDetailModel_getListWeekly" . $Weekly;
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();
        $obj = BaseModel::getC(array('WeeklyID' => $Weekly));
        $obj->order = 'Sorting DESC,AddTime DESC';
        $datas = self::model()->findAll($obj);
        foreach ($datas as $key => $value) {
        	$arrCache[] = $value->attributes;
        }

        yii::app()->cache->set($strKey, $arrCache, 3600);
        return $arrCache;
	}	

}
