<?php

/**
 * 网站配置模型
 * File Name：ConfigModel.php
 * File Encoding：UTF-8
 * File New Time：2014-5-4 17:20:43
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class ConfigModel extends BaseModel {

    public function tableName() {
        return '{{config}}';
    }

    public function rules() {
        return array(
            array('varname,info,value', 'safe'),
        );
    }

    /**
     * 获取站点配置信息，这里建议以后要从新设计表，不科学
     * @param type $options 布尔值true刷新缓存，字符串表示获取某一个选项值
     * @return array|string 配置
     */
    public function getConfig($options = '') {
        $key = 'ConfigModel_getConfig_config';
        //如果是布尔值true，进行缓存更新
        if ((is_bool($options) && $options == true) || is_null($options)) {
            Yii::app()->cache->delete($key);
        }
        $cache = Yii::app()->cache->get($key);
        if (empty($cache)) {
            $dataAll = $this->findAll();
            $cache = array();
            foreach($dataAll as $rs){
                $cache[$rs->varname] = $rs->value;
            }
            //进行永久性缓存
            Yii::app()->cache->set($key, $cache);
        }
        //如果是字符串，返回指定配置
        if (is_string($options) && $options) {
            return isset($cache[$options]) ? $cache[$options] : null;
        }
        return $cache;
    }

    //数据保存后回调
    protected function afterSave() {
        //更新缓存
        $this->getConfig(true);
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
