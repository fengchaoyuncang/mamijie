<?php

/**
 * This is the model class for table "{{articles}}".
 * 文章MODEL,,,,,,,通过文章分类来获取文章，，，，，
 * The followings are the available columns in table '{{articles}}':
 * @property string $ArticlesID
 * @property string $Title
 * @property integer $CatID
 * @property string $Content
 * @property string $AddTime
 * @property string $UserID
 * @property string $UserName
 * @property string $Sorting
 * @property integer $IsAdmin
 */
class ArticlesNewModel extends ArticleBaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{articles_new}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Title, Content,CatID', 'required'),
			array('IsShow,CatID, IsAdmin', 'numerical', 'integerOnly'=>true),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Title, CatID, Content, AddTime, UserID, UserName, Sorting, IsAdmin,IsShow,IsHot,IsRecommend,IsNew,BigImg,MidImg,SmlImg,TitleSeo,KeywordsSeo,DescriptionSeo,IsAppIndex,AppImg', 'safe', 'on'=>'admin'),
			array('ArticlesID, Title, CatID, Content, AddTime, UserID, UserName, Sorting, IsAdmin,IsShow', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ArticlesID' => '文章ID',
			'Title' => '标题',
			'CatID' => '分类ID',
			'Content' => '内容',
			'AddTime' => '添加时间',
			'UserID' => '用户ID',
			'UserName' => '用户名',
			'Sorting' => '排序',
			'IsAdmin' => '是否后台人员上传',
			'IsShow' => '是否显示',
			'IsHot' => '热门',
			'IsRecommend' => '推荐',
			'IsNew' => '最新',
			'BigImg' => '大banner图',
			'MidImg' => '中banner图',
			'SmlImg' => '小banner图',
			'TitleSeo' => 'Seo标签',
			'KeywordsSeo' => 'seo关键字',
			'DescriptionSeo' => 'seo描述',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ArticlesID',$this->ArticlesID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('CatID',$this->CatID);
		$criteria->compare('Content',$this->Content,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('UserID',$this->UserID,true);
		$criteria->compare('UserName',$this->UserName,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('IsAdmin',$this->IsAdmin);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArticlesNewModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function createSearchCriteria(){
		$criteria=new CDbCriteria;

		$criteria->compare('ArticlesID',$this->ArticlesID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('CatID',$this->CatID);
		$criteria->compare('Content',$this->Content,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('UserID',$this->UserID,true);
		$criteria->compare('UserName',$this->UserName,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('IsAdmin',$this->IsAdmin);
		$criteria->compare('IsHot',$this->IsHot);
		$criteria->compare('IsRecommend',$this->IsRecommend);
		$criteria->compare('IsNew',$this->IsNew);
		return 	$criteria;	
	}

	public function beforeSave(){
		if($this->isNewRecord){
			if(GROUP_NAME == 'admin'){
				$this->UserID = AdminBase::$uid;
				$this->UserName = AdminBase::$userInfo['real_name'];
				$this->IsAdmin = 1;
			}else{
				$this->IsAdmin = 0;
				if(yii::app()->user->isLogin()){
					$this->UserID = yii::app()->user->id;	
					$this->UserName = AdminBase::$userInfo['UserName'];			
				}else{
					$this->UserID = 0;
					$this->UserName = '';
				}
			}
			$this->AddTime = time();
		}else{
			$m = self::model()->findByPk($this->ArticlesID);
			if($m->BigImg != $this->BigImg){
				UploadFile::deleteImg($m->BigImg);
			}
			if($m->MidImg != $this->MidImg){
				UploadFile::deleteImg($m->MidImg);
			}
			if($m->SmlImg != $this->SmlImg){
				UploadFile::deleteImg($m->SmlImg);
			}
		}
		$this->Title = CHtml::encode($this->Title);
		//$this->Content = CHtml::encode($this->Content);
		return true;
	}


	//取文章
    /** 获取列表，杂项的
		与取商品是一样的条件。。。。。。。。。实现方法
		把内容去掉，因为取缓存数据太大。
     * @return [type] [description]
     */
    public  function getVaried($Conditions = array(), $bloIsUpset = true, $bloIsCache = true, $intCacheTime = 600){
    	$strOrder = '';
        $CatID = isset($Conditions['cid']) ?  $Conditions['cid'] : Yii::app()->request->getParam('cid');        //分类条件
        $UserID = isset($Conditions['uid']) ?  $Conditions['uid'] : Yii::app()->request->getParam('uid');           //分类
        $IsAdmin = isset($Conditions['admin']) ?  $Conditions['admin'] : Yii::app()->request->getParam('admin');           //分类
        $IsRecommend = isset($Conditions['recommend']) ?  $Conditions['recommend'] : Yii::app()->request->getParam('recommend');           //分类
        $IsHot = isset($Conditions['hot']) ?  $Conditions['hot'] : Yii::app()->request->getParam('hot');           //分类
        $IsNew = isset($Conditions['new']) ?  $Conditions['new'] : Yii::app()->request->getParam('new');           //分类
        $IsImg = isset($Conditions['IsImg']) ?  $Conditions['IsImg'] : Yii::app()->request->getParam('IsImg');           //中间的图显示
        $AppImg = isset($Conditions['AppImg']) ?  $Conditions['AppImg'] : Yii::app()->request->getParam('AppImg');           //中间的图显示
        $gettype = isset($Conditions['gettype']) ? $Conditions['gettype'] : Yii::app()->request->getParam('gettype');   //取数据模式
        $IsAppIndex = isset($Conditions['IsAppIndex']) ?  $Conditions['IsAppIndex'] : Yii::app()->request->getParam('IsAppIndex');           //分类
        if(isset($Conditions['page'])){
        	$intPage = $Conditions['page'];
        }else{
        	$intPage = Yii::app()->request->getParam('page');
        	empty($intPage) ? $intPage = 1 : '';
        }
        if(isset($Conditions['pagesize'])){
        	$intPageSize = $Conditions['pagesize'];
        }else{
        	$intPageSize = Yii::app()->request->getParam('pagesize');
        }
        empty($intPageSize) ? $intPageSize = 20 : '';


        $strCache = "ArticlesNewModel_getVaried_" . $CatID . '_' . $UserID . '_' . $IsAdmin . '_' . $IsRecommend . '_' . $IsHot . '_'  . $IsNew . '_' . $IsImg . '_'  . $intPage . '_'  . $intPageSize . '_'  . $gettype . '_'  . $AppImg . '_'  . $IsAppIndex;
        $arrResult = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrResult !== false){
            return $arrResult;
        }
        $arrResult = array();
        //这里把一些很长的content，字段给去除了，
        $select = '*';
        $sql = "select {$select} from tk_articles_new where IsShow = 1 ";

        //分类的条件
        if($CatID !== ''){
            $CatID = CategoryArticlesNewModel::getStrChildid($CatID);
            if(is_numeric($CatID)){
                $sql .= " AND CatID = {$CatID}";  
            }else{
                $sql .= " AND CatID in({$CatID})";  
            }            
        }  
        if($UserID !== ''){
            $sql .= " AND UserID = {$UserID}";     
        }              
        if($IsAdmin !== ''){
            $sql .= " AND IsAdmin = 1";     
        }             
        if($IsRecommend !== ''){
            $sql .= " AND IsRecommend = 1";     
        }             
        if($IsHot !== ''){
            $sql .= " AND IsHot = 1";     
        }             
        if($IsNew !== ''){
            $sql .= " AND IsNew = 1";     
        }

        if($IsImg){
            $sql .= " AND MidImg != ''";     
        }else if($IsImg === false){
        	$sql .= " AND MidImg = ''";     
        }
        if($AppImg !== ''){
            $sql .= " AND AppImg != ''";     
        }
        if($IsAppIndex !== ''){
            $sql .= " AND IsAppIndex = 1";     
        }


        $this->getResult($gettype,$sql,$strOrder,$strCache,$intCacheTime,$bloIsUpset,$intPage,$intPageSize,$arrResult);
        return $arrResult;

    }

    /**
     * 取具体的一篇文章
     * @return [type] [description]
     */
    public static function getDataDetail($ArticlesID, $bloIsCache = true){
        $strCache = "ArticlesNewModel_getDataDetail" . $ArticlesID;

        $arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();
        $objModel = self::model()->findByPk($ArticlesID);
        if($objModel){
        	$arrCache = $objModel->attributes;
        	yii::app()->cache->set($strCache, $arrCache, 60);
        }else{
        	yii::app()->cache->delete($strCache);
        }

        return $arrCache;   	
    }

    public function afterSave(){
    	self::getDataDetail($this->ArticlesID, false);
    }

    public function afterDelete(){
    	UploadFile::deleteImg($this->BigImg);
    	UploadFile::deleteImg($this->MidImg);
    	UploadFile::deleteImg($this->SmlImg);
    	self::getDataDetail($this->ArticlesID, false);
    }

	/**
	 * 是否显示
	 * @return [type] [description]
	 */
	public static function getIsShowHtml($mixData = false){
		$arrData = array(
			0 => '不显示',
			1 => '显示',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}
	/**
	 * 取分类下面的子分类及其文章
	 * @return [type] [description]
	 */
	public static function getCatData($CatID){
		$arrResult = array();
		$arrCat = CategoryArticlesNewModel::getArrChildid($CatID);
		foreach($arrCat as $cat){
			if($cat == $CatID){
				continue;
			}
			$arrResult[$cat] = CategoryArticlesNewModel::getDataDetail($cat);
			$arrResult[$cat]['data'] = self::model()->getVaried(array('cid' => $cat, 'pagesize' => 20));
		}

		return $arrResult;
	}

}
