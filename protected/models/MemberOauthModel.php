<?php

/**
 * This is the model class for table "{{member_oauth}}".
 *
 * The followings are the available columns in table '{{member_oauth}}':
 * @property integer $OauthID
 * @property integer $Type
 * @property integer $AddTime
 * @property integer $UpdateTime
 * @property integer $UserID
 * @property integer $openid
 * @property integer $token
 */
class MemberOauthModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{member_oauth}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Type, AddTime, UpdateTime, UserID, openid, token', 'safe'),
			array('OauthID, Type, AddTime, UpdateTime, UserID, openid, token', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OauthID' => 'Oauth',
			'Type' => 'Type',
			'AddTime' => 'Add Time',
			'UpdateTime' => 'Update Time',
			'UserID' => 'User',
			'openid' => 'Openid',
			'token' => 'Token',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OauthID',$this->OauthID);
		$criteria->compare('Type',$this->Type);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('UpdateTime',$this->UpdateTime);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('openid',$this->openid);
		$criteria->compare('token',$this->token);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MemberOauthModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = TIME_TIME;
			$this->UpdateTime = TIME_TIME;
		}else{
			$this->UpdateTime = TIME_TIME;
		}
		return true;
	}	

	public function saveData($token,$openid,$type = 1){
		$model = self::model()->find(BaseModel::getC(array('openid' => $openid, 'type' => $type)));
		if($model){
			//已经登录过了,
			$model->token = $token;
			$model->save(false);
		}else{
			//从来没有绑定登录过,
			$model = new self();
			$model->Type = $type;
			$model->openid = $openid;
			$model->token = $token;
			$model->save();
		}
		return $model;
	}

	//获取基本信息
	public function get_user_info($bloIsCache = true){

		$strKey = "MemberOauthModel_get_user_info" . $this->OauthID;
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

		switch ($this->Type) {
			case '1':
				include(yii::app()->basePath . '/extensions/Login/qq/qqConnectAPI.php');
		        $qc = new QC($this->token, $this->openid);//先token,再openid
		        $arrCache = $qc->get_user_info(); 		       				
				break;
			
			default:
				break;
		}

        yii::app()->cache->set($strKey, $arrCache, 3600);
        return $arrCache;		
	}

	//关联用户
	public function relationLogin($username, $emailphone = '', $password = ''){
		if($this->UserID){
			$this->addError('UserID','已经关联用户');
			return false;
		}
		if($emailphone){
			$model = new RelationLoginNewModel();
			$model->username = $username;
			$model->emailphone = $emailphone;
			if($model->validate() && $memberModel = $model->saveData($this)){
				return $memberModel;
			}else{
				$error = $model->getErrors();
	            $current = current($error);
				$this->addError('UserID',$current[0]);	
				return false;		
			}			
		}else{
			$model = new RelationLoginModel();
			$model->username = $username;
			$model->password = $password;
			if($model->validate() && $memberModel = $model->checkUserName($this)){
				return $memberModel;
			}else{
				$error = $model->getErrors();
	            $current = current($error);	            
				$this->addError('UserID',$current[0]);	
				return false;			
			}			
		}
	}
}
