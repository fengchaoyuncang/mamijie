<?php

/**
 * 
 * 关联登录，，新的登录
 * 
 */
class RelationLoginNewModel extends CFormModel {

    public $username;  //用户名
    public $emailphone;  //
    public $Phone = '';
    public $Email = '';


    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('username', 'required'),
            array('username', 'match', 'pattern' => '/^[a-z 0-9 _]{3,}$/i', 'message' => '用户名只能包含大小写字母，0-9，下划线'),
            array('username', 'checkUserName'),
            array('emailphone', 'checkEmailPhone'),
        );
    }

    public function checkUserName() {
        if (is_numeric($this->username)) {
            $this->addError('username', '用户名不能是纯数字');
            return false;
        }

        if (MemberModel::model()->find(BaseModel::getC(array('username' => $this->username)))) {
            $this->addError('username', '用户名被占用');
            return false;
        }
        return true;
    }

    public function checkEmailPhone() {

        if (is_numeric($this->emailphone) && strlen($this->emailphone) == 11) { //是手机
            $this->Phone = $this->emailphone;
            if (!preg_match('/^1[34578][0-9]{9}$/', $this->emailphone)) {
                $this->addError('emailphone', '手机格式不正确');
                return false;
            }
            if (MemberModel::model()->find(BaseModel::getC(array('Phone' => $this->Phone)))) {
                $this->addError('emailphone', '手机号被占用');
                return false;
            }
        } else {
            $this->Email = $this->emailphone;
            if (!preg_match('/^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/', $this->emailphone)) {
                $this->addError('emailphone', '邮箱格式不正确');
                return false;
            }
            if (MemberModel::model()->find(BaseModel::getC(array('Email' => $this->Email)))) {
                $this->addError('emailphone', '邮箱被占用');
                return false;
            }
        }
        return true;
    }

    public function saveData($MemberOauthModel){
    	$model = new MemberModel();
    	$model->UserName = $this->username;
        $model->Phone = $this->Phone;
        $model->Email = $this->Email;
    	$model->save(false);

    	$MemberOauthModel->UserID = $model->UserID;
    	$MemberOauthModel->save(false);

        return $model;
    }


}
