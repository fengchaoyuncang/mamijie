<?php

/**
 * This is the model class for table "{{merchants_settled}}".
 * 商家入注Model
 */
class MerchantsSettledModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{merchants_settled}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('MobilePhone, HeadMobilePhone', 'match', 'pattern'=>'/^\s*1[0-9]{10}\s*$/', 'message'=>'{attribute}必须为1开头的11位纯数字'),//验证手机号码			
			
			array('CompanyName, RegistrationNo, StartTime, EndTime, Location, ScopeBusiness, FullName, Address, MobilePhone, FaxNumber, CompanyUrl, BrandAgent, TaobaoUrl, BusinessLicenseImg, OrganizationImg, TaxRegistrationImg, PositiveIDImg, ReverseIDImg, HeadName, HeadPosition, HeadCardID, HeadMobilePhone, HeadQQ, HeadEmail', 'required'),
			array('RegisteredCapital, UserID, AddTime, Status', 'numerical', 'integerOnly'=>true),
			array('TrademarkImg, BrandAuthorizationImg, Certification3cImg', 'safe'),
			array('UserID', 'checkUnique', 'on'=>'insert', 'message' => '你已经提交过了'),//用户ID必须是唯一的
			array('HeadEmail', 'email'),//验证邮箱
			array('HeadEmail', 'checkLogin'),//验证邮箱
			array('FaxNumber', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			//array('MerchantsSettledID, CompanyName, RegistrationNo, StartTime, EndTime, Location, RegisteredCapital, ScopeBusiness, FullName, Address, MobilePhone, FaxNumber, CompanyUrl, BrandAgent, TaobaoUrl, BusinessLicenseImg, OrganizationImg, TaxRegistrationImg, PositiveIDImg, ReverseIDImg, HeadName, HeadPosition, HeadCardID, HeadMobilePhone, HeadQQ, HeadEmail, UserID, AddTime, Status,Cause', 'safe'),
			array('MerchantsSettledID, CompanyName, RegistrationNo, StartTime, EndTime, Location, RegisteredCapital, ScopeBusiness, FullName, Address, MobilePhone, FaxNumber, CompanyUrl, BrandAgent, TaobaoUrl, BusinessLicenseImg, OrganizationImg, TaxRegistrationImg, PositiveIDImg, ReverseIDImg, HeadName, HeadPosition, HeadCardID, HeadMobilePhone, HeadQQ, HeadEmail, UserID, AddTime, Status,Cause,Money', 'safe', 'on'=>'search'),
			array('CompanyName, RegistrationNo, StartTime, EndTime, Location, RegisteredCapital, ScopeBusiness, FullName, Address, MobilePhone, FaxNumber, CompanyUrl, BrandAgent, TaobaoUrl, BusinessLicenseImg, OrganizationImg, TaxRegistrationImg, PositiveIDImg, ReverseIDImg, HeadName, HeadPosition, HeadCardID, HeadMobilePhone, HeadQQ, HeadEmail, UserID, AddTime, Status, Cause,Money', 'safe', 'on'=>'admin'),
		);
	}

	public function checkUnique(){
		$intUid = empty($this->UserID) ? yii::app()->user->id :  $this->UserID;
		$arrData = self::model()->find(array('condition' => 'UserID = :UserID AND Status != 2', 'params' => array('UserID' => $intUid)));
		if($arrData){
			if($arrData->Status == 0){
				$strHtml = '已经提交过但未审核';
			}else{
				$strHtml = '已经提交过且已经审核通过';
			}
			$this->addError('UserID', $strHtml);
			return false;
		}
	}

	public function checkLogin(){
		if(GROUP_NAME != 'admin'){
			if(!$uid = yii::app()->user->isLogin()){
				$this->addError('', '你还未登录');
				return false;
			}
			if(is_array($uid)){
				$this->addError('', '你的登录未绑定账户');
				return false;				
			}
/*			if(empty($_POST['agree'])){
				$this->addError('', '你未同意条款');
				return false;
			}*/


		}

/*		if(yii::app()->user->Type == 0){
			$this->addError('', '必须商家用户才能添加');
			return false;
		}*/
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MerchantsSettledID' => 'ID',
			'CompanyName' => '公司名称',
			'RegistrationNo' => '营业执照注册号',
			'StartTime' => '营业执照有效期开始时间',
			'EndTime' => '营业执照有效期结束时间',
			'Location' => '营业执照所在地',
			'RegisteredCapital' => '注册资金',
			'ScopeBusiness' => '经营范围',
			'FullName' => '法人姓名',
			'Address' => '联系地址',
			'MobilePhone' => '法人电话',
			'FaxNumber' => '传真',
			'CompanyUrl' => '公司网址',
			'BrandAgent' => '旗下母婴品牌/代理',
			'TaobaoUrl' => '淘宝/天猫店网址',
			'BusinessLicenseImg' => '营业执照扫描件',
			'OrganizationImg' => '组织机构代码证扫描件',
			'TaxRegistrationImg' => '税务登记证件扫描件(国税或者地税)',
			'PositiveIDImg' => '法人身份证正面',
			'ReverseIDImg' => '法人身份证反面',
			'TrademarkImg' => '商标注册证',
			'BrandAuthorizationImg' => '品牌授权书',
			'Certification3cImg' => '3c认证或质检报告',
			'HeadName' => '姓名',
			'HeadPosition' => '职位',
			'HeadCardID' => '身份证号码',
			'HeadMobilePhone' => '手机',
			'HeadQQ' => 'QQ',
			'HeadEmail' => '常用邮箱',
			'UserID' => '用户ID',
			'AddTime' => '添加时间',
			'Status' => '审核状态',
			'PaymentSettled' => '入驻保证金',
			'Cause' => '拒绝理由',
			'Imgs' => '其它图片',
			'Money' => '入驻冻结金额',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MerchantsSettledID',$this->MerchantsSettledID);
		$criteria->compare('CompanyName',$this->CompanyName,true);
		$criteria->compare('RegistrationNo',$this->RegistrationNo,true);
		$criteria->compare('StartTime',$this->StartTime);
		$criteria->compare('EndTime',$this->EndTime);
		$criteria->compare('Location',$this->Location,true);
		$criteria->compare('RegisteredCapital',$this->RegisteredCapital);
		$criteria->compare('ScopeBusiness',$this->ScopeBusiness,true);
		$criteria->compare('FullName',$this->FullName,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('MobilePhone',$this->MobilePhone,true);
		$criteria->compare('FaxNumber',$this->FaxNumber,true);
		$criteria->compare('CompanyUrl',$this->CompanyUrl,true);
		$criteria->compare('BrandAgent',$this->BrandAgent,true);
		$criteria->compare('TaobaoUrl',$this->TaobaoUrl,true);
		$criteria->compare('BusinessLicenseImg',$this->BusinessLicenseImg,true);
		$criteria->compare('OrganizationImg',$this->OrganizationImg,true);
		$criteria->compare('TaxRegistrationImg',$this->TaxRegistrationImg,true);
		$criteria->compare('PositiveIDImg',$this->PositiveIDImg,true);
		$criteria->compare('ReverseIDImg',$this->ReverseIDImg,true);
		$criteria->compare('HeadName',$this->HeadName,true);
		$criteria->compare('HeadPosition',$this->HeadPosition,true);
		$criteria->compare('HeadCardID',$this->HeadCardID,true);
		$criteria->compare('HeadMobilePhone',$this->HeadMobilePhone,true);
		$criteria->compare('HeadQQ',$this->HeadQQ,true);
		$criteria->compare('HeadEmail',$this->HeadEmail,true);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Status',$this->Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MerchantsSettledID',$this->MerchantsSettledID);
		$criteria->compare('CompanyName',$this->CompanyName,true);
		$criteria->compare('RegistrationNo',$this->RegistrationNo,true);
		$criteria->compare('StartTime',$this->StartTime);
		$criteria->compare('EndTime',$this->EndTime);
		$criteria->compare('Location',$this->Location,true);
		$criteria->compare('RegisteredCapital',$this->RegisteredCapital);
		$criteria->compare('ScopeBusiness',$this->ScopeBusiness,true);
		$criteria->compare('FullName',$this->FullName,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('MobilePhone',$this->MobilePhone,true);
		$criteria->compare('FaxNumber',$this->FaxNumber,true);
		$criteria->compare('CompanyUrl',$this->CompanyUrl,true);
		$criteria->compare('BrandAgent',$this->BrandAgent,true);
		$criteria->compare('TaobaoUrl',$this->TaobaoUrl,true);
		$criteria->compare('BusinessLicenseImg',$this->BusinessLicenseImg,true);
		$criteria->compare('OrganizationImg',$this->OrganizationImg,true);
		$criteria->compare('TaxRegistrationImg',$this->TaxRegistrationImg,true);
		$criteria->compare('PositiveIDImg',$this->PositiveIDImg,true);
		$criteria->compare('ReverseIDImg',$this->ReverseIDImg,true);
		$criteria->compare('HeadName',$this->HeadName,true);
		$criteria->compare('HeadPosition',$this->HeadPosition,true);
		$criteria->compare('HeadCardID',$this->HeadCardID,true);
		$criteria->compare('HeadMobilePhone',$this->HeadMobilePhone,true);
		$criteria->compare('HeadQQ',$this->HeadQQ,true);
		$criteria->compare('HeadEmail',$this->HeadEmail,true);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Status',$this->Status);

		return $criteria;
	}




	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MerchantsSettledModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if ($this->isNewRecord){
			$this->AddTime = time();
			$this->MobilePhone = trim($this->MobilePhone);
			$this->HeadMobilePhone = trim($this->HeadMobilePhone);
			$this->Status = 0;
			if(!$this->UserID){
				$this->UserID = yii::app()->user->id;
			}
			//变成普通商家
			$memberModel = MemberModel::model()->findByPk($this->UserID);
			if($memberModel && $memberModel->Type == 0){
				$memberModel->Type = 1;
				$memberModel->save(false, array('Type'));
			}

		}else{
			//修改的时候如果图片跟原来的不一样了，则需要删除原先的图片
			$m = self::model()->findByPk($this->MerchantsSettledID);
			if($this->BusinessLicenseImg != $m->BusinessLicenseImg){
				UploadFile::deleteImg($m->BusinessLicenseImg);
			}
			if($this->OrganizationImg != $m->OrganizationImg){
				UploadFile::deleteImg($m->OrganizationImg);
			}
			if($this->TaxRegistrationImg != $m->TaxRegistrationImg){
				UploadFile::deleteImg($m->TaxRegistrationImg);
			}
			if($this->PositiveIDImg != $m->PositiveIDImg){
				UploadFile::deleteImg($m->PositiveIDImg);
			}
			if($this->ReverseIDImg != $m->ReverseIDImg){
				UploadFile::deleteImg($m->ReverseIDImg);
			}
			if($this->TrademarkImg != $m->TrademarkImg){
				UploadFile::deleteImg($m->TrademarkImg);
			}
			if($this->BrandAuthorizationImg != $m->BrandAuthorizationImg){
				UploadFile::deleteImg($m->BrandAuthorizationImg);
			}
			if($this->Certification3cImg != $m->Certification3cImg){
				UploadFile::deleteImg($m->Certification3cImg);
			}
			if(GROUP_NAME == 'admin'){
				$model == self::model()->findByPk($this->MerchantsSettledID);
				if($model->Status != $this->Status){
					$title = '入驻审核通知';
					$content = '';
					switch ($this->Status) {
						case 1:
							$content = "您好，贵公司:{$this->CompanyName}在妈咪街入驻已经通过审核";
							break;
						case 2:
							$content = "您好，贵公司:{$this->CompanyName}在妈咪街入驻审核不通过，理由：{$this->Cause}";
							break;
						case 4:
							$content = "您好，贵公司:{$this->CompanyName}在妈咪街入驻已进入合同期";
							break;
						case 5:
							$content = "您好，贵公司:{$this->CompanyName}在妈咪街入驻已进入付款期";
							break;
						
						default:
							# code...
							break;
					}
					$memberModel = MemberModel::model()->findByPk($this->UserID);
					if($memberModel && $memberModel->Phone && $content){
						Sns::getInstance()->send($memberModel->Phone, $content);
					}
					if($memberModel && $memberModel->Email && $content){
						$mail = Yii::createComponent('application.extensions.mailer.EMailer');
						$mail->sendEmail($memberModel->Email, $title, $content);
					}
				}
			}
		}
		if(!is_numeric($this->StartTime)){
			$this->StartTime = strtotime($this->StartTime);
		}
		if(!is_numeric($this->EndTime)){
			$this->EndTime = strtotime($this->EndTime);
		}
		//$config = ConfigModel::model()->getConfig();
		return true;
	}

	public function afterSave(){
		if ($this->isNewRecord){
			//看是否有传多张图
			if(isset($_POST['MerchantsSettledModel']['Imgs']) && is_array($_POST['MerchantsSettledModel']['Imgs'])){
				foreach ($_POST['MerchantsSettledModel']['Imgs'] as $key => $value) {
					$m = new MerchantsSettledImgModel();
					$m->MerchantsSettledID = $this->MerchantsSettledID;
					$m->Img = $value;
					$m->save(false);
				}
			}
		}else{
			//判断是否是新加的，如果是则加上，如果不是则删除，然后把原来的一些删除掉。
			//先获取所有的图片
			$datas = MerchantsSettledImgModel::getMerchantsSettledImg($this->MerchantsSettledID);
			if(!empty($_POST['MerchantsSettledModel']['Imgs']) && is_array($_POST['MerchantsSettledModel']['Imgs'])){
				$d = array();
				foreach($datas as $key => $value) {
					$d[$value['Img']] = $value;
				}
				foreach($_POST['MerchantsSettledModel']['Imgs'] as $key => $value) {
					if(isset($d[$value])){
						unset($d[$value]);
						continue;
					}else{
						//新增
						$m = new MerchantsSettledImgModel();
						$m->MerchantsSettledID = $this->MerchantsSettledID;
						$m->Img = $value;
						$m->save(false);						
					}
				}
				//删除
				foreach ($d as $key => $value) {
					UploadFile::deleteImg($key);
					MerchantsSettledImgModel::model()->deleteByPk($value['ImgID']);
				}
			}else{
				foreach ($datas as $key => $data) {
					UploadFile::deleteImg($data['Img']);
					MerchantsSettledImgModel::model()->deleteByPk($data['ImgID']);
				}
			}
		}
		return true;
	}

	public static function getStatusHtml($intData = ''){
		$arrData = array(
			0 => '未审核',
			1 => '审核通过',
			2 => '审核不通过',
			3 => '取消',
			4 => '进入合同期',
			5 => '进入付款期',
 		);
		if($intData === ''){
			return $arrData;
		}else{
			return $arrData[$intData];
		}
	}

	public static function getList($bloIsCache = true){
		$strKey = "MerchantsSettledModel_getList";
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();
        $objModels = self::model()->findAll();
        foreach ($objModels as $key => $objModel) {
        	$arrCache[$objModel->UserID] = $objModel->CompanyName;
        }        
        yii::app()->cache->set($strKey, $arrCache, 3600);
        return $arrCache;		
	}

	public function getImgs(){
		return MerchantsSettledImgModel::getMerchantsSettledImg($this->MerchantsSettledID);
	}
}
