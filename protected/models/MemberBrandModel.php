<?php

/**
 * This is the model class for table "{{member_brand}}".
 *
 * The followings are the available columns in table '{{member_brand}}':
 * @property integer $UserID
 * @property string $ImgBig
 * @property string $ImgSmall
 * @property integer $AddTime
 * @property string $Name
 */
class MemberBrandModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{member_brand}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Name', 'required'),
			array('UserID, AddTime,Sorting', 'numerical', 'integerOnly'=>true),
			//array('UserID', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('UserID, ImgBig, ImgSmall, AddTime, Name,Sorting, Remark, AppImg,TitleSeo,KeywordsSeo,DescriptionSeo', 'safe', 'on'=>'admin'),
			array('UserID, ImgBig, ImgSmall, AddTime, Name,Sorting', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function primaryKey(){
		return 'UserID';//自定义主键
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'UserID' => '用户',
			'ImgBig' => '大图',
			'ImgSmall' => '小的logo图',
			'AddTime' => '添加时间',
			'Name' => '名称',
			'Sorting' => '排序',
			'Remark' => '备注',
			'AppImg' => 'App图片路径',
			'TitleSeo' => 'Seo标签',
			'KeywordsSeo' => 'seo关键字',
			'DescriptionSeo' => 'seo描述',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('ImgBig',$this->ImgBig,true);
		$criteria->compare('ImgSmall',$this->ImgSmall,true);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Name',$this->Name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('ImgBig',$this->ImgBig,true);
		$criteria->compare('ImgSmall',$this->ImgSmall,true);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Name',$this->Name,true);

		return $criteria;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MemberBrandModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function afterSave(){
		$this->getList(false);
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = TIME_TIME;
		}else{
			$m = self::model()->findByPk($this->BrandID);
			if($this->ImgBig != $m->ImgBig){
				UploadFile::deleteImg($m->ImgBig);
			}
			if($this->ImgSmall != $m->ImgSmall){
				UploadFile::deleteImg($m->ImgSmall);
			}			
			if($this->AppImg != $m->AppImg){
				UploadFile::deleteImg($m->AppImg);
			}						
		}
		return true;
	}


	//取所有有报名的用户
	public static function getAllUserMerchantsSettled($bloIsCache = true){
		$datas = MerchantsSettledModel::getList();
		return $datas;
	}



	//取用户的品牌   id=>name
	public static function getAllUser($bloIsCache = true){
		$result = array();
		$datas = self::getList();
		foreach ($datas as $key => $data) {
			if($data['UserID'] >0){
				$result[$key] = $data['Name'];
			}
		}
		return $result;
	}

	//取系统的品牌id=>名字
	public static function getAllUserSystem($bloIsCache = false){

		$result = array();
		$datas = self::getList();
		foreach ($datas as $key => $data) {
			if($data['UserID'] == 0){
				$result[$data['BrandID']] = $data['Name'];
			}
		}
		return $result;
	}
	//取系统的品牌id=>名字
	public static function getAll($bloIsCache = false){

		$result = array();
		$datas = self::getList();
		foreach ($datas as $key => $data) {
			$result[$data['BrandID']] = $data['Name'];
		}
		return $result;
	}

	//取所有的品牌
	public static function getList($bloIsCache = true){
		$strKey = "MemberBrandModel_getList";
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }
        $arrCache = array();
		$datas = self::model()->findAll(BaseModel::getC(array('order' => 'Sorting desc')));
		foreach ($datas as $key => $data) {
			$arrCache[] = $data->attributes;
		}
		shuffle($arrCache);
		yii::app()->cache->set($strKey, $arrCache, 600);
		return $arrCache;		
	}

	//取单个品牌数组
	public static function getDetail($id){
		$strKey = "MemberBrandModel_getDetail" . $id;
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }
        $arrCache = array();
		$datas = self::model()->findByPk($id);
		$arrCache = $datas->attributes;
		yii::app()->cache->set($strKey, $arrCache, 600);
		return $arrCache;			
	}


	public static function getDetailUser($field = '', $uid = ''){
		empty($uid) ? $uid = yii::app()->user->isLogin() : '';
		$result = array();
		$datas = self::getList();
		foreach ($datas as $key => $data) {
			if($data['UserID'] == $uid){
				if($field){
					$result[$data['BrandID']] = $data[$field];
				}else{
					$result[$data['BrandID']] = $data;
				}
			}
		}
		return $result;
	}	
}
