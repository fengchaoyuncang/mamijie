<?php

/**
 * This is the model class for table "{{category}}".
 *
 * The followings are the available columns in table '{{category}}':
 * @property string $CatID
 * @property string $ParentID
 * @property string $Title
 * @property string $Brief
 * @property string $AddTime
 * @property string $Sorting
 * @property string $Status
 */
class CategoryArticlesAskModel extends BaseModel
{
	public static $UPDATACACHE = '';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{category_articles_ask}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Title', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ParentID, Brief, Sorting', 'safe'),
			array('ParentID, Title, Brief, AddTime, Sorting, Status,IsHot,IsRecommend,IsNew, UpdateTime,TitleSeo,KeywordsSeo,DescriptionSeo,IsAppIndex,AppImg,AppDesc', 'safe', 'on'=>'admin'),
			array('CatID, ParentID, Title, Brief, AddTime, Sorting, Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CatID' => 'ID',
			'ParentID' => '父ID',
			'Title' => '标题',
			'Brief' => '简介',
			'AddTime' => '添加时间',
			'Sorting' => '排序',
			'Status' => '是否可用',
			'IsHot' => '热门',
			'IsRecommend' => '推荐',
			'IsNew' => '最新',
			'TitleSeo' => 'Seo标签',
			'KeywordsSeo' => 'seo关键字',
			'DescriptionSeo' => 'seo描述',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CatID',$this->CatID,true);
		$criteria->compare('ParentID',$this->ParentID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Brief',$this->Brief,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('Status',$this->Status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CatID',$this->CatID,true);
		$criteria->compare('ParentID',$this->ParentID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Brief',$this->Brief,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('IsHot',$this->IsHot);
		$criteria->compare('IsRecommend',$this->IsRecommend);
		$criteria->compare('IsNew',$this->IsNew);

		return $criteria;
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CategoryArticlesAskMode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = TIME_TIME;
			$this->UpdateTime = TIME_TIME;			
		}else{
			$this->UpdateTime = TIME_TIME;			
		}
		return true;
	}

	/**
	 * 取所有的数据然后缓存，其它的函数都是先通过这个函数来取，然后再处理
	 * @param  [type] $intCatID [description]
	 * @return [type]           [description]
	 */
	public static function getList($bloIsCache = true){
		static $arrCache;
		if($arrCache && !self::$UPDATACACHE){
			return $arrCache;
		}


		if(GROUP_NAME == 'admin'){
			$bloIsCache = false;
		}		

		$strKey = "CategoryArticlesAskMode_getList";
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();
        $objCriteria = BaseModel::getC(array('Status' => 1));
        $objCriteria->order = 'Sorting desc,UpdateTime desc,CatID desc';
        $objModels = self::model()->findAll($objCriteria);
        foreach ($objModels as $key => $objModel) {
        	$arrCache[$objModel->CatID] = $objModel->attributes;
        }
        
        yii::app()->cache->set($strKey, $arrCache, 600);
        return $arrCache;		
	}

	public function afterSave(){
		parent::afterSave();
		self::$UPDATACACHE = true;
		self::getList();
	}
	public function afterDelete(){
		parent::afterDelete();
		self::$UPDATACACHE = true;
		self::getList();
	}



	/**
	 * array(
	 * 	 CatID => 1,
	 * 	 Title => 'ffff',
	 * 	 ''''''',
	 * 	 Layer => 1,
	 * 	 data => array(
	 * 	 	CatID => array(
	 * 	 		
	 * 	 	),
	 * 	 ),
	 * )
	 * 通过catid来获取它底下的所有的数据，包括它本身
	 * 此分类ID不能为0，不能为空。
	 * $intLayer 取几层的数据  默认false所有的都取出来
	 * 格式
	 * @return [type] [description]
	 */
	public static function getListCatID($intCatID, $intLayer = false, $intLayerNum = 1){
		$datas = self::getList();

		$arrResult = $datas[$intCatID];
		$arrResult['Layer'] = $intLayerNum;
		$arrResult['data'] = array();
		$intLayerNum++;
		if($intLayer && $intLayerNum > $intLayer){
			return $arrResult;
		}		
		foreach ($datas as $CatID => $data) {
			if($data['ParentID'] == $intCatID){
				$arrResult['data'][$CatID] = self::getListCatID($CatID, $intLayer, $intLayerNum);
			}
		}
		return $arrResult;
	}

	/**
	 * $intLayer定义了第几层  如果是第1层则获取所有第一层的数据
	 * @param  boolean $intLayer [description]
	 * @return [type]            [description]
	 */
	public static function  getListMenu($intLayer = 1, $bloIsCache = true){

		/*		
		$strKey = "CategoryAtriclesNewModel_getListMenu" . $intLayer;
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }*/
        $arrCache = array();

        $datas = self::getList();
        foreach ($datas as $CatID => $data) {
        	if($data['ParentID'] == 0){
        		$arrCache[$CatID] = self::getListCatID($CatID, $intLayer, 1);
        	}
        }
    	//yii::app()->cache->set($strKey, $arrCache, 3600);
    	return $arrCache;		
	}

	/**
	 * 获取全部树型的数据 数组形式呈现
	 * 形式是catid => Title
	 * @return [type] [description]
	 */
	public static function getListTree(){
        Tree::$arrData = self::getList();
        return Tree::getArrData();
	}

	/**
	 * 获取所有的数据，除了标题 按数组方式呈现
	 * 形式是catid => array()
	 * @return [type] [description]
	 */
	public static function getListTreeArr(){
		$arrResult = array();
		$arrTree = self::getListTree();
		$arrDatas = self::getList();
		foreach ($arrTree as $key => $value) {
			$arrResult[$key] = $arrDatas[$key];
			$arrResult[$key]['treetitle'] = $value;
		}
		return $arrResult;
	}
	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			0 => '不可用',
			1 => '可用',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

    /**
     * 根据栏目ID删除
     * @param type $catid 栏目ID
     * @return type
     */
    public static function deleteCategoryByPk($catid) {
        if (empty($catid)) {
            return $catid;
        }
        $model = self::model()->findByPk($catid);
        if (empty($model)) {
            return false;
        }
        $info = $model->attributes;
        //检查是否有子栏目
        $childid = self::getStrChildid($info['CatID']);
        if (empty($childid)) {
            return $model->delete();
        }

        $bloResult = self::model()->deleteAll(self::model()->where(array('CatID' => array('IN', $childid))));
        self::getList();

        return $bloResult;
    }

    /**
     * 获取子栏目ID列表,,用于删除的时候跟前台需要获取相关的分类的时候用得以
     * 取其底下的所有的1,2,3格式
     * @param type $catid 栏目id
     * @return string 返回栏目子列表，以逗号隔开
     */
    public static function getStrChildid($catid) {
        //栏目数据
        if(is_numeric($catid)){
        	return self::getStrChildidDetail($catid);
        }else{
        	$data = array();
        	$arr = explode(',', $catid);
        	foreach ($arr as $value) {
        		if($value){
        			$data[] = self::getStrChildidDetail($value);
        		}
        	}
        	$d = implode(',', $data);
        	return $d;
        }
    }

    /**
     * 获取子栏目ID列表,,用于删除的时候跟前台需要获取相关的分类的时候用得以
     * 取其底下的所有的1,2,3格式
     * @param type $catid 栏目id
     * @return string 返回栏目子列表，以逗号隔开
     */
    public static function getStrChildidDetail($catid) {
        //栏目数据
    	$arrDatas = self::getList();
        $arrchildid = $catid;
        foreach ($arrDatas as $id => $cat) {
            if ($cat['ParentID'] && $id != $catid && $cat['ParentID'] == $catid) {
                $arrchildid .= ',' . self::getStrChildidDetail($id);
            }
        }
        return $arrchildid;
    }
    /**
     * 获取子栏目ID列表,,用于删除的时候跟前台需要获取相关的分类的时候用得以
     * 取其底下的所有的array(1,2,3)格式
     * @param type $catid 栏目id
     * @return string 返回栏目子列表，以逗号隔开
     */
    public static function getArrChildid($catid) {
        //栏目数据
    	$str = self::getStrChildid($catid);
    	return explode(',', $str);
    }
    //取这子分类下面的详细的信息。。。。
    public static function getArrChildidDetail($catid) {
        //栏目数据
    	$arr = self::getArrChildid($catid);
    	$arrResult = array();
    	foreach ($arr as $key => $value) {
    		$arrResult[$value] = self::getDataDetail($value);
    	}
    	return $arrResult;
    }
    /**
     * 根据catid取其单条信息
     * 可根据某字段来取
     * @param  [type] $intCatID [description]
     * @return [type]           [description]
     */
    public static function getDataDetail($intCatID, $strField = ''){
    	$arrData = self::getList();
    	if($strField){
    		return $arrData[$intCatID][$strField];
    	}
    	return $arrData[$intCatID];
    }

    /**
     * 取得所有的父ID为0的
     * 根据某个字段来取值。。。。
     * @return [type] [description]
     */
    public static function getParentList($strField = ''){
    	$arrResult = array();
    	$arrDatas = self::getList();
    	foreach ($arrDatas as $key => $arrData) {
    		if($arrData['ParentID'] == 0){
    			if($strField){
    				$arrResult[$key] = $arrData[$strField];
    			}else{
    				$arrResult[$key] = $arrData;
    			}
    		}
    	}
    	return $arrResult;
    }

    /**
     * 翻转过来获取。。
     * 使用递归的方式，获取全部的父栏目id列表
     * @param type $catid 栏目ID
     * @param type $arrparentid 父目录ID
     * @param type $n 查找的层次
     * @return string 返回父栏目id列表，以逗号隔开
     */
    public static function getStrParentid($catid) {
    	$result = $catid;    	
        $datas = self::getList();
        $parent = $datas[$catid]['ParentID'];//当前栏目的父ID
        if($parent != 0){
        	$result .= ',' . self::getStrParentid($parent);
        }
        return $result;
    }

    /**
     * 使用递归的方式，获取全部的父栏目id列表
     * @param type $catid 栏目ID
     * @param type $arrparentid 父目录ID
     * @param type $n 查找的层次
     * @return string 返回父栏目id列表，以逗号隔开
     */
    public static function getArrParentid($catid) {
    	$data = self::getStrParentid($catid);
    	return explode(',', $data);
    }

    public static function getMianbao($catid){
    	$arrResult = array();
    	$datas = self::getArrParentid($catid);
    	$datas = array_reverse($datas);
    	foreach ($datas as $key => $data) {
    		$title = self::getDataDetail($data, 'Title');
    		$arrResult[$title] = array('/baike/index/askList', 'cid' => $data);
    	}
    	return $arrResult;
    }

    /**
     * 首页所用的方法
     * 获取若干个分类下面的若干条文章
     * 获取4个最新分类与其下面的4文章
     * @return [type] [description]
     */
    public static function getNewCategory($categoryNum = 4, $articleNum = 4){
    	$datas = self::getList();
    	$arrResult = array();
    	$num = 0;
    	foreach ($datas as $key => $data) {
    		if($data['IsNew'] == 1){
    			$num++;
    			if($num > $categoryNum){
    				break;
    			}else{
    				$arrResult[$key] = $data;
    			}
    		}
    	}
    	foreach ($arrResult as $key => $value) {
    		$arrResult[$key]['data'] = ArticlesNewModel::model()->getVaried(array('page' => '', 'pagesize' => $articleNum, 'cid' => $key));
    	}
    	return $arrResult;
    }

    public static function getAppHot(){
		$datas = self::getList();	
		$arrResult = array();
		foreach ($datas as $CatID => $data) {
			if($data['IsAppIndex']){
				$arrResult[] = $data;
			}
		}
		return $arrResult;    	
    }

}
