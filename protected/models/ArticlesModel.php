<?php

/**
 * This is the model class for table "{{articles}}".
 * 文章MODEL,,,,,,,通过文章分类来获取文章，，，，，
 * The followings are the available columns in table '{{articles}}':
 * @property string $ArticlesID
 * @property string $Title
 * @property integer $CatID
 * @property string $Contnet
 * @property string $AddTime
 * @property string $UserID
 * @property string $UserName
 * @property string $Sorting
 * @property integer $IsAdmin
 */
class ArticlesModel extends ArticleBaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{articles}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Title, Contnet,CatID', 'required'),
			array('IsShow,CatID, IsAdmin', 'numerical', 'integerOnly'=>true),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Title, CatID, Contnet, AddTime, UserID, UserName, Sorting, IsAdmin,IsShow,TitleSeo,KeywordsSeo,DescriptionSeo', 'safe', 'on'=>'admin'),
			array('ArticlesID, Title, CatID, Contnet, AddTime, UserID, UserName, Sorting, IsAdmin,IsShow', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ArticlesID' => '文章ID',
			'Title' => '标题',
			'CatID' => '分类ID',
			'Contnet' => '内容',
			'AddTime' => '添加时间',
			'UserID' => '用户ID',
			'UserName' => '用户名',
			'Sorting' => '排序',
			'IsAdmin' => '是否后台人员上传',
			'IsShow' => '是否显示',
			'TitleSeo' => 'Seo标签',
			'KeywordsSeo' => 'seo关键字',
			'DescriptionSeo' => 'seo描述',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ArticlesID',$this->ArticlesID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('CatID',$this->CatID);
		$criteria->compare('Contnet',$this->Contnet,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('UserID',$this->UserID,true);
		$criteria->compare('UserName',$this->UserName,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('IsAdmin',$this->IsAdmin);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArticlesModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function createSearchCriteria(){
		$criteria=new CDbCriteria;

		$criteria->compare('ArticlesID',$this->ArticlesID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('CatID',$this->CatID);
		$criteria->compare('Contnet',$this->Contnet,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('UserID',$this->UserID,true);
		$criteria->compare('UserName',$this->UserName,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('IsAdmin',$this->IsAdmin);
		return 	$criteria;	
	}

	public function beforeSave(){
		if($this->isNewRecord){
			if(GROUP_NAME == 'admin'){
				$this->UserID = AdminBase::$uid;
				$this->UserName = AdminBase::$userInfo['real_name'];
				$this->IsAdmin = 1;
			}else{
				$this->IsAdmin = 0;
				if(yii::app()->user->isLogin()){
					$this->UserID = yii::app()->user->id;	
					$this->UserName = AdminBase::$userInfo['UserName'];			
				}else{
					$this->UserID = 0;
					$this->UserName = '';
				}
			}
			$this->AddTime = time();
		}
		$this->Title = CHtml::encode($this->Title);
		//$this->Contnet = CHtml::encode($this->Contnet);
		return true;
	}


	//取文章
    /** 获取列表，杂项的
		$intCatID 通过分类id来获取，不传值则不加这条件，
		$intPage 获取第几页的  如果是等于 ''，则不需要分页获取
		$intPageSize 每页获取多少条
		$strOrder排序方式
		$bloIsCache 是否需要缓存  默认true，是需要缓存起来的
		$intCacheTime  默认缓存的时间是多长时间
		$bloIsUpset  取出的数据是否需要打乱顺序
     * @return [type] [description]
     */
    public  function getVaried($Conditions = array(), $bloIsUpset = true, $bloIsCache = true, $intCacheTime = 600){
        $CatID = isset($Conditions['cid']) ?  $Conditions['cid'] : Yii::app()->request->getParam('cid');        //分类条件
        $UserID = isset($Conditions['uid']) ?  $Conditions['uid'] : Yii::app()->request->getParam('uid');           //分类
        $gettype = isset($Conditions['gettype']) ? $Conditions['gettype'] : Yii::app()->request->getParam('gettype');   //取数据模式
        $strOrder = '';
        if(isset($Conditions['page'])){
        	$intPage = $Conditions['page'];
        }else{
        	$intPage = Yii::app()->request->getParam('page');
        	empty($intPage) ? $intPage = 1 : '';
        }
        if(isset($Conditions['pagesize'])){
        	$intPageSize = $Conditions['pagesize'];
        }else{
        	$intPageSize = Yii::app()->request->getParam('pagesize');
        }
        empty($intPageSize) ? $intPageSize = 20 : '';


        $strCache = "ArticlesModel_getVaried_" . $CatID . '_' . $UserID . '_' . $intPage . '_'  . $intPageSize. '_'  . $gettype;
        

        $arrCache = yii::app()->cache->get($strCache);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }
        //开始拼SQL

        $sql = "select * from tk_articles where IsShow = 1 ";

        //分类的条件
        if($CatID !== ''){
            $CatID = CategoryArticleModel::getStrChildid($CatID);
            if(is_numeric($CatID)){
                $sql .= " AND CatID = {$CatID}";  
            }else{
                $sql .= " AND CatID in({$CatID})";  
            }            
        }  
        if($UserID !== ''){
            $sql .= " AND UserID = {$UserID}";     
        }              


        $this->getResult($gettype,$sql,$strOrder,$strCache,$intCacheTime,$bloIsUpset,$intPage,$intPageSize,$arrResult);
        return $arrResult;

    }

    /**
     * 取具体的一篇文章
     * @return [type] [description]
     */
    public static function getDataDetail($ArticlesID, $bloIsCache = true){
        $strCache = "ArticlesModel_getDataDetail" . $ArticlesID;

        $arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();
        $objModel = self::model()->findByPk($ArticlesID);
        if($objModel){
        	$arrCache = $objModel->attributes;
        	yii::app()->cache->set($strCache, $arrCache, 60);
        }else{
        	yii::app()->cache->delete($strCache);
        }

        return $arrCache;   	
    }

    public function afterSave(){
    	self::getDataDetail($this->ArticlesID, false);
    }

    public function afterDelete(){
    	self::getDataDetail($this->ArticlesID, false);
    }

	/**
	 * 是否显示
	 * @return [type] [description]
	 */
	public static function getIsShowHtml($mixData = false){
		$arrData = array(
			0 => '不显示',
			1 => '显示',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	/**
	 * 取分类下面的子分类及其文章
	 * @return [type] [description]
	 */
	public static function getCatData($CatID){
		$arrResult = array();
		$arrCat = CategoryArticleModel::getArrChildid($CatID);
		foreach($arrCat as $cat){
			if($cat == $CatID){
				continue;
			}
			$arrResult[$cat] = CategoryArticleModel::getDataDetail($cat);
			$arrResult[$cat]['data'] = self::model()->getVaried(array('cid' => $cat, 'pagesize' => 20));
		}

		return $arrResult;
	}


}
