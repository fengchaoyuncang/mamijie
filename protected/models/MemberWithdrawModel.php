<?php

/**
 * This is the model class for table "{{member_withdraw}}".
 *
 * The followings are the available columns in table '{{member_withdraw}}':
 * @property integer $WithdrawID
 * @property integer $UserID
 * @property string $Money
 * @property integer $Status
 * @property integer $AddTime
 * @property string $Ordersn
 * @property string $Remark
 * 提现申请的时候我需要先冻结这笔钱
 * 提现如果不同意，则我需要解冻这笔钱
 * 如果如果同意我需要扣除这笔钱
 * 
 */
class MemberWithdrawModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{member_withdraw}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UserID, Status, AddTime', 'numerical', 'integerOnly'=>true),
			array('Money', 'numerical'),
			array('Money,Account', 'required'),
			array('Remark', 'safe'),
			array('Status', 'checkStatus'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('WithdrawID, UserID, Money, Status, AddTime, Ordersn, Remark,Account,Cause', 'safe', 'on'=>'admin'),
			array('WithdrawID, UserID, Money, Status, AddTime, Ordersn, Remark', 'safe', 'on'=>'search'),
		);
	}

	public function checkStatus(){
		if(!$this->IsNewRecord){
			//如果不是新增 如果原来的记录的状态不是0则不能修改
			$data = self::model()->findByPk($this->WithdrawID);
			if($data->Status != 0){
				$this->addError('Status','此状态下不能修改');
				return false;
			}
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'WithdrawID' => 'Withdraw',
			'UserID' => 'User',
			'Money' => '提现金额',
			'Status' => 'Status',
			'AddTime' => 'Add Time',
			'Ordersn' => 'Ordersn',
			'Remark' => '备注',
			'Account' => '账号',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('WithdrawID',$this->WithdrawID);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('Money',$this->Money,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Ordersn',$this->Ordersn,true);
		$criteria->compare('Remark',$this->Remark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('WithdrawID',$this->WithdrawID);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('Money',$this->Money,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('AddTime',$this->AddTime);
		$criteria->compare('Ordersn',$this->Ordersn,true);
		$criteria->compare('Account',$this->Account,true);
		$criteria->compare('Remark',$this->Remark,true);
		return $criteria;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MemberWithdrawModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			0 => '未处理',
			1 => '已处理',	
			2 => '已拒绝',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	//生成提现记录
	// 申请提现  添加记录
	//1验证是否符合提现条件   还有未处理的订单，然后判断余额，通过了后再提现
	//2验证通过  则生成记录，并且 冻结用户资金
	public function withdraw($arrData = array()){


		$userID = $this->UserID;
		$key = "MemberWithdrawModel_withdraw" . $userID;		
		$cache = yii::app()->cache->get($key);
		if($cache){
			$this->addError('','请不要重复提交');
			return false;
		}else{
			yii::app()->cache->set($key, 1, 60);
		}


		try {
			if($this->Money <= 0){
				throw new Exception("提现金额不正确");				
			}

			//如果有未处理的则不能再次申请
			$data = self::model()->find(BaseModel::getC(array('UserID' => $this->UserID, 'Status' => 0)));
			if($data){
				throw new Exception("还有未处理的订单");				
			}



			MemberMoneyChangeModel::getLock($userID);

			$userMoney = MemberMoneyChangeModel::getUserMoney($userID);	
			if(sprintf("%.2f", $userMoney[0] - $userMoney[1] - $this->Money) < 0){
				throw new Exception("余额不足");
			}


			$transaction=Yii::app()->db->beginTransaction();

			//保存这个表
            $this->Status = 0;
            if(!$this->save()){
            	throw new Exception($this->getOneError());            	
            }


            //增加用户金额变动记录  增加冻结这金额
            $data = array(
                'UserID' => $userID,
                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
                'Money' => $this->Money,
                'Type' => 34,
                'ChangeType' => 2,
                'RelationID' => $this->WithdrawID,//入驻ID
                'AddTime' => TIME_TIME,
                'NumOnly' => $userID . '_34_' . $this->WithdrawID .  '_'. TIME_TIME,
            );

            //如果金额变动表保存失败则抛出异常
            if(!MemberMoneyChangeModel::saveData($data)){
                throw new Exception("金额添加异常");                    
            }



            $transaction->commit();
            yii::app()->cache->delete($key);
			MemberMoneyChangeModel::releaseLock($userID);
			return true;	

		} catch (Exception $e) {

	        if(!empty($transaction)){
	          $transaction->rollBack();
	        }
	        //请求删除
	        yii::app()->cache->delete($key);
	        MemberMoneyChangeModel::releaseLock($userID);
	        $error = $e->getMessage();
	        $error = empty($error) ? '请求异常' : $error;
	        $this->addError('',$error);
	        return false;  	
		}



	}

	public function beforeSave(){
		if($this->IsNewRecord){
			$this->AddTime = TIME_TIME;
			$this->Status = 0;
		}
		$this->Remark = CHtml::encode($this->Remark);
		$this->Account = CHtml::encode($this->Account);
		return true;
	}

	//同意这笔款的提现
	//我要先找出原来冻结的那部份，然后对比，然后再对这部份的
	public function agree(){
		$userID = $this->UserID;
		try {
			if($this->Status != 0){
				throw new Exception("此状态不能修改");
			}
			MemberMoneyChangeModel::getLock($userID);

			//冻结了这一部份钱
			$money = MemberMoneyChangeModel::getTypeMoney(array('RelationID' => $this->WithdrawID,'UserID' => $userID, 'Type' => 34));//这个用户这个类型有多少钱
			if($money != $this->Money){
				throw new Exception("提现金额不对等，异常错误");				
			}

			$transaction=Yii::app()->db->beginTransaction();
            //解冻
            $data = array(
                'UserID' => $userID,
                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
                'Money' => -$money,
                'Type' => 34,
                'ChangeType' => 2,
                'RelationID' => $this->WithdrawID,//入驻ID
                'AddTime' => TIME_TIME,
                'Remarks' => '解冻',
                'NumOnly' => $userID . '_34_' . $this->WithdrawID .  '_'. TIME_TIME,
            );
            //如果金额变动表保存失败则抛出异常
            if(!MemberMoneyChangeModel::saveData($data)){
                throw new Exception("金额添加异常");                    
            }

            //解冻了后需要扣除这个钱
            $data = array(
                'UserID' => $userID,
                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
                'Money' => -$money,
                'Type' => 3,
                'ChangeType' => 1,
                'RelationID' => $this->WithdrawID,//入驻ID
                'AddTime' => TIME_TIME,
                'Remarks' => '扣金额',
                'NumOnly' => $userID . '_3_' . $this->WithdrawID,
            );            
            //如果金额变动表保存失败则抛出异常
            if(!MemberMoneyChangeModel::saveData($data)){
                throw new Exception("金额添加异常");                    
            }
            //扣除了钱之后保存此记录
            $this->Status = 1;
            $this->save(false, array('Status'));

			$transaction->commit();	
			MemberMoneyChangeModel::releaseLock($userID);
			return true;

		} catch (Exception $e) {
	        if(!empty($transaction)){
	          $transaction->rollBack();
	        }
	        if(defined('USER_MONEY_LOCK')){
	        	MemberMoneyChangeModel::releaseLock($userID);
	        }	        
	        $error = $e->getMessage();
	        $error = empty($error) ? '请求异常' : $error;
	        $this->addError('',$error);
	        return false;  				
		}
	}

	//不同意这笔款的提现
	//需要解冻，但不扣款
	public function noAgree(){
		try {
			if($this->Status != 0){
				throw new Exception("此状态不能修改");
			}
			MemberMoneyChangeModel::getLock($this->UserID);

			//冻结了这一部份钱
			$money = MemberMoneyChangeModel::getTypeMoney(array('RelationID' => $this->WithdrawID,'UserID' => $this->UserID, 'Type' => 34));//这个用户这个类型有多少钱
			if($money != $this->Money){
				throw new Exception("提现金额不对等，异常错误");				
			}

			$transaction=Yii::app()->db->beginTransaction();
            //解冻
            $data = array(
                'UserID' => $this->UserID,
                'UserName' => MemberModel::getDetailForUserID($this->UserID, 'UserName'),
                'Money' => -$money,
                'Type' => 34,
                'ChangeType' => 2,
                'RelationID' => $this->WithdrawID,//提现ID
                'AddTime' => TIME_TIME,
                'Remarks' => '解冻',
                'NumOnly' => $this->UserID . '_34_' . $this->WithdrawID .  '_'. TIME_TIME,
            );
            //如果金额变动表保存失败则抛出异常
            if(!MemberMoneyChangeModel::saveData($data)){
                throw new Exception("金额添加异常");                    
            }


            //扣除了钱之后保存此记录
            $this->Status = 2;
            $this->Cause = Yii::app()->request->getParam('Cause');
            $this->save(false, array('Status','Cause'));

			$transaction->commit();	
			MemberMoneyChangeModel::releaseLock($this->UserID);
			return true;

		} catch (Exception $e) {
			if(!empty($transaction)){
				$transaction->rollBack();
			}
			MemberMoneyChangeModel::releaseLock($this->UserID);
			$error = $e->getMessage();
			$error = empty($error) ? '请求异常' : $error;
			$this->addError('',$error);
			return false;			
		}
	}



}
