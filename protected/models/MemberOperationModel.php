<?php

/**
 * This is the model class for table "{{member_operation}}".
 *
 * The followings are the available columns in table '{{member_operation}}':
 * @property string $OperationID
 * @property string $UserID
 * @property integer $Type
 * @property string $AddTime
 * @property string $Remarks
 */
class MemberOperationModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{member_operation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Remarks', 'required'),
			array('Type', 'numerical', 'integerOnly'=>true),
			array('UserID, AddTime', 'length', 'max'=>11),
			array('Remarks', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('UserID, Type, AddTime, Remarks', 'safe'),
			array('OperationID, UserID, Type, AddTime, Remarks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OperationID' => 'Operation',
			'UserID' => 'User',
			'Type' => 'Type',
			'AddTime' => 'Add Time',
			'Remarks' => 'Remarks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OperationID',$this->OperationID,true);
		$criteria->compare('UserID',$this->UserID,true);
		$criteria->compare('Type',$this->Type);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Remarks',$this->Remarks,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MemberOperationModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function saveData($data){
		$model = new MemberOperationModel();
		$model->attributes = $data;
		$model->save(false);
	}
	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
		}
		return true;
	}	
}
