<?php

/**
 * This is the model class for table "{{ordersn}}".
 *
 * The followings are the available columns in table '{{ordersn}}':
 * @property string $OrdersnID
 * @property string $Type
 * @property string $OrderNo
 * @property string $AddTime
 * @property string $AlipayNo
 * @property string $UserID
 * @property string $UserName
 * @property string $OrderTitle
 * @property integer $Status
 * @property string $Remarks
 * @property string $KeyID
 */
class OrdersnModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ordersn}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('OrderNo, Money,Type,UserID', 'required'),
			array('OrderNo', 'unique'),
			array('Type,UserID', 'numerical','integerOnly'=>true),
			array('Money', 'numerical'),
			array('Remarks,KeyID', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('OrdersnID, Type, OrderNo, AddTime, AlipayNo, UserID, UserName, OrderTitle, Status, Remarks, KeyID', 'safe'),
			array('OrdersnID, Type, OrderNo, AddTime, AlipayNo, UserID, UserName, OrderTitle, Status, Remarks, KeyID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'OrdersnID' => '主键ID',
			'Type' => '订单类型',
			'OrderNo' => '订单编号',
			'AddTime' => '添加时间',
			'AlipayNo' => '支付宝交易号',
			'UserID' => '用户ID',
			'UserName' => '用户名',
			'OrderTitle' => '订单标题',
			'Status' => '付款状态',
			'Remarks' => '备注信息',
			'KeyID' => '关联ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OrdersnID',$this->OrdersnID,true);
		$criteria->compare('Type',$this->Type,true);
		$criteria->compare('OrderNo',$this->OrderNo,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('AlipayNo',$this->AlipayNo,true);
		$criteria->compare('UserID',$this->UserID,true);
		$criteria->compare('UserName',$this->UserName,true);
		$criteria->compare('OrderTitle',$this->OrderTitle,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Remarks',$this->Remarks,true);
		$criteria->compare('KeyID',$this->KeyID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('OrdersnID',$this->OrdersnID,true);
		$criteria->compare('Type',$this->Type,true);
		$criteria->compare('OrderNo',$this->OrderNo,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('AlipayNo',$this->AlipayNo,true);
		$criteria->compare('UserID',$this->UserID,true);
		$criteria->compare('UserName',$this->UserName,true);
		$criteria->compare('OrderTitle',$this->OrderTitle,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Remarks',$this->Remarks,true);
		$criteria->compare('KeyID',$this->KeyID,true);

		return $criteria;
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
			$this->Status = 0;
			$this->UserID = yii::app()->user->id;
			$this->UserName = yii::app()->user->UserName;
		}
		return true;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrdersnModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			0 => '未付款',
			1 => '已付款',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}
	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getTypeHtml($mixData = false){
		$arrData = array(
			1 => '用户充值',
			2 => '支付保证金充值',
			3 => '支付入驻充值',
			4 => '支付商品充值',					
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}
}
