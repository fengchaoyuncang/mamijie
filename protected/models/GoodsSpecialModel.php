<?php

/**
 * This is the model class for table "{{goods_special}}".
 *
 * The followings are the available columns in table '{{goods_special}}':
 * @property integer $SpecialID
 * @property string $Name
 * @property integer $Sorting
 * @property integer $IsHot
 * @property integer $IsNew
 * @property integer $AddTime
 */
class GoodsSpecialModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{goods_special}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Name, Sorting, IsHot, IsNew, AddTime,Status', 'safe', 'on'=>'admin'),
			array('SpecialID, Name, Sorting, IsHot, IsNew, AddTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'SpecialID' => 'ID',
			'Name' => '专场名',
			'Sorting' => '排序',
			'IsHot' => '热门专场',
			'IsNew' => '是否最新专场',
			'AddTime' => '添加时间',
			'Status' => '是否显示',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('SpecialID',$this->SpecialID);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Sorting',$this->Sorting);
		$criteria->compare('IsHot',$this->IsHot);
		$criteria->compare('IsNew',$this->IsNew);
		$criteria->compare('AddTime',$this->AddTime);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('SpecialID',$this->SpecialID);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Sorting',$this->Sorting);
		$criteria->compare('IsHot',$this->IsHot);
		$criteria->compare('IsNew',$this->IsNew);
		$criteria->compare('AddTime',$this->AddTime);

		return $criteria;
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GoodsSpecialModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
		}
		return true;
	}


	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getIsHotHtml($mixData = false){
		$arrData = array(
			0 => '否',
			1 => '是',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}
	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getIsNewHtml($mixData = false){
		$arrData = array(
			0 => '否',
			1 => '是',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}
	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			1 => '显示',
			2 => '不显示',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	public static function getList($bloIsCache = true){
	
		$strKey = "GoodsSpecialModel_getList";
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();
        $objCriteria = BaseModel::getC(array('Status' => 1));
        $objCriteria->order = 'Sorting desc,SpecialID desc';
        $objModels = self::model()->findAll($objCriteria);
        foreach ($objModels as $key => $objModel) {
        	$arrCache[] = $objModel->attributes;
        }
        
        yii::app()->cache->set($strKey, $arrCache, 600);
        return $arrCache;		
	}


	/**
	 * 根据某一字段来获取全部数据
	 * @param  [type] $strField [description]
	 * @return [type]           [description]
	 */
	public static function getFieldList($strField = ''){
		$arrDatas = self::getList();
		$arrResult = array();
		if($strField){
			foreach ($arrDatas as  $arrData) {
				$arrResult[$arrData['SpecialID']] = $arrData[$strField];
			}			
		}else{
			foreach ($arrDatas as  $arrData) {
				$arrResult[$arrData['SpecialID']] = $arrData;
			}			
		}
		return $arrResult;
	}


}
