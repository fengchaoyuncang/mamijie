<?php

/**
 * This is the model class for table "{{goods_attribute}}".
 *
 * The followings are the available columns in table '{{goods_attribute}}':
 * @property integer $id
 * @property integer $GoodsBmID
 * @property string $Key
 * @property string $Value
 * @property integer $AddTime
 */
class GoodsAttributeModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{goods_attribute}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, Key, Value', 'required'),
			array('GoodsBmID, AddTime', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('GoodsBmID, Key, Value, AddTime', 'safe'),
			array('id, GoodsBmID, Key, Value, AddTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'GoodsBmID' => 'Goods Bm',
			'Key' => 'Key',
			'Value' => 'Value',
			'AddTime' => 'Add Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('GoodsBmID',$this->GoodsBmID);
		$criteria->compare('Key',$this->Key,true);
		$criteria->compare('Value',$this->Value,true);
		$criteria->compare('AddTime',$this->AddTime);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GoodsAttributeModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
		}
		return true;
	}

	//根据商品ID取所有的列表
	public static function getList($GoodsBmID, $bloIsCache = true){
		$strCache = 'GoodsAttributeModel_getList' . '_' . $GoodsBmID;
		$arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrResult = array();
        $objModels = self::model()->findAll(BaseModel::getC(array('GoodsBmID' => $GoodsBmID)));
        foreach ($objModels as $key => $objModel) {
        	$arrResult[$objModel->id] = $objModel->attributes;
        }
        yii::app()->cache->set($strCache, $arrResult, 600);
        return $arrResult;
	}

	//根据商品ID取这列表根据字段来取
	public static function getListField($GoodsBmID, $field){
		$datas = self::getList($GoodsBmID);
        $arrResult = array();
        foreach ($datas as $key => $data) {
        	$arrResult[$key] = $data[$field];
        }
        return $arrResult;
	}

}
