<?php

/**
 * 模型基类
 * File Name：BaseModel.php
 * File Encoding：UTF-8
 * File New Time：2014-5-5 15:21:32
 * Author：水平凡
 * Mailbox：admin@abc3210.com
 */
class ArticleBaseModel extends BaseModel {


    public  function getOrderd($order = ''){
        return ' Sorting desc, ArticlesID desc';
    }


    /**
     * 取结束数据
     * $gettype  取值类型
     * @param  [type] $sql          SQL语句
     * @param  [type] $strOrder     排序方式
     * @param  [type] $strCache     缓存字符串
     * @param  [type] $intCacheTime 缓存时间
     *          $bloIsUpset 是否打乱顺序
     *          $arrResult 保存结果
     * @return [type]               [description]
     */
    public  function getResult($gettype,$sql,$strOrder,$strCache,$intCacheTime,$bloIsUpset,$intPage,$intPageSize,&$arrResult){
        /*取数据*/
        $connection=Yii::app()->db;    
        $arrResult['data'] = array();   
        $arrResult['count'] = false; 
        $arrResult['page'] = false;

        //0或''只取数据   1只取总数+分页    2取数据与总数+分页
        if($gettype == 1){//只取总数
            $sql = str_replace('*', 'count(*)', $sql);
            $command=$connection->createCommand($sql);
            $intCount=$command->queryScalar();
            $objPager = new CPagination($intCount);
            $objPager->setCurrentPage($intPage - 1 < 0 ? 0 : $intPage - 1);
            $objPager->pageSize = $intPageSize;
            $arrResult['count'] = $intCount;
            $arrResult['page'] = $objPager;

        }else if($gettype == 2){//取总数与数据
            $order = $this->getOrderd($strOrder);
            if($order){
                $order = " order by  {$order}"; 
            }        
            $sql_count = str_replace('*', 'count(*)', $sql);
            $sql = $sql .  $order;

            $command=$connection->createCommand($sql_count);
            $intCount=$command->queryScalar();

            $arrResult['count'] = $intCount;

            $objPager = new CPagination($intCount);
            $objPager->setCurrentPage($intPage - 1 < 0 ? 0 : $intPage - 1);
            $objPager->pageSize = $intPageSize;
            $arrResult['page'] = $objPager;

            $offset = ($intPage - 1) < 0 ? 0 : ($intPage - 1);
            $offset = $offset*$intPageSize;
            $sql .= " limit " . $offset . ' , ' . $intPageSize;              
            $command=$connection->createCommand($sql);
            $arrDatas=$command->queryAll();

        }else{//只取数据
            $order = $this->getOrderd($strOrder);
            if($order){
                $order = " order by  {$order}"; 
            }        
            $sql = $sql .  $order;

            if($intPage == ''){
                $sql .= " limit {$intPageSize}";
            }else{
                $offset = ($intPage - 1) < 0 ? 0 : ($intPage - 1);
                $offset = $offset*$intPageSize;
                $sql .= " limit " . $offset . ' , ' . $intPageSize;             
            }


            $command=$connection->createCommand($sql);
            $arrDatas=$command->queryAll();//取出的数据          

        }
        /*取数据结束*/

        if(!empty($arrDatas)){
            foreach ($arrDatas as $key => $arrData) {
                //这两个特殊处理了。。。
                if(!empty($arrData['Content'])){
                    $arrData['Content'] = mb_substr(trim(strip_tags($arrData['Content'])), 0, 200, 'utf-8');
                }                                 
                if(!empty($arrData['Answer'])){
                    $arrData['Answer'] = mb_substr(trim(strip_tags($arrData['Answer'])), 0, 200, 'utf-8');
                }              
                $arrResult['data'][] = $arrData;
            }
            //打乱顺序
/*            if($bloIsUpset === true && !$strOrder){
                shuffle($arrResult['data']);
            } */                      
        }

        yii::app()->cache->set($strCache, $arrResult, $intCacheTime);
        return $arrResult;
    }    

}



