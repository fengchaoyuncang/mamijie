<?php

/**
 * This is the model class for table "{{member_money_change}}".
 *
 * The followings are the available columns in table '{{member_money_change}}':
 * @property string $ChangeID
 * @property string $UserID
 * @property string $UserName
 * @property string $Money
 * @property string $Remarks
 * @property integer $Type
 * @property string $AddTime
 * @property integer $ChangeType
 * @property string $RelationID
 * @property string $NumOnly *
 *用户金额变动表
 *
 *	RelationID 关联ID，
 *	如果是1：充值，则此ID代表的是充值记录ID，
 *		  2：商品技术服务费扣款，则表示商品ID
 *		  3：提现  此ID表示的是用户ID
 *		  31： 商品技术服务费  此ID表示的是 报名ID
 *		  32入驻  此ID表示的是入驻ID
 *		  33保证金  此ID表示的是用户的ID
 *		  
* 	操作金额类型
* 	1充值         增加总额
* 	2商品技术服务费扣款    减少总额
* 	3提现       减少总额
* 
* 
* 	31商品技术服务费 冻结解冻
* 	32入驻       冻结解冻
* 	33保证金     冻结解冻
*
*
* 如果缓存没有开则是全部获取到的东西都是0，用户冻结额，用户金额等都是为0，这样关于用户资金的操作就不能继续，只能等到缓存开启的时候。。。。
* 当我要操作这个表的时候我是要去获取锁的，如果缓存没有开，等于没有锁住，这样我就可以去操作这个东西。。。。。
*
* 
*
* NumOnly 如果是类型1，2，3直接扣款金额的则UserID_Type_RelationID这三个唯一确定，  如果是类型31，32，33则是UserID_Type_RelationID_AddTime来唯一确定
 */
class MemberMoneyChangeModel extends BaseModel
{

	//这个key是redis的Set类型，一个集合，如果这个用户ID存在于集合中，说明此时已经上锁，如果不存在于集合，说明此没有上锁。。。。。
	//需要锁住这个
	const USER_REDIS_KEY = 'MoneyChangeKey_';//后面跟用户的ID，有多少个用户就有多少把锁。而且这个锁有时效性，一般是


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{member_money_change}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NumOnly', 'unique'),
			array('UserID, UserName, Money, Remarks, Type, AddTime, ChangeType, RelationID, NumOnly', 'safe'),
			array('ChangeID, UserID, UserName, Money, Remarks, Type, AddTime, ChangeType, RelationID, NumOnly', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ChangeID' => 'Change',
			'UserID' => 'User',
			'UserName' => 'User Name',
			'Money' => 'Money',
			'Remarks' => 'Remarks',
			'Type' => 'Type',
			'AddTime' => 'Add Time',
			'ChangeType' => 'Change Type',
			'RelationID' => 'Relation',
			'NumOnly' => 'Num Only',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ChangeID',$this->ChangeID,true);
		$criteria->compare('UserID',$this->UserID,true);
		$criteria->compare('UserName',$this->UserName,true);
		$criteria->compare('Money',$this->Money,true);
		$criteria->compare('Remarks',$this->Remarks,true);
		$criteria->compare('Type',$this->Type);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('ChangeType',$this->ChangeType);
		$criteria->compare('RelationID',$this->RelationID,true);
		$criteria->compare('NumOnly',$this->NumOnly,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ChangeID',$this->ChangeID,true);
		$criteria->compare('UserID',$this->UserID);
		$criteria->compare('UserName',$this->UserName,true);
		$criteria->compare('Money',$this->Money,true);
		$criteria->compare('Remarks',$this->Remarks,true);
		$criteria->compare('Type',$this->Type);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('ChangeType',$this->ChangeType);
		$criteria->compare('RelationID',$this->RelationID,true);
		$criteria->compare('NumOnly',$this->NumOnly,true);

		return 	$criteria;	
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MemberMoneyChangeModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if($this->isNewRecord && !$this->AddTime){
			$this->AddTime = TIME_TIME;
		}
		return true;
	}

	public static function saveData($arrData){
        $model = new  MemberMoneyChangeModel();
		$model->attributes = $arrData;
		return $model->save(true);
	}	
	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getChangeTypeHtml($mixData = false){
		$arrData = array(
			1 => '金额变动',
			2 => '冻结额变动',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}
	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getTypeHtml($mixData = false){
		$arrData = array(
			1 => '充值',
			2 => '商品技术服务费扣款',	
			3 => '提现处理',
			31 => '技术服务费',
			32 => '入驻',
			33 => '保证金',	
			34 => '提现申请',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

	//找到某用户  ，某类型的总金额
	//没有开启redis，这样锁的状态不正常，相关的操作有问题这样的话则不正常。
	//这个锁意外锁住了，一直打不开，这怎么办？？？需要时间来确认其的有效性，，，，，
	//
	//注释锁是因为获取锁跟释放锁不是只在这一个逻辑里面完成，是需要整个逻辑完成了后我再最后释放锁。
	public static function getTypeMoney($data){
		$redis = RedisCluster::getInstance()->getRedis();
		if(!RedisCluster::getInstance()->isConnect()){
			//没有开启redis都是返回0
			return 0;
		}
		
		$datas = self::model()->findAll(BaseModel::getC($data));
		$money = 0;
		foreach ($datas as $data) {
			$money += $data->Money;
		}
		return $money;
	}

	//获取用户的金额
	public static function getUserMoney($userID = ''){
		$redis = RedisCluster::getInstance()->getRedis();
		if(!RedisCluster::getInstance()->isConnect()){
			//没有开启redis都是返回0
			return array(0,0);
		}
		empty($userID) ? $userID = yii::app()->user->id : '';


		$datas = self::model()->findAll(BaseModel::getC(array('UserID' => $userID)));
		$money = array(0,0);//第一项是总额，第二项是冻结额
		foreach ($datas as $data) {
			if($data->ChangeType == 1){
				$money[0] += $data->Money;
			}else if($data->ChangeType == 2){
/*				echo $data->Money;
				echo '--';*/
				$money[1] += $data->Money;
			}
		}
		//exit;
		return $money;		
	}



	//获取锁锁的时效为两分钟
	public static function getLock($userID){

		defined('USER_MONEY_LOCK') or define('USER_MONEY_LOCK',$userID);

		if(!RedisCluster::getInstance()->isConnect()){
			return false;
		}
		while(RedisCluster::getInstance()->get(self::USER_REDIS_KEY . $userID)){
			sleep(1);
		}
		RedisCluster::getInstance()->set(self::USER_REDIS_KEY . $userID, 1, 120);		
	}

	//释放锁
	public static function releaseLock($userID){
		if(!RedisCluster::getInstance()->isConnect()){
			return false;
		}
		RedisCluster::getInstance()->remove(self::USER_REDIS_KEY . $userID);
	}

    public function payment($orders){

		Yii::import('ext.alipay.*');

		$alipay = new Alipay();

		if ($alipay->data($orders)->createOrders() !== true) {
			$error = $alipay->getError();			
			$this->addError('',$error ? $error : '订单创建失败，请重新提交！');			
		}
		//跳转到支付宝支付页面
		$url = $alipay->payment(true);

		if($url == false){
			$error = $alipay->getError();			
			$this->addError('',$error ? $error : '订单处理错误');
			return false;	
		}else{
			return array('content' => '余额不足，马上前往支付页面！', 'url' => $url);
		}     	
    }


	//验证    是否交了
	// 金额足够  直接扣    金额不足 跳转过去
    public  function paymentBaoZhengJing($userID = ''){
		empty($userID) ? $userID = yii::app()->user->id : '';

		$key = "MemberMoneyChangeModel_paymentBaoZhengJing" . $userID;		
		$cache = yii::app()->cache->get($key);
		if($cache){
			$this->addError('','请不要重复提交');
			return false;
		}else{
			yii::app()->cache->set($key, 1, 60);
		}

		try{
	        //先锁定
	        MemberMoneyChangeModel::getLock($userID);

	        //获取此用户需要交付的保证金
	        $moneyBaoZhengJing = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'Type' => 33));

	        if($moneyBaoZhengJing >= Yii::app()->params['BaoZhengJing']){
	        	throw new Exception("已经交付了保证金");         
	        }else{
	        	$moneyBaoZhengJing = Yii::app()->params['BaoZhengJing'] - $moneyBaoZhengJing;
	        }

	        $userMoney = MemberMoneyChangeModel::getUserMoney($userID);
	        if(sprintf("%.2f", $userMoney[0] - $userMoney[1]  - $moneyBaoZhengJing) < 0 ){
				$orders = array(
				  'KeyID' => $userID,
				  'Type' => 2,
				  'Money' => sprintf("%.2f", $moneyBaoZhengJing - $userMoney[0] + $userMoney[1]),//充值的钱
				);
	        	if($return = $this->payment($orders)){
			        yii::app()->cache->delete($key);
			        MemberMoneyChangeModel::releaseLock($userID);
	        		return $return;
	        	}else{
	        		throw new Exception($this->getOneError());	        		
	        	} 
	        }else{
	            //这里就直接给其扣款
	            $transaction=Yii::app()->db->beginTransaction();
				$data = array(
					'UserID' => $userID,
					'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
					'Money' => $moneyBaoZhengJing,
					'Type' => 33,
					'ChangeType' => 2,
					'RelationID' => $userID,//商品报名ID
					'AddTime' => TIME_TIME,
					'NumOnly' => $userID . '_33_' . $userID .  '_'. TIME_TIME,
					'Remarks' => '冻结',
				);
				if(!MemberMoneyChangeModel::saveData($data)){
					throw new Exception("金额添加异常");                    
				}

				$transaction->commit(); 
				yii::app()->cache->delete($key);
				MemberMoneyChangeModel::releaseLock($userID);
				return true;          
	        }						
		}catch(Exception $e){
	        if(!empty($transaction)){
	          $transaction->rollBack();
	        }
	        //请求删除
	        yii::app()->cache->delete($key);
	        MemberMoneyChangeModel::releaseLock($userID);
	        $error = $e->getMessage();
	        $error = empty($error) ? '请求异常' : $error;
	        $this->addError('',$error);
	        return false;  	        			
		}		   	
    }





    public  function paymentRuzhu($userID = ''){
		empty($userID) ? $userID = yii::app()->user->id : '';

		$key = "MemberMoneyChangeModel_paymentRuzhu" . $userID;		
		$cache = yii::app()->cache->get($key);
		if($cache){
			$this->addError('','请不要重复提交');
			return false;
		}else{
			yii::app()->cache->set($key, 1, 60);
		}

		try{

			//获取这个用户这个入驻所需要的冻结金额
			$SettledModel = MerchantsSettledModel::model()->find("UserID = {$userID} and Status != 2 and  Money > 0");
			if(!$SettledModel){
				throw new Exception("提交错误");				
			}
	        if($SettledModel->Status == 0 || $SettledModel->Status == 2 || $SettledModel->Status == 3){
				throw new Exception("入驻不符合要求");			    
	        }

	        //先锁定
	        MemberMoneyChangeModel::getLock($userID);

			$money = MemberMoneyChangeModel::getTypeMoney(array('UserID' => $userID, 'Type' => 32));//这个用户这个类型有多少钱
			
			//冻结了足够多的钱则说明已经入驻
			if($money >= $SettledModel->Money){
				throw new Exception("你已经入驻");					
			}

			//需要支付的金额是
			$m = $SettledModel->Money - $money;
			//用户的总额与冻结额有多少
			$userMoney = MemberMoneyChangeModel::getUserMoney($userID);	



	        if(sprintf("%.2f", $userMoney[0] - $userMoney[1] - $m) < 0){

				$orders = array(
				  'KeyID' => $SettledModel->MerchantsSettledID,
				  'Type' => 3,
				  'Money' => $m,//充值的钱
				);
	        	if($return = $this->payment($orders)){
			        yii::app()->cache->delete($key);
			        MemberMoneyChangeModel::releaseLock($userID);
	        		return $return;
	        	}else{
	        		throw new Exception($this->getOneError());	        		
	        	} 
	        }else{
	        	$transaction=Yii::app()->db->beginTransaction();

	            //增加用户金额变动记录  增加冻结这金额
	            $data = array(
	                'UserID' => $userID,
	                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
	                'Money' => $m,
	                'Type' => 32,
	                'ChangeType' => 2,
	                'RelationID' => $SettledModel->MerchantsSettledID,//入驻ID
	                'AddTime' => TIME_TIME,
	                'NumOnly' => $userID . '_32_' . $SettledModel->MerchantsSettledID .  '_'. TIME_TIME,
	            );
	            
	            //如果金额变动表保存失败则抛出异常
	            if(!MemberMoneyChangeModel::saveData($data)){
	                throw new Exception("金额添加异常");                    
	            }

	            $SettledModel->Status = 1;
	            $SettledModel->save(false, array('Status'));

	            $transaction->commit();

	            yii::app()->cache->delete($key);
	            MemberMoneyChangeModel::releaseLock($userID);
	
				return true;        
	        }						
		}catch(Exception $e){
	        if(!empty($transaction)){
	          $transaction->rollBack();
	        }
	        //请求删除
	        yii::app()->cache->delete($key);
	        if(defined('USER_MONEY_LOCK')){
	        	MemberMoneyChangeModel::releaseLock($userID);
	        }	        
	        $error = $e->getMessage();
	        $error = empty($error) ? '请求异常' : $error;
	        $this->addError('',$error);
	        return false;  	        			
		}		   	
    }

	//用户付钱
	//$goodsBmID 报名的ID
	//num为用户预定的库存 ,如果值为空则是为这商品付钱，如果值不为空则是为这个商品续存库存
	//验证
	//1有没有达到付款的条件2有没有付款3钱够不够
	//
	//验证通过后
	//增加冻结钱的记录，改变此商品的状态。改变此商品的库存
	public function paymentGoods($userID, $goodsBmID, $num = ''){

		$key = "MemberMoneyChangeModel_paymentGoods" . $userID;
		$cache = yii::app()->cache->get($key);
		if($cache){
			$this->addError('','请不要重复提交');
			return false;
		}else{
			yii::app()->cache->set($key, 1, 60);
		}

		try {

			$goodBmModel = GoodsBmModel::model()->findByPk($goodsBmID);
			if(!$goodBmModel){
				throw new Exception("找不到报名记录");				
			}
			if($goodBmModel->UserID != $userID){
				throw new Exception("此报名记录不属于你");				
			}

			if($goodBmModel->Status == 0 && $goodBmModel->AdminID == 0){
				throw new Exception("未安排人审核", 99);				
			}

			if($goodBmModel->Pattern == 0){
				throw new Exception("此商品的模式不是技术服务费模式", 99);				
			}
			if($goodBmModel->Status == 2){
				throw new Exception("此商品未通过审核", 99);				
			}
			if($goodBmModel->Status == 4){
				throw new Exception("此商品你已取消", 99);				
			}
			if($goodBmModel->Status == 5){
				throw new Exception("此商品已经下架", 99);				
			}



			MemberMoneyChangeModel::getLock($userID);

			//获取用户的金额
			$userMoney = MemberMoneyChangeModel::getUserMoney($userID);			
			if($num){
				//这说明是增加库存
				if($goodBmModel->IsPay == 0){
					throw new Exception("此商品未付款不能增加库存");	
				}

				//获取此用户冻结的保证金
				//$hasBaoZhengJing = 1 表示已经交了保证金，等于0表示没有交，如果没有交保证金$moneyBaoZhengJing表示所需要交的保证金
				$moneyBaoZhengJing = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'Type' => 33));
				if($moneyBaoZhengJing >= Yii::app()->params['BaoZhengJing']){
					$moneyBaoZhengJing = 0;
					$hasBaoZhengJing = 1;
				}else{
					$hasBaoZhengJing = 0;
					$moneyBaoZhengJing = Yii::app()->params['BaoZhengJing'] - $moneyBaoZhengJing;
				}

				//确认余额是否足够
				//需要付的钱
				$m = $goodBmModel->getDoMoney($num);
				if(sprintf("%.2f", $userMoney[0] - $userMoney[1] - $m - $moneyBaoZhengJing) < 0 ){
					$orders = array(
					  'KeyID' => $goodsBmID,
					  'Remarks' => $num,
					  'Type' => 4,
					  'Money' => sprintf("%.2f", $m+$moneyBaoZhengJing-$userMoney[0]+$userMoney[1]),//充值的钱
					);
		        	if($return = $this->payment($orders)){
				        yii::app()->cache->delete($key);
				        MemberMoneyChangeModel::releaseLock($userID);
		        		return $return;
		        	}else{
		        		throw new Exception($this->getOneError());	        		
		        	} 	
				}


				//增加记录
				$transaction=Yii::app()->db->beginTransaction();

				//先交保证金
	            if(!$hasBaoZhengJing){//没有交保证金的情况下
		            $data = array(
		                'UserID' => $userID,
		                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
		                'Money' => $moneyBaoZhengJing,
		                'Type' => 33,
		                'ChangeType' => 2,
		                'RelationID' => $userID,//商品报名ID
		                'AddTime' => TIME_TIME,
		                'NumOnly' => $userID . '_33_' . $userID . '_'. TIME_TIME,
		            );
	                if(!MemberMoneyChangeModel::saveData($data)){
	                    throw new Exception("金额添加异常");                    
	                }
	            }

	            //再冻结商品技术服务费用
	            //增加用户金额变动记录  增加冻结这金额
	            $data = array(
	                'UserID' => $userID,
	                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
	                'Money' => $m,
	                'Type' => 31,
	                'ChangeType' => 2,
	                'RelationID' => $goodsBmID,//商品报名ID
	                'AddTime' => TIME_TIME,
	                'NumOnly' => $userID . '_31_' . $goodsBmID .  '_'. TIME_TIME,
	                'Remarks' => '补充技术服务费',
	            );
                if(!MemberMoneyChangeModel::saveData($data)){
                    throw new Exception("金额添加异常");                    
                }

	            //商品表状态修改  改变金额与计划销量
	            $goodBmModel->PlanSale = $goodBmModel->PlanSale + $num;
	            $goodBmModel->getDoMoney();
	            $goodBmModel->save(false);
				$transaction->commit();
			}else{
				//获取此商品现在冻结了多少钱
				$money = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'RelationID' => $goodsBmID, 'Type' => 31));

				$moneyBaoZhengJing = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'Type' => 33));
				if($moneyBaoZhengJing >= Yii::app()->params['BaoZhengJing']){
					$moneyBaoZhengJing = 0;
					$hasBaoZhengJing = 1;
				}else{
					$hasBaoZhengJing = 0;
					$moneyBaoZhengJing = Yii::app()->params['BaoZhengJing'] - $moneyBaoZhengJing;
				}

				//获取此商品需要冻结的钱  $goodBmModel->Money
				$goodBmModel->getDoMoney();

				if($money >= $goodBmModel->Money){
					throw new Exception("已经付款了");		
				}

				//需要付的钱
				$m = $goodBmModel->Money - $money;
				if(sprintf("%.2f", $userMoney[0] - $userMoney[1] - $m - $moneyBaoZhengJing) < 0 ){//余额不足
					$orders = array(
					  'KeyID' => $goodsBmID,
					  'Remarks' => $num,
					  'Type' => 4,
					  'Money' => sprintf("%.2f", $m+$moneyBaoZhengJing-$userMoney[0]+$userMoney[1]),//充值的钱
					);
		        	if($return = $this->payment($orders)){
				        yii::app()->cache->delete($key);
				        MemberMoneyChangeModel::releaseLock($userID);
		        		return $return;
		        	}else{
		        		throw new Exception($this->getOneError());	        		
		        	} 
				}else{
					$transaction=Yii::app()->db->beginTransaction();

					//先交保证金
		            if(!$hasBaoZhengJing){//没有交保证金的情况下
			            $data = array(
			                'UserID' => $userID,
			                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
			                'Money' => $moneyBaoZhengJing,
			                'Type' => 33,
			                'ChangeType' => 2,
			                'RelationID' => $userID,//商品报名ID
			                'AddTime' => TIME_TIME,
			                'NumOnly' => $userID . '_33_' . $userID .  '_'. TIME_TIME,
			            );
		                if(!MemberMoneyChangeModel::saveData($data)){
		                    throw new Exception("金额添加异常");                    
		                }
		            }

		            //再冻结商品技术服务费用
		            //增加用户金额变动记录  增加冻结这金额
		            $data = array(
		                'UserID' => $userID,
		                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
		                'Money' => $m,
		                'Type' => 31,
		                'ChangeType' => 2,
		                'RelationID' => $goodsBmID,//商品报名ID
		                'AddTime' => TIME_TIME,
		                'NumOnly' => $userID . '_31_' . $goodsBmID .  '_'. TIME_TIME,
		            );
	                if(!MemberMoneyChangeModel::saveData($data)){
	                    throw new Exception("金额添加异常");                    
	                }

		            //商品表状态修改
		            $goodBmModel->IsPay = 1;
		            $goodBmModel->save(false);
					$transaction->commit();
				}										
			}

			yii::app()->cache->delete($key);
			MemberMoneyChangeModel::releaseLock($userID);
			return true;			

		} catch (Exception $e) {
			if(!empty($transaction)){
				$transaction->rollBack();
			}
			//请求删除
			yii::app()->cache->delete($key);
	        if(defined('USER_MONEY_LOCK')){
	        	MemberMoneyChangeModel::releaseLock($userID);
	        }
			$error = $e->getMessage();
			$error = empty($error) ? '请求异常' : $error;
			$this->addError('',$error);
			return false;
		}


	}

	//用户取消这个报名
	//验证  如果不是未审核的则取消不了
	//验证OK了后  看是什么模式，再判断
	//Status = 4   默认是取消
	//可以是其它的比如说拒绝什么的。
	public function cancelGoods($userID, $goodsBmID,$Status = 4){
		if(!in_array($Status, array(2,4))){
			$this->addError('','请求错误');
			return false;			
		}
		$key = "MemberMoneyChangeModel_cancelGoods" . $userID;
		$cache = yii::app()->cache->get($key);
		if($cache){
			$this->addError('','请不要重复提交');
			return false;
		}else{
			yii::app()->cache->set($key, 1, 60);
		}

		try {
			$goodBmModel = GoodsBmModel::model()->findByPk($goodsBmID);
			if(!$goodBmModel){
				throw new Exception("找不到报名记录");				
			}
			if($goodBmModel->UserID != $userID){
				throw new Exception("此报名记录不属于你");				
			}

			if(GROUP_NAME != 'admin'){
				if(!($goodBmModel->Status == 0 && $goodBmModel->AdminID == 0)){
					throw new Exception("商品正在审核或者已经处理", 99);				
				}				
			}


			if($goodBmModel->Pattern == 0){//普通商品的取消
				$goodBmModel->Status = $Status;
				$goodBmModel->save(false,array('Status'));
				yii::app()->cache->delete($key);
				return ture;
			}else{
				MemberMoneyChangeModel::getLock($userID);
				$money = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'RelationID' => $goodsBmID, 'Type' => 31));
				if($money > 0){//此商品付款了
		        	$transaction=Yii::app()->db->beginTransaction();

		            //解冻这金额
		            $data = array(
		                'UserID' => $userID,
		                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
		                'Money' => -$money,
		                'Type' => 31,
		                'ChangeType' => 2,
		                'RelationID' => $goodsBmID,//入驻ID
		                'AddTime' => TIME_TIME,
		                'NumOnly' => $userID . '_31_' . $goodsBmID .  '_'. TIME_TIME,
		            );
		            
		            //如果金额变动表保存失败则抛出异常
		            if(!MemberMoneyChangeModel::saveData($data)){
		                throw new Exception("金额添加异常");                    
		            }

		            $goodBmModel->Status = $Status;
		            $goodBmModel->IsPay = 0;
		            $goodBmModel->save(false,array('Status','IsPay'));

		            $transaction->commit();

		            yii::app()->cache->delete($key);
		            MemberMoneyChangeModel::releaseLock($userID);
		
					return true;        					
				}else{//此商品没有付款,,如果redis没有开此时若返回都是0则可能会有问题
					$this->Status = $Status;
					$goodBmModel->save(false,array('Status'));
					yii::app()->cache->delete($key);
					MemberMoneyChangeModel::releaseLock($userID);
					return ture;					
				}	
			}
						
		} catch (Exception $e) {
			if(!empty($transaction)){
				$transaction->rollBack();
			}
			//请求删除
			yii::app()->cache->delete($key);
	        if(defined('USER_MONEY_LOCK')){
	        	MemberMoneyChangeModel::releaseLock($userID);
	        }
			$error = $e->getMessage();
			$error = empty($error) ? '请求异常' : $error;
			$this->addError('',$error);
			return false;			
		}
	}



	//技术服务费的扣款，，商品结算  对应类型等于2
	//对于某个用户的这个商品我需要结算
	//验证
	//1这个状态是1，2下架了才能结算  3此商品的模式是正确的模式  4此商品已经付款
	//技术服务费结算  总额扣除一部份钱   冻结的扣除解冻一部份钱   还有一部份冻结的钱是等到三天后再给它解冻   
	public function paymentGoodsAccounts($userID, $goodsBmID){	
		$key = "MemberMoneyChangeModel_paymentGoodsAccounts" . $userID;
		$cache = yii::app()->cache->get($key);
		if($cache){
			$this->addError('','请不要重复提交');
			return false;
		}else{
			yii::app()->cache->set($key, 1, 60);
		}

		$GoodsBmModel = GoodsBmModel::model()->findByPk($goodsBmID);	

		//现在是需要真正的结算的时候了
		try {

			if(!$GoodsBmModel){
				throw new Exception("找不到此报名记录");				
			}

			if($GoodsBmModel->Status != 1){
				throw new Exception("此商品的状态不符合条件");	
			}

			if($GoodsBmModel->Pattern == 0){
				throw new Exception("此商品的模式不是技术服务费模式");	
			}

			if($GoodsBmModel->IsPay == 0){
				throw new Exception("此商品未付款");	
			}

			if($GoodsBmModel->IsAccounts == 1){
				throw new Exception("此商品已经结算");	
			}


			//上锁
			MemberMoneyChangeModel::getLock($userID);

			//获取关于此商品的冻结的钱 //总的为其冻结了多少钱
			$money = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'RelationID' => $goodsBmID, 'Type' => 31));	
			//这次结算所需要的费用
			$m = $GoodsBmModel->realCanMoney();
			if($m > $money){
				throw new Exception("异常错误：结算所需要的费用比冻结的费用大");				
			}

			//增加记录
			$transaction=Yii::app()->db->beginTransaction();
			//先解冻这部份钱，再从总额扣除这部份钱		
            $data = array(
                'UserID' => $userID,
                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
                'Money' => -$m,
                'Type' => 31,
                'ChangeType' => 2,
                'RelationID' => $goodsBmID,//商品报名ID
                'AddTime' => TIME_TIME,
                'Remarks' => '解冻',
                'NumOnly' => $userID . '_31_' . $goodsBmID .  '_'. TIME_TIME,
            );
            if(!MemberMoneyChangeModel::saveData($data)){
                throw new Exception("金额添加异常");                    
            }

            //从总额扣除费用
            $data = array(
                'UserID' => $userID,
                'UserName' => MemberModel::getDetailForUserID($userID, 'UserName'),
                'Money' => -$m,
                'Type' => 2,
                'ChangeType' => 1,
                'RelationID' => $goodsBmID,//商品报名ID
                'AddTime' => TIME_TIME,
                'NumOnly' => $userID . '_2_' . $goodsBmID,
            );
            if(!MemberMoneyChangeModel::saveData($data)){
                throw new Exception("金额添加异常");                    
            }

            //再标记一下这个说明已经是结算了
            $GoodsBmModel->IsAccounts = 1;
            $GoodsBmModel->AccountsTime = TIME_TIME;
            $GoodsBmModel->save(false);
            

			$transaction->commit();	
			yii::app()->cache->delete($key);
			MemberMoneyChangeModel::releaseLock($userID);
			return true;


		} catch (Exception $e) {
	        if(!empty($transaction)){
	          $transaction->rollBack();
	        }
	        //请求删除
	        yii::app()->cache->delete($key);
	        if(defined('USER_MONEY_LOCK')){
	        	MemberMoneyChangeModel::releaseLock($userID);
	        }	        
	        $error = $e->getMessage();
	        $error = empty($error) ? '请求异常' : $error;
	        $this->addError('',$error);
	        return false;  				
		}
		
	}

	//用户申请退保证金
	public function cancelBaoZhengJing($userID){
		$key = "MemberMoneyChangeModel_cancelBaoZhengJing" . $userID;
		$cache = yii::app()->cache->get($key);
		if($cache){
			$this->addError('','请不要重复提交');
			return false;
		}else{
			yii::app()->cache->set($key, 1, 60);
		}

		//找这个用户是否还有正在上架的，没有结算的，在报名的一些商品
		try {

			$d = GoodsBmModel::model()->find("UserID={$userID} and Status in(0,3)");
			if($d){
				throw new Exception("还有未审核或正在审核的商品,申请被拒绝");				
			}

			$d = GoodsBmModel::model()->find("UserID={$userID} and Status=1 and Pattern=1 and (IsAccounts=0 or PlanAccounts=0)");
			if($d){
				throw new Exception("还有未完成结算的商品");				
			}
			//最后一次结算的记录
			$d = GoodsBmModel::model()->find(array('condition' => "UserID={$userID} and AccountsTime > 0",'order' => 'AccountsTime desc'));
			
			//用户的最后一条记录
			$d_d = GoodsBmModel::model()->find(array('condition' => "UserID={$userID}",'order' => 'GoodsBmID desc'));
			$time =   $d_d->UpdateTime > $d->AccountsTime ? $d_d->UpdateTime : $d->AccountsTime;
			$time = $time + 86400*15;
			if(TIME_TIME <= $time){
				throw new Exception("保证金在最后一次结算或最后一次审核处理后15天才可以申请解冻");				
			}
			$da = MemberThawApplyModel::model()->find(BaseModel::getC(array('UserID' => $userID, 'Type' => '1', 'Status' => 0)));
			if($da){
				throw new Exception("你还有未处理的申请");				
			}

			//以上条件符合了之后可以申请退款保证金
						

			//上锁
			MemberMoneyChangeModel::getLock($userID);

			//获取冻结掉的保证金
			$money = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'Type' => 33));	
			if($money <= 0){
				throw new Exception("你未被冻结保证金");				
			}


			$arr = array(
				'Type' => 1,
				'UserID' => $userID,
			);
			MemberThawApplyModel::saveData($arr);
			yii::app()->cache->delete($key);
			MemberMoneyChangeModel::releaseLock($userID);
			return true;


		} catch (Exception $e) {
	        if(!empty($transaction)){
	          $transaction->rollBack();
	        }
	        //请求删除
	        yii::app()->cache->delete($key);
	        if(defined('USER_MONEY_LOCK')){
	        	MemberMoneyChangeModel::releaseLock($userID);
	        }	        
	        $error = $e->getMessage();
	        $error = empty($error) ? '请求异常' : $error;
	        $this->addError('',$error);
	        return false;  				
		}		
	}

	//用户申请退入驻费用
	public function cancelRuzhu($userID){
		$key = "MemberMoneyChangeModel_cancelRuzhu" . $userID;
		$cache = yii::app()->cache->get($key);
		if($cache){
			$this->addError('','请不要重复提交');
			return false;
		}else{
			yii::app()->cache->set($key, 1, 60);
		}

		//找这个用户是否还有正在上架的，没有结算的，在报名的一些商品
		try {

			$d = MerchantsSettledModel::model()->find(BaseModel::getC(array('UserID' => $userID)));
			if(!$d){
				throw new Exception("找不到入驻信息");			
			}
						

			//上锁
			MemberMoneyChangeModel::getLock($userID);

			//获取入驻的保证金
			$money = MemberMoneyChangeModel::getTypeMoney(array('userID' => $userID, 'Type' => 32));	
			if($money <= 0){
				throw new Exception("你未被冻结入驻保证金");				
			}

			$da = MemberThawApplyModel::model()->find(BaseModel::getC(array('UserID' => $userID, 'Type' => '2', 'Status' => 0)));
			if($da){
				throw new Exception("你还有未处理的申请");				
			}

			$arr = array(
				'Type' => 2,
				'UserID' => $userID,
			);
			MemberThawApplyModel::saveData($arr);
			yii::app()->cache->delete($key);
			MemberMoneyChangeModel::releaseLock($userID);
			return true;


		} catch (Exception $e) {
	        if(!empty($transaction)){
	          $transaction->rollBack();
	        }
	        //请求删除
	        yii::app()->cache->delete($key);
	        if(defined('USER_MONEY_LOCK')){
	        	MemberMoneyChangeModel::releaseLock($userID);
	        }	        
	        $error = $e->getMessage();
	        $error = empty($error) ? '请求异常' : $error;
	        $this->addError('',$error);
	        return false;  				
		}		
	}
}
