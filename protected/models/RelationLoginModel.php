<?php

/**
 * 
 * 关联登录，，新的登录
 * 
 */
class RelationLoginModel extends CFormModel {

    public $username;  //用户名
    public $password;  //

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('username', 'required'),
        );
    }

    public function checkUserName($MemberOauthModel) {
        $password = MemberModel::encrypt($this->password);
        if (is_numeric($this->username) && strlen($this->username) == 11) { //是手机

            if (!$model = MemberModel::model()->find(BaseModel::getC(array('Phone' => $this->username, 'Password' => $password)))) {
                $this->addError('username', '找不到此用户');
                return false;
            }
        } else if(preg_match('/^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/', $this->username)){
            if (!$model = MemberModel::model()->find(BaseModel::getC(array('Email' => $this->username, 'Password' => $password)))) {
                $this->addError('username', '找不到此用户');
                return false;
            }
        }else{
            if (!$model = MemberModel::model()->find(BaseModel::getC(array('UserName' => $this->username, 'Password' => $password)))) {
                $this->addError('username', '找不到此用户');
                return false;
            }         
        }

        $MemberOauthModel->UserID = $model->UserID;
        $MemberOauthModel->save(false);
        return $model;
    }
}
