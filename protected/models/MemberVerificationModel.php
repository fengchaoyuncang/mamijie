<?php

/**
 * This is the model class for table "{{member_verification}}".
 *
 * The followings are the available columns in table '{{member_verification}}':
 * @property string $UserID
 * @property string $Code
 * @property string $AddTime
 */
class MemberVerificationModel extends BaseModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{member_verification}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Code, AddTime,Email', 'safe'),
			array('Code, AddTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Code' => 'Code',
			'AddTime' => 'Add Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Code',$this->Code,true);
		$criteria->compare('AddTime',$this->AddTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MemberVerificationModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
			$this->Status = 0;
		}
		return true;
	}

	/**
	 * 获取用户最新的一条记录,根据邮箱
	 * @return [type] [description]
	 */
	public static function getOneData($email){
		return self::model()->find(BaseModel::getC(array('Email' => $email, 'order' => 'AddTime DESC')));
	}

	/**
	 * 获取用户最新的一条记录,根据邮箱
	 * @return [type] [description]
	 */
	public static function verification($Code){
		$model = self::model()->find(BaseModel::getC(array('Code' => $Code, 'order' => 'AddTime DESC')));
		$message = '';
		if(!$model){
			return $message = 0;
		}else{
			if((TIME_TIME - $model->AddTime) > 3600){
				yii::app()->user->setFlash($model->Email);
				return $message = -1;
			}
			if(($memberModel = MemberModel::model()->find(BaseModel::getC(array("Email" => $model->Email))))  &&  $memberModel->Status == 0){
				$memberModel->Status = 1;
				$memberModel->save(false, array('Status'));
				self::model()->deleteAll(BaseModel::getC(array("Email" => $model->Email)));
				return true;
			}
		}
	}

	public static function saveData($data){
		$model = new MemberVerificationModel;
		$model->attributes = $data;
		$model->save(false);
	}
		
}
