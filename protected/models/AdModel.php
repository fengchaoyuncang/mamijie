<?php

/**
 * This is the model class for table "{{ad}}".
 *
 * The followings are the available columns in table '{{ad}}':
 * @property integer $AdID
 * @property string $AkMask
 * @property string $Name
 * @property integer $AddTime
 */
class AdModel extends BaseModel
{
	public static $db;

    public function getDbConnection(){
        if(self::$db!==null){
            return self::$db;
        }else{
            self::$db=Yii::app()->getComponent('db_count');
            if(self::$db instanceof CDbConnection)
                return self::$db;
            else
                throw new CDbException('连接出错');                    
        }            
    }	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ad}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('AkMask, Name', 'required'),
			array('AddTime', 'numerical', 'integerOnly'=>true),
			array('AkMask, Name', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('AdID, AkMask, Name, AddTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'AdID' => 'ID',
			'AkMask' => '广告标识',
			'Name' => '广告名称',
			'AddTime' => '添加时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('AdID',$this->AdID);
		$criteria->compare('AkMask',$this->AkMask,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('AddTime',$this->AddTime);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('AdID',$this->AdID);
		$criteria->compare('AkMask',$this->AkMask,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('AddTime',$this->AddTime);

		return $criteria;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * 获取所有的数据
	 * @return [type] [description]
	 */
	public static function getList($bloIsCache = true){

		$strCache = 'AdModel_getList';
        $arrCache = yii::app()->cache->get($strCache);
        //如果不是取缓存数据  或者 缓存已经过期
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();
		$objModels = self::model()->findAll();
		foreach ($objModels as $key => $objModel) {
			$arrCache[] = $objModel->attributes;
		}
        yii::app()->cache->set($strCache, $arrCache, 600);
        return $arrCache;		
	}
	public function afterSave(){
		self::getList(false);
	}
	public function afterDelete(){
		self::getList(false);
	}
	public function beforeSave(){
        if ($this->isNewRecord) {
            $this->AddTime = TIME_TIME;
        }
		return true;
	}

	/*取数据   ID对应NAME*/
	public static function  getListName($field = 'Name'){
		$arrDatas = self::getList();
		$arrResutl = array();
		foreach ($arrDatas as $key => $arrData) {
			$arrResutl[$arrData['AkMask']] = $arrData[$field];
		}
		return $arrResutl;
	}

}
