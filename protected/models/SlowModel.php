<?php

/**
 * This is the model class for table "{{ad}}".
 *
 * The followings are the available columns in table '{{ad}}':
 * @property integer $AdID
 * @property string $AkMask
 * @property string $Name
 * @property integer $AddTime
 */
class SlowModel extends BaseModel
{
	public static $db;

    public function getDbConnection(){
        if(self::$db!==null){
            return self::$db;
        }else{
            self::$db=Yii::app()->getComponent('db_count');
            if(self::$db instanceof CDbConnection)
                return self::$db;
            else
                throw new CDbException('连接出错');                    
        }            
    }	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{slow}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('SlowTime, AddTime, Url, AddTime, AddTimeDate', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('AdID',$this->AdID);
		$criteria->compare('AkMask',$this->AkMask,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('AddTime',$this->AddTime);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    //记录
    public static function record($time) {
        $arrData = array(
            'Url' => Yii::app()->request->hostInfo . Yii::app()->request->getUrl(),
            'AddTime' => TIME_TIME,            //记录时间
            'AddTimeDate' => date('Y-m-d H:i:s', TIME_TIME),
            'SlowTime' => $time,            //记录时间
        );
        if ((defined('YII_CESHI')) || defined('YII_DEBUG') && YII_DEBUG == false) {
            try {
                @RedisCluster::getInstance()->push('slow_record', $arrData);
            } catch (Exception $exc) {
                
            }
        }
    }
}
