<?php

/**
 * This is the model class for table "{{category}}".
 *
 * The followings are the available columns in table '{{category}}':
 * @property string $CatID
 * @property string $ParentID
 * @property string $Title
 * @property string $Brief
 * @property string $AddTime
 * @property string $Sorting
 * @property string $Status
 * 比较常用，如果是说需要找某个分类下面的子分类，子子分类，这样子比较耗时，
 * 所以最好前台要用到的函数用缓存建立起来。。。。。这样就不需要每次都递归去查询我想要的数据。。。。。
 * 或者我直接体现在一个文件里面看这样可能会更好一点来展示所需要的。。。。。 ？？？？？？？？？
 */
class CategoryModel extends BaseModel
{
	public static $UPDATACACHE = '';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Title', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ParentID, Brief, Sorting', 'safe'),
			array('ParentID, Title, Brief, AddTime, Sorting, Status,IsHot, AppImg,TitleSeo,KeywordsSeo,DescriptionSeo', 'safe', 'on'=>'admin'),
			array('CatID, ParentID, Title, Brief, AddTime, Sorting, Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CatID' => 'ID',
			'ParentID' => '父ID',
			'Title' => '标题',
			'Brief' => '简介',
			'AddTime' => '添加时间',
			'Sorting' => '排序',
			'Status' => '是否可用',
			'AppImg' => 'APP图',
			'TitleSeo' => 'Seo标签',
			'KeywordsSeo' => 'seo关键字',
			'DescriptionSeo' => 'seo描述',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CatID',$this->CatID,true);
		$criteria->compare('ParentID',$this->ParentID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Brief',$this->Brief,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('Status',$this->Status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function createSearchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CatID',$this->CatID,true);
		$criteria->compare('ParentID',$this->ParentID,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Brief',$this->Brief,true);
		$criteria->compare('AddTime',$this->AddTime,true);
		$criteria->compare('Sorting',$this->Sorting,true);
		$criteria->compare('Status',$this->Status,true);

		return $criteria;
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CategoryModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if($this->isNewRecord){
			$this->AddTime = time();
		}
		return true;
	}

	/**
	 * 取得所有的数据，这里建立了两种缓存
	 * $bloIsCache  是否需要缓存
	 * $key 有两种缓存，然后这是看要取哪一种，
	 * GROUP_NAME是后台 且要满足 $key为false  则取CategoryModel_getList_admin这个缓存
	 * 否则其它的都是取CategoryModel_getList这个缓存
	 * 
	 * @param  [type] $intCatID [description]
	 * @return [type]           [description]
	 */
	public static function getList($bloIsCache = true, $key = ''){
		static $arrCache;
		if($arrCache && !self::$UPDATACACHE){
			return $arrCache;
		}

		if(GROUP_NAME == 'admin'){
			$bloIsCache = false;
		}		

		if(GROUP_NAME == 'admin' && !$key){
			$strKey = "CategoryModel_getList_admin";
		}else{
			$strKey = "CategoryModel_getList";
		}
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }

        $arrCache = array();
        if($strKey == 'CategoryModel_getList_admin'){
        	$objCriteria = BaseModel::getC(array());
        }else{
        	$objCriteria = BaseModel::getC(array('Status' => 1));
        }
        $objCriteria->order = 'CatID asc';
        $objModels = self::model()->findAll($objCriteria);
        foreach ($objModels as $key => $objModel) {
        	$arrCache[$objModel->CatID] = $objModel->attributes;
        }
        
        yii::app()->cache->set($strKey, $arrCache, 3600);
        return $arrCache;		
	}

	public function afterSave(){
		parent::afterSave();
		self::$UPDATACACHE = true;
		self::getList();//后台缓存
		self::getList(false, true);//前台缓存
		self::getListMenu(2,false);//前台取两层数据
	}
	public function afterDelete(){
		parent::afterDelete();
		self::$UPDATACACHE = true;
		self::getList();//后台缓存
		self::getList(false, true);//前台缓存
		self::getListMenu(2,false);//前台取两层数据
	}

	/**
	 * 根据分类ID来获取其子分类包括自身
	 * 通过catid来获取它底下的所有的数据，包括它本身
	 * 此分类ID不能为0，不能为空。
	 * $intLayer 取几层的数据  默认false所有的都取出来
	 * 格式
	 * @return [type] [description]
	 */
	public static function getListCatID($intCatID, $intLayer = false, $intLayerNum = 1){
		$datas = self::getList();

		$arrResult = $datas[$intCatID];
		$arrResult['Layer'] = $intLayerNum;
		$arrResult['data'] = array();
		$intLayerNum++;
		if($intLayer && $intLayerNum > $intLayer){
			return $arrResult;
		}		
		foreach ($datas as $CatID => $data) {
			if($data['ParentID'] == $intCatID){
				$arrResult['data'][$CatID] = self::getListCatID($CatID, $intLayer, $intLayerNum);
			}
		}
		return $arrResult;
	}

	/**
	 * 获取所有
	 * 获取所有的分类，最多获取两层的数据
	 * $intLayer定义了第几层  如果是第1层则获取所有第一层的数据
	 * @param  boolean $intLayer [description]
	 * @return [type]            [description]
	 */
	public static function  getListMenu($intLayer = 1, $bloIsCache = true){

		$strKey = "CategoryModel_getListMenu" . $intLayer;
		$arrCache = yii::app()->cache->get($strKey);
        if($bloIsCache && $arrCache !== false){
            return $arrCache;
        }
        $arrCache = array();

        $datas = self::getList();
        foreach ($datas as $CatID => $data) {
        	if($data['ParentID'] == 0){
        		$arrCache[$CatID] = self::getListCatID($CatID, $intLayer, 1);
        	}
        }
    	yii::app()->cache->set($strKey, $arrCache, 3600);
    	return $arrCache;		
	}

	/**获取所有以树型方式
	 * 获取全部树型的数据 数组形式呈现
	 * 形式是catid => Title
	 * @return [type] [description]
	 */
	public static function getListTree(){
        Tree::$arrData = self::getList();
        return Tree::getArrData();
	}

	/**获取所有，包含树型
	 * 获取所有的数据，除了标题 按数组方式呈现
	 * 形式是catid => array()
	 * @return [type] [description]
	 */
	public static function getListTreeArr(){
		$arrResult = array();
		$arrTree = self::getListTree();
		$arrDatas = self::getList();
		foreach ($arrTree as $key => $value) {
			$arrResult[$key] = $arrDatas[$key];
			$arrResult[$key]['treetitle'] = $value;
		}
		return $arrResult;
	}


	/**
	 * 状态
	 * @return [type] [description]
	 */
	public static function getStatusHtml($mixData = false){
		$arrData = array(
			0 => '不可用',
			1 => '可用',			
		);
		if($mixData !== false){
			return $arrData[$mixData];
		}else{
			return $arrData;
		}
	}

    /**
     * 根据栏目ID删除
     * @param type $catid 栏目ID
     * @return type
     */
    public static function deleteCategoryByPk($catid) {
        if (empty($catid)) {
            return $catid;
        }
        $model = self::model()->findByPk($catid);
        if (empty($model)) {
            return false;
        }
        $info = $model->attributes;
        //检查是否有子栏目
        $childid = self::getStrChildid($info['CatID']);
        if (empty($childid)) {
            return $model->delete();
        }

        $bloResult = self::model()->deleteAll(self::model()->where(array('CatID' => array('IN', $childid))));
        //删除了数据之后更新缓存
		self::getList();//后台缓存
		self::getList(false, true);//前台缓存
		self::getListMenu(2,false);//前台取两层数据
        return $bloResult;
    }

    /**
     * 根据分类ID获取其子分类ID包括自己 以字符串形式a
     * 此分类还可以是字符串  1,2,3这样的方式来
     * 获取子栏目ID列表,,用于删除的时候跟前台需要获取相关的分类的时候用得以
     * 取其底下的所有的1,2,3格式
     * @param type $catid 栏目id
     * @return string 返回栏目子列表，以逗号隔开
     */
    public static function getStrChildid($catid) {
        //栏目数据
        if(is_numeric($catid)){
        	return self::getStrChildidDetail($catid);
        }else{
        	$data = array();
        	$arr = explode(',', $catid);
        	foreach ($arr as $value) {
        		if($value){
        			$data[] = self::getStrChildidDetail($value);
        		}
        	}
        	$d = implode(',', $data);
        	return $d;
        }
    }

    /**
     * 获取子栏目ID列表,,用于删除的时候跟前台需要获取相关的分类的时候用得以
     * 取其底下的所有的1,2,3格式
     * @param type $catid 栏目id
     * @return string 返回栏目子列表，以逗号隔开
     */
    public static function getStrChildidDetail($catid) {
        //栏目数据
    	$arrDatas = self::getList();
        $arrchildid = $catid;
        foreach ($arrDatas as $id => $cat) {
            if ($cat['ParentID'] && $id != $catid && $cat['ParentID'] == $catid) {
                $arrchildid .= ',' . self::getStrChildidDetail($id);
            }
        }
        return $arrchildid;
    }


    /**
     * 以数据的方式呈现
     * 获取子栏目ID列表,,用于删除的时候跟前台需要获取相关的分类的时候用得以
     * 取其底下的所有的array(1,2,3)格式
     * @param type $catid 栏目id
     * @return string 返回栏目子列表，以逗号隔开
     */
    public static function getArrChildid($catid) {
        //栏目数据
    	$str = self::getStrChildid($catid);
    	return explode(',', $str);
    }

    public static function getArrChildidDetail($catid) {
        //栏目数据
    	$arr = self::getArrChildid($catid);
    	$arrResult = array();
    	foreach ($arr as $key => $value) {
    		$arrResult[$value] = self::getDataDetail($value);
    	}
    	return $arrResult;
    }

    /**
     * 根据catid取其单条信息
     * 可根据某字段来取
     * @param  [type] $intCatID [description]
     * @return [type]           [description]
     */
    public static function getDataDetail($intCatID, $strField = ''){
    	$arrData = self::getList();
    	if($strField){
    		return $arrData[$intCatID][$strField];
    	}
    	return $arrData[$intCatID];
    }

    /**
     * 取得所有的父ID为0的
     * 根据某个字段来取值。。。。
     * @return [type] [description]
     */
    public static function getParentList($strField = ''){
    	$arrResult = array();
    	$arrDatas = self::getList();
    	foreach ($arrDatas as $key => $arrData) {
    		if($arrData['ParentID'] == 0){
    			if($strField){
    				$arrResult[$key] = $arrData[$strField];
    			}else{
    				$arrResult[$key] = $arrData;
    			}
    		}
    	}
    	return $arrResult;
    }


    /**
     * 使用递归的方式，获取全部的父栏目id列表
     * @param type $catid 栏目ID
     * @param type $arrparentid 父目录ID
     * @param type $n 查找的层次
     * @return string 返回父栏目id列表，以逗号隔开
     */
    public static function getStrParentid($catid) {
    	$result = $catid;    	
        $datas = self::getList();
        $parent = $datas[$catid]['ParentID'];//当前栏目的父ID
        if($parent != 0){
        	$result .= ',' . self::getStrParentid($parent);
        }
        return $result;
    }

    /**
     * 使用递归的方式，获取全部的父栏目id列表
     * @param type $catid 栏目ID
     * @param type $arrparentid 父目录ID
     * @param type $n 查找的层次
     * @return string 返回父栏目id列表，以逗号隔开
     */
    public static function getArrParentid($catid) {
    	$data = self::getStrParentid($catid);
    	return explode(',', $data);
    }

    public static function getMianbao($catid){
    	$arrResult = array();
    	$datas = self::getArrParentid($catid);
    	$datas = array_reverse($datas);
    	foreach ($datas as $key => $data) {
    		$title = self::getDataDetail($data, 'Title');
    		$arrResult[$title] = array('/front/index/goods', 'cid' => $data);
    	}
    	return $arrResult;
    }
    //面包，并且把同级也弄进去。
    public static function getMiaobaoAddTong($catid){
    	$datas = self::getArrParentid($catid);//找到此ID的所有的父类
    	$datas = array_reverse($datas);//翻转顺序
    	$result = array();//返回结果
    	$catlist = self::getList();
    	foreach ($datas as $key => $data) {
    		$d = self::getDataDetail($data);   		
    		$result[$key]['CatID'] = $data;
    		if($d){
    			if($catid == $data){
    				$result['data'] = $d;
    			}
    			$result[$key]['Title'] = $d['Title'];
    		}    		
    		if($key == 0){
    			$result[$key]['data'] = array();
    			continue;
    		}
    		$c = CategoryModel::getListCatID($d['ParentID'],2);//找到此cid的所有同级。。
    		$result[$key]['data'] = $c['data'];
    		unset($result[$key]['data'][$data]);    		
    	}
    	return $result;
    }
}
