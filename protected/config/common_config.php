<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '妈咪街 mamijie',
    'defaultController' => 'front/index',
    'language' => 'zh_cn',
    'theme' => 'mmj',
    'preload' => array('log'),
    //设置别名
    'aliases' => array(
        'theme' => 'webroot.themes.mmj',
        'bootstrap' => 'ext.YiiBooster.widgets',
    ),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.SsoClient.*', //关联用户
    ),
    'modules' => array(
        'admin' => array(
            'class' => 'application.modules.admin.AdminModule',
        ),
        'front' => array(
            'class' => 'application.modules.front.FrontModule',
        ),
        'user' => array(
            'class' => 'application.modules.user.UserModule',
        ),
        'cron' => array(
            'class' => 'application.modules.cron.CronModule',
        ),
        'app' => array(
            'class' => 'application.modules.app.AppModule',
        ),
        'baike' => array(
            'class' => 'application.modules.baike.BaikeModule',
        ),
        'huodong' => array(
            'class' => 'application.modules.huodong.HuodongModule',
        ),
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            //'allowAutoLogin' => true,
            'class' => 'UserPassport',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'front/public/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
        'cache' => array(
            'class' => 'system.caching.CDummyCache',
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false, //隐藏index.php   
            'urlSuffix' => '.html', //后缀   
            'rules' => array(
                'articlelist-<cid:\d+>' => 'baike/index/articleList',
                'asklist-<cid:\d+>' => 'baike/index/askList',
                //'map' => 'front/public/sitemapXml',
                'baike' => 'baike/index/index',
                'huodong' => 'huodong/index/index',
                'qqlogin' => 'user/user/loginqq',
                'article-<id:\d+>' => 'baike/index/view',
                'ask-<id:\d+>' => 'baike/index/askDetail',
                'weekly-<id:\d+>' => 'baike/index/weekly',
                'banner' => 'front/goods/banner',
                'sitemap' => 'front/public/sitemap',
                'banner-<bid:\d+>' => 'front/goods/bannerDetail',
                'list-<cid:\d+>' => 'front/index/good',
                'yugao' => 'front/index/tomorrow',
                'ruzhu' => 'user/merchantsSettled/introduction',
                'shenqing' => 'user/merchantsSettled/create',
                'baoming' => 'user/baoming/create',
                'item-<gid:\d+>' => 'front/goods/detail',
                'tejia' => 'front/index/new',
                'http://appapi.mamijie.com<_q:.*>/*' => 'app<_q>',
                '/' => 'front/index/index',
                'tag-<tag:\w+>' => 'front/index/tag',
            ),
        ),
    /*        'clientScript' => array(
      'class' => 'application.extensions.EClientScript.EClientScript',
      'combineScriptFiles' => true, // 默认情况下，它被设置为true，则设置为true，如果你想合并的脚本文件
      'combineCssFiles' => true, // 默认情况下，它被设置为true，则设置为true，如果你想合并CSS文件
      'optimizeScriptFiles' => false, // @since: 1.1
      'optimizeCssFiles' => false, // @since: 1.1
      'optimizeInlineScript' => false, // @since: 1.6, This may case response slower
      'optimizeInlineCss' => false, // @since: 1.6, This may case response slower
      ), */
    ),
    //一些常用的配置。邮箱
    'params' => array(
        'adminEmail' => '2428552416@qq.com',
        'taoAPI' => array(
            'AppKey' => '21499153',
            'AppSecret' => '56590c6bcb55d8d7d101a4655f8c3493',
        ),
        //Authcode算法加密/解密密钥
        'authcode' => 'kD"221N>Udd~3cdB{f9WS~@qZt]LdmsCx:2E+z[!Tl/Vf&PM|0',
        //app客户端通信密钥
        'appKey' => array(
            'ios' => 'L7qQGvF4NtF3D5VFVP1yCI6tLkFIvtsfihwfn4U0nkUC2v8ivfUY6WNcHFg',
            'android' => '1wt0p2W0oaShev2v8iv3Eoym6gYlepykUGIfzb09BbLkFI5AisMsAclfUY6R',
        ),
        'sinaAPI' => array(
            'WB_AKEY' => "526570810",
            'WB_SKEY' => "c9d28a326bbfabf038352e7d8582b9b6",
            //'WB_CALLBACK_URL' => "http://cs.1zw.com/user/oauth/sina"
            'WB_CALLBACK_URL' => "http://www.mamijie.com/user/oauth/sina"
        ),
        'QQAPI' => array(
            'appid' => "101142447",
            'appkey' => "6f13b2dfe294c93b3c632875ba3a37ce",
            'callback' => "http://www.mamijie.com/user/oauth/qq",
            'scope' => "get_user_info,add_t,add_pic_t"
        ),
        'smtp' => array(
            'Host' => "smtp.qiye.163.com",
            'SMTPAuth' => true,
            'account' => array(
                /*                array('Username' => 'systems001@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems001@1zw.com',),
                  array('Username' => 'systems002@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems002@1zw.com',),
                  array('Username' => 'systems003@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems003@1zw.com',),
                  array('Username' => 'systems004@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems004@1zw.com',),
                  array('Username' => 'systems005@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems005@1zw.com',),
                  array('Username' => 'systems006@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems006@1zw.com',),
                  array('Username' => 'systems007@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems007@1zw.com',),
                  array('Username' => 'systems008@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems008@1zw.com',),
                  array('Username' => 'systems009@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems009@1zw.com',),
                  array('Username' => 'systems010@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems010@1zw.com',),
                  array('Username' => 'systems011@1zw.com', 'Password' => 'shpf1zw', 'From' => 'systems011@1zw.com',), */

                array('Username' => 'systems001@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems001@mamijie.com',),
                array('Username' => 'systems002@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems002@mamijie.com',),
                array('Username' => 'systems003@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems003@mamijie.com',),
                array('Username' => 'systems004@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems004@mamijie.com',),
                array('Username' => 'systems005@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems005@mamijie.com',),
                array('Username' => 'systems006@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems006@mamijie.com',),
                array('Username' => 'systems007@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems007@mamijie.com',),
                array('Username' => 'systems008@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems008@mamijie.com',),
                array('Username' => 'systems009@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems009@mamijie.com',),
                array('Username' => 'systems010@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems010@mamijie.com',),
                array('Username' => 'systems011@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems011@mamijie.com',),
                array('Username' => 'systems012@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems012@mamijie.com',),
                array('Username' => 'systems013@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems013@mamijie.com',),
                array('Username' => 'systems014@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems014@mamijie.com',),
                array('Username' => 'systems015@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems015@mamijie.com',),
                array('Username' => 'systems016@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems016@mamijie.com',),
                array('Username' => 'systems017@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems017@mamijie.com',),
                array('Username' => 'systems018@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems018@mamijie.com',),
                array('Username' => 'systems019@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems019@mamijie.com',),
                array('Username' => 'systems020@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems020@mamijie.com',),
                array('Username' => 'systems021@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems021@mamijie.com',),
                array('Username' => 'systems022@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems022@mamijie.com',),
                array('Username' => 'systems023@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems023@mamijie.com',),
                array('Username' => 'systems024@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems024@mamijie.com',),
                array('Username' => 'systems025@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems025@mamijie.com',),
                array('Username' => 'systems026@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems026@mamijie.com',),
                array('Username' => 'systems027@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems027@mamijie.com',),
                array('Username' => 'systems028@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems028@mamijie.com',),
                array('Username' => 'systems029@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems029@mamijie.com',),
                array('Username' => 'systems030@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems030@mamijie.com',),
                array('Username' => 'systems031@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems031@mamijie.com',),
                array('Username' => 'systems032@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems032@mamijie.com',),
                array('Username' => 'systems033@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems033@mamijie.com',),
                array('Username' => 'systems034@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems034@mamijie.com',),
                array('Username' => 'systems035@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems035@mamijie.com',),
                array('Username' => 'systems036@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems036@mamijie.com',),
                array('Username' => 'systems037@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems037@mamijie.com',),
                array('Username' => 'systems038@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems038@mamijie.com',),
                array('Username' => 'systems039@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems039@mamijie.com',),
                array('Username' => 'systems040@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems040@mamijie.com',),
                array('Username' => 'systems041@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems041@mamijie.com',),
                array('Username' => 'systems042@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems042@mamijie.com',),
                array('Username' => 'systems043@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems043@mamijie.com',),
                array('Username' => 'systems044@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems044@mamijie.com',),
                array('Username' => 'systems045@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems045@mamijie.com',),
                array('Username' => 'systems046@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems046@mamijie.com',),
                array('Username' => 'systems047@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems047@mamijie.com',),
                array('Username' => 'systems048@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems048@mamijie.com',),
                array('Username' => 'systems049@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems049@mamijie.com',),
                array('Username' => 'systems050@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems050@mamijie.com',),
                array('Username' => 'systems051@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems051@mamijie.com',),
                array('Username' => 'systems052@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems052@mamijie.com',),
                array('Username' => 'systems053@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems053@mamijie.com',),
                array('Username' => 'systems054@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems054@mamijie.com',),
                array('Username' => 'systems055@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems055@mamijie.com',),
                array('Username' => 'systems056@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems056@mamijie.com',),
                array('Username' => 'systems057@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems057@mamijie.com',),
                array('Username' => 'systems058@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems058@mamijie.com',),
                array('Username' => 'systems059@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems059@mamijie.com',),
                array('Username' => 'systems060@mamijie.com', 'Password' => 'shpfmmj1zw', 'From' => 'systems060@mamijie.com',),
            ),
        ),
        //Redis配置
        'RedisConfig' => array(
            //'host' => '10.168.82.94',
            'host' => '127.0.0.1',
            'port' => 6379,
        ),
        //服务费折扣 百分比
        'Discount' => 5,
        //保证金
        'Bond' => 0,
        'email' => require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'email.php'),
        'fileUrl' => 'http://file.mamijie.com/',
        'BaoZhengJing' => 2000,
    ),
);
