<!-- 用户个人中心 -->
<div class="user_center" style="">

	<?php include $this->getViewFile('//layouts/user/left') ?>


	<div class="user_right">

		
		<div class="user_optBox">
			<h3>报名列表</h3>
			<?php include $this->getViewFile('//layouts/user/question') ?>
			
			<table class="user_optTable">
				<tr>
					<th>宝贝名称</th>
					<th width="50">原价</th>
					<th width="50">活动价</th>
					<th>报名类目</th>
					<th>审核状态</th>
					<th width="88">报名时间</th>
					<th>操作</th>
				</tr>

				<?php  foreach($data['data'] as $GoodsBmID => $rs){ ?>
				<tr>
					<td><a target="_blank" href="<?php echo $rs->DetailUrl; ?>"><?php echo $rs->GoodsName ?></a></td>
					<td><?php echo $rs->Pprice ?></td>
					<td><?php echo $rs->PromoPrice ?></td>
					<td><?php echo CategoryModel::getDataDetail($rs->CatID, 'Title') ?></td>
					<td><?php  echo $rs->getStatusOperationHtml() ?></td>
					<td><?php  echo date('Y-m-d H:i:s', $rs->AddTime) ?></td>

					<td style="">
						<?php if($rs->Status == 0 && $rs->AdminID == 0){  ?>
							<a href="<?php echo $this->createUrl('update', array('id' => $rs->GoodsBmID))  ?>" class="user_btnAdd"><i></i>修改</a>
							<a href="<?php echo $this->createUrl('cancel', array('id' => $rs->GoodsBmID))  ?>" class="user_btnPay"><i></i>取消</a>
						<?php } ?>
						<?php if($rs->IsPay == 0 && $rs->Pattern == 1 && in_array($rs->Status, array(0,3))){  ?>
							<a href="<?php echo $this->createUrl('ordersn/goods', array('id' => $rs->GoodsBmID))  ?>" class="indiCenter-amend">支付</a>
						<?php } ?>

						<?php if($rs->IsPay == 1 && $rs->Pattern == 1 && !in_array($rs->Status, array(2,4))){  ?>
							<a href="<?php echo $this->createUrl('ordersn/goods', array('id' => $rs->GoodsBmID))  ?>" class="zhuijia indiCenter-amend">追加</a>
						<?php } ?>	

					</td>

				</tr>
				<?php }  ?>

			</table>
		</div>

	</div>
</div>



<script>
	$(function(){
		$(".zhuijia").click(function(e){
			e.preventDefault();
			var href = $(this).attr('href');
			tips = dialog({
				fixed: true,
				cancelDisplay: false,
				title:'追加个数',
				content:'<input type="text" name="num" value=""/>',
				ok:function(){
					var num = $("[name='num']").val();
					$.post(href,{num:num},function(data){
						if(data.status){
							alert('追加成功');
						}else{
							alert(data.info);
						}
					},'json');
				}
			}).showModal();

			return false;					
		})
	})
</script>