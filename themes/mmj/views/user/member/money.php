
<div class="user_center" style="">

	<?php include $this->getViewFile('//layouts/user/left') ?>


	<div class="user_right">
		<div class="user_optBox">
			<h3>我的账户</h3>
			<?php include $this->getViewFile('//layouts/user/question') ?>
			<table class="user_optTable">
				<tr>
					<th width="200">用户总额</th>
					<td><?php echo $userMoney[0] ?></td>
				</tr>
				<tr>
					<th>用户可用余额</th>
					<td><?php echo sprintf("%.2f", $userMoney[0] -  $userMoney[1]); ?></td>
				</tr>
				<tr>
					<th>用户冻结金额</th>
					<td><? echo sprintf("%.2f", $userMoney[1]); ?></td>
				</tr>
				<tr>
					<th>入驻待付款冻结</th>
					<td><a id="" href="<?php echo $this->createUrl('/user/ordersn/ruzhu') ?>"><? echo $ruzhuMoney; ?></a></td>
				</tr>
				<tr>
					<th>申请入驻服务费解冻</th>
					<td><a id="" href="<?php echo $this->createUrl('/user/ordersn/cannelRuzhu') ?>"><? echo $ruzhuMoney; ?></a></td>
				</tr>
			</table>
		</div>

	</div>
</div>