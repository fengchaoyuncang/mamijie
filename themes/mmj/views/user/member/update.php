
<div class="user_center">

	<?php include $this->getViewFile('//layouts/user/left') ?>


	<div class="user_right">
		<div class="user_optBox">

			<h3>修改密码</h3>
			<?php include $this->getViewFile('//layouts/user/question') ?>
			<?php $objForm = $this->beginWidget( 'CActiveForm', array('id' => 'SubmitForm')); ?>
			<dl>
				<dt>会员名:</dt>
				<dd><?php  echo $model->UserName; ?></dd>



				<dt>密码：</dt>
				<dd>
						<input name="Password" class="signUp-price-common signUp-input-common" id="Password" type="password">
						<span>不修改密码无需填写</span>					
				</dd>


				<dt>新密码：</dt>
				<dd>
<input name="newPassword" class="signUp-price-common signUp-input-common" id="newPassword" type="password">							
				</dd>


				<dt>确认新密码：</dt>
				<dd>
<input name="newPassword1" class="signUp-price-common signUp-input-common" id="newPassword1" type="password">							
				</dd>

				<dt><?php echo MemberModel::model()->getAttributeLabel( 'WangWang' ); ?>：</dt>
				<dd>
<?php  echo $objForm->textField($model, 'WangWang', array('name' => 'WangWang', 'class' => 'signUp-title signUp-input-common')) ?>							
				</dd>

				<dt><?php echo MemberModel::model()->getAttributeLabel( 'Nickname' ); ?>：</dt>
				<dd>
<?php  echo $objForm->textField($model, 'Nickname', array('name' => 'Nickname', 'class' => 'signUp-title signUp-input-common')) ?>							
				</dd>


				<dt>性别：</dt>
				<dd>

				<?php  echo $objForm->radioButtonList($model, 'Gender',  array('1' => '男', '2' => '女'), array('name' => 'Gender', 'class' => 'user_optRadio')) ?>
				</dd>

				<dt></dt>
				<dd>
					<button class="user_post" id="onlineButton">提交</button>
				</dd>
			</dl>			
			<?php $this->endWidget(); ?>
		</div>

	</div>
</div>

	<script src="<?php  echo yii::app()->theme->baseUrl ?>/assets/js/jquery.js"></script>
	<script src="<?php  echo yii::app()->theme->baseUrl ?>/assets/js/divselect.js"></script>
	<script>
		$.divselect("#divselect","#inputselect");
		var signUpBg = document.getElementById('signUpBg'),
			bg = document.getElementById('bg'),
			change = true;
	</script>

    <script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/statics/js/ajaxForm.js"></script> 
  <link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/statics/js/artDialog/skins/blue.css">
  <script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/statics/js/artDialog/artDialog.js"></script> 

  <script type="text/javascript">
	$(function(){
		var LOCK = false;

		$('#SubmitForm').ajaxForm({
			dataType:'json',		
			beforeSubmit:function(data, obj, es){
				if(LOCK){
					alert('正在处理');
					return false;
				}		
			},
			success:function(data){
				LOCK = false;
				if(data.status){
					alert('提交成功');	
					if(data.url){
						window.location.href = data.url;					
					}					
				}else{
					alert(data.info);					
				}
			}
		});

	})
  </script>
