
<div class="user_center" style="">

	<?php include $this->getViewFile('//layouts/user/left') ?>


	<div class="user_right">

		<div class="user_optBox">
			<h3>资金变动记录</h3>
			<?php include $this->getViewFile('//layouts/user/question') ?>
			
			<table class="user_optTable">
				<tr>

					<th>用户名</th>
					<th>金额</th>
					<th>备注</th>
					<th>类型</th>
					<th width="90">添加时间</th>
					<th>改变类型</th>

				</tr>
				<?php foreach($data  as $rs){  ?>
					<tr>
		
						<td><?php  echo $rs['UserName'];?></td>		
						<td><?php  echo $rs['Money'];?></td>		
						<td><?php  echo $rs['Remarks'];?></td>		
						<td><?php  echo MemberMoneyChangeModel::getTypeHtml($rs['Type']);?></td>	
						<td><?php echo date('Y-m-d', $rs['AddTime']); ?></td>


						<td><?php  echo MemberMoneyChangeModel::getChangeTypeHtml($rs['ChangeType']);?></td>	

					</tr>
				<?php } ?>
			</table>

			<div id="page">
					<?php  
						if(!empty($pages)){
                            $this->widget('CLinkPager', array(
                                'header' => "",
                                'firstPageLabel' => '首页',
                                'lastPageLabel' => '末页',
                                'prevPageLabel' => '上一页',
                                'nextPageLabel' => '下一页',
                                'pages' => $pages,
                                'footer' => "<span>共<em>{$pages->pageCount}</em>页</span>",
                                'maxButtonCount' => 20
                                    )
                            );										
						}



					 ?>				
			</div>
		</div>

	</div>
</div>

