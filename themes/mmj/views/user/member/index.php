<!-- 用户个人中心 -->
<div class="user_center" style="">

	<?php include $this->getViewFile('//layouts/user/left') ?>


	<div class="user_right">
		<div class="user_msgBox">
			<div class="user_msgList">
				<div class="user_msgTop">
					<div class="user_msgPhoto">
						<img width="100" height="100" src="<?php echo !is_array(yii::app()->user->isLogin()) ? yii::app()->theme->baseUrl . '/assets/images/user/photo.jpg' : yii::app()->user->oatthUserInfo['figureurl_qq_2'] ;  ?>">
					</div>
					<h3><span><?php echo !is_array(yii::app()->user->isLogin()) ? yii::app()->user->UserName : yii::app()->user->oatthUserInfo['nickname'] ;  ?></span>欢迎光临~</h3>
					<div class="user_safe">
						<?php
						  $grade = MemberModel::safeGrade();
						  switch ($grade) {
						  	case 0:
						  		$class = 'rank1';
						  		$html = '低等';
						  		break;
						  	case 1:
						  		$class = 'rank2';
						  		$html = '中等';
						  		break;
						  	case 2:
						  		$class = 'rank3';
						  		$html = '高等';
						  		break;
						  	
						  	default:
						  		$class = 'rank0';
						  		$html = '低等';
						  		break;
						  }
						?>
						账户安全级别：<i class="user_msgRank <?php echo $class; ?>"></i><span><?php echo $html; ?></span>
						<span class="user_sep">|</span>
						<i class="icon_common <?php if(!$phone = yii::app()->user->Phone){echo 'user_msgActive';}  ?>"></i><a href="#">绑定手机</a>
						<span class="user_sep">|</span>
						<i class="icon_common <?php if(!$phone = yii::app()->user->Email){echo 'user_msgActive';}  ?>"></i><a href="#">绑定邮箱</a>
					</div>
				</div>
			</div>
			<div class="user_msgList">
				<div class="user_msgHd">基础资料<!-- <a href="#"><i class="user_edit"></i>编辑</a> --></div>
				<div class="user_msgBd">
					<ul>
						<li>姓名：<span><?php echo !is_array(yii::app()->user->isLogin()) ? yii::app()->user->UserName : yii::app()->user->oatthUserInfo['nickname'] ;  ?></span></li>
						<li>生日：<span><?php  echo !is_array(yii::app()->user->isLogin()) ?  date('Y-m-d',yii::app()->user->Birthday) : yii::app()->user->oatthUserInfo['year'] ; ?></span></li>
						<li>积分：<span><?php  echo yii::app()->user->Score; ?></span></li>
						<li class="last">旺旺：<span><?php  echo yii::app()->user->WangWang; ?></span></li>
					</ul>
				</div>
			</div>
		</div>

	</div>
</div>