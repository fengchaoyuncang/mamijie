<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/indicenter.js"></script>


<!-- 用户个人中心 -->
<div class="user_center" style="">

	<?php include $this->getViewFile('//layouts/user/left') ?>


	<div class="user_right">
		<div class="user_optBox">
			<h3>我的消息</h3>
			<?php include $this->getViewFile('//layouts/user/question') ?>

			<div class="user_optBar">



				<div class="user_optB1">
					<span class="">
						<i class="user_iconCheck"></i>
					</span>
				</div>
				<a href="#" class="indi-center-delete user_optDel">删除</a>
				<a href="#" class="indi-center-read user_optRead">全部标为已读</a>
			</div>

			<ul class="user_optList">


				<?php  foreach ($data['data'] as $key => $rs) { ?>


				<li>
					<div class="user_optSel">
						<span data-id="<?php echo $rs['MessageID'] ?>" class="">
							<i class="user_iconCheck"></i>
						</span>
					</div>

					<div class="user_optImg">
						<img width="80" height="80" src="<?php echo $rs['Image'] ?>">
					</div>

					<div class="user_optTxt">
						<p><?php echo $rs['Title'] ?></p>
						<div class="user_optTime">
							<span class="user_o1"><?php echo date('Y-m-d H:i:s',$rs['AddTime']) ?></span>
							<a class="show" href="javascript:void(0)">查看详情</a>
							<input type="hidden" class="title" name="" value='<?php echo $rs['Title'] ?>'/>
							<input type="hidden" class="content" name="" value='<?php echo $rs['Content'] ?>'/>							
						</div>
					</div>
				</li>
				<?php } ?>




			</ul>
		</div>

	</div>
</div>



<script type="text/javascript">
	$(function(){

		$(".user_optB1").click(function(){
			var selected = $(this).find('span').eq(0).hasClass('selected');
			if(selected){
				$(".user_optSel > span").removeClass('selected');
				$(this).find('span').eq(0).removeClass('selected');
			}else{
				$(".user_optSel > span").addClass('selected');
				$(this).find('span').eq(0).addClass('selected');
			}
		})

		$(".user_optSel").click(function(){
			var selected = $(this).find('span').eq(0).hasClass('selected');
			if(selected){
				$(this).find('span').eq(0).removeClass('selected');
			}else{
				$(this).find('span').eq(0).addClass('selected');
			}
		})

		$(".show").click(function(){
			var title = $(this).next().val();
			var content = $(this).next().next().val();
			tips = dialog({
				fixed: true,
				cancelDisplay: false,
				title:title,
				content:content,
			}).showModal();				
		})

		LOCK = false;
		$(".indi-center-delete,.indi-center-read").click(function(){
			tips = dialog({
				fixed: true,
				cancelDisplay: false
			});					
			if(LOCK){
				tips.title('还有正在提交的');
				tips.showModal();
				return false;
			}else{
				LOCK = true;
				tips.title('正在提交');
			}
			var data = [];
			var obj = $("span[data-id].selected");
			length = obj.length;

			for(var i=0;i<length;i++){
				data[i] = obj.eq(i).data('id');
			}
			var type = '';
			if($(this).hasClass('indi-center-delete')){
				type = 1;
			}else{
				type = 2;
			}
			$.post('<?php echo $this->createUrl("updateMessage") ?>',{id:data,type:type},function(data){
				console.log(data);
				LOCK = false;
				if(data.status){
					tips.title('提交成功');
					window.location.reload();
				}else{
					tips.title('提交失败');
					tips.content(data.info);
				}				
			},'json');
					
		})



	})
</script>

