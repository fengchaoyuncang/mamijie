<div class="user_center">

	<?php include $this->getViewFile('//layouts/user/left') ?>


	<div class="user_right">

		<div class="user_optBox">
			<h3>提现记录</h3>
			<?php include $this->getViewFile('//layouts/user/question') ?>
			<table class="user_optTable">
				<tr>
					<th>提现金额</th>
					<th>处理状态</th>
					<th>提现时间</th>
					<th>处理订单</th>
					<th>备注</th>
				</tr>
				<?php foreach($data  as $rs){  ?>
				<tr>
					<td><?php  echo $rs['Money'];?></td>
					<td><?php  echo MemberWithdrawModel::getStatusHtml($rs['Status']);?></td>	
					<td style="width:158px"><?php echo date('Y-m-d H:i:s', $rs['AddTime']); ?></td>											
					<td><?php  echo $rs['Ordersn'];?></td>		
					<td><?php  echo $rs['Remark'];?></td>	
				</tr>
				<?php } ?>
			</table>
			<div id="page">
					<?php  
						if(!empty($pages)){
                            $this->widget('CLinkPager', array(
                                'header' => "",
                                'firstPageLabel' => '首页',
                                'lastPageLabel' => '末页',
                                'prevPageLabel' => '上一页',
                                'nextPageLabel' => '下一页',
                                'pages' => $pages,
                                'footer' => "<span>共<em>{$pages->pageCount}</em>页</span>",
                                'maxButtonCount' => 20
                                    )
                            );										
						}



					 ?>				
			</div>


		</div>

	</div>
</div>




