<!DOCTYPE html>
<html>
<head lang="cn">
    <meta charset="UTF-8">
    <title>修改密码</title>
    <link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/css/common.css">
    <script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/jquery-1.11.1.min.js"></script>
</head>
<body>
<div class="password-shadow">

    <div class="password-content">
        <div class="pw-title">
            <h4>修改密码</h4>
        </div>
        <div class="change-password" style="display: block;">
            <?php $objForm = $this->beginWidget( 'CActiveForm', array('id' => 'SubmitForm')); ?>
                <div class="login-account pw-acc">
                    <input type="password" name="password" class="login-acc pw-acc-p" placeholder="新密码"/>
                    <div class="error login-acc-text"></div>
                </div>
                <div class="login-codeForPhone pw-code">
                    <input type="password" name="repassword" class="login-forphone pw-code-p" placeholder="重复密码"/>
                    <div class="error login-acc-text"></div>
                </div>
                <div class="change-btn">
                    <div>确定更改密码</div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="change-success">
            <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/call_success.png" alt=""/>
            <p>您的密码已经重置成功</p>
            <div class="pw-form-sm-remail-btn change-success-btn">
                <a href="<?php echo $this->createUrl('/front/index/index') ?>">返回首页</a>
            </div>
        </div>
    </div>
</div>
<script>
    var bodyH = $(document).height();
    $(".password-shadow").height(bodyH);

    $(window).resize(function() {
        var bodyH = $(document).height();
        $(".password-shadow").height(bodyH);
    });

    $(".change-password input").keyup(function() {
        $(this).siblings(".error").hide();
    })    

    $(".change-btn").click(function() {
        var obj = {};
        obj.password = $(".password-content").find('[name="password"]').val();
        obj.repassword = $(".password-content").find('[name="repassword"]').val();
        $.post('<?php echo $this->createUrl("findPas", array("uid" => $uid, "code" => $code)) ?>', obj, function(data){
            if(data.status){
                $(".change-password").stop(true, true).animate({
                    "opacity" : "0"

                },400, function() {
                    $(".change-password").hide();
                    $(".password-content").stop(true, true).animate({
                        "height" : "342px",
                        "top" : "30%"
                    },400);
                    $(".change-success").show();
                    $(".pw-title h4").text("密码重置成功");
                })                         
            }else{
                $.each(data.errors, function(index, val) {
                     $(".password-content").find("[name='"+index+"']").next('.error').html(val[0]).show();
                });
            }
        }, 'json');        
    })
</script>
</body>
</html>