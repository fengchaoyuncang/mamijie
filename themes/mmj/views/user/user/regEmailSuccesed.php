
	<div class="register-container">
    <div class="register">
        <div class="register-logo">
            <a href="" title="妈咪街">
                <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/register_logo.png" alt="妈咪街" title="妈咪街">
            </a>
        </div>
        <div class="register-info">
            <div class="register-bottom-prompt">

                <!-- <div class="register-title">登录错误</div> -->
                <div class="register-common-mes"><?php echo $content; ?></div>
                <button class="register-commonBtn">
                	<a  target="_blank" href="http://mail.qq.com/">进入邮箱</a>
                	&nbsp; &nbsp;&nbsp;
                	<a  target="_blank" id="send" href="javascript:void(0)">重新发送</a>
                </button>

                <div class="register-bottom-bg"></div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/statics/js/ajaxForm.js"></script> 



<script type="text/javascript">
	var LOCK = false;
	$(function(){
		$("#send").click(function(e){
			e.preventDefault();
			tips = dialog({
				fixed: true,
				cancelDisplay: false
			});					
			if(LOCK){
				tips.title('正在处理');
				tips.showModal();
				return false;
			}else{
				LOCK = true;
				tips.title('正在提交');
				tips.showModal();
			}				
			$.post('<?php echo $this->createUrl("regEmailSuccesed", array("mail" => $mail)) ?>',{ajax:true},function(data){
				LOCK = false;
				tips.title('处理成功');
				tips.content(data.info);
			},'json');
		})
	})
</script>