<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>【妈咪街官网】母婴之家,母婴用品第一站,妈咪街做最好的母婴用品网站！</title>
<meta content="妈咪街,妈咪街官网,母婴之家,母婴用品网站,母婴用品,母婴之家官网" name="Keywords">
<meta content="妈咪街(mamijie.com)专业的母婴用品网上商城,母婴电商领导者.每天9点精选上百款母婴用品,1元秒杀、1折抢购等独家特价商品,做最好的母婴用品网站." name="Description">
<link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/css/dialogin.css">
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/new/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/new/user/login.js" type="text/javascript"></script>
</head>
<body>
<?php $objForm = $this->beginWidget( 'CActiveForm', array('id' => 'SubmitFormLogin')); ?>
	<div class="diaLogin_box">
		<div class="diaLogin_msg">
			<p class="diaLogin_error">请输入用户名</p>
		</div>
		<div class="diaLogin_list">
			<label>登录名：</label>
			<?php  echo $objForm->textField($model, 'UserName', array('placeholder' => '邮箱/手机号/昵称', 'class' => 'login_username login_input', 'autocomplete' => 'off'));  ?>
		</div>
		<div class="diaLogin_list">
			<label>登陆密码：</label>
			<?php  echo $objForm->passwordField($model, 'Password', array('placeholder' => '密码', 'class' => 'login_username login_input', 'autocomplete' => 'off'));  ?>
		</div>
		<div class="diabtn_list">
			<input type="submit" class="dia_login" value="登 录">
			<a target="_blank" href="<?php echo $this->createUrl('/user/user/register') ?>" class="dia_register" >立即注册</a>
		</div>
		<div class="diamsg_other">使用其他账户登录</div>
		<div class="login_way2">	
			<!-- <a href="#" class="login_way2_weibo"></a> -->
			<a target="_blank"  href="<?php echo $this->createUrl('/front/oauth/qq')  ?>" class="login_way2_qq"></a>
		</div>
	</div>
<?php $this->endWidget(); ?>
</body>
</html>