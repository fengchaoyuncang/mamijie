<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>【妈咪街官网】母婴之家,母婴用品第一站,妈咪街做最好的母婴用品网站！</title>
<meta content="妈咪街,妈咪街官网,母婴之家,母婴用品网站,母婴用品,母婴之家官网" name="Keywords">
<meta content="妈咪街(mamijie.com)专业的母婴用品网上商城,母婴电商领导者.每天9点精选上百款母婴用品,1元秒杀、1折抢购等独家特价商品,做最好的母婴用品网站." name="Description">
<link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/css/login.css">
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/new/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/new/user/login.js" type="text/javascript"></script>
</head>
<body>
<div class="login_box">
	<div class="login_head">
		<a href="/"><img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/register/login_logo.png" width="272" height="41"></a>
	</div>
	<div class="login_ct">
		<div class="login_left">
			<a href="#"><img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/register/login_pic.png" width="380" height="380"></a>
		</div>
		
		<div class="login_right">
			<?php $objForm = $this->beginWidget( 'CActiveForm', array('id' => 'SubmitFormLogin')); ?>
				<div class="register_msg">
					<h2><a href="<?php echo $this->createUrl('login')  ?>">登录</a>用户注册</h2>
						<div class="register_list">
							<?php  echo $objForm->textField($model, 'UserName', array('placeholder' => '用户名', 'class' => 'register_username login_input', 'autocomplete' => 'off'));  ?>
							<div class="error_msg"><?php echo $objForm->error($model,'UserName'); ?></div>							
						</div>
						<div class="register_list">
							<?php  echo $objForm->textField($model, 'EmailPhone', array('placeholder' => '手机/邮箱', 'class' => 'register_phone login_input', 'autocomplete' => 'off'));  ?>
							<div class="error_msg"><?php echo $objForm->error($model,'EmailPhone'); ?></div>							
						</div>
						<div class="register_list">
							<?php  echo $objForm->passwordField($model, 'Password', array('placeholder' => '密码', 'class' => 'register_password login_input', 'autocomplete' => 'off'));  ?>
							<div class="error_msg"><?php echo $objForm->error($model,'Password'); ?></div>								
						</div>
						<div class="register_list">
							<?php  echo $objForm->passwordField($model, 'Repassword', array('placeholder' => '确认密码', 'class' => 'register_Repassword login_input', 'autocomplete' => 'off'));  ?>
							<div class="error_msg"><?php echo $objForm->error($model,'Repassword'); ?></div>									
						</div>
						<div class="register_list">
							<?php  echo $objForm->textField($model, 'VerifyCode', array('placeholder' => '验证码', 'class' => 'register_VerifyCode login_input', 'autocomplete' => 'off'));  ?>
							<div class="VerifyCode_box"><?php $this->widget('CCaptcha',array('showRefreshButton'=>true,'clickableImage'=>true,'buttonLabel'=>''));  ?></div>							
							<div class="error_msg"><?php echo $objForm->error($model,'VerifyCode'); ?></div>									
						</div>
						<div class="register_list">
							<input type="submit" class="register_btn" value="立即注册">
						</div>
						<div class="register_opt">
							<div class="register_agree">
								<?php  echo $objForm->checkBox($model, 'Remember');  ?>
								
								<label>同意</label><a href="#">妈咪街服务条款</a>
							</div>
						</div>
						<div class="error_msg"><?php echo $objForm->error($model,'Remember'); ?></div>
				</div>
			<?php $this->endWidget(); ?>

<!-- 			<div class="login_fast">
				<p>无需注册，即可登录</p>
				<div class="login_way">
					<a href="#" class="way_qq"></a>
					<a href="#" class="way_weibo"></a>
				</div>
			</div> -->
		</div>
	</div>

	<div class="login_copyRight"> 
		<span>&copy;</span> 2014-2015 www.mamijie.com 版权所有 备案号：闽ICP备14015365号
	</div>
</div>
</body>
</html>