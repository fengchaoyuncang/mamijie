<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="qc:admins" content="164641171765151215637571147716" />
<title>【妈咪街官网】母婴之家,母婴用品第一站,妈咪街做最好的母婴用品网站！</title>
<meta content="妈咪街,妈咪街官网,母婴之家,母婴用品网站,母婴用品,母婴之家官网" name="Keywords">
<meta content="妈咪街(mamijie.com)专业的母婴用品网上商城,母婴电商领导者.每天9点精选上百款母婴用品,1元秒杀、1折抢购等独家特价商品,做最好的母婴用品网站." name="Description">
<link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/css/login.css">
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/new/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/new/user/login.js" type="text/javascript"></script>
</head>
<body>
<div class="login_box">
	<div class="login_head">
		<a href="/"><img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/register/login_logo.png" width="272" height="41"></a>
	</div>
	<div class="login_ct">
		<div class="login_left">
			<a href="#"><img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/register/login_pic.png" width="380" height="380"></a>
		</div>
		
		<div class="login_right">
			<div class="login_msg">
				<h2><a href="<?php echo $this->createUrl('register')  ?>">新用户注册</a>登录</h2>
				<?php $objForm = $this->beginWidget( 'CActiveForm', array('id' => 'SubmitFormLogin')); ?>

					<div class="login_list">
						<?php  echo $objForm->textField($model, 'UserName', array('placeholder' => '邮箱/手机号/昵称', 'class' => 'login_username login_input', 'autocomplete' => 'off'));  ?>
						<div class="error_msg"><?php echo $objForm->error($model,'UserName'); ?></div>							
					</div>

					<div class="login_list">
						<?php  echo $objForm->passwordField($model, 'Password', array('placeholder' => '密码', 'class' => 'login_username login_input', 'autocomplete' => 'off'));  ?>
						<div class="error_msg"><?php echo $objForm->error($model,'Password'); ?></div>							
					</div>

					<div class="login_list">
						<input type="submit" class="login_btn" value="登 录">
					</div>

					<div class="login_opt">
<!-- 						<div class="login_remember">
							<input type="checkbox"><label>记住密码</label>
						</div>
						<div class="login_forget">
							<a href="#">忘记密码</a>
						</div> -->
					</div>

				<?php $this->endWidget(); ?>

			</div>

			<div class="login_fast">
				<p>无需注册，即可登录</p>
				<div class="login_way">
					<a target="_blank" href="<?php echo $this->createUrl('/front/oauth/qq')  ?>" class="way_qq"><img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/register/Connect_logo_3.png"></a>
					<!-- <a href="#" class="way_weibo"></a> -->
				</div>
			</div>
		</div>
	</div>

	<div class="login_copyRight"> 
		<span>&copy;</span> 2014-2015 www.mamijie.com 版权所有 备案号：闽ICP备14015365号
	</div>
</div>
</body>
</html>