<div class="col-md-6 login_reg_page" id="login_reg_page" style="display:none">
	<div class="close" onclick="$('div.login_reg_page').hide()"></div>
	<div class="tab">
		<div class="tab-pane " data-click="false" id="login">
            <a href="javascript:void(0);" class="pink">
            	<i class="border-top "></i>
				请登录
			</a>
		</div>
		<div class="tab-pane gray" id="reg">
			<a href="javascript:void(0);" class="gray">
				<i class="border-top gray"></i>
	 			立即注册
	 		</a>
		</div>
	</div>
	<div class="login_page tab_content">
		<div class="login_content">
			<form id="login-form" method="post" action="/user/login/login.html">
				<div class="row icon user-group">
					<i class="login-spt"></i>
					<input class="input2" id="LoginForm_username" type="text" name="username" placeholder="请输入注册邮箱">
				</div>
				<div class="row icon password-group">
					<i class="login-spt"></i>
					<input class="input2" id="LoginForm_password" type="password" name="password" placeholder="请输入密码">
				</div>
				<div class="remember-me-row">
					<div class="remember-me pull-left">
						<input id="ytLoginForm_rememberMe" type="hidden" name="rememberMe" value="0">
						<input id="LoginForm_rememberMe" type="checkbox" value="1" name="rememberMe">
						<label for="LoginForm_rememberMe">记住登录状态</label>
					</div>
					<div class="pull-right">
						<a href="/user/user/findpassword.html" target="_blank">忘记密码</a>
					</div>
					<div class="clear"></div>
				</div>
				<div class="buttons row">
					<input class="login-spt login_btn" type="submit" value="" name="yt0">
				</div>
			</form>
		</div>
		<!--
		<div class="shortcut">
			<div class="content">你也可以用以下方式登录：</div>
			<div class="content">
				<a class="loginbtn" href="/user/oauth/qqreg.html">
					<img src="/themes/1zw/assets/images/loginbtn_qq.png">
				</a>
				<a class="loginbtn" href="/user/oauth/sinareg.html">
					<img src="/themes/1zw/assets/images/loginbtn_sina.png">
				</a>
			</div>
		</div>
		-->
	</div>
	
	<div class="reg_page tab_content"  style="display:none;">
		<div class="reg_content">
			<form id="login-regmobile" method="post" action="/user/user/regmobile.html">
				<div class="row icon user-group">
					<div class="lable">手机：</div>
					<input class="input" id="LoginForm_phone" type="text" name="MembersModel[username]" placeholder="请输入手机">
				</div>
<!-- 				<div class="row icon user-group">
					<div class="lable">验证码：</div>
					<input class="input1" id="LoginForm_username" type="text" name="snscode" placeholder="请输入验证码">
					<input id="snscode" type="button" style="width:120px;height:35px;padding:0;" value="免费获取验证码">
				</div> -->
                <div class="row icon user-group">
					<div class="lable">邮箱：</div>
					<input class="input" id="LoginForm_username" type="text" name="MembersModel[email]" placeholder="请输入注册邮箱">
				</div>
				<div class="row icon user-group">
					<div class="lable">密码：</div>
					<input class="input" id="LoginForm_username" type="password" name="MembersModel[password]" placeholder="请输入密码">
				</div>
				<div class="row icon user-group">
					<div class="lable">确认密码：</div>
					<input class="input" id="LoginForm_username" type="password" name="MembersModel[repassword]" placeholder="请重复密码">
				</div>
				<div class="row icon user-group">
					<div class="lable">旺旺：</div>
					<input class="input" id="LoginForm_username" type="text" name="MembersModel[wangwang]" placeholder="请输入旺旺">
				</div>
				<div class="row icon user-group">
					<div class="lable"></div>
                    <input id="ytMembersModel_agreement" type="hidden" value="0" name="MembersModel[agreement]">
					<input class="checkbox" id="LoginForm_username" type="checkbox" value="1" name="MembersModel[agreement]" >
					我已经阅读并同意妈咪街的《用户注册协议》
				</div>
				<div class="row icon user-group">
					<input class="btn reg-spt" type="submit" value="">
				</div>
			</form>
		</div>
	</div>
	
</div>

<script type="text/javascript">
//层切换
$('.tab-pane').click(function(){
	var id = $(this).attr('id');
	$('.tab-pane').attr('class','tab-pane gray');
	$(this).attr('class','tab-pane');
	$('.tab_content').hide();	//隐藏"登录,注册"层
	$('.'+id+"_page").show();	//显示当前层
	
	$('.tab-pane a').attr('class','gray');
	$(this).children("a").attr('class','pink');
	//border-top gray
	$('.tab-pane').children("a").children("i").attr('class','border-top gray');
	$(this).children("a").children("i").attr('class','border-top');
});

var wait = 60;	//60秒才能发送一次
if (getCookie("time") == null || getCookie("time") < 0){
	setCookie("time",wait,wait);
}else if(getCookie("time") != wait)
{
	timeRef();
}

//计时器
function time(o) {
	
		if(!o)
			var o = document.getElementById("snscode");
		
		if(getCookie("time") == wait){
			var moblie = $('#LoginForm_phone').val();
			$.getJSON('<?php echo YzwController::U('user/public/snscode') ?>',{moblie:moblie},function(data){
				if(data.status){
					
				}else{
					alert(data.info);
				}
			});
		}
	
		if (getCookie("time") == null || getCookie("time") <= 0) {
			o.removeAttribute("disabled");
			o.value="免费获取验证码";

		} else {

			timeRef();
			
		}
}
//刷新按钮
function timeRef(){
	if(!o)
		var o = document.getElementById("snscode");
	waittime = getCookie("time");
	o.setAttribute("disabled", true);
	o.value="重新发送(" + waittime + ")";
	waittime--;
	setCookie("time",waittime);
	setTimeout(function() {
		time(o)
	},
	1000)
}
//点击获取验证码
document.getElementById("snscode").onclick=function(){
	var moblie = $('#LoginForm_phone').val();
	if(moblie == ''){
		alert('手机号不能为空！');
		return false;
	}
	
  	if(!(/^1[3|4|5|7|8][0-9]\d{8}$/.test(moblie))){ 
        alert("手机号码格式不正确");  
        return false;
    }
  	setCookie("time",wait,wait);
	time(this);
}

//js设置Cookie
function setCookie(name,value,time)
{
    var exp = new Date();
    exp.setTime(exp.getTime() + time * 1000);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}
//js获取Cookie
function getCookie(name)
{
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
 
    if(arr=document.cookie.match(reg))
        return (arr[2]);
    else
        return null;
}

</script>
