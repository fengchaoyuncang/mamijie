<div class="container member-container">
    <?php include $this->getViewFile('../lib/leftmenu'); ?>
    <div class="col-md-10 member-con member-info-con">
        
        <div class="col-md-4 content">
        	<div class="title"><i class="left-red"></i>基本资料</div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'address-form',
            ));
            ?>
            <?php //echo $form->errorSummary($memberAddress); ?>
            <table>
                <tr>
                    <td class="head">用户名：</td>
                    <td width="200">
                    	<?php echo $form->hiddenField($user, 'username',array('name'=>'username','class'=>"update_input"));?>
                    	<div class="hidden_text"><?php echo $user->username ?></div>
                    </td>
                    <!--
                    <td>
                   		<a class="update" href="javascript::void(0);" onclick="">[ 修改 ]</a>
                   	</td>
                   	 -->
                </tr>

                <tr>
                    <td class="head">登录邮箱：</td>
                    <td width="200">
                    	<div class="hidden_text"><?php echo $user->email ?></div>
                    </td>
                </tr>
                <tr>
                    <td class="head">联系手机：</td>
                    <td >
                    <div id="update_phone">
	                    <?php if($memberInfo->phone){
							echo $memberInfo->phone.' ';
						}
						?>
						<input name="phone" type="hidden" value="<?php echo $memberInfo->phone?>">
					</div>
                    <!-- <div id="myphone" style="<?php echo $memberInfo->phone?'display:none':'' ?>"><input name="phone" style=" width:100px;" id="phone" type="text" value="<?php echo $memberInfo->phone?>"> 短信验证：<input name="snscode" style=" width:50px;" id="phone" type="text" value=""> <input type="button" id="snscode" value="免费获取验证码" style="width:120px;height: 35px;" /></div>-->
                   	</td>
<!--                    	<td width="100px">
                   		<a class="update" href="javascript:void(0);" >[ 修改 ]</a>
                   	</td> -->
                </tr>
<!--                 <tr id="phone_sns" style="display:none;">
                    <td class="head">短信验证：</td>
                    <td width="255"><input name="snscode" style=" width:50px;" id="phone" type="text" value=""><input type="button" id="snscode" value="免费获取验证码" style="width:120px;height: 35px;margin-left:5px;" /></td>
                </tr> -->
                
                
                <tr>
                    <td class="head">积分：</td>
                    <td width="255"><a href="<?php echo $this->createUrl('/user/score/index'); ?>"><?php echo $user->score; ?></a></td>
                </tr>
                <tr>
                    <td class="head">上次登录：</td>
                    <td><?php echo date('Y-m-d H:i:s', $user->lastlogintime); ?></td>
                </tr>
                <tr>
                    <td class="head">注册时间：</td>
                    <td><?php echo date('Y-m-d H:i:s', $user->regdate); ?></td> 
                </tr>
                <tr>
                    <td class="head">性别：</td>
                    <td>
                        <?php
                        echo Form::radio(array(
                            1 => '<span>男</span>',
                            2 => '<span>女</span>',
                                ), $user->sex, 'name="sex"','50')
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="head">生日：</td>
                    <td><?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'name' => 'Date', 'language' => 'zh_cn', 'value' => $user->year . '-' . $user->month . '-' . $user->day,
                            // additional javascript options for the date picker plugin            
                            'options' => array(
                                'showAnim' => 'fold',
                                'dateFormat' => 'yy-mm-dd',
                                'showOn' => 'focus',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => '-80:+0',
                            ),
                            'htmlOptions' => array(
                                'style' => 'height:30px;',
                            ),
                        ));
                        ?></td>
                </tr>   
                <tr>
                    <td class="head"></td>
                    <td width=""><?php
                        echo CHtml::submitButton('保 存', array('class' => "save_btn"));
                        ?></td>
                    <td></td>
                </tr>                 
            </table> 
            <?php $this->endWidget(); ?>
        </div>
        
        
        <div class="content invite ">
        	<div class="invite_title">
        		<div class="invite_top">
        			<i class="left-red"></i>邀请好友注册:
	        	</div>
	        	<div class="invite_middle">
	        		<input id="invite_url" type="text" readonly value="<?php echo Yii::app()->request->hostInfo.Yii::app()->createUrl('user/user/register', array('invite' => $user->uid)) ?>">
	        		<span id="invite_btn" >复 制</span>
	        	</div>
	        	<div class="invite_bottom">
	        		<font color="#000" >注：</font>邀请好友注册成功，即可获得<font color="#dd004e" > 50 </font>积分奖励
	        	</div>
        	</div>
        	
        	<div style="height:360px;overflow-y:auto" class="invite_record">
        		<div class="invite_top">
        			<i class="left-red"></i>已接受邀请的好友:
	        	</div>
	        	<table>
	        		<thead>
	        			<tr>
	        				<th>注册时间</th>
	        				<th>好友</th>
	        				<th>是否激活</th>
	        			</tr>
	        		</thead>
	        		<tbody>
	        			<?php
	        			$i = 1; 
	        			foreach ($data as $row){
	        				$odd_even = $i % 2 == 0 ? 'even' : 'odd';
	        				$status = $row->status ? '是' : '否';
	        				echo '
	        				<tr class="'.$odd_even.'">
		        				<td>'.date("Y-m-d H:i:s",$row->time).'</td>
		        				<td>'.$row->invite_username.'</td>
		        				<td>'.$status.'</td>
	        				</tr>';
	        				$i++;
	        			}
	        			?>
	        			<tr class="<?php echo $i % 2 == 0 ? 'even' : 'odd';?>">
	        				<td colspan="3">
	        					<!--
		        				<div class="invite_page">
					        		<a>1</a>
					        		<a class="current">2</a>
					        		<a>...</a>
					        		<a>4</a>
					        		<a>5</a>
					        		<a>></a>
					        	</div>
					        	-->
					        	<div class="invite_page">
					        		<?php echo $page;?>
					        	</div>
				        	</td>
	        			</tr>
	        		</tbody>
	        	</table>
	        	
        	</div>
		</div>
        
        
    </div>
    <div class="clearfix"></div>
</div>

<script type="text/javascript">
//var wait=60;
//function time(o) {
//		if(wait == 60){
//			var moblie = $('input[name="phone"]').val();
//			$.getJSON('<?php echo YzwController::U('Public/snscode') ?>',{moblie:moblie},function(data){
//				if(data.status){
//					
//				}else{
//					alert(data.info);
//				}
//			});
//		}
//		if (wait == 0) {
//			o.removeAttribute("disabled");			
//			o.value="免费获取验证码";
//			wait = 60;
//		} else {
//			o.setAttribute("disabled", true);
//			o.value="重新发送(" + wait + ")";
//			wait--;
//			setTimeout(function() {
//				time(o)
//			},
//			1000)
//		}
//	}
//document.getElementById("snscode").onclick=function(){
//	var moblie = $('input[name="phone"]').val();
//	if(moblie == ''){
//		alert('手机号不能为空！');
//		return false;
//	}
//	time(this);
//}


var wait = 60;	//60秒才能发送一次
if (getCookie("time") == null || getCookie("time") < 0){
	setCookie("time",wait,wait);
}else if(getCookie("time") != wait)
{
	timeRef();
}

//计时器
function time(o) {
	
		if(!o)
			var o = document.getElementById("snscode");
		
		if(getCookie("time") == wait){
			var moblie = $('input[name="phone"]').val();
			$.getJSON('<?php echo YzwController::U('Public/snscode') ?>',{moblie:moblie},function(data){
				if(data.status){
					
				}else{
					alert(data.info);
				}
			});
		}
	
		if (getCookie("time") == null || getCookie("time") <= 0) {
			o.removeAttribute("disabled");
			o.value="免费获取验证码";

		} else {

			timeRef();
			
		}
}
//刷新按钮
function timeRef(){
	if(!o)
		var o = document.getElementById("snscode");
	waittime = getCookie("time");
	o.setAttribute("disabled", true);
	o.value="重新发送(" + waittime + ")";
	waittime--;
	setCookie("time",waittime);
	setTimeout(function() {
		time(o)
	},
	1000)
}
//点击获取验证码
document.getElementById("snscode").onclick=function(){
	var moblie = $('input[name="phone"]').val();
	if(moblie == ''){
		alert('手机号不能为空！');
		return false;
	}
  	if(!(/^1[3|4|5|7|8][0-9]\d{8}$/.test(moblie))){ 
        alert("手机号码格式不正确");  
        return false; 
    }
  	setCookie("time",wait,wait);
	time(this);
}

//js设置Cookie
function setCookie(name,value,time)
{
    var exp = new Date();
    exp.setTime(exp.getTime() + time * 1000);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}
//js获取Cookie
function getCookie(name)
{
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
 
    if(arr=document.cookie.match(reg))
        return (arr[2]);
    else
        return null;
}

$('.update').click(function(){
	var display = $('#phone_sns').css('display');
	if(display == "none"){
		$('#update_phone').html("<input name='phone' id='phone' type='text' value='<?php echo $memberInfo->phone?>'>");
		$('#phone_sns').css('display','');
	}else{
		$('#update_phone').html('<?php echo $memberInfo->phone?> <input name="phone" type="hidden" value="<?php echo $memberInfo->phone?>">');
		$('#phone_sns').css('display','none');
	}
	
});


//
//var clipboardswfdata;
//	 
//var setcopy_gettext = function(){
//	clipboardswfdata = document.getElementById('invite_url').value;
//	//alert(clipboardswfdata);
//	window.document.clipboardswf.SetVariable('str', clipboardswfdata);
//}
//
//var floatwin = function(){
//	alert('copy success, ' + clipboardswfdata);
//}

$('#invite_btn').click(function(){
	//alert(clipboardswfdata);
	//$('#invite_url').val().clone();
	//alert($('#invite_url').val());
	
});

//$(document).ready(function(){
//    $("#invite_btn").zclip({
//        path:'<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/zclip/ZeroClipboard.swf',
//        copy:function(){
//            return $('#invite_url').val();
//        }
//        //copy:$('#invite_url').val()
//    });
//});



</script>