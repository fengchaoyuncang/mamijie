<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl ?>/assets/js/plupload-2.1.2/js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl ?>/assets/js/plupload-2.1.2/js/common.js"></script>
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/statics/js/ajaxForm.js"></script> 

<style type="text/css">
	.form-group label{
		font-size: 12px;
	}
	.form-group input{
		font-size: 12px;
	}
	.form-group span{
		font-size: 12px;
	}

</style>

<div class="container merchantssettled">
	<!-- <div class="process_img">
		<img src="">
	</div> -->
	<div class="merchantssettled_form">
		<div class="title"><?php  if(ACTION_NAME == 'create'){echo '企业信息填写';}else{echo '企业信息修改';}  ?></div>
		
		<?php $objForm = $this->beginWidget('bootstrap.TbActiveForm',array('id'=>'SubmitForm',)); ?>	
		
		<?php //echo $objForm->errorSummary($model); ?>


		<?php  echo $objForm->textFieldGroup($model, 'CompanyName', array('append' => '请确保填写正确，与公司营业执照公司名完全一致','appendOptions' => array('class' => 'block')));   ?>
		<?php  echo $objForm->textFieldGroup($model, 'RegistrationNo');   ?>
		
		<div class="form-group">
			<label class="control-label required" for="MerchantsSettledModel_StartTime">营业执照有效期 <span class="required">*</span></label>
			<div class="input_form">
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'name'=>'MerchantsSettledModel[StartTime]',
                    'value' => empty($model->StartTime) ? '' : date('Y-m-d H:i:s', $model->StartTime),
                    'language'=>'zh-CN', 
                    'options'=>array(  
                        'showAnim'=>'slide',      
                        'showButtonPanel'=>true,
                        'dateFormat'=>'yy-mm-dd',
                    ),
                    'htmlOptions'=>array(
                         'class'=>'input_1',
                         'placeholder' => '开始时间',
                        ),

                ));
                ?>				
				-
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'name'=>'MerchantsSettledModel[EndTime]', 
                    'value' => empty($model->EndTime) ? '' : date('Y-m-d H:i:s', $model->EndTime),
                    'language'=>'zh-CN', 
                    'options'=>array(  
                        'showAnim'=>'slide',      
                        'showButtonPanel'=>true,
                        'dateFormat'=>'yy-mm-dd',
                    ),
                    'htmlOptions'=>array(
                         'class'=>'input_1',
                         'placeholder' => '结束时间',
                        ),

                ));
                ?>		
				<?php echo $objForm->error($model, 'StartTime')  ?>
				<?php echo $objForm->error($model, 'EndTime')  ?>
			</div>		
		</div>

		<?php  echo $objForm->textFieldGroup($model, 'Location');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'RegisteredCapital', array('append' => '单位(万元)'));   ?>
		<?php  echo $objForm->textAreaGroup($model, 'ScopeBusiness', array('append' => '提示：只接受童装、童鞋、玩具、婴童用品、孕妇装等。', 'appendOptions' => array('class' => 'block')));   ?>
		<?php  echo $objForm->textFieldGroup($model, 'FullName');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'Address');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'MobilePhone');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'FaxNumber');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'CompanyUrl');   ?>
		<?php  echo $objForm->textAreaGroup($model, 'BrandAgent');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'TaobaoUrl');   ?>


		<div class="title">资料上传<span>图片尺寸需小于1280px * 1280px</span></div>
		<?php  $url =  ConfigModel::model()->getConfig('sitefileurl'); ?>


		<?php if(!empty($model->BusinessLicenseImg)){  
				$html = '<div class="div_img"><img  src="'.$url . $model->BusinessLicenseImg.'"><input type="hidden" value="'.$model->BusinessLicenseImg.'" name="MerchantsSettledModel[BusinessLicenseImg]"><div data-id="MerchantsSettledModel_BusinessLicenseImg" data-filename="o_1984uul1c8v1fh6pu71n0rhsar" data-path="'.$model->BusinessLicenseImg.'" class="uploader-close update"></div></div>';
			}else{
				$html = '';
			}
		?>
		<?php  echo $objForm->fileFieldGroup($model, 'BusinessLicenseImg', array('append' => $html, 'appendOptions' => array('isRaw' => true)));   ?>



		<?php if(!empty($model->OrganizationImg)){  
				$html = '<div class="div_img"><img  src="'.$url . $model->OrganizationImg.'"><input type="hidden" value="'.$model->OrganizationImg.'" name="MerchantsSettledModel[OrganizationImg]"><div data-id="MerchantsSettledModel_OrganizationImg" data-filename="o_1984uul1c8v1fh6pu71n0rhsar" data-path="'.$model->OrganizationImg.'" class="uploader-close update"></div></div>';
			}else{
				$html = '';
			}
		?>
		<?php  echo $objForm->fileFieldGroup($model, 'OrganizationImg', array('append' => $html, 'appendOptions' => array('isRaw' => true)));   ?>



		<?php if(!empty($model->TaxRegistrationImg)){  
				$html = '<div class="div_img"><img  src="'.$url . $model->TaxRegistrationImg.'"><input type="hidden" value="'.$model->TaxRegistrationImg.'" name="MerchantsSettledModel[TaxRegistrationImg]"><div data-id="MerchantsSettledModel_TaxRegistrationImg" data-filename="o_1984uul1c8v1fh6pu71n0rhsar" data-path="'.$model->TaxRegistrationImg.'" class="uploader-close update"></div></div>';
			}else{
				$html = '';
			}
		?>		
		<?php  echo $objForm->fileFieldGroup($model, 'TaxRegistrationImg', array('append' => $html, 'appendOptions' => array('isRaw' => true)));   ?>



		<?php if(!empty($model->PositiveIDImg)){  
				$html = '<div class="div_img"><img  src="'.$url . $model->PositiveIDImg.'"><input type="hidden" value="'.$model->PositiveIDImg.'" name="MerchantsSettledModel[PositiveIDImg]"><div data-id="MerchantsSettledModel_PositiveIDImg" data-filename="o_1984uul1c8v1fh6pu71n0rhsar" data-path="'.$model->PositiveIDImg.'" class="uploader-close update"></div></div>';
			}else{
				$html = '';
			}
		?>		
		<?php  echo $objForm->fileFieldGroup($model, 'PositiveIDImg', array('append' => $html, 'appendOptions' => array('isRaw' => true)));   ?>




		<?php if(!empty($model->ReverseIDImg)){  
				$html = '<div class="div_img"><img  src="'.$url . $model->ReverseIDImg.'"><input type="hidden" value="'.$model->ReverseIDImg.'" name="MerchantsSettledModel[ReverseIDImg]"><div data-id="MerchantsSettledModel_ReverseIDImg" data-filename="o_1984uul1c8v1fh6pu71n0rhsar" data-path="'.$model->ReverseIDImg.'" class="uploader-close update"></div></div>';
			}else{
				$html = '';
			}
		?>		
		<?php  echo $objForm->fileFieldGroup($model, 'ReverseIDImg', array('append' => $html, 'appendOptions' => array('isRaw' => true)));   ?>



		<?php if(!empty($model->TrademarkImg)){  
				$html = '<div class="div_img"><img  src="'.$url . $model->TrademarkImg.'"><input type="hidden" value="'.$model->TrademarkImg.'" name="MerchantsSettledModel[TrademarkImg]"><div data-id="MerchantsSettledModel_TrademarkImg" data-filename="o_1984uul1c8v1fh6pu71n0rhsar" data-path="'.$model->TrademarkImg.'" class="uploader-close update"></div></div>';
			}else{
				$html = '';
			}
		?>		
		<?php  echo $objForm->fileFieldGroup($model, 'TrademarkImg', array('append' => $html, 'appendOptions' => array('isRaw' => true)));   ?>


		<?php if(!empty($model->BrandAuthorizationImg)){  
				$html = '<div class="div_img"><img  src="'.$url . $model->BrandAuthorizationImg.'"><input type="hidden" value="'.$model->BrandAuthorizationImg.'" name="MerchantsSettledModel[BrandAuthorizationImg]"><div data-id="MerchantsSettledModel_BrandAuthorizationImg" data-filename="o_1984uul1c8v1fh6pu71n0rhsar" data-path="'.$model->BrandAuthorizationImg.'" class="uploader-close update"></div></div>';
			}else{
				$html = '';
			}
		?>		
		<?php  echo $objForm->fileFieldGroup($model, 'BrandAuthorizationImg', array('append' => $html, 'appendOptions' => array('isRaw' => true)));   ?>


		<?php if(!empty($model->Certification3cImg)){  
				$html = '<div class="div_img"><img  src="'.$url . $model->Certification3cImg.'"><input type="hidden" value="'.$model->Certification3cImg.'" name="MerchantsSettledModel[Certification3cImg]"><div data-id="MerchantsSettledModel_Certification3cImg" data-filename="o_1984uul1c8v1fh6pu71n0rhsar" data-path="'.$model->Certification3cImg.'" class="uploader-close update"></div></div>';
			}else{
				$html = '';
			}
		?>		
		<?php  echo $objForm->fileFieldGroup($model, 'Certification3cImg', array('append' => $html, 'appendOptions' => array('isRaw' => true)));   ?>



		<!-- 增加一个多图片上传功能 -->
		<?php  $datas =  $model->getImgs(); ?>
		<?php if(!empty($datas)){
				$html = '';
				foreach($datas as $data){
					$html .= '<div class="div_img"><img  src="'.$url . $data['Img'].'"><input type="hidden" value="'.$data['Img'].'" name="MerchantsSettledModel[Imgs][]"><div data-id="MerchantsSettledModel_Imgs" data-filename="o_1984uul1c8v1fh6pu71n0rhsar" data-path="'.$data['Img'].'" class="uploader-close update"></div></div>';
				}  
			}else{
				$html = '';
			}
		?>		
		<?php  echo $objForm->fileFieldGroup($model, 'Imgs', array('append' => $html, 'appendOptions' => array('isRaw' => true)));   ?>




		<div class="title">业务负责人信息</div>
		<p>该负责人将是妈咪街业务沟通的首要对接人，请保证联系信息真实有效！</p>

		<?php  echo $objForm->textFieldGroup($model, 'HeadName');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'HeadPosition');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'HeadCardID');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'HeadMobilePhone');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'HeadQQ');   ?>
		<?php  echo $objForm->textFieldGroup($model, 'HeadEmail');   ?>


		<div class="form-group button" style="position: relative;">
			<button>提交信息</button>
		
			<input name="agree" type="checkbox" class="form-agree" disabled="disabled">
			
			<span id="mamijie-agreement" style="position: absolute;color:#666666;font-size: 14px;top:-25px;left:-17px;padding-left:17px;text-decoration:underline;cursor:pointer;">妈咪街商家合作协议</span>
		</div>

		<div class="bar_message">
			<div class="contact">
	            <div class="by-qq">
	                <span>咨询QQ：</span>
					<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin=1206666868&amp;site=qq&amp;menu=yes" class="qq" rel="nofollow" _orighref="http://wpa.qq.com/msgrd?v=3&amp;uin=1206666868&amp;site=qq&amp;menu=yes" _tkworked="true">
        			<img src="<?php echo yii::app()->theme->baseUrl ?>/assets/images/qq-socre.gif">
                        </a>	            </div>
	            <div class="by-tel" style="padding-top: 10px">
	                招商旺旺：<a href="http://amos.im.alisoft.com/msg.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid=厦门妈咪街" target="_blank"><img src="http://amos.im.alisoft.com/online.aw?v=2&amp;site=cntaobao&amp;s=2&amp;charset=utf-8&amp;uid=厦门妈咪街" alt=""></a>	            	

	            </div>
	        </div>


			<div class="tips">
	            <div class="tips-info">
	                入驻提示:
	            </div>
	            <ul>
	                <li>1.<span>妈咪街目前不接受个体户入驻</span></li>
	                <li>2.<span>妈咪街仅接受母婴类商家入驻</span></li>
	                <li>3.<span>商家入驻申请信息要求完整性和准确性</span></li>
	                <li>4.<span>商家提供的所有相关资质必须在有效期内</span></li>
	                <li>5.<span>商家在申请入驻获得批准时需一次性缴纳保证金</span></li>
	                <li>6.<span>各类目入驻保证金不同，请详询妈咪街招商人员</span></li>
	                <li>7.<span>商家入驻申请提交后，招商人员将于3个工作日内联系您</span></li>
	            </ul>
        	</div>	        			
		</div>


	</div>
	<div class="agreement-shadow">
		<div class="agreement-shadow-box">
			
		</div>
		<div class="agreement-shadow-overflow">
			<div class="agreement-shadow-overflow_p">
				<?php include $this->getViewFile('tiaokuang');  ?>
			</div>
			<span class="agreement-con">接受</span>
		</div>
	</div>

	<?php $this->endWidget(); ?>
</div>


<script type="text/javascript">


  /*定义图片处理的一些全局变量 路径等*/
  var HostUrl = '<?php echo $url;   ?>/';
  var savePath = 'shangjiaruzhu/';
  var deleteUrl = '<?php echo $this->createUrl("/front/public/deleteImg") ?>';
  var uploadUrl = '<?php echo $this->createUrl("/front/public/upload") ?>';  /*上传图片URL*/
  var flash_swf_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.swf';;
  var silverlight_xap_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.xap';


    //第一个参数，是表示哪个ID来弄这个上传的,第二个参数是表示这个接收的name是什么，第三个参数是保存的路径，第四个参数是是否支持多上传
	saveImg('MerchantsSettledModel_BusinessLicenseImg', 'MerchantsSettledModel[BusinessLicenseImg]', savePath, false);
	saveImg('MerchantsSettledModel_OrganizationImg', 'MerchantsSettledModel[OrganizationImg]', savePath, false);
	saveImg('MerchantsSettledModel_TaxRegistrationImg', 'MerchantsSettledModel[TaxRegistrationImg]', savePath, false);
	saveImg('MerchantsSettledModel_PositiveIDImg', 'MerchantsSettledModel[PositiveIDImg]', savePath, false);
	saveImg('MerchantsSettledModel_ReverseIDImg', 'MerchantsSettledModel[ReverseIDImg]', savePath, false);
	saveImg('MerchantsSettledModel_TrademarkImg', 'MerchantsSettledModel[TrademarkImg]', savePath, false);
	saveImg('MerchantsSettledModel_BrandAuthorizationImg', 'MerchantsSettledModel[BrandAuthorizationImg]', savePath, false);
	saveImg('MerchantsSettledModel_Certification3cImg', 'MerchantsSettledModel[Certification3cImg]', savePath, false);

	saveImg('MerchantsSettledModel_Imgs', 'MerchantsSettledModel[Imgs][]', savePath, true);


	var LOCK = false;
	$('#SubmitForm').ajaxForm({
		dataType:'json',		
		beforeSubmit:function(data, obj, es){
			tips = dialog({
				fixed: true,
				cancelDisplay: false
			});					
			if(LOCK){
				tips.title('还有正在提交的');
				tips.showModal();
				return false;
			}else{

				LOCK = true;
				tips.title('正在提交');
				tips.showModal();
				return true;
			}			
		},
		success:function(data){
			LOCK = false;
			if(data.status){
				tips.title('提交成功');
				if(data.url){
					window.location.href = data.url;					
				}
			}else{
				tips.title('提交失败');
				tips.content(data.info);
			}
		}
	});	

	var bodyHeight = $(window).height();
	
	$(".agreement-shadow-box").css({
		"height" : bodyHeight
	});
	$(window).resize(function(){
		var bodyHeight = $(window).height();
		$(".agreement-shadow-box").css({
			"height" : bodyHeight
		});
	})

	$("#mamijie-agreement").click(function() {
		$(".agreement-shadow-box").show();
		$(".agreement-shadow-overflow").show();
	})

	$(".agreement-con").click(function() {
		$(".agreement-shadow-box").hide();
		$(".agreement-shadow-overflow").hide();
		$(".form-agree").prop("checked",true);
	})

	
</script>