    <script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/jquery.mousewheel.min.js"></script>
   
    <div class="introduction-box">
        <div class="enter-dip">
            <div class="enter-wrapper">
                <div class="enter-box">
                    <div class="enter-cream"></div>
                    <div class="enter-blue"></div>
                    <div class="enter-red"></div>
                    <div class="enter-pink">
                    </div>
                </div>
            </div>
            <div class="enter-pink-box">
                <div class="enter-pink-shadow">
                    <div class="enter-common">
                        <div class="enter-common1"></div>
                        <div class="enter-common2"></div>
                        <a href="<?php echo $this->createUrl('create') ?>" title="" class="enter-common3">申请入驻</a>
                    </div>
                    <!-- 放置导航位置 -->
                    <ul  class="enter-nav">
                        <li id="enter-nav"></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    <!-- page1 -->
                    <div class="enter-cream1-1"></div>
                    <div class="enter-cream1"></div>
                    <div class="enter-cream2"></div>
                    <div class="enter-cream3"></div>
                    <div class="enter-cream4"></div>
                    <!-- page2 -->
                    <div class="enter-blue1"></div>
                    <div class="enter-blue2"></div>
                    <div class="enter-blue3"></div>
                    <!-- page3 -->
                    <div class="enter-red1-1"></div>
                    <div class="enter-red1"></div>
                    <div class="enter-red2"></div>
                    <div class="enter-red3"></div>
                    <div class="enter-red4"></div>
                    <!-- page4 -->
                    <div class="enter-pink1"></div>
                    <div class="enter-pink2"></div>
                    <div class="enter-pink3"></div>
                    <div class="enter-pink7-text"></div>
                </div>
            </div>
        </div>
    </div>
     <script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/enter.js"></script>
</body>
</html>