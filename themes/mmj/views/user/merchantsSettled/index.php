<!-- 用户个人中心 -->
<div class="user_center" style="">

	<?php include $this->getViewFile('//layouts/user/left') ?>


	<div class="user_right">
		<div class="user_optBox">

			<h3>我的入驻</h3>
			<?php include $this->getViewFile('//layouts/user/question') ?>
			
			<table class="user_optTable">
				<tr>
					<th width="200">公司名称</th>
					<td><?php echo $data->CompanyName;  ?></td>
				</tr>
				<tr>
					<th>营业执照注册号</th>
					<td><?php echo $data->RegistrationNo;  ?></td>
				</tr>
				<tr>
					<th>营业执照所在地</th>
					<td><?php echo $data->Location ?></td>
				</tr>
				<tr>
					<th>法人姓名</th>
					<td><?php echo $data->FullName ?></td>
				</tr>
				<tr>
					<th>联系地址</th>
					<td><?php echo $data->Address ?></td>
				</tr>
				<tr>
					<th>联系电话</th>
					<td><?php echo $data->MobilePhone ?></td>
				</tr>
				<tr>
					<th>添加时间</th>
					<td><?php  echo date('Y-m-d H:i:s', $data->AddTime) ?></td>
				</tr>
				<tr>
					<th>审核状态</th>
					<td><?php echo MerchantsSettledModel::getStatusHtml($data->Status) ?></td>
				</tr>
			</table>
			<div class="user_optArray">
				<?php if($data->Status == 0){  ?>
					<a href="<?php echo $this->createUrl('update', array('id' => $data->MerchantsSettledID))  ?>">修改</a>
				<?php } ?>
				<?php if($data->Status == 0){  ?>
					<a href="javascript:void(0)" class="indiCenter-amend">查看拒绝理由</a>
					<input type="hidden" value="<?php echo $data->Cause; ?>"/>
				<?php } ?>
				<?php if($data->Status == 0){  ?>
					<a  href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/download/妈咪街商家入驻协议.docx">下载合同</a>
				<?php } ?>	
			</div>
		</div>

	</div>
</div>


<script>
	$(function(){
		$(".cause").click(function(){
			var obj = $(this);
			dialog({
				'title':'拒绝理由',
				'content':obj.next().val(),
			}).showModal();
		})
	})
</script>
	