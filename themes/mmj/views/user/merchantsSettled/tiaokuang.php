<p style="margin-bottom:10px;text-align:center;text-indent:43px;line-height:25px">
    <strong><span style="font-size:21px;font-family:宋体">《妈咪街商家入驻协议》</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">&nbsp;</span>
</p>
<p style="margin-bottom:10px;text-indent:35px;line-height:25px">
    <strong><span style=";font-family:宋体">妈咪街网站（域名为：</span></strong><a href="http://www.mamijie.com/"><strong><span style=";font-family:宋体;color:windowtext">http://www.mamijie.com</span></strong></a><strong><span style="text-decoration:underline;"><span style=";font-family:宋体">，以下简称“妈咪街”或者“本站”）的各项服务系由厦门勋恒网络科技有限公司（以下简称“妈咪街运营公司”）提供，妈咪街所提供的各项服务的所有权和运作权均属于妈咪街运营公司。</span></span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:27px">
    <strong><span style=";font-family:宋体">&nbsp;</span></strong><strong><span style=";font-family:宋体">妈咪街在此特别提醒您（以下或称“商家”、“卖家”）在签订本协议之前，请您务必审慎阅读、充分理解各条款内容，特别是免除或者限制责任的条款、争议解决和法律适用条款。免除或者限制责任的条款可能将以加粗字体显示，您应重点阅读。如您对本协议有任何疑问的，应向妈咪街客服咨询。</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:27px">
    <strong><span style="text-decoration:underline;"><span style=";font-family:宋体">本协议内容包括本协议正文及所有妈咪街已经发布的或将来可能发布的各类规则。所<a name="_GoBack"></a>有规则为本协议不可分割的一部分，与本协议正文具有同等法律效力。</span></span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:27px">
    <span style=";font-family:宋体">妈咪街在此特别提醒您，</span><span style=";font-family: 宋体">服务条款的解释权、修改权归妈咪街运营公司所有。<strong><span style="text-decoration:underline;">您应当随时关注本服务条款的修改，并决定是否继续使用本网站提供的各项服务。您使用妈咪街各项服务的行为，将被视为您对本服务条款的同意和承诺遵守。</span></strong>如您不同意相关变更，必须停止使用妈咪街提供的服务。除另行明确声明外，任何使服务范围扩大或功能增强的新内容均受本协议约束。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:27px">
    <strong><span style=";font-family:宋体">一、协议接受条款</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:27px">
    <span style=";font-family:宋体">1</span><span style=";font-family:宋体">、您一旦使用妈咪街的服务，即视为您已充分了解并完全同意本服务条款各项内容，包括妈咪街对服务条款随时做的任何修改。本协议所有规则为协议不可分割的一部分，与协议正文具有同等法律效力。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:27px">
    <span style=";font-family:宋体">2</span><span style=";font-family:宋体">、以任何方式入驻妈咪街并使用服务即表示您已充分阅读、理解并同意接受本协议的条款和条件(以下合称</span><span style=";font-family:宋体">“</span><span style=";font-family:宋体">条款</span><span style=";font-family:宋体">”</span><span style=";font-family:宋体">)</span><span style=";font-family:宋体">。届时您不应以未阅读本协议的内容或者未获得妈咪街对您问询的解答等理由，主张本协议无效，或要求撤销本协议。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:27px">
    <strong><span style=";font-family:宋体">3</span></strong><strong><span style=";font-family:宋体">、妈咪街有</span></strong><strong><span style=";font-family:宋体">权根据业务需要酌情修订</span></strong><strong><span style=";font-family:宋体">“</span></strong><strong><span style=";font-family:宋体">条款</span></strong><strong><span style=";font-family:宋体">”</span></strong><strong><span style=";font-family:宋体">，并以网站公告或发布在特定服务版块的形式进行更新，不再单独通知予您。经修订的</span></strong><strong><span style=";font-family:宋体">“</span></strong><strong><span style=";font-family:宋体">条款</span></strong><strong><span style=";font-family:宋体">”</span></strong><strong><span style=";font-family:宋体">一经在妈咪街公布，即产生效力。</span></strong><span style=";font-family:宋体">当您与妈咪街发生争议时，应以最新的</span><span style=";font-family:宋体">“</span><span style=";font-family:宋体">条款</span><span style=";font-family:宋体">”</span><span style=";font-family:宋体">为准。</span>&nbsp;
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">二、协议名词释义</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1.</span><span style=";font-family:宋体">“妈咪街商品”指妈咪街网上所有陈列并供出售的商品和服务；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2.</span><span style=";font-family:宋体">“商家”/“卖家”指加盟妈咪街并具备加盟合作所须的相应资质和能力的公司或企业；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3.</span><span style=";font-family:宋体">“客户”/“买家”指购买妈咪街商品或接受妈咪街服务的任何个人、企业或组织；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4.</span><span style=";font-family:宋体">“保密信息”指任何与妈咪街产品或服务有关的包括但不限于销售数据和客户信息；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">5.</span><span style=";font-family:宋体">“招商旺旺号”指妈咪街为与商家沟通、发布信息等用途而申请的阿里旺旺号，此旺旺号是其特定招商员工与商家及客户沟通的重要工具。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">三、入驻商家资格</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1.</span><span style=";font-family:宋体">入驻商家为合法设立并有效经营的公司或企业，且其注册资本在人民币10万元以上（“以上”含本数）；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2.</span><span style=";font-family:宋体">入驻商家的母婴类商品品牌在母婴行业具有一定的知名度；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3.</span><span style=";font-family:宋体">入驻商家是销售商品该品牌的所有者、授权渠道商及具有法人资格的经销商；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4.</span><span style=";font-family:宋体">入驻商家提供的商品必须保证是品牌正品，且价格具有足够竞争力；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">5.</span><span style=";font-family:宋体">入驻商家的经营类目须是以下一种或几种</span><span style=";font-family:宋体">,</span><span style=";font-family:宋体">女装、鞋类箱包、日用百货、母婴用品、数码配件、生活家电、护肤彩妆、家居家纺、特产美食、文化玩乐等；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">6.</span><span style=";font-family:宋体">具有能力履行本协议项下之条款，同时履行义务的行为不违反任何对其有约束力的法律文件中的任何规定；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">7.</span><span style=";font-family:宋体">入驻商家必须在行业内具有良好的信誉，未曾因违法、违规行为而被有关部门惩罚。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">四、入驻要求</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">商家须按国家《网络商品交易及有关服务行为管理暂行办法》的规定配合妈咪街提交相关真实有效的资质证明文件（包括但不限于营业执照、组织机构代码证、税务登记证，品牌认证书、商标注册证，质检报告、食品流通许可证或3C认证证书等），该等资质文件由妈咪街归档保存；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">申请商家必须按照妈咪街的要求如实、准确、详细地填写注册信息，如有任何不真实、不准确或不完整，妈咪街有权拒绝该申请，因商家未如实、准确、详细地填写注册信息而导致的法律责任由商家自行承担；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3.</span><span style=";font-family:宋体">若商家初审中符合加盟条件，在妈咪街复核期间发现违规情况，或在合作过程中发现商家不符合加盟条件，妈咪街有权单方面终止本协议，并通知商家立即退出商城；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4.</span><span style=";font-family:宋体">商家注册妈咪街不代表用户其已经成功入驻妈咪街，仍须经过公司审核并获得同意，以双方签订《妈咪街商家入驻协议》为准；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">5</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">商家如新增入驻商品种类的，则其应当重新向妈咪街提出书面申请并经妈咪街同意和交清相应的保证金后方可入驻。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">五、技术支持服务</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1.</span><span style=";font-family:宋体">公司负责妈咪街的建设、维护、运行和管理，为商家在妈咪街发布商品信息及销售商品提供双方在本协议中约定的技术服务；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2.</span><span style=";font-family:宋体">公司仅负责向商家提供本协议规定的技术服务，除此之外与商家销售商品有关的一切事务(包括但不限于发货、维修、质量保证、退换货等)均应商家自行负责。如果商家要妈咪街提供额外的服务(包括但不限于推广服务等)，商家须要支付额外费用，具体事项由双方另行约定。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">六、协议期限</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">本协议的有效期限为<span style="text-decoration:underline;">1</span>年或<span style="text-decoration:underline;">12</span>月，自<span style="text-decoration:underline;">申请审核入驻通过之日起</span>至<span style="text-decoration:underline;">办理完退出商城手续</span>为止。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">七、保证金</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1</span><span style=";font-family:宋体">、商家在活动上线前须向妈咪街交纳保证金，作为商品质量和服务保证金，保证商家遵从国家法律法规规定及妈咪街管理规则，具体保证金依商家入驻的经营类目不同而有所不同，具体金额如下：</span>
</p>
<table cellspacing="0" cellpadding="0">
    <tbody>
        <tr style=";height:18px" class="firstRow">
            <td width="231" colspan="2" valign="top" style="border-color: windowtext; border-width: 1px; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">商品种类</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-style: none; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">保证金数额（元）</span></strong>
                </p>
            </td>
        </tr>
        <tr style=";height:19px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">女装</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:18px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">鞋类箱包</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:19px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">内衣配饰</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:18px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">运动户外</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:19px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">饰品</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:18px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">数码配件</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:19px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">生活家电</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>5000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:18px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">护肤彩妆</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>5000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:3px">
            <td width="115" rowspan="6" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">母婴用品</span></strong>
                </p>
            </td>
            <td width="116" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">童装</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:3px">
            <td width="116" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">孕妇用品</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:3px">
            <td width="116" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">新生儿</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>5000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:3px">
            <td width="116" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">美食每克</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>10000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:3px">
            <td width="116" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">宝宝用品</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>5000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:3px">
            <td width="116" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">儿童玩具</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="3">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>0</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:18px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">家居家纺</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:19px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">美食特产</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:18px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">日用百货</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="18">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>2000</strong>
                </p>
            </td>
        </tr>
        <tr style=";height:19px">
            <td width="231" colspan="2" valign="top" style="border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: none; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong><span style="font-family:   宋体">文化玩乐</span></strong>
                </p>
            </td>
            <td width="231" valign="top" style="border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1px; border-right-color: windowtext; border-right-width: 1px; padding: 0px 7px;" height="19">
                <p style="margin-bottom:3px;text-align:center;text-indent:28px;line-height:25px">
                    <strong>0</strong>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2.</span><span style=";font-family:宋体">除非因商家原因被扣除的，保证金始终存于妈咪街支付宝账户（账号：<span style="text-decoration:underline;"><span style="background:yellow;background:yellow">mamijiecom@163.com </span></span>）中。如合作终止且约定期满后商家无任何违规、违约行为，则保证金无息退还商家。同时妈咪街对商家缴纳的保证金不提供任何收据或发票，商家对此无异议； </span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3.</span><span style=";font-family:宋体">若因商家违法/违规行为给妈咪街造成损失的，妈咪街有权从保证金中扣除部分或全部保证金。保证金不足以弥补损失的，商家应在接到妈咪街通知后3个工作日内（遇节假日顺延）补齐，否则妈咪街有权从商家货款中扣划以补齐保证金。通过上述方式仍然无法弥补的，妈咪街有权解除合同并保留通过法律途径向商家追偿的权利；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4.</span><span style=";font-family:宋体">如商家在合作期间内（包括：交易、费用结算、售后处理及买家维权处理）无任何违法违规行为，双方终止合作时已经结清所有款项，权利义务履行完毕，商家可向妈咪街书面提出保证金退还申请，妈咪街审核通过后15个工作日（遇节假日顺延）内将保证金退还商家；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">5.</span><span style=";font-family:宋体">合作期间内，若妈咪街发现商家存在或可能存在违规或违约行为且在该审核通过的20个工作日内（遇节假日顺延）无法处理完毕的，妈咪街有权将保证金冻结时间再次延长30个工作日（遇节假日顺延）或紧急扣除保证金，待商家违规、违约行为处理完毕后视具体情况解冻或退还。但商家尚有欠款未向妈咪街付清的，保证金仍无法解冻或退还；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">6.</span><span style=";font-family:宋体">妈咪街在下列情况下可扣除商家全部保证金，商家无权要求返还：</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">（1）商家资质造假；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">（2）商家销售的商品为假冒伪劣产品、侵犯第三方知识产权、肖像权或严重违反我国《产品质量法》等法律法规的规定；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">&nbsp;(3) </span><span style=";font-family:宋体">商家私下给妈咪街招商人员回扣；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">（4）妈咪街收到客户投诉后与商家沟通，商家拒不改正，客户投诉达到5次以上；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">（5）其它违法、违规行为；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">7.</span><span style=";font-family:宋体">妈咪街在下列情况下可处置商家保证金：</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">（1）商家违反妈咪街规则，且根据妈咪街规则，商家应当向妈咪街支付违约金或赔偿金，商家同意妈咪街有权直接划扣、使用商家保证金；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">（2）如商家未按照法律规定、本协议规定或妈咪街的规则向客户履行相关义务，违反其对客户的承诺时，商家同意妈咪街有完全的权利根据妈咪街的判断，直接使用保证金向客户退款、适度支付违约金或赔偿金；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">8.</span><span style=";font-family:宋体">妈咪街如使用保证金对客户进行任何赔付，应以书面方式（包括但不限于电子邮件、传真等）通知商家。在向商家出具的书面通知中，应说明赔付原因及赔付金额。赔付完成后，应向商家出具必要的赔付资金往来的凭证；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">9.</span><span style=";font-family:宋体">商家除应遵守本协议内容外，还应当遵守《妈咪街商家保证金实施规定》（详见附件一）之相关规定。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">八、妈咪街的权利和义务</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">公司提供网上销售平台，负责对平台的技术支持，实时监控和维护，保证网络稳定、正常运行，但公司不对任何电力、电脑、通讯或其他系统故障、第三方行为、不可抗力等非因妈咪街原因造成的损失承担责任；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">公司（或通过第三方）提供网上支付、商品销售、后台管理、订单跟踪等系统及相应技术支持，以利于下单、结算流程顺畅，便于商家业务的发展；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">妈咪街有权定期对商家的相关商品信息进行备份，确保数据安全；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">妈咪街有权根据市场环境或政策法规对双方合作方式进行修订、调整；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">5</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">妈咪街将向商家提供必要的技术支持，使商家可以便捷地运营、管理妈咪街的商家后台；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">6.</span><span style=";font-family:宋体">妈咪街有权审核商家提供的商品信息，妈咪街对商家商品的审核仅是普通人，非专业知识的审核，对不符合法律、法规或妈咪街有理由相信如果发布将给妈咪街带来不利影响的商品信息，妈咪街有权要求商家进行修改，也有权拒绝发布或删除该信息；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">7.</span><span style=";font-family:宋体">妈咪街有权随时变更、升级平台相关服务及功能，有权不定期对交易平台进行系统维护，有权在交易平台中开发新的模块、功能和软件或提供其他服务；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">8.</span><span style=";font-family:宋体">妈咪街指定的招商人员用旺旺号与商家联系，商家因第三方以妈咪街的名义开展合作而上当受骗，妈咪街不承担责任，商家应谨慎审查非妈咪街指定招商人员的旺旺号；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">9.</span><span style=";font-family:宋体">妈咪街有权不定期地对商家在妈咪街销售的产品进行抽样检验，如发现检验结果与产品描述或者样品质量不相符，则妈咪街有权根据《妈咪街商家违规/违约处罚细则》（详见附件二）进行处罚。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">九、商家的权利和义务</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1.</span><span style=";font-family:宋体">商家有权要求妈咪街提供必要的技术支持，并开通商家主页信息平台的管理权限，维持商家主页正常运营，商家可向妈咪街提出系统优化的意见或建议；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2.</span><span style=";font-family:宋体">商家应保证其在妈咪街商家主页中的商品信息合法、真实及准确；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3.</span><span style=";font-family:宋体">商家必须按照协议签订范围在妈咪街销售相应的商品，并保证有关商品的质量及合法性；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4.</span><span style=";font-family:宋体">商家有义务保证对其提供给妈咪街的商品为中国相关法律法规准许在中国境内销售的，由合法成立企业依法生产和授权经销的商品，并保证商品的质量、服务及附随义务符合法律法规要求，及提供合法正规的发票；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">5.</span><span style=";font-family:宋体">商家保证销售的产品未侵犯第三方的合法权益，包括但不限于第三方知识产权、肖像权及物权等构成侵害，如因其行为导致妈咪街遭受任何第三方提起的索赔，并承担民事、行政及刑事责任的，妈咪街有权向商家主张因此给其造成的一切损失及费用；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">6.</span><span style=";font-family:宋体">商家根据双方销售目标，完成商品或原料备货。所有商品的仓储、发货（物流配送）、退货等均由商家完成，且商品包装不得出现其他网上销售平台的网址等任何相关信息；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">7.</span><span style=";font-family:宋体">对于商家寄给妈咪街供审核的样品，妈咪街仅以普通或非专业人员的知识水平标准评估商家寄来样品，商家保证当次活动所有发货的商品不低于样品标准（包括但不限于商品批次、商品包装、商品分量、同色商品颜色、同规格/尺码商品的规格/尺码及商品材质等各个方面）；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">8.</span><span style=";font-family:宋体">销售过程中，若发生消费者投诉或经妈咪街抽检的商品质量有争议的，妈咪街有权以此样品与争议商品进行比对，独立判断争议商品与样品是否存在差异及差异程度。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">十、 商品配送</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1.</span><span style=";font-family:宋体">商家自行提供商品配送服务且承担全部邮费，商家不得要求客户承担运费，除因退货而产生的邮费由客户自行承担；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">商家因配送问题与客户产生纠纷，商家应负责解决；如给妈咪街造成损失，商家还应负责赔偿；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">商家要由妈咪街协助配送的产品应在合同签订前与妈咪街协商一致；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4.</span><span style=";font-family:宋体">商家须保证订单生成的<span style="text-decoration:underline;">72</span>小时内（除妈咪街大型促销活动导致的大量发货情况外）全部发货给客户，同时将订单所对应的快递单据信息上传到妈咪街网站，以便妈咪街提示客户订单状态。商家须支持7天无理由退货且遵循平台统一的退货政策。商家无法履行按时发货义务或出现其他违规操作的，妈咪街有权按《妈咪街商家违规/违约处罚细则》（详见附件二）对商家进行相应处罚。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">十一、 技术服务费</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">商家应当根据通过其在妈咪街平台的成交额（成交额=商品单价X销售数量额）按5%的比例向妈咪街支付技术服务费，该服务费由妈咪街与商家货款结算时一次性收取；技术服务费由妈咪街与商家在每次进行货款结算时从应付给商家的货款中扣除；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">妈咪街可根据将来政策、自身因素及市场因素调整技术服务费</span><span style=";font-family:宋体">,</span><span style=";font-family:宋体">妈咪街应至少提前30日通知商家。如商家对技术服务费政策调整存在任何异议，可于政策调整之日起3个工作日内向妈咪街提出异议或选择终止与妈咪街的合作，若未提出异议，则视为接受此调整；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">无论交易最终是否实际成功，客户只要有在妈咪街平台上购买商家商品且已生成订单，妈咪街均收取商家技术服务费。商家不得以客户退货等任何理由向妈咪街主张退还相应的技术服务费。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">十二、 货款结算</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">商家通过妈咪街平台实现产品销售，产品销售款将通过第三方支付平台统一进入妈咪街账户，在每笔订单生效后（买家即客户确认收货）的15个工作日内双方进行统一货款结算；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">每日订单生效后（买家即客户确认收货），妈咪街在15个工作日内将该批次销售数据上传至妈咪街商家后台，商家如有异议则应于数据上传后 3 个工作日内向公司以书面形式提出，逾期则视为对妈咪街对结算数据无异议。商家核对无误后可申请货款提现，公司自收到商家申请后 15个工作日内将货款支付到商家银行账户上</span><span style=";font-family:宋体">.</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">非因妈咪街原因导致延迟结算货款时，商家不得向妈咪街索要货款利息；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4.</span><span style=";font-family:宋体">如商家存在欠付妈咪街、买家或者第三方任何款项情形，则妈咪街有权直接从应付给商家的货款中直接予以扣减，待相关款项结清后无息返还给商家。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">十三、违约责任&nbsp;&nbsp; </span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">商家必须按照协议签订范围在妈咪街销售相应的商品，并保证有关商品的质量及合法性。如因商家非法经营而引起任何法律纠纷，商家负责解决，妈咪街有权根据《妈咪街商家违规/违约处罚细则》（附件二）给予商家相应处罚；若给妈咪街造成损失，商家应赔偿因此给妈咪街造成的一切损失；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">商家须确保完善商品管理、订单处理、售后服务项目，如果因以上事宜引起的消费者投诉、退货及起诉等行为造成的所有损失均由商家赔偿，妈咪街有权根据《妈咪街商家违规/违约处罚细则》（附件二）给予商家相应处罚，且妈咪街有权要求商家承担由此造成妈咪街的全部损失（包括但不限于商业信誉损失）；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">商家应保证其在妈咪街商家主页中的商品信息合法、真实、准确。如因商家提供的商品信息存在问题而引起任何纠纷，商家应负责解决；妈咪街有权根据《妈咪街商家违规/违约处罚细则》（附件二）给予商家相应处罚，如给妈咪街造成任何损失，妈咪街有权解除合同并要求商家赔偿损失；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">若商家逾期支付技术服务费天数超过15天的，妈咪街有权解除本协议，终止合作，并可要求商家按照欠付款金额的百分之五十的标准向其支付违约金；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">5</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">妈咪街网站因遭遇不法分子攻击或非法劫持、战争、自然灾害、政府行为、互联网灾难、互联网通讯提供商等不可抗力事件导致妈咪街网络平台服务器不能正常运行的；或遇大型促销活动期间，用户访问量巨大时，网络环境可能存在一定的不稳定性，妈咪街系统平台偶尔出现短时无法访问的，因以上情形导致妈咪街网络平台服务器临时性不能正常运行或妈咪街网站不能正常访问的妈咪街不承担责任。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">十四、 协议的生效、变更及终止</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1.</span><span style=";font-family:宋体">本协议自商家点击确认后成立，自妈咪街足额收到商家保证金次日起生效。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">妈咪街可以根据市场环境或政策法规合理变更本协议及附件中的合作方式、条款，修改的条款将于妈咪街首页进行公告，若10个工作日内商家未提出异议即视为对双方产生效力，商家不得以妈咪街未通知或商家不同意而不遵照执行；本协议及相关附件变更的，以变更后协议及附件的效力为准。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style="text-decoration:underline;"><span style=";font-family:宋体">3</span></span></strong><strong><span style="text-decoration:underline;"><span style=";font-family:宋体">.</span></span></strong><strong><span style="text-decoration:underline;"><span style=";font-family:宋体">商家若不同意妈咪街提出变更的方式及条款，则将视为其与妈咪街终止本协议，并妈咪街有权停止向商家提供任何服务内容。</span></span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4</span><span style=";font-family:宋体">.</span><span style=";font-family:宋体">本协议终止后，双方应在一个月内对本协议项下应付未付的货款进行清算。商家必须在协议终止后30个工作日内向妈咪街申请进行应付未付的货款清算，逾期未清算的，则视为商家放弃未清算货款的债权，同时也不得向妈咪街主张货款利息，妈咪街不再承担任何清算与支付责任。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">十五、保密责任</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">商家应对其自妈咪街获取或知悉的保密信息（包括但不限于客户信息、销售数据）予以严格保密。除非经妈咪街书面同意，商家不得直接或间接地使用保密信息或向任何第三方透露或允许任何第三方使用保密信息。此保密义务在本协议终止后继续有效，直至妈咪街向公众公开秘密信息之日止。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">十六、版权说明</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1.</span><span style=";font-family:宋体">商家签订本协议，即表明其主动将其在任何时间段在本站发表的任何形式的信息的著作财产权，包括并不限于：复制权、发行权、出租权、展览权、表演权、放映权、广播权、信息网络传播权、摄制权、改编权、翻译权、汇编权以及应当由著作权人享有的其它可转让权利无偿独家转让给妈咪街所有，同时表明商家授权许可妈咪街有权利就任何主体侵权而单独提起诉讼，并由妈咪街获得全部赔偿。本条款已经构成《著作权法》第二十五条所规定的书面协议，其效力及于商家在妈咪街网站发布的任何受著作权法保护的作品内容，无论该内容形成于本协议签订前还是本协议签订后。商家同意并明确了解上述条款，不将已发表于本站的信息，以任何形式发布或授权其它网站（及媒体）使用。同时，妈咪街网站保留删除站内各类不符合规定帖子或者点评信息而不通知用户的权利；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2.</span><span style=";font-family:宋体">公司是妈咪街网站的制作者</span><span style=";font-family:宋体">,</span><span style=";font-family:宋体">拥有此网站内容及资源的版权</span><span style=";font-family:宋体">,</span><span style=";font-family:宋体">受国家知识产权保护</span><span style=";font-family:宋体">,</span><span style=";font-family:宋体">享有对本网站声明的最终解释与修改权；未经公司明确书面许可</span><span style=";font-family:宋体">,</span><span style=";font-family:宋体">任何单位或个人不得以任何方式作全部和局部复制、转载、引用和链接。否则妈咪街将追究其法律责任；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3.</span><span style=";font-family:宋体">商家保证所有销售商品不存在侵犯第三方知识产权情形，包括不得侵犯他人商标权、专利权及著作权（版权）以及我国法律规定的其他知识产权权利及关邻权利，第三方包括权利人及授权许可人；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4.</span><span style=";font-family:宋体">商家保证供应商品，如含有商标、专利、肖像权或著作权等内容，商家应当自己为合法的权利人或授权许可人；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">5.</span><span style=";font-family:宋体">如涉及商家提供的商品，被第三方投诉或起诉知识产权或肖像权侵权并要求妈咪街运营公司承担赔偿责任或其他责任的，商家应当承担妈咪街因此承受的全部损失，包括对第三方作出的经济赔偿及应诉、应对成本（含诉讼费、律师费）等。同时，视为商家违反协议义务，应当向妈咪街运营公司支付相当于侵权商品货款总值的违约金。商家承担上述经济责任的履行期限为妈咪街运营公司与第三方签订协议生效或司法判决生效后的10个工作日内；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">6.</span><span style=";font-family:宋体">涉及由妈咪街营运公司出面应对第三方权利人知识产权索赔的，妈咪街运营公司通知商家后，商家应当立即派员协助处理，并最终在和解、调解或者其他协商文件上签字确认。如妈咪街运营公司通知商家后，商家未派员协助处理，则视为全权委托妈咪街处理事务。妈咪街运营公司/妈咪街与第三方签署的和解、调解或其他协商文件中涉及公司/妈咪街对第三方承担的赔偿（补偿）责任，均由商家承担。涉及司法机关判决的，则依据判决书确定妈咪街的损失，由商家承担；</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">7.</span><span style=";font-family:宋体">商家在遇到可能涉及侵犯第三方知识产权或者肖像权等情形，包括接受到权利人警告或调查时，应当及时告知妈咪街，以便妈咪街作出提前应对和减少损失的措施。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">十七、其他条款</span></strong>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">1.</span><span style=";font-family:宋体">附件是本协议的有效组成部分，与本协议具有同等法律效力。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">2.</span><span style=";font-family:宋体">若本协议中部分条款被认定无效或无法执行，不影响其他有效条款的执行。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">3.</span><span style=";font-family:宋体">商家地址变更须在变更前7日内以书面形式通知公司，若公司按照商家提供的公司地址寄出任何通知，即视为送达，商家不得以其公司地址变更否认通知已经送达。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">4.</span><span style=";font-family:宋体">凡因执行本协议或本协议的补充协议所发生的一切争议，双方应友好协商加以解决；若协商不成，双方均应向妈咪街运营公司所在地人民法院提起诉讼。</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">&nbsp;</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">附件一：《妈咪街商家保证金实施规定》</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">附件二：《妈咪街商家违规/违约处罚细则》</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <span style=";font-family:宋体">&nbsp;</span>
</p>
<p style="margin-bottom:10px;text-indent:28px;line-height:25px">
    <strong><span style=";font-family:宋体">此协议为厦门勋恒网络科技有限公司与您/商家签订，您点击以下“确认”按键就代表您已经认真阅读了并认可以上协议，协议对双方均有法律约束力。</span></strong>
</p>