<style type="text/css">
	.detail-view tr{
		height: 40px;
	}
</style>
<div class="container">
	<div style="margin-left:100px;">
	<div style="font-size:20px;margin:30px auto">入驻详情</div>
	<?php  $homeUrl = ConfigModel::model()->getConfig('sitefileurl'); ?>
	<?php  $Imgs = MerchantsSettledImgModel::getMerchantsSettledImg($model->MerchantsSettledID);
		   $html = '';
		   foreach ($Imgs as $key => $Img) {
		   		$html .= "<img src='{$homeUrl}{$Img['Img']}'  />";
		   }
	 ?>
	<?php $this->widget('ext.YiiBooster.widgets.TbDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'CompanyName',
			'RegistrationNo',
			'StartTime' => array('type' => 'date', 'name' => 'StartTime', ),
			'EndTime' => array('type' => 'date', 'name' => 'EndTime', ),
			'Location',
			'RegisteredCapital',
			'ScopeBusiness',
			'FullName',
			'Address',
			'MobilePhone',
			'FaxNumber',
			'CompanyUrl',
			'BrandAgent',
			'TaobaoUrl',
			'BusinessLicenseImg' => array('type'=>'image', 'name' => 'BusinessLicenseImg', 'value' => $homeUrl . $model->BusinessLicenseImg),
			'OrganizationImg' => array('type'=>'image', 'name' => 'OrganizationImg', 'value' => $homeUrl . $model->OrganizationImg),
			'TaxRegistrationImg' => array('type'=>'image', 'name' => 'TaxRegistrationImg', 'value' => $homeUrl . $model->TaxRegistrationImg),
			'PositiveIDImg' => array('type'=>'image', 'name' => 'PositiveIDImg', 'value' => $homeUrl . $model->PositiveIDImg),
			'ReverseIDImg' => array('type'=>'image', 'name' => 'ReverseIDImg', 'value' => $homeUrl . $model->ReverseIDImg),
			'TrademarkImg' => array('type'=>'image', 'name' => 'TrademarkImg', 'value' => $homeUrl . $model->TrademarkImg),
			'BrandAuthorizationImg' => array('type'=>'image', 'name' => 'BrandAuthorizationImg', 'value' => $homeUrl . $model->BrandAuthorizationImg),
			'Certification3cImg' => array('type'=>'image', 'name' => 'Certification3cImg', 'value' => $homeUrl . $model->Certification3cImg),
			'HeadName',
			'HeadPosition',
			'HeadCardID',
			'HeadMobilePhone',
			'HeadQQ',
			'HeadEmail',
			'AddTime' => array('type' => 'date', 'name' => 'AddTime', ),
			'Status' => array('name' => 'Status', 'value' => MerchantsSettledModel::getStatusHtml($model->Status)),
			'Imgs' => array('type'=>'html', 'name' => 'Certification3cImg', 'value' => $html),
			'Money',
		),
	)); ?>

	</div>

</div>
