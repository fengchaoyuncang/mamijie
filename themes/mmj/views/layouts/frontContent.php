<?php $this->beginContent('theme.views.layouts.lib.front'); ?>
    <?php include 'lib/header.php'; ?>
    <?php include 'lib/mainMenu.php'; ?>
    <?php echo $content; ?>
    <?php include 'lib/footer.php'; ?>
<?php $this->endContent(); ?>