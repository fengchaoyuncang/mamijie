<?php $todaytime = strtotime('today');//获取今天凌晨时间   ?>

<?php 
  
        $ajaxpage = Yii::app()->request->getParam('ajaxpage'); //ajax的时候的分页，一页有20        
        $page = Yii::app()->request->getParam('page');//大的分页 一页有两百

        empty($ajaxpage) ? $ajaxpage = 1 : '';
        empty($page) ? $page = 1 : '';
        //然后算出真正的我要找第几页的产品  200/20
        $page = ($page - 1)*10 + $ajaxpage;

?>
<?php if($page > 1){  ?>
  <div class="home_pagination"><span>第<?php echo $page;  ?>页</span></div> 
<?php } ?>


<ul >              
<?php $num = 0; foreach($goods['data'] as $arrGoodsData){ $num++; ?>

<?php if(($num%4) == 0){  ?>
  <li class="last">
<?php }else{  ?> 
  <li>
<?php }  ?> 
<!-- data-original -->

    <div class="everyDay_hotBOrder"></div>
    <div class="everyDay_hotList">
       <a target="_blank" href="<?php echo $this->createUrl('/front/goods/detail', array('gid' => $arrGoodsData['GoodsID'])) ?>" title="">
        <img width="202" height="202"  data-original="<?php  echo $arrGoodsData['Image']  ?>" alt="<?php  echo $arrGoodsData['GoodsName']  ?>">
      </a>
           <?php if($arrGoodsData['ItemType'] == 0){  ?>
            <div class="everyDay_hotFrom ed_Fromtaobao"></div>
          <?php }else{  ?> 
            <div class="everyDay_hotFrom ed_FromTmall"></div>
          <?php }  ?>           

           
           <?php if($arrGoodsData['StartTime'] >= $todaytime){  ?>
            <div class="tag_xinPin"></div>
           <?php }  ?>
           
           

           <div class="everyDay_hotTxt">
              <div class="everyDay_hotName"><span><?php  echo $arrGoodsData['GoodsName']  ?></span></div>
                <div class="everyDay_hotPrice">￥<em><?php  echo $arrGoodsData['PromoPrice']  ?></em><small>￥<?php  echo $arrGoodsData['Pprice']  ?></small></div>
                <div class="everyDay_hotTime">距结束:<?php echo Tool::getDataTime($arrGoodsData['EndTime']-TIME_TIME);  ?></div>
                <div class="everyDay_hotMsg">
                     <?php if($arrGoodsData['IsFreeShipping']){  ?>
                      <span class="tag_baoyou"></span>
                     <?php }  ?>                  
                     
                </div>
              <div class="everyDay_hotNum"><?php  echo $arrGoodsData['Sales']  ?>人已买</div>
           </div>
      </div>
  </li>

<?php }  ?> 
 </ul>            	   

