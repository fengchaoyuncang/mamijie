
    <?php $count_goods_detail = 0; foreach($data['data'] as $arrGodsData){ $count_goods_detail++; ?>
        <dl class="<?php  if($count_goods_detail%4 == 0){echo 'floorone-con';} ?>" >
            <dt>

                <a target="_blank" href="<?php echo $this->createUrl('/front/goods/detail', array('gid' => $arrGodsData['GoodsID'])) ?>" title="">
                    <img src="<?php  echo $arrGodsData['Image']  ?>" alt="<?php  echo $arrGodsData['GoodsName']  ?>" class="floorone-dt-img">
                </a>
                <?php if($arrGodsData['IsSoldout']){  ?>
                <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/index_saleout.png" alt="" class="floorone-saleout">
                <?php  } ?>  
            </dt>


            <?php  if(ACTION_NAME == 'tomorrow'){  ?>
                <!-- 以下 弹出框 -->
                <div  id="xianshi">
                    <div  class="floorone-common-shadow">
                        <div class="floorone-common-shadow-border"></div>
                    </div>
                    <div  class="floorone-common-l-box">
                        <div class="floorone-common-link">
                            
                        </div>
                        
                    </div>
                </div>

            <?php  } ?>


            <dd>
                <div>
                    <span>￥</span>
                    <strong><?php  echo $arrGodsData['PromoPrice']  ?></strong>
                    <em>￥<?php  echo $arrGodsData['Pprice']  ?></em>
                    <b><?php  echo $arrGodsData['Sales']  ?>人已买</b>
                </div>
                <p>
                    <?php echo mb_substr($arrGodsData['GoodsName'], 0, 10, 'utf-8');  ?>
                     <?php if($arrGodsData['StartTime'] >= TIME_TIME){ ?>
                        <am class="daojishi" style="float:right">还有<?php echo Tool::getDataTime($arrGodsData['StartTime'] - TIME_TIME)  ?>开始</am>
                    <?php  }else{ ?>
                        <am class="daojishi" style="float:right">距结束:<?php echo Tool::getDataTime($arrGodsData['EndTime']-TIME_TIME);  ?></am>
                    <?php } ?>                        
                </p>
                <div class="floor1-icon  clearfix">
                    <?php if(!empty($arrGodsData['IsFreeShipping'])){echo '<div>包邮</div>';}  ?>
                    <div class="index-floorone-changeP">拍下改价</div>

                    <?php if($arrGodsData['ItemType'] == 1){ ?>
                        <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/mao.png" alt="">
                    <?php  }else{ ?>
                        <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/taologo.png" alt="">
                    <?php } ?>   

                </div>
            </dd>
        </dl>                                       

    <?php  } ?>
