<?php
    $this->widget('CLinkPager', array(
        'route' => $route,
        'header' => "",
        'firstPageLabel' => '首页',
        'lastPageLabel' => '末页',
        'prevPageLabel' => '上一页',
        'nextPageLabel' => '下一页',
        'pages' => $page,
        'footer' => "<span'>共<em>{$page->pageCount}</em>页</span>",
        'maxButtonCount' => 8
            )
    );
?>