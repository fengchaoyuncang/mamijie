<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $this->title;  ?></title>
	<meta name="Keywords" content="<?php echo $this->keywords;  ?>">
	<meta name="Description" content="<?php echo $this->description;  ?>">
	<meta name="baidu-site-verification" content="bK4Y6gKKMy" />
	<link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/css/common.css">
</head>
<body>
<?php echo $content; ?>
</body>
</html>