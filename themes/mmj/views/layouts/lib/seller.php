<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $this->title;  ?></title>
	<meta name="Keywords" content="<?php echo $this->keywords;  ?>">
	<meta name="Description" content="<?php echo $this->description;  ?>">
	<link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/css/reset.css">	
	<link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/css/enter.css">
	<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/jquery.mousewheel.min.js"></script>
	<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/enter1.js"></script>
</head>
<body>
<?php echo $content; ?>
</body>
</html>