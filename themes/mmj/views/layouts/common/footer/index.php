<div class="footer">
    <div class="gbottom">
         <ul>
            <li><a href="<?php echo $this->createUrl('/baike/articles/index', array('id' => 355)) ?>">关于妈咪街</a></li>
            <li><a href="<?php echo $this->createUrl('/user/baoming/create') ?>">商家报名</a></li>
            <li><a href="<?php echo $this->createUrl('/user/merchantsSettled/introduction') ?>">商家入驻</a></li>
            <li><a href="<?php echo $this->createUrl('/baike/articles/index', array('id' => 363)) ?>">入驻须知</a></li>
            <li><a href="<?php echo $this->createUrl('/baike/articles/index', array('id' => 361)) ?>">帮助中心</a></li>
            <li><a href="<?php echo $this->createUrl('/baike/articles/index', array('id' => 356)) ?>">联系我们</a></li>
            <li><a href="<?php echo $this->createUrl('/front/public/sitemap') ?>">网站地图</a></li>
            <li class="last"><a href="<?php echo $this->createUrl('/baike/articles/index', array('id' => 362)) ?>">服务条款</a></li>
         </ul>
    </div>

    <!-- 友情链接 -->
    <div class="links_box">
        <div class="friendship">友情链接：</div>
        <div class="links"  id="links">
            <ul>
                <?php  $datas = LinkModel::getList();  ?>
                <?php $num = 0; foreach($datas as $value){ $num++; ?>
                    <?php if($num%8 == 1){  ?>
                        <li>
                    <?php  } ?>
                    
                        <a target="_blank" href="<?php  echo $value->Url; ?>"><?php  echo $value->Title; ?></a>
                    <?php if($num%8 == 7){  ?>
                        </li>
                    <?php  } ?>
                <?php }  ?>
            </ul>
        </div>
    </div>

    <div class="copyRight_box">
        <div class="copyRight_msg">
            <p class="white"><span>&copy;</span> 2003-2015 MaMiJie.com 版权所有</p>
            <p>地址：福建省厦门市集美区杏林湾商务运营中心2号楼24层</p>
            <p>电话：<em>400-639-8090</em>&nbsp;&nbsp;|&nbsp;&nbsp;服务时间09:00~18:00（法定工作日）</p>
            <p>备案号：闽ICP备14015365号-1 厦门勋恒网络科技有限公司</p>
        </div>
         
        <div class="rz_360">
            <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/anquanbz1.png">
        </div>
        <div class="rz_right">
            <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/anquanbz2.png">
        </div>
         
        <div class="gIOS">
            <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/38/ios.jpg">
            <p>IOS APP</p>
        </div>
         
        <div class="gandroid">
            <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/38/Android.jpg">
            <p>Android APP</p>
        </div>
    </div>
</div>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?ba03d3039c43a6efb92dffbc7c52683b";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl;?>/assets/js/new/links.js"></script>
<script>
$.newsScroll({
    selector: '#links'
});
</script>
<?php 

    try {
        GuestDataModel::model()->record();
    } catch (Exception $exc) {
        
    }
?>