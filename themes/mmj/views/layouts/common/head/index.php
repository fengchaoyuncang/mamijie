<?php  //$POSTGET = CMap::mergeArray($_GET,$_POST);     ?>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<title><?php echo $this->title;  ?></title>
<meta name="Keywords" content="<?php echo $this->keywords;  ?>">
<meta name="Description" content="<?php echo $this->description;  ?>">
<meta property="wb:webmaster" content="1ac54451335f8a55" />
<meta property="qc:admins" content="1646411717651512156375" />
<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl;?>/assets/js/new/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl;?>/assets/js/jquery_lazyload/jquery.lazyload.min.js"></script>
 <script type="text/javascript">
 	<?php  if(empty($_GET)){  ?>
 		var DATA_GET = {};
 	<?php }else{  ?>
 		var DATA_GET = <?php echo json_encode($_GET);  ?>;
 	<?php }  ?>
 </script>
<script src="<?php echo yii::app()->theme->baseUrl;?>/assets/js/new/main.js"  type="text/javascript"></script>
<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/main.css" type="text/css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;  ?>/assets/statics/js/artDialog6/css/ui-dialog.css">	
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/statics/js/artDialog6/js/dialog-plus.js"></script>
<script type="text/javascript" src="<?php echo yii::app()->theme->baseUrl;?>/assets/js/new/lrscroll.js"></script>
</head>