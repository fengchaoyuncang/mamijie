<div class="navigation">
    <?php 
    if(!empty($this->menus)){
    $this->widget('zii.widgets.CMenu',array(
            'items'=>$this->menus,
            'encodeLabel' => FALSE,
            'lastItemCssClass'=>'qdLI',
            'htmlOptions' => array('class' => 'clearfix'),
    ));
	}
     ?>
    <div class="navigation_right"></div>

    <div  id="signIn_box" class="signIn_box">
        <?php if(MemberSignModel::model()->canSign()){  ?>
            <a id="" class="signIn_btn" href="javascript:void(0)">已签到</a>
        <?php }else{ ?>
            <a id="signIn_btn" class="signIn_btn" href="javascript:void(0)">点击签到</a>
        <?php }  ?>
        <div id="signIn_calendar" class="signIn_calendar">
             <div id="signIn_ct"></div>
             <div class="signIn_bottom">
                <div class="signIn_score">积分<span id="sign_num"><?php echo yii::app()->user->Score  ?></span></div>
                <div class="signIn_myScore">会员积分</div>
             </div>
             <div id="" class="signIn_upScore">+5</div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo yii::app()->theme->baseUrl;?>/assets/js/e-calendar/css/jquery.e-calendar.css">
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/e-calendar/jquery.e-calendar.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#signIn_ct').eCalendar();

        $(".signIn_btn,#signIn_calendar").hover(function(){
            if(ISSIGNGET === false){
                //获取签到数据
                $.post('<?php echo $this->createUrl("/front/public/getsign") ?>',{},function(data){
                    ISSIGNGET = true;
                    if(data.status){
                        var length = data.content.length;
                        for (var i = 0; i < length; i++) {
                            $('.c-day').eq(data.content[i]-1).addClass('on');
                        };
                    }else{
                        return false;
                    }
                },'json');
            }            
            $("#signIn_box").addClass('active');
        },function(){
            $("#signIn_box").removeClass('active');
        });


        //签到
        //$(".c-today").click(function(){
        $("#signIn_btn").click(function(){
            var obj = $(".c-today").eq(0);
            if(obj.hasClass('on')){
                return false;
            }

            $.post('<?php echo $this->createUrl("/front/public/sign") ?>',{},function(data){
                if(data.status){
                    obj.addClass('on');
                    $("#sign_num").html(data.score);


                    $(".signIn_upScore").animate({
                        marginTop: "-100px",
                        opacity:1
                    }, 800 ).fadeOut("fast");

                    $("#signIn_btn").html("已签到")

                }else{
                    if(data.content == '未登录'){
                        loginregister(1);
                    }else{
                        alert(data.content);
                    }
                }
            },'json');

        })

    });
</script>