<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/brand.css" type="text/css" rel="stylesheet">
    <div class="ency-focus clearfix">
        <div class="ency-focus-left">
            <div class="ency-focus-img">

                <div class="ency-focus-img-content">

                    <?php foreach($advertising as $ad){  ?>

                    <div>
                        <a href="<?php echo $ad['Url'] ?>"><img src="<?php echo Yii::app()->params->fileUrl . $ad['Image'] ?>" alt="<?php echo $ad['Title'] ?>" /></a>
                        
                    </div>

                    <?php } ?>                    
                </div>
            </div>
            <ul id="page_ad" class="clearfix">
                <li class="ency-fl-bg">1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
                <li>6</li>
                <li>7</li>
                <li class="ency-fl-last">8</li>
            </ul>

        </div>
        <div class="ency-focus-mid">
            <div class="ency-focus-mid-tit clearfix">
                <h3>今日聚焦
                    <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/ency_tit.png" alt=""/>
                </h3>
                <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/ency_box.png" alt="" class="ency-focus-mid-box"/>

            </div>
            <div class="ency-focus-content">

                <?php  foreach($new as $rs){  ?>
                    <div class="ency-focus-content-box">
                        <h4><a href="<?php echo $this->createUrl('/baike/index/articleList', array('cid' => $rs['CatID']))  ?>"><?php echo $rs['Title'] ?></a></h4>
                        <ul class="clearfix">
                            <?php $cou = 0; foreach($rs['data']['data'] as $value){  ?>
                            <?php
                              $cou++;
                              $k = $cou%2;
                            ?>
                            <?php if($k == 1){  ?>

                                <li class="ency-fc-left"><a href="<?php  echo $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID'])) ?>"><?php echo $value['Title']  ?></a></li>
                                <li class="ency-fc-line">|</li>
                            <?php }else{ ?>

                                <li class="ency-fc-right"><a href="<?php  echo $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID'])) ?>"><?php echo $value['Title']  ?></a></li>
                            <?php }  ?>

                            <?php  } ?>
                        </ul>
                    </div>                    

                <?php } ?>
            </div>
        </div>
        <div class="ency-focus-right">
            <h3>妈咪说
                <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/ency_tit.png" alt=""/>
            </h3>
                            <?php  $num = 0;foreach($mamishuo['data'] as $value){ $num++; ?>
                                <dl class="ency-fr-dl clearfix">
                                    <dt>
                                        <a href="<?php  echo $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID'])) ?>">
                                            <img src="<?php echo Yii::app()->params->fileUrl . $value['SmlImg'];  ?>" alt="<?php echo $value['Title'];  ?>"/>
                                        </a>

                                    </dt>
                                    <dd>
                                        <h4><a href="<?php  echo $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID'])) ?>"><?php echo $value['Title'];  ?></a></h4>
                                        <?php  $d = ArticlesNewModel::getDataDetail($value['ArticlesID']);  ?>
                                        <p id="encyFrDlp">
                                            <?php echo mb_substr($value['Content'], 0, 20, 'utf-8');  ?>
                                            <a href="<?php  echo $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID'])) ?>">
                                                详情<em>></em>
                                            </a>
                                        </p>
                                    </dd>
                                </dl>

                            <?php } ?>     


<!--             <div class="ency-fr-bottom">
                <ul class="clearfix">
                    <li>
                        <a href="">
                            <img src="<?php //echo yii::app()->theme->baseUrl;  ?>/assets/images/ency-camera.png" alt="" style="margin-left:15px;"/>
                            <div>宝宝发育指标</div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="<?php //echo yii::app()->theme->baseUrl;  ?>/assets/images/ency_rr.png" alt="" style="margin-left:15px;"/>
                            <div>宝宝发育指标</div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="<?php //echo yii::app()->theme->baseUrl;  ?>/assets/images/ency_add.png" alt=""/>
                            <div>宝宝发育</div>
                        </a>
                    </li>
                </ul>
            </div> -->
        </div>
    </div>
<!--     <div class="ency-chosen clearfix">
        <img src="">
    </div> -->
    <!-- 育儿知识 -->
    <div class="ency-know clearfix">
        <div class="ency-know-titbg">
        </div>
        <ul id="yuerList" class="ency-know-nav clearfix">


            <?php  $num = 0;foreach($askCategory as $value){  ?>
                <li  num="<?php echo $num;  ?>" cid="<?php echo $value['CatID'];  ?>" <?php if($num == 0){echo 'class="ency-know-active"';}  ?>><a href="<?php echo $this->createUrl('/baike/index/askList', array('cid' => $value['CatID']));  ?>"><?php echo $value['Title'];  ?></a>
                    <em></em>
                </li>

            <?php $num++;} ?>     
        </ul>

        <div id="yuer" class="ency-know-left clearfix">
            <?php include $this->getViewFile('data') ?>
        </div>

        <script type="text/javascript">
            var DATA = [];
            DATA[0] = $("#yuer").html();
            $(function(){
                $("#yuerList li").mouseenter(function(){
                    var obj = $(this);
                    var num = obj.attr('num');
                    var cid = obj.attr('cid');
                    $("#yuerList li").removeClass('ency-know-active');
                    obj.addClass('ency-know-active');
                    if(typeof(DATA[num]) != 'undefined'){
                        $("#yuer").html(DATA[num]);
                    }else{
                        $.post('<?php echo $this->createUrl('/baike/index/ajaxArticle')  ?>', {id:cid}, function(data){
                            if(data.status){
                                DATA[num] = data.content;
                                $("#yuer").html(data.content);
                            }else{
                                return false;
                            }
                        },'json');
                    }

                })
            })
        </script>



        <div class="ency-know-right">
            <h3>焦点新闻</h3>
            <div>
                <ul>

                    <?php  $num = 0;foreach($hotarticles['data'] as $value){ $num++; ?>
                        <li class="ency-know-right-<?php echo $num; ?> ">
                            <a href="<?php  echo $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID'])) ?>">
                                <?php echo $value['Title'];  ?>
                            </a>
                        </li>

                    <?php } ?>            
                </ul>
            </div>

        </div>
    </div>
    <!-- 学问周刊 -->
    <div class="ency-weekly">
        <div class="ency-weekly-titbg"> 
        </div>
        <h3>孕期周刊 <em>Pregnancy week</em></h3>
        <div class="ency-weekly-nav">
            <table class="ency-weekly-table">



                <?php
                  $box_count = 0;
                 foreach($weekly[0] as $key => $arrData){
                    
                    if($box_count%7 == 0){
                        echo '<tr>';
                    }

                ?>

                    <?php if($box_count%7 == 6){  ?>
                    <td style="width:62px;"><a href="<?php  echo $this->createUrl('/baike/index/weekly', array('id' =>$arrData['WeeklyID'])) ?>"><?php  echo $arrData['Title'] ?></a></td>
                    <?php }else{  ?>
                    <td style="width:190px;"><a href="<?php  echo $this->createUrl('/baike/index/weekly', array('id' =>$arrData['WeeklyID'])) ?>"><?php  echo $arrData['Title'] ?></a></td>
                    <?php } ?>

                
                <?php

                    if($box_count%7 == 6){
                        echo '</td>';
                    }
                    $box_count++;                       
                  } 
                ?>
            </table>
        </div>
    </div>
<!--     <div class="ency-ad-1">
        <a href="">
            <img src="" alt="">
        </a>
    </div> -->

    <!-- 以下为婴儿期育儿周刊 -->
    <div class="ency-baby-weekly">
        <h3>婴儿期育儿周刊<em>Pregnancy week</em></h3>
        <div class="ency-weekly-nav">
            <table class="ency-weekly-table">

                <?php
                  $box_count = 0;
                 foreach($weekly[1] as $key => $arrData){
                    
                    if($box_count%7 == 0){
                        echo '<tr>';
                    }

                ?>

                    <?php if($box_count%7 == 6){  ?>
                    <td style="width:121px;"><a href="<?php  echo $this->createUrl('/baike/index/weekly', array('id' =>$arrData['WeeklyID'])) ?>"><?php  echo $arrData['Title'] ?></a></td>
                    <?php }else{  ?>
                    <td style="width:190px;"><a href="<?php  echo $this->createUrl('/baike/index/weekly', array('id' =>$arrData['WeeklyID'])) ?>"><?php  echo $arrData['Title'] ?></a></td>
                    <?php } ?>

                
                <?php

                    if($box_count%7 == 6){
                        echo '</td>';
                    }
                    $box_count++;                       
                  } 
                ?>


            </table>
        </div>
    </div>

    <!-- 1-6岁育儿指南 -->
    <div class="ency-baby-leader">
        <h3>1-6岁育儿指南<em>1-6 years old child rearing manuals</em></h3>
        <div class="ency-weekly-nav">
            <table class="ency-weekly-table">

                <?php
                  $box_count = 0;
                 foreach($weekly[2] as $key => $arrData){
                    
                    if($box_count%7 == 0){
                        echo '<tr>';
                    }

                ?>

                    <?php if($box_count%7 == 6){  ?>
                    <td style="width:61px;"><a href="<?php  echo $this->createUrl('/baike/index/weekly', array('id' =>$arrData['WeeklyID'])) ?>"><?php  echo $arrData['Title'] ?></a></td>
                    <?php }else{  ?>
                    <td style="width:190px;"><a href="<?php  echo $this->createUrl('/baike/index/weekly', array('id' =>$arrData['WeeklyID'])) ?>"><?php  echo $arrData['Title'] ?></a></td>
                    <?php } ?>

                
                <?php

                    if($box_count%7 == 6){
                        echo '</td>';
                    }
                    $box_count++;                       
                  } 
                ?>
                
            </table>
        </div>
    </div>
    
    <!-- 二维码扫描 -->
    <div class="ency-code">
        <div class="ency-forcode clearfix">
            <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/code.png" alt="" class="ency-forcode-y">
<!--             <a href="" class="ency-forcode-z">
                <img src="<?php //echo yii::app()->theme->baseUrl;  ?>/assets/images/ency_blog.png" alt="">
                <h4>妈咪街微淘</h4>
            </a> -->
        </div>
    </div>
    <script>
        moveStart();

        var ulLength = $(".ency-focus-img-content div").length - 1,
            boxWidth = $(".ency-focus-img-content div").outerWidth(),
            totleWidth = boxWidth * (ulLength + 1),
            contentWidth = $(".ency-focus-img-content").outerWidth();
        function moveStart(){

            var i = 0,
                j = 0,
                dis = null,
                moveJudge = true;
            function movePic() {
                i--;
                j++;
                if(i < -ulLength) {
                    i = 0;
                    j=0;
                    $(".ency-focus-img-content").animate({
                        "left" : 0
                    },600);
                    $(".ency-focus-left ul li").eq(j).addClass("ency-fl-bg");
                    $(".ency-focus-left ul li").eq(j).siblings().removeClass("ency-fl-bg");
                } else {

                    dis = 350 * i;
                    $(".ency-focus-img-content").animate({
                        "left" : dis
                    },600);


                    $(".ency-focus-left ul li").mouseover(function() {
                        var index= $(this).index();
                        dis = 350 * index;

                        clearInterval(timer);
                        $(this).addClass("ency-fl-bg");
                        $(this).siblings().removeClass("ency-fl-bg");
                        $(".ency-focus-img-content").css({
                            "left" : -dis
                        });
                        i = -index;
                        j = index;
                    });

                    $(".ency-focus-left ul li").eq(j).addClass("ency-fl-bg");
                    $(".ency-focus-left ul li").eq(j).siblings().removeClass("ency-fl-bg");
                }


            }

            timer = setInterval(movePic, 2500);

            $(".ency-focus-img").mouseover(function() {
                clearInterval(timer);
            });
            $(".ency-focus-img").mouseout(function() {
               
                    timer = setInterval(movePic, 2500);
            
            });

            $(".ency-focus-left ul li").mouseout(function() {
                if(timer) {
                    clearInterval(timer);
                } 
                timer = setInterval(movePic, 2500);
                    
            
            })


        }
    </script>
