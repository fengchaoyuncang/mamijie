<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/articleList.css" type="text/css" rel="stylesheet">
<div class="ency-prq-wrap">
    <div class="ency-prq-box clearfix">
        <div class="ency-prq-top-bg"></div>

            <?php
                $arr['妈咪百科'] = array('/baike/index/index');
                $arr = CMap::mergeArray($arr,CategoryArticlesNewModel::getMianbao($model->CatID));   
                $arr[] = $model->Title;


                $this->widget('zii.widgets.CBreadcrumbs', array( 
                    'homeLink'=>'<a href="/">首页</a>', //根的是什么
                    'links'=>$arr, 
                    'separator'=>'<em> &gt; </em>',  //分隔符
                    'htmlOptions' => array('class' => 'ency-prq-top'),
                    'inactiveLinkTemplate' => '<strong>{label}</strong>',
                )); 
            ?>

        <div class="ency-prq-left">
            <div id="ency-prq-l-tit" class="ency-prq-l-tit clearfix">
                <h1><?php echo $model->Title;  ?></h1>
                <!-- <span>收藏</span> -->
            </div>
            <div class="ency-prq-l-body clearfix">
                <p><?php echo $model->Content;  ?></p>
                <span>提问来自:网页</span>
                <strong><?php echo date('Y-m-d', $model->ContentTime);  ?></strong>
            </div>
            <div class="ency-prq-l-bottom clearfix">
                <h4>最佳答案</h4>
                <span><?php echo date('Y-m-d', $model->AnswerTime);  ?></span>
                <p><?php echo $model->Answer;  ?></p>
                <strong id="ency-prq-good" class="ency-prq-good">没帮助(<em><?php echo $model->NoHelp;  ?></em>)</strong>                
                <strong id="ency-prq-bad" class="ency-prq-bad">有用(<em><?php echo $model->Help;  ?></em>)</strong>


            </div>
        </div>

        <!--公共的右边部分-->
    	<?php include $this->getViewFile('//layouts/baike/right') ?>
    </div>
    
</div>


<script type="text/javascript">
    $(function(){


        $("#ency-prq-bad,#ency-prq-good").click(function(){
            var obj_this = $(this);
            var id = $(this).attr('id');
            var obj = {};
            if(id == 'ency-prq-bad'){
                obj.help = 1;
            }else{
                obj.help = 0;
            }
            obj.id = '<?php echo $model->ArticlesID  ?>';
            $.post('<?php  echo $this->createUrl('/baike/index/ajaxHelp') ?>', obj, function(data){
                if(id == 'ency-prq-bad'){
                    obj_this.find('em').html(data.Help);
                }else{
                    obj_this.find('em').html(data.NoHelp);
                }
            }, 'json');
        })
    })
</script>