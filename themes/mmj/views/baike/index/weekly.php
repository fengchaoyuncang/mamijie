<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/articleList.css" type="text/css" rel="stylesheet">
<div class="ency-preg-box">
    <?php  if($data['Type'] == 1){  ?>
    <div class="ency-preg-weekly">
        <span class="ency-preg-choose">选择您的孕育周刊：</span>
        <span class="ency-preg-det"><?php  echo $data['Title'] ?></span>
    </div>
    <div class="ency-preg-info clearfix">
        <?php  $num = 0; foreach($weeklyType as $WeeklyID => $value){ $num++; ?>
            <?php  
                if($num > 1){
                    $a = next($weeklyType);
                }else{
                    reset($weeklyType);
                }
                if($data['WeeklyID'] == $value['WeeklyID']){
                    $html = 'ency-preg-active';
                    $prev = prev($weeklyType);
                    if(!$prev){
                        reset($weeklyType);
                        $next = next($weeklyType);
                    }else{
                        next($weeklyType);
                        $next = next($weeklyType);                        
                    }
                    empty($prev) ? $prev = $value : '';
                    empty($next) ? $next = $value : '';
                }else{
                    $html = '';
                }
            ?>
            <a  class="<?php  echo $html;  ?>"    href="<?php echo $this->createUrl('/baike/index/weekly', array('id' => $value['WeeklyID']))  ?>"><?php echo $value['Week']  ?>
                <span>怀孕<?php echo $value['Week']  ?>周</span>
            </a>

        <?php }  ?>

    </div>

    <?php }else{

       
        $num = 0; 
        foreach($weeklyType as $WeeklyID => $value){
         $num++;

        if($num > 1){
            $a = next($weeklyType);
        }else{
            reset($weeklyType);
        }
        if($data['WeeklyID'] == $value['WeeklyID']){
            $prev = prev($weeklyType);
            if(!$prev){
                reset($weeklyType);
                $next = next($weeklyType);
            }else{
                next($weeklyType);
                $next = next($weeklyType);                        
            }
            empty($prev) ? $prev = $value : '';
            empty($next) ? $next = $value : '';
        }

    }}  ?>




    <div class="ency-preg-count clearfix">
        <div class="ency-preg-count-tit clearfix">
            <a href="<?php echo $this->createUrl('/baike/index/weekly', array('id' => $prev['WeeklyID']))  ?>" class="ency-preg-count-prev">
                上一周
            </a>
            <span><?php echo $data['Title']  ?></span>
            <a href="<?php echo $this->createUrl('/baike/index/weekly', array('id' => $next['WeeklyID']))  ?>" class="ency-preg-ct-next">下一周</a>
        </div>
        <div class="ency-preg-font">
            <span><?php echo $data['Week']  ?></span>
            <strong><?php if($data['Type'] == 3){echo '周岁';}else{echo '周';}  ?></strong>
        </div>
        <img src="/themes/mmj/assets/images/ency_tt.png" alt="" class="ency-preg-countimg"/>
        <div class="ency-preg-count-h1">
            <h2>妈咪街<?php echo ArticlesWeeklyModel::getTypeHtml($data['Type']); ?></h2>
            <strong>Weekly Digest</strong>
        </div>
        <div class="ency-preg-count-zz">
            <p><span>[综述]</span><?php echo $data['Brief']  ?></p>

        </div>
        <?php  $num = 0; foreach($data['data'] as $value){ $num++; ?>
            <div class="ency-preg-articles">
                <h3><?php echo $value['Title']  ?></h3>
                <h4><?php echo $value['Subhead']  ?></h4>
                <p> <?php echo $value['Content']  ?></p>
            </div>

        <?php }  ?>

    </div>
</div>
<span></span>
