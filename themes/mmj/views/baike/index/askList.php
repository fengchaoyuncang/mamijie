<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/articleList.css" type="text/css" rel="stylesheet">
    <div class="ency-list-wrap">
        <div class="ency-list-box">


            <?php
                $arr['妈咪百科'] = array('/baike/index/index');
                $arr = CMap::mergeArray($arr,CategoryArticlesNewModel::getMianbao($cid));   
                $arr[] = $model->Title;


                $this->widget('zii.widgets.CBreadcrumbs', array( 
                    'homeLink'=>'<a href="/">首页</a>', //根的是什么
                    'links'=>$arr, 
                    'separator'=>'<em> &gt; </em>',  //分隔符
                    'htmlOptions' => array('class' => 'ency-prq-top'),
                    'inactiveLinkTemplate' => '<strong>{label}</strong>',
                )); 
            ?>

            <div class="ency-list-box clearfix">


                <?php  include $this->getViewFile('//layouts/baike/article_left');  ?>


                <div class="ency-list-b-right">
                    <ul>
                        <?php foreach($data['data'] as $articlesID => $rs){  ?>
                            <li>
                                <a href="<?php echo $this->createUrl('/baike/index/askDetail', array('id' => $rs['ArticlesID']))  ?>">
                                    <h3><?php echo $rs['Title']  ?></h3>
                                    <p><?php echo $rs['Answer']  ?> </p>
                                    <!-- <span>(0)</span> -->
                                </a>
                            </li>

                        <?php }  ?>
                    </ul>
                </div>

                
            </div>
        </div>
                    <div id="page">

                        <?php $page = $data['page'];$route=''; include $this->getViewFile('//layouts/index/page');  ?>
                    </div>
    </div>
    <script>
        
        $(".ency-list-b-left-ul li").click(function() {
            
            if($(this).children(".ency-list-b-left-tit").hasClass("ency-list-nav")) {
                $(this).children("ul").hide();
                $(this).children(".ency-list-b-left-tit").removeClass("ency-list-nav");
                $(this).children(".ency-list-b-left-tit").find("em").css({
                    "background" : "url(" + "../images/ency_arrow.png" + ")no-repeat"
                    
                });
            } else {
                $(this).children(".ency-list-b-left-tit").addClass("ency-list-nav");
                $(this).children("ul").show();
                $(this).children(".ency-list-b-left-tit").find("em").css({
                    "background" : "url(" + "../images/ency_arrow_b.png" + ")no-repeat"
                   
                });
            }
        });

    </script>
