<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/articleList.css" type="text/css" rel="stylesheet">
<div class="ency-articles-box clearfix">
    <div class="ency-articles-left">
        <div class="ency-articles-l-tit">
            <h1><?php echo $model->Title  ?></h1>
            <span><?php echo date('Y-m-d', $model->AddTime)  ?></span>
        </div>
        <div class="ency-articles-l-content">
            <p><?php  echo $model->Content ?></p>
        </div>
    </div>

    <?php include $this->getViewFile('//layouts/baike/right') ?>
</div>
