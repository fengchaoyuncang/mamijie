

            <div class="ency-know-tab clearfix">
                <div class="ency-kt-left">
                    <div class="ency-kt-left-hidden">
                        <ul class="clearfix">
                            <?php  $num = 0;foreach($datacate['ask']['data'] as $value){ $num++; ?>
                                <li>
                                    <?php if($num == 1){  ?>
                                        <dl class="ency-kt-left-top">
                                            <dt>
                                                <img src="/themes/mmj/assets/images/ency_kt.png" alt=""/>
                                            </dt>
                                            <dd>
                                                <a  href="<?php  echo $this->createUrl('/baike/index/askDetail', array('id' => $value['ArticlesID'])) ?>"><?php echo $value['Title'];  ?></a>
                                                <?php  $d = ArticlesAskModel::getDataDetail($value['ArticlesID']);  ?>
                                                <p><a class="ency-kt-left-hidden-hover" href="<?php  echo $this->createUrl('/baike/index/askDetail', array('id' => $value['ArticlesID'])) ?>"><?php echo mb_substr(trim(strip_tags($d['Answer'])), 0, 20, 'utf-8');  ?></a></p>
                                            </dd>
                                        </dl>
                                    <?php }else{ ?>
                                        <div><a href="<?php  echo $this->createUrl('/baike/index/askDetail', array('id' => $value['ArticlesID'])) ?>"><?php echo $value['Title'];  ?></a></div>
                                    <?php } ?>

                                </li>

                            <?php } ?>
                        </ul>
                    </div>

                </div>
                <div class="ency-kt-right">
                    <div class="ency-kt-right-img clearfix">
                        <?php  $num = 0;foreach($datacate['imgarticle']['data'] as $value){ $num++; ?>
                            <a title="<?php echo $value['Title'];  ?>" href="<?php  echo $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID'])) ?>" class="ency-kt-right-img-ac">

                                <img src="<?php echo Yii::app()->params->fileUrl . $value['MidImg'];  ?>" alt="<?php echo $value['Title'];  ?>"/>
                                <div class="ency-kt-right-img-a">
                                </div>
                                    <span><?php echo $value['Title'];  ?></span>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="ency-kt-right-tit clearfix">
                        <?php
                         $d = current($datacate['article']['data']);
                        ?>
                        <div class="encykt-rt-bg">
                            育儿宝典
                        </div>
                        <h2 class="encykt-rt-h2"><a href="<?php  echo $this->createUrl('/baike/index/view', array('id' => $d['ArticlesID'])) ?>"><?php echo $d['Title'];  ?></a></h2>
                    </div>
                    <div class="ency-kt-right-body">
                        <p><?php echo mb_substr($d['Content'], 0, 30, 'utf-8');  ?></p>

                        <ul class="ency-kt-rb-ul clearfix">
                            <?php  $num = 0;foreach($datacate['article']['data'] as $value){ $num++; ?>
                                <li>
                                    <a href="<?php  echo $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID'])) ?>">
                                        <?php echo $value['Title'];  ?>
                                    </a>
                                </li>

                            <?php } ?>                            
                        </ul>
                    </div>

                </div>
            </div>
