<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/articleList.css" type="text/css" rel="stylesheet">
	<div class="ency-list-wrap">
		<div class="ency-list-box clearfix">

            <?php  include $this->getViewFile('//layouts/baike/article_left');  ?>

            <div class="ency-artlist-b-right">
                <ul class="ency-artlist-ul">
                    <?php foreach($data['data'] as $articlesID => $rs){  ?>
                        <li>
                        	<h1><?php echo $rs['Title']  ?></h1>
                        	<div class="ency-artlist-ss clearfix">
    	                    	<p><?php echo mb_substr($rs['Content'], 0, 54, 'utf-8') . '...';  ?></p>
    	                    	<a href="<?php  echo $this->createUrl('/baike/index/view',array('id' => $rs['ArticlesID'])) ?>">详细>></a>
    	                    	<!-- 分享DIV容器 -->
    	                    	<div class="ency-artlist-share"></div>
                        		
                        	</div>
                        </li>
                    <?php }  ?>        



                </ul>

            </div>
            <div style="clear:both"></div>
                <div id="page">
                    <?php $page = $data['page'];$route=''; include $this->getViewFile('//layouts/index/page');  ?>                    
                </div>


		</div>
	</div>
	<script>
		

		 $(".ency-list-b-left-ul li").click(function() {
            
            if($(this).children(".ency-list-b-left-tit").hasClass("ency-list-nav")) {
                $(this).children("ul").hide();
                $(this).children(".ency-list-b-left-tit").removeClass("ency-list-nav");
                $(this).children(".ency-list-b-left-tit").find("em").css({
                    "background" : "url(" + "/themes/mmj/assets/images/ency_arrow.png" + ")no-repeat"
                    
                });
            } else {
                $(this).children(".ency-list-b-left-tit").addClass("ency-list-nav");
                $(this).children("ul").show();
                $(this).children(".ency-list-b-left-tit").find("em").css({
                    "background" : "url(" + "/themes/mmj/assets/images/ency_arrow_b.png" + ")no-repeat"
                   
                });
            }
        });
	</script>