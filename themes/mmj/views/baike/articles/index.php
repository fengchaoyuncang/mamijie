<div class="user_center" style="">
	<div class="user_left">
		<?php  foreach($datas as $catID => $rs){ ?>
		<div class="user_navBox">
			<div class="user_navHd"><h3><?php echo $rs['Title']  ?></h3></div>
			<div class="user_navBd">
				<ul>
					<?php foreach($rs['data']['data'] as $articleModel){ ?>

						<li class="<?php if($articleModel['ArticlesID'] == $model->ArticlesID){echo 'active';} ?>"><a href="<?php  echo $this->createUrl('/baike/articles/index', array('id' => $articleModel['ArticlesID'])) ?>"><?php echo $articleModel['Title'] ?></a></li>
					 <?php  } ?>
				</ul>
			</div>
		</div>
		<?php  } ?>		
	</div>

	<div class="user_right">
		<div class="user_optBox">
			<h3><?php  echo $model->Title ?></h3>
			<div class="problem_box"><?php echo $model->Contnet;  ?></div>
		</div>
	</div>
</div>
