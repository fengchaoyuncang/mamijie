<?php  if(!empty($mianbao[0]['Title'])){ ?>
<div class="page_route">
	<a href="/">首页</a>/
	<?php $num = count($mianbao); foreach($mianbao as $key => $catlist){   ?>

		<?php if(empty($catlist['data'])){  ?>
			<a href="<?php echo $this->createUrl('/front/index/good', array('cid' => $catlist['CatID']))  ?>"><?php echo $catlist['Title'];  ?></a>   <?php if($key != ($num-1)){echo '/';}  ?>

		<?php  }else{   ?>
			<a class="page_hasClass" href="<?php echo $this->createUrl('/front/index/good', array('cid' => $catlist['CatID']))  ?>"><?php echo $catlist['Title'];  ?><i></i>
				<div class="page_routeClass">
					<ul>

						<?php  foreach($catlist['data'] as $cid => $catl){   ?>
							<li class="url" data-url="<?php echo $this->createUrl('/front/index/good', array('cid' => $catl['CatID']))  ?>"><?php echo $catl['Title'];  ?></li>
						<?php  }   ?>						
					</ul>
				</div>
			</a>  <?php if($key != ($num-1)){echo '/';}  ?>

		<?php  }   ?>



	<?php  }   ?>

	<script type="text/javascript">
		$('.url').click(function(e){
			e.preventDefault();
			var url = $(this).data('url');
			window.location.href = url;
		})
	</script>

</div>


<div class="page_title"><?php echo $catlist['Title'];  ?></div>
<!-- <div class="page_classBOx">
	<ul class="page_className">
		<li><a href="#">运动鞋</a></li>
		<li><a href="#">皮鞋</a></li>
		<li><a href="#">凉鞋</a></li>
		<li class="active"><a href="#">休闲鞋</a></li>
		<li><a href="#">帆布鞋</a></li>
		<li><a href="#">学步鞋</a></li>
		<li><a href="#">橡胶鞋</a></li>
		<li><a href="#">泡沫鞋</a></li>
	</ul>
	<ul class="page_classSize">
		<li><a href="#">25码/17.5cm</a></li>
		<li><a href="#">26码/18cm</a></li>
		<li><a href="#">27码/18.5cm</a></li>
		<li><a href="#">28码/19cm</a></li>
		<li><a href="#">29码/19.5cm</a></li>
	</ul>
</div> -->
<div class="height20"></div>
<?php  } ?>

<div class="page_goodList">
	<div class="page_goodTitle">
		<div class="page_goodTotal">Total<strong id="good_count">0</strong></div>
		<div class="page_goodMsg">每天9点更新200款</div>
		<div class="page_goodSelect">
			<?php  
				if(!empty($order)){
					$price_order = $order == 2 ? 3 : 2;
				}else{
					$price_order = 2;
				}
			?>
			<a class="page_sort1" href="<?php echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('order' => ''))) ?>">默认<i></i></a>			
			<a class="<?php if($order == 1){echo 'active ';}  ?>page_sort1" href="<?php echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('order' => 1))) ?>">销量<i></i></a>
			<!-- <a class="<?php //if($order == 4){echo 'active ';}  ?>page_sort1" href="<?php //echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('order' => 4))) ?>">人气<i></i></a> -->
			<a class="<?php if($order == 5){echo 'active ';}  ?>page_sort1" href="<?php echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('order' => 5))) ?>">折扣<i></i></a>
			<a class="<?php if($order == 2){echo 'active_down ';}else if($order == 3){echo 'active_up ';}  ?>page_sort2" href="<?php echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('order' => $price_order))) ?>">价格<i class="page_sortUp"></i><i class="page_sortDown"></i></a>
			<a class="page_sort3" href="#">价格区间<i></i>
				<div class="page_sortClass">
					<ul>
						<li class="url_eid" data-url="<?php echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('eid' => ''))) ?>">全部</li>
						<li class="url_eid" data-url="<?php echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('eid' => 1))) ?>">10元以下</li>
						<li class="url_eid" data-url="<?php echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('eid' => 2))) ?>">10至30元</li>
						<li class="url_eid" data-url="<?php echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('eid' => 3))) ?>">30至60元</li>
						<li class="url_eid" data-url="<?php echo $this->createUrl('/front/index/good', CMap::mergeArray($_GET,array('eid' => 4))) ?>">60元以上</li>
					</ul>
					<script type="text/javascript">
						$('.url_eid').click(function(e){
							e.preventDefault();
							var url = $(this).data('url');
							window.location.href = url;
						})
					</script>
				</div>
			</a>
		</div>
	</div>

	<div id="goodlist"></div>
</div>
<div id="page"> </div>
<script type="text/javascript">
	var ROUTE = '/front/index/good';//用于ajax获取分页所用
    var GOODDSTA = '//layouts/index/good_good';//用于ajax获取数据时所用到的获取的商品的元素类型
    var SORT = '';//用于ajax获取数据时分页后面是否用SORT
    $(function(){
        //滚动的时候事件
       HomePageAjax();
       pageAjax();  
       $(window).on('scroll', function(){
           var he = $(document).scrollTop() / $(document).height();
           if (he > 0.20) {
               HomePageAjax();
           }
       });                  
    })
</script> 