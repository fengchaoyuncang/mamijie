
<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/brand.css" type="text/css" rel="stylesheet">
<div class="brand-bg">
    <div class="brand-box">

        <div class="brand-top-logos clearfix">
            <span class="prev-btn"></span>
            <span class="next-btn"></span>
            <div class="brand-logo-box clearfix">
                <ul class="clearfix">
                        <?php
                          $box_count = 0;
                          $b_c = array();
                         foreach($banner as $id => $arrData){
                            $b_c[$arrData['BrandID']] = $arrData;
                            if($box_count%20 == 0){
                                echo '<li>';
                            }

                        ?>

                        <a href="<?php echo $this->createUrl('bannerDetail', array('bid' => $arrData['BrandID']))  ?>">
                            <img src="<?php  echo Yii::app()->params->fileUrl . $arrData['ImgSmall']; ?>" alt="<?php echo $arrData['Name']; ?>" />
                        </a>

                        
                        <?php

                            if($box_count%20 == 19){
                                echo '</li>';
                            }
                            $box_count++;                       
                          } 
                        ?>
                </ul>
            </div>
        </div>
        
                <?php  foreach($data['data'] as $banner_id => $rs){ ?>
                <div class="speOfferGoods brand-content floorone-price clearfix">
                <div class="brand-title clearfix">
                    <a href="<?php echo $this->createUrl('bannerDetail', array('bid' => $banner_id))  ?>" class="clearfix">
                        <div class="brand-title-T">
                            <h3><?php echo $b_c[$banner_id]['Name']  ?></h3>
                        </div>
                        <span class="brand-title-text">
                            <?php echo $b_c[$banner_id]['Remark'];  ?>
                        </span>
                    </a>
                </div>
            <?php  if($rs['count'] >= 8){$cou = 8;}else{$cou = $rs['count'];}  ?>
            <?php $count_goods_detail = 0; foreach($rs['data'] as $arrGodsData){ $count_goods_detail++; ?>
            <?php  $GoodsID = $arrGodsData['GoodsID']  ?>
                <dl class="<?php  if($count_goods_detail%4 == 0){echo 'floorone-con';} ?> brand-dl" >
                    <dt><a target="_blank" href="<?php echo $this->createUrl('/front/goods/detail', array('gid' => $GoodsID)) ?>" title=""><img src="<?php  echo $arrGodsData['Image']  ?>" alt="<?php  echo $arrGodsData['GoodsName']  ?>"></a>
                        <?php if($arrGodsData['IsSoldout']){  ?>
                        <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/index_saleout.png" alt="" class="floorone-saleout">
                        <?php  } ?>
                    </dt>

                <!-- 以下 弹出框 -->
                    <div style="<?php if(TIME_TIME < $arrGodsData['StartTime']){ echo 'display:block';}else{ echo 'display:none';} ?>" id="xianshi">
                        <div  style="width:280px" class="floorone-common-shadow">
                            <div class="floorone-common-shadow-border"></div>
                        </div>
                        <div  style="width:280px" class="floorone-common-l-box">
                            <div style="width:280px;background:url('<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/icon.png') -311px -284px" class="floorone-common-link">
                                
                            </div>
                            
                        </div>
                    </div>


                    <dd>
                        <div>
                            <span>￥</span>
                            <strong><?php  echo $arrGodsData['PromoPrice']  ?></strong>
                            <em>￥<?php  echo $arrGodsData['Pprice']  ?></em>
                            <b><?php  echo $arrGodsData['Sales']  ?>人已买</b>
                        </div>
                        <p><?php  echo $arrGodsData['GoodsName']  ?></p>
                        <div class="floor1-icon  clearfix">
                            <?php if(!empty($arrGodsData['IsFreeShipping'])){echo '<div>包邮</div>';}  ?>
                            <div class="index-floorone-changeP">拍下改价</div>

                            <?php if($arrGodsData['ItemType'] == 1){ ?>
                                <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/mao.png" alt="">
                            <?php  }else{ ?>
                                <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/taologo.png" alt="">
                            <?php } ?>  
                                                        
                        </div>
                    </dd>
                    <div <?php if($count_goods_detail == $cou){echo 'class="brand-formore"';} ?> >
                        <?php if($count_goods_detail == $cou){   ?>
                        <a href="<?php echo $this->createUrl('bannerDetail', array('bid' => $banner_id)) ?>">
                            <div>共<em class="brand-formore-em"><?php  echo $rs['count']; ?></em>款</div>
                        </a>
                        <?php  } ?>
                    </div>
                </dl>                                       

            <?php  } ?>


            </div>

                <?php  } ?>





                <div id="page">
                    <?php $page = $data['page'];$route=''; include $this->getViewFile('//layouts/index/page');  ?>
                </div>
    </div>
</div>
<script src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/js/brand.js"></script>
