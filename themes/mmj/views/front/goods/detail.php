<div class="good_detail">
	<div class="good_container">
		<div class="good_imgBox">
			<?php
			//$url = !empty($goods['TaobaoUrl']) ? $goods['TaobaoUrl'] : $goods['DetailUrl'];
			$url = $goods['DetailUrl'];
			?>
			<a href="<?php echo $url;  ?>">
				<img alt="<?php  echo $goods['GoodsName']  ?>" width="100%" height="100%" src="<?php echo $goods['Image'];  ?>">
			</a>
			<div></div>
		</div>
		<div class="good_msg">
			<h1><?php echo $goods['GoodsName'];  ?></h1>
			<div class="good_tag">
				<i class="good_icon1"></i>
				<i class="good_icon2"></i>
				<i class="good_icon3"></i>
			</div>
			<dl>
				<dt>原价</dt>
				<dd><div class="good_oriPrice"><em><?php echo $goods['Pprice'];  ?></em>元</div></dd>
			</dl>
			<dl>
				<dt>活动价</dt>
				<dd><div class="good_actPrice"><em><?php echo $goods['PromoPrice'];  ?></em>元</div></dd>
			</dl>
			<dl>
				<dt>销量</dt>
				<dd><strong><?php echo $goods['Sales'];  ?></strong>件</dd>
			</dl>
			<dl>
				<dt>促销方式</dt>
				<dd class="good_sales">
					<span>包邮</span>
					<span>拍下改价</span>
					<span>活动折扣</span>
				</dd>
			</dl>
			<dl>
				<dt>产品来源</dt>
			           <?php if($goods['ItemType'] == 0){  ?>
			            <dd><div class="good_from good_fromTaobao"></div></dd>
			          <?php }else{  ?> 
			            <dd><div class="good_from good_fromTmall"></div></dd>
			          <?php }  ?>  				
			</dl>
			<dl>
				<dt>消费保障</dt>
				<dd class="good_security">
					<i class="good_secIcon1"></i>
					<i class="good_secIcon2"></i>
					<i class="good_secIcon3"></i>
				</dd>
			</dl>
			<div class="good_action">
				<a href="<?php echo $url;  ?>">立刻购买</a>
			</div>
		</div>
	</div>
</div>

<div class="height40"></div>

<div class="good_Recommend">
	<div class="good_RecommendTitle">妈咪街为您推荐</div>
	<div class="good_RecommendCt">
		<ul>
			<?php foreach($datas['data'] as $arrGoodsData){ ?>	

	        <li>
	        	<a target="_blank" href="<?php echo $this->createUrl('/front/goods/detail', array('gid' => $arrGoodsData['GoodsID'])) ?>"><img alt="<?php  echo $arrGoodsData['GoodsName']  ?>" src="<?php  echo $arrGoodsData['Image']  ?>" width="160" height="160"></a>
                <div class="good_RecommendName"><span><?php  echo $arrGoodsData['GoodsName']  ?></span></div>
                <div class="good_RecommendPrice"><em>￥<?php  echo $arrGoodsData['PromoPrice']  ?></em><small>￥<?php  echo $arrGoodsData['Pprice']  ?></small></div>
            </li>


			<?php }  ?> 		

		</ul>
	</div>
	<div class="good_RecommendArrow">
		<a class="gR_left" href="javascript:void(0);"></a>
		<a class="gR_right" href="javascript:void(0);"></a>
	</div>
</div>

<div class="height20"></div>

<div class="good_RecommendBanner">
	<a target="_blank" href="/"><img src="<?php echo yii::app()->theme->baseUrl;?>/assets/images/index/good_RecommendBanner.jpg" width="1020" height="100"></a>
</div>

<div class="height20"></div>

<div class="good_mainwrap">
	<div class="good_mainwrapTitle"><strong>产品详细</strong></div>
	<div class="good_mainwrapPara">
		<h3>产品参数</h3>
		<ul>
			<?php foreach($goods['Spec'] as $d){ ?>	

				<li><?php  echo $d;  ?></li>

			<?php }  ?>			
		</ul>
	</div>
	<div class="good_mainwrapImg">
		<?php  echo CHtml::decode($goods['DetailsData']);  ?>
	</div>
</div>
<div class="height20"></div>
<div class="good_Recommend">
	<div class="good_RecommendTitle">妈咪街为您推荐</div>
	<div class="good_RecommendCt">
		<ul>
			<?php foreach($datas['data'] as $arrGoodsData){ ?>	

	        <li>
	        	<a target="_blank" href="<?php echo $this->createUrl('/front/goods/detail', array('gid' => $arrGoodsData['GoodsID'])) ?>"><img alt="<?php  echo $arrGoodsData['GoodsName']  ?>" src="<?php  echo $arrGoodsData['Image']  ?>" width="160" height="160"></a>
                <div class="good_RecommendName"><span><?php  echo $arrGoodsData['GoodsName']  ?></span></div>
                <div class="good_RecommendPrice"><em>￥<?php  echo $arrGoodsData['PromoPrice']  ?></em><small>￥<?php  echo $arrGoodsData['Pprice']  ?></small></div>
            </li>


			<?php }  ?> 		

		</ul>
	</div>
	<div class="good_RecommendArrow">
		<a class="gR_left" href="javascript:void(0);"></a>
		<a class="gR_right" href="javascript:void(0);"></a>
	</div>
</div>