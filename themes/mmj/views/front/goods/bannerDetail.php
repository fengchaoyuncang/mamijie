<link href="<?php echo yii::app()->theme->baseUrl;?>/assets/css/brand.css" type="text/css" rel="stylesheet">
    <script>
        $(function(){
            var bodyX = $('body').width();
            $(".brand-info-img a").css({width:bodyX+"px"});         
        })
    </script>
    <div class="brand-info-img">
        <a style="" href="javascript:void(0);">
            <img src="<?php  echo Yii::app()->params->fileUrl . $banner['ImgBig']  ?>" alt="<?php echo $banner['Name'] ?>"/>
        </a>
    </div>
    <div class="brand-box">
        <div class="speOfferGoods brand-content floorone-price clearfix">
            <?php $count_goods_detail = 0; foreach($data['data'] as $arrGodsData){ $count_goods_detail++; ?>
            <?php  $GoodsID = $arrGodsData['GoodsID']  ?>
                <dl class="<?php  if($count_goods_detail%4 == 0){echo 'floorone-con';} ?> brand-dl" >
                    <dt><a target="_blank" href="<?php echo $this->createUrl('/front/goods/detail', array('gid' => $GoodsID)) ?>" title=""><img src="<?php  echo $arrGodsData['Image']  ?>" alt="<?php  echo $arrGodsData['GoodsName']  ?>"></a>
                        <?php if($arrGodsData['IsSoldout']){  ?>
                        <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/index_saleout.png" alt="" class="floorone-saleout">
                        <?php  } ?>
                    </dt>

                    <!-- 以下 弹出框 -->
                    <div style="<?php if(TIME_TIME < $arrGodsData['StartTime']){ echo 'display:block';}else{ echo 'display:none';} ?>" id="xianshi">
                        <div  style="width:280px" class="floorone-common-shadow">
                            <div class="floorone-common-shadow-border"></div>
                        </div>
                        <div  style="width:280px" class="floorone-common-l-box">
                            <div style="width:280px;background:url('<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/icon.png') -311px -284px" class="floorone-common-link">
                                
                            </div>
                            
                        </div>
                    </div>

                    <dd>
                        <div>
                            <span>￥</span>
                            <strong><?php  echo $arrGodsData['PromoPrice']  ?></strong>
                            <em>￥<?php  echo $arrGodsData['Pprice']  ?></em>
                            <b><?php  echo $arrGodsData['Sales']  ?>人已买</b>
                        </div>
                        <p><?php  echo $arrGodsData['GoodsName']  ?></p>
                        <div class="floor1-icon  clearfix">
                            <?php if(!empty($arrGodsData['IsFreeShipping'])){echo '<div>包邮</div>';}  ?>
                            <div class="index-floorone-changeP">拍下改价</div>

                    <?php if($arrGodsData['ItemType'] == 1){ ?>
                        <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/mao.png" alt="">
                    <?php  }else{ ?>
                        <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/taologo.png" alt="">
                    <?php } ?>  


                        </div>
                    </dd>
                    <div <?php if($count_goods_detail == $cou){echo 'class="brand-formore"';} ?> >
                        <?php if($count_goods_detail == $cou){   ?>
                        <a href="<?php echo $this->createUrl('bannerDetail', array('bid' => $banner_id)) ?>">
                            <div>共<em><?php  echo $rs['count']; ?></em>款</div>
                        </a>
                        <?php  } ?>
                    </div>
                </dl>                                       

            <?php  } ?>          
        </div>
    </div>