<style type="text/css">
  .sitemap li {
    float: left;
    margin-top: 4px;
    padding: 3px 5px; }

  .sitemap a {
    text-decoration:none;
    border: 1px solid #cccccc;
    border-radius: 3px;
    color: #666666;
    font-size: 12px;
    height: 20px;
    line-height: 20px; }
  .sitemap h3 {
    padding: 10px 0;
    text-align: left; }


</style>

<div class="production-container">
  <?php if($page <=1){  ?>

  <div class="sitemap">
    <h3>分类</h3>
    <ul>
      <?php foreach($category as $dd){  ?>

        <li class="li"><a href="<?php echo $this->createUrl('/front/index/good', array('cid' => $dd['CatID']))  ?>"><?php echo $dd['Title']  ?></a></li>
      <?php  } ?>
    </ul>
    <div style="clear:both"></div>
  </div>



	<div class="sitemap">
		<h3>爱逛街</h3>
		<ul>
			<?php foreach($goodsData['data'] as $goods){  ?>

				<li class="li"><a href="<?php echo $this->createUrl('/front/goods/detail', array('gid' => $goods['GoodsID']))  ?>"><?php echo $goods['GoodsName']  ?></a></li>
			<?php  } ?>
		</ul>
    <div style="clear:both"></div>
	</div>

  <div class="sitemap">
    <h3>明日预告</h3>
    <ul>
      <?php foreach($goodsTomorrowData['data'] as $goods){  ?>

        <li class="li"><a href="<?php echo $this->createUrl('/front/goods/detail', array('gid' => $goods['GoodsID']))  ?>"><?php echo $goods['GoodsName']  ?></a></li>
      <?php  } ?>
    </ul>
    <div style="clear:both"></div>
  </div>


  <div class="sitemap">
    <h3>玩转妈咪街</h3>
    <ul>
      <?php foreach($wangzhuang as $value){  ?>

        <li class="li"><a href="<?php echo $value['url'];  ?>"><?php echo $value['title']  ?></a></li>
      <?php  } ?>
    </ul>
    <div style="clear:both"></div>
  </div>


  <div class="sitemap">
    <h3>系统文章</h3>
    <ul>
      <?php foreach($articlesSystem as $cid => $value){  ?>
        <?php foreach($value['data']['data'] as $article){  ?>
        <li class="li"><a href="<?php echo $this->createUrl('/baike/articles/index', array('id' => $article['ArticlesID']))  ?>"><?php echo $article['Title']  ?></a></li>
        <?php  } ?>
      <?php  } ?>
    </ul>
    <div style="clear:both"></div>
  </div>

  <?php  } ?>


  <div class="sitemap">
    <h3>文章</h3>
    <ul>
      <?php foreach($articlelist['data'] as $value){  ?>
        <li class="li"><a href="<?php echo $this->createUrl('/baike/index/view', array('id' => $value['ArticlesID']))  ?>"><?php echo $value['Title']  ?></a></li>
      <?php  } ?>
    </ul>
    <div style="clear:both"></div>
  </div>  

  <div class="sitemap">
    <h3>问答</h3>
    <ul>
      <?php foreach($asklist['data'] as $value){  ?>
        <li class="li"><a href="<?php echo $this->createUrl('/baike/index/askDetail', array('id' => $value['ArticlesID']))  ?>"><?php echo $value['Title']  ?></a></li>
      <?php  } ?>
    </ul>
    <div style="clear:both"></div>
  </div>

  <div id="page">

      <?php $page = $objpage;$route=''; include $this->getViewFile('//layouts/index/page');  ?>
  
  </div>  



</div>