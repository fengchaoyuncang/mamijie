<div class="register-container">
    <div class="register">
        <div class="register-logo">
            <a href="" title="妈咪街">
                <img src="<?php echo yii::app()->theme->baseUrl;  ?>/assets/images/register_logo.png" alt="妈咪街" title="妈咪街">
            </a>
        </div>
        <div class="register-info">
            <div class="register-bottom-prompt">
                <!-- 传入登陆错误信息容器 -->
                <div class="register-title"><?php echo $msgTitle;?></div>
                <!-- 传入提示信息容器 -->
                <div class="register-common-mes"><?php echo $message;?></div>

                <!-- 返回按钮 -->
                <button class="register-commonBtn"><a href="<?php echo $jumpUrl;?>" class="btn">返回</a></button>

                <div class="register-bottom-bg"></div>
            </div>
        </div>
    </div>
</div>
<script language="javascript">
setTimeout(function(){
    location.href = '<?php echo $jumpUrl;?>';
},<?php echo $waitSecond;?>);
</script>

