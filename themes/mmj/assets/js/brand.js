/**
 * Created by chargo on 2015/1/14.
 */
$(function() {
    var ulLength = $(".brand-logo-box li").length - 1,
        boxWidth = $(".brand-logo-box li").length * 1200;

    $(".brand-logo-box ul").css({
        "width" : boxWidth + 'px'
    })
    function moveStart(){

        var i = 0,
            j = 0,
            dis = null,
            moveJudge = true;

        $(".prev-btn").click(function() {

            if(i == -ulLength) {
                i = 0;
                $(".brand-logo-box ul").stop(true, true).animate({
                    "left" : "0"
                },200)
            }
            else {
                i--;
                dis = 1190 * i;
                $(".brand-logo-box ul").stop(true, true).animate({
                    "left" : dis + "px"
                },200);
            }
        });

        $(".next-btn").click(function() {

            if( i == 0) {
                i = -ulLength;
                dis = 1190 * i;
                $(".brand-logo-box ul").stop(true, true).animate({
                    "left" : dis + "px"
                },200)
            } else {
                i++;
                dis = 1190 * i;
                $(".brand-logo-box ul").stop(true, true).animate({
                    "left" : dis + "px"
                },200);
            }
        });

    }

    moveStart();
})
