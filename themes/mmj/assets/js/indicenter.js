window.onload = function() {
	var	change = true,
		chooseAll = document.getElementById('chooseAll'),
		choose = document.getElementById('choose'),
		boxHeight = document.documentElement.clientHeight || document.body.clientHeight,
		key = true,
		indiH = $(".indi-center").height(),
		indiCenterBox = $(".indi-center-con-r-bottom").height();
	
	// 控制我的消息页面选中框
	$(".indi-center-con-r-bottom .indiChoose").click(function() {
		var index = $(this).index();
		if(change) {
			$(this).css({
				"background" : "url(/themes/mmj/assets/images/" + "indi_icon.png)"
			});
			$(".indi-center-ch input").eq(index).prop("checked", "true");
			change = false;
			
		} else {
			$(this).css({
				"background" : "url(/themes/mmj/assets/images/" + "indi_icon_off.png)"
			});
			$(".indi-center-ch input").eq(index).removeAttr("checked");
			change = true;
		}
	});
	// 我的消息页面全选控制
	$(".chooseAll").click(function() {
		if(key) {
			$(".indi-center-con-r-bottom label").css({
				"background" : "url(/themes/mmj/assets/images/" + "indi_icon.png)"
			});
			$(".indi-center-con-r-bottom input").prop("checked",true);
			key = false;
		} else {
			$(".indi-center-con-r-bottom label").css({
				"background" : "url(/themes/mmj/assets/images/" + "indi_icon_off.png)"
			});
			$(".indi-center-con-r-bottom input").prop("checked",false);
			key = true;
		}
	});

	
	$(".indi-center-delete").click(function() {
		$(".indi-center-choose div input:checked").parent().detach();
	});

		// control tite
		$(".indi-center-nav div ul li").each(function() {
			$this = $(this);
			if($this[0].href == String(window.location)) {
				$(this).siblings().removeAttr("id");
				$(this).attr("id", "indi-center-nav-common");
				
			}
		})


$(window).resize(function() {
		var boxHeight = document.documentElement.clientHeight || document.body.clientHeight;
		$(".indi-center-bg").height(boxHeight);
});

	
}