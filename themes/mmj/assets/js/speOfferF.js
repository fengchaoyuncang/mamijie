/**
 * Created by chargo on 2014/12/30.
 */
$(function() {
    $(window).scroll(function() {
        var speOfferFsH = $(".index-nav").height(),
            speOfferNavH = $(".index-navigation").outerHeight() + 16,
            bodyScroll = document.body.scrollTop || document.documentElement.scrollTop;



        //console.log(speOfferOL);
        if(bodyScroll >= speOfferFsH + speOfferNavH) {
            $(".speOffer-topFix").css({
                "display" : "block",
                "position" : "fixed",
                "top" : "0",
                "z-index" : "300"
            });
/*            $(".speOfferF-nav").css({
                "margin-top" : "115px"
            });*/
            $(".top-login-bg").hide();
        } else {
            $(".speOffer-topFix").css({
                "display" : "none"
            });
            $(".speOfferF-nav").css({
                "margin-top" : "0"
            });
            $(".top-login-bg").show();
        }
    })

//    $(".index-nav").addClass("speOfferFS");

    $(".spe-offer-icon").mouseover(function() {
        $(this).children("div").css({
            "background" : "url(" + "/themes/mmj/assets/images/icon.png" + ") -79px -259px"
        });
    });

    $(".spe-offer-icon").mouseout(function() {
        $(this).children("div").css({
            "background" : "url(" + "/themes/mmj/assets/images/icon.png" + ") -79px -198px"
        });
    });

    $(".produSelectNavL li").eq(0).mouseover(function(){
        $(this).children("a").find("em").css({
            "background": "url(/themes/mmj/assets/images/icon.png" + ") -82px -83px"
        });
    });
    $(".produSelectNavL li").eq(0).mouseout(function(){
        $(this).children("a").find("em").css({
            "background": "url(/themes/mmj/assets/images/icon.png" + ") -89px -83px"
        });
    });
    $(".produSelectNavL li").eq(3).mouseover(function(){
        $(this).children("a").find("em").css({
            "background": "url(/themes/mmj/assets/images/icon.png" + ") -148px -83px"
        });
    });
    $(".produSelectNavL li").eq(3).mouseout(function(){
        $(this).children("a").find("em").css({
            "background": "url(/themes/mmj/assets/images/icon.png" + ") -141px -83px"
        });
    });

    $(".produSelectNavL li").eq(1).mouseover(function(){
        $(this).children("a").find("em").css({
            "background": "url(/themes/mmj/assets/images/icon.png" + ") -155px -83px"
        });
    });
    $(".produSelectNavL li").eq(1).mouseout(function(){
        $(this).children("a").find("em").css({
            "background": "url(/themes/mmj/assets/images/icon.png" + ") -121px -80px"
        });
    });
    $(".produSelectNavL li").eq(2).mouseover(function(){
        $(this).children("a").find("em").css({
            "background": "url(/themes/mmj/assets/images/icon.png" + ") -82px -83px"
        });
    });
    $(".produSelectNavL li").eq(2).mouseout(function(){
        $(this).children("a").find("em").css({
            "background": "url(/themes/mmj/assets/images/icon.png" + ") -89px -83px"
        });
    });

//    left navigation



    
    $(".speOffer-LeftFix-B").click(function() {
        $("html,body").stop(true, true).animate({
            "scrollTop" : "0"
        },600);
    });

 
})

