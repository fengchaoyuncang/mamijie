(function($){
    $.newsScroll = function(options){
        if(!options.selector)
            return false;

        var my = this;

        my.init = function(){
            my.o = $.extend({
                selector: '',
                element: 'ul li',
                show:0,
                interval: 6000,
				isAuto:true,
                line: 1,
                lineHeight: 17,
                ft:2 // 1 left/top 2 right/bottom
            }, options);

            my.s = $(my.o.selector);
            my.w = my.s.width();
            my.h = my.o.lineHeight;
            my.e = my.s.find(my.o.element);
            my.c = my.e.size();
            my.t = 0;
            my.index = parseInt(my.o.show);
            my.e.css({
                width:my.w+'px',
                height:my.h+'px'
            })
            my.e.hide();
            my.e.eq(my.index).show();

			// 注册按键
			$.each(['left','top','right','bottom'],function(key, val){
				my.btn(val);
			});
		
			if(my.o.isAuto === true) {
				my.pasue(my.s);
				my.run();
			}

        };

        // auto
        my.run = function(){
            my.t = setInterval(my.rule, my.o.interval);
        };
		
        my.stop = function(){
            clearInterval(my.t);
            my.t = 0;
        };

		my.pasue = function(pt){
			pt.hover(function(){
				my.stop();
			},function(){
				my.run();
			});
		}

		my.btn = function(btn){
			var ft = 2, Obj = my.o.right;
			switch(btn){
				case 'left':
					ft = 1;
					Obj = my.o.left;
				break;
				case 'top':
					ft = 1;
					Obj = my.o.top;
				break;
				case 'right':
					ft = 2;
					Obj = my.o.right;
				break;
				case 'bottom':
					ft = 2;
					Obj = my.o.bottom;
				break;
				default:
				break;
			}
			if(typeof Obj == 'undefined')
				return false;

			$(Obj).click(function(){
				my.rule(ft)	
			});
			my.pasue($(Obj));
		}

		// 运转规则
        my.rule = function(ft){
            var index = my.index;
			ft = (typeof ft != 'undefined') ? ft : my.o.ft;
			switch(ft){
				case 2:
					index = (index == my.c - 1) ? 0 : index+1;
				break;
				case 1:
				default:
					index = (index == 0) ? my.c - 1 : index-1;
				break;
			}
			my.e.eq(my.index).hide("600");  // 这里可以处理一些效果
			my.index = index;
			my.e.eq(my.index).show("600"); // 这里可以处理一些效果
        };

        my.init();
    }
})(jQuery);