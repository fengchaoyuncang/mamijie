$(document).ready(function(){
/* ----------------------------------------------------- */
/* 每天新品特惠 HOVER
/* ----------------------------------------------------- */
	$(document).on('mouseover','.everyDay_hotBox ul li',function(){
		$(this).addClass('active');
	})
	$(document).on('mouseout','.everyDay_hotBox ul li',function(){
		$(this).removeClass('active');
	})
/* ----------------------------------------------------- */
/* 导航NAV
/* ----------------------------------------------------- */
	$(".top_tabLi3").hover(function(){
		$(".top_tabLi3 a").html("敬请期待");
	},function(){
		$(".top_tabLi3 a").html("妈咪商城");
	});

	$(".top_tabLi4").hover(function(){
		$(".top_tabLi4 a").html("敬请期待");
	},function(){
		$(".top_tabLi4 a").html("积分商城");
	});

	// $('.top_tab ul li').click(function(){
	// 	var obj = $(this);
	// 	var objs = $('.top_tab ul li');
	// 	var length = objs.length;
	// 	var num = '';
	// 	for(var i=0;i<length;i++){
	// 		if(objs.eq(i).find('a').attr('class') == obj.find('a').attr('class')){
	// 			num = i;
	// 		}	
	// 	}
	// 	objs.attr('class','');
	// 	obj.addClass('active');
	// 	for(var i=0;i<length;i++){
	// 		var n = i+1;
	// 		if(i < num){
	// 			objs.eq(i).addClass("tab_hv" + n);				
	// 		}else{
	// 			objs.eq(i).addClass("top_tabLi" + n);					
	// 		}
			
	// 	}
	// })

/* ---------------------------------------------------------------------- */
/*	右侧滚动 展开详细
/* ---------------------------------------------------------------------- */
$('a.rightMenu_smallPic').hover(function(){
	$(this).parents('li').eq(0).addClass('active');
},function(){
	$(this).parents('li').eq(0).removeClass('active');
});


	//initGoToTop();
});

/* ---------------------------------------------------------------------- */
/*	右侧滚动栏
/* ---------------------------------------------------------------------- */
var oldTop = 0;
var oldLeft = 0;
$(function(){
	var z = getPosition (document.getElementById("rightMenu"));

	//oldTop = $('#rightMenu').offset().top;
	//oldLeft = $('#rightMenu').offset().left;


	oldTop = z.y;
	oldLeft = z.x;	
	$('.rightMenu').css("position","fixed");
	$('.rightMenu').css("top",oldTop);
	$('.rightMenu').css("left",oldLeft);
	$('.rightMenu').css("margin-left",0);
	
	$(window).scroll(function() {
		var z = getPosition (document.getElementById("rightMenu"));
		oldLeft = z.x;//防止窗口变化后 距离左边的距离变化产生位置偏移的问题
		var scrollTop = $(window).scrollTop(); 
		if(scrollTop>oldTop)
		{
			$('.rightMenu').css("position","fixed");
			$('.rightMenu').css("top","30px");
		}else
		{
			$('.rightMenu').css("position","fixed");
			$('.rightMenu').css("top",oldTop-scrollTop+"px");
		}
	});

	$(window).resize( function (){//窗口变化时
		setTimeout("changePosition()",200)
	});

});

function changePosition(){
	var screenWidth = 1300 ;
	var currentWidth=$(window).width();
	var z = getPosition (document.getElementById("rightMenu"));
	oldTop = z.y;
	oldLeft = z.x;
	if( currentWidth < screenWidth){
		$('.rightMenu').css('left','auto');
		$('.rightMenu').css("top",oldTop);
		$('.rightMenu').css('right','5px');
	}else{
		$('.rightMenu').css('left','');
		$('.rightMenu').css('margin-left','');
		$('.rightMenu').css("top",oldTop);
		$('.rightMenu').css('right','auto');
	}
}
function getPosition (ele,oRefer)
{
	//oRefer是定位参照物。可以不写，不写就是和浏览器的绝对位置
	//注意：oRefer必须是ele的offset祖先，要不然取到的值是ele距离body的绝对偏移量
		oRefer=oRefer||document.body;
		var x=ele.offsetLeft;
		var y=ele.offsetTop;
		p=ele.offsetParent;//重在理解好offsetParent
		while(p!=oRefer&&p!=document.body&&p!=null)
		{
			if(window.navigator.userAgent.indexOf('MSIE 8.0')>-1){//ie8有个bug（边框问题）
				x+=p.offsetLeft;
				y+=p.offsetTop;
			}else{
				x+=p.offsetLeft+p.clientLeft;
				y+=p.offsetTop+p.clientTop;
			}		
			p=p.offsetParent;
			
		}
		var obj={};
		obj.x=x;
		obj.y=y;
		return obj;
}

/* ---------------------------------------------------------------------- */
/*	back to top
/* ---------------------------------------------------------------------- */
function initGoToTop() {
	// fade in #top_button
	jQuery(function () {
		jQuery('#rightMenu').click(function () {
			jQuery('body,html').animate({
				scrollTop: 0
			},  400);
			return false;
		});
	});	
}

//需要用到的全局变量
var AJAXPAGE = 1;//通过AJAX获取商品的页面
var _isAjaxsuo = false;//是否正在获取
var ISSIGNGET = false;//签到数据是否有获取

//获取商品总数与分页等 
function pageAjax(){
	DATA_GET.route = ROUTE;
	DATA_GET.sort = SORT;
    $.ajax({
        url: '/front/public/ajaxGoodCount',
        type: "post",
        timeout: 5000,
        dataType: "json",
        data:DATA_GET,
        success: function(data) {
            if(data.status){
                $("#good_count").html(data.count);
                $("#page").append(data.content);
            }
        }
    });     
}

//获取商品的数据
function HomePageAjax(){
    if (_isAjaxsuo) {
        return false;
    }
    if(AJAXPAGE > 10){
      //$(window).off('scroll');          
      return false;
    }
    _isAjaxsuo = true;
	DATA_GET.gooddata = GOODDSTA;
	DATA_GET.ajaxpage = AJAXPAGE;
    $.ajax({
        url: '/front/public/ajaxGood',
        type: "post",
        timeout: 5000,
        dataType: "json",
        data:DATA_GET,
        success: function(data) {
            if(data.status){
                $("#goodlist").append(data.content);
                $("img[data-original]").lazyload();
                AJAXPAGE++;
            	_isAjaxsuo = false;                
            }
        }
    });     
}

//js设置Cookie
function setCookie(name,value,time)
{
    var exp = new Date();
    exp.setTime(exp.getTime() + time * 1000);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}
//js获取Cookie
function getCookie(name)
{
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
 
    if(arr=document.cookie.match(reg))
        return (arr[2]);
    else
        return null;
}


function loginregister(type){
  if(type == 1){
    dialog({
      id:'login',
      title: '妈咪街',
      url:'/user/user/diaLogin'
    }).showModal();
  }else{
    dialog({
      id:'register',
      url:'/user/user/diaRegister'
    }).showModal();  	
  }
}
