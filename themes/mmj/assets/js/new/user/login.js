$(function(){
	/* ------------------------------------------------- */
	/*	输入框 focus
	/* ------------------------------------------------- */
	$("input.login_input").focus(function(){
		$(this).addClass("active");
	});
	$("input.login_input").blur(function(){
		$(this).removeClass("active");
	});

    /* ------------------------------------------------- */
    /*  输入框 快速登录
    /* ------------------------------------------------- */
    $(".diaLogin_list input").focus(function(){
        $(this).addClass("active");
    });
    $(".diaLogin_list input").blur(function(){
        $(this).removeClass("active");
    });

    /* ------------------------------------------------- */
    /*  支持placeholder属性
    /* ------------------------------------------------- */
	var JPlaceHolder = {
    //检测
    _check : function(){
        return 'placeholder' in document.createElement('input');
    },
    //初始化
    init : function(){
        if(!this._check()){
            this.fix();
        }
    },
    //修复
    fix : function(){
        jQuery(':input[placeholder]').each(function(index, element) {
            var self = $(this), txt = self.attr('placeholder');
            self.wrap($('<div></div>').css({position:'relative', zoom:'1', border:'none', background:'none', padding:'none', margin:'none'}));
            var pos = self.position(), h = self.outerHeight(true), paddingleft = self.css('padding-left');
            var holder = $('<label></label>').text(txt).css({position:'absolute', left:pos.left, top:pos.top="0px", height:h, lineHeight:pos.lineHeight=h+"px", paddingLeft:paddingleft, color:'#aaa'}).appendTo(self.parent());
            self.focusin(function(e) {
                holder.hide();
            }).focusout(function(e) {
                if(!self.val()){
                    holder.show();
                }
            });
            holder.click(function(e) {
                holder.hide();
                self.focus();
            });
        });
    }
	};
	//执行
	jQuery(function(){
	    JPlaceHolder.init();  
	});
	
});