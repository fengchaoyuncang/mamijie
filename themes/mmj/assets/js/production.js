/**
 * Created by chargo on 14/12/17.
 */
$(function() {
//    var proKey = true;
    var production = {
    	proKey: true,
        proIndex: $(".production-container .produForForm").index(),
        proscroll: function () {
            $(".produCForMore").click(function () {
                if (this.proKey) {
                    $(this).parent(".produCheck").siblings("form").find(".produVal-box").css({
                        "overflow-y" : "scroll"
                    });
                    $(this).parent().siblings(".productionNavForm").find(".produValue").stop(true, true).animate({
                        "padding-top" : "21px"
                    },600);
                    console.log($(this).siblings());
                    $(this).parent(".produCheck").siblings("form").find(".produ_brand").stop(true, true).animate({
                        "height": "130px"
                    }, 600);
                    $(this).html("收起<em></em>");
                    $(this).children("em").css({
                        "background" : "url(" + "/themes/mmj/assets/images/icon.png" + ") -104px -83px"
                    });

                    this.proKey = false;
                } else {
                    $(this).parent(".produCheck").siblings("form").find(".produ_brand").stop(true, true).animate({
                        "height": "43px"
                    }, 600);
                    $(".produCForMore").html("更多<em></em>");
                    $(this).children("em").css({
                        "background" : "url(" + "/themes/mmj/assets/images/icon.png" + ") -113px -83px"
                    });
                    $(this).parent(".produCheck").siblings("form").find(".produVal-box").css({
                        "overflow-y" : "hidden"
                    });
                    $(this).parent().siblings(".productionNavForm").find(".produValue").stop(true, true).animate({
                        "padding-top" : "14px"
                    },600);
                    this.proKey = true;
                }
            });
			$(".produVal-box li").click(function() {
				$(this).css({
					"border-color" : "#ff7694"
				})
			})
            $(".produChoose").click(function() {
                $(this).siblings(".produCForMore").css({
                    "visibility" : "hidden"
                });
                $(this).parent(".produCheck").siblings("form").find("li").removeAttr("href");
                $(this).css({
                	"display" : "none"
                })
//                开启Y轴的滚动条
                $(this).parent(".produCheck").siblings("form").find(".produVal-box").css({
                    "overflow-y" : "scroll"
                });
                $(this).parent(".produCheck").siblings("form").find(".produBtn").css({
                    "display" : "block"
                });
                $(this).parent(".produCheck").siblings("form").find(".produ_brand").css({
                    "height" : "163px"
                });
            });
            //设置取消按钮弹回
            $(".produ-reset").click(function() {
                $(this).parents(".produ_brand").stop(true, true).animate({
                    "height" : "43px"
                },600);
                $(this).parent(".produBtn").siblings(".produVal-box").css({
                    "overflow-y" : "hidden"
                });
            })
        },
        proNav: function() {
            $(".produSelectNavL li").mouseover(function(){
                $(this).children("a").find("em").css({
                    "background": "url(/themes/mmj/assets/images/icon.png" + ") -82px -83px"
                });
            });
            $(".produSelectNavL li").mouseout(function(){
                $(this).children("a").find("em").css({
                    "background": "url(/themes/mmj/assets/images/icon.png" + ") -89px -83px"
                });
            });
        },
        proHiddenBtn: function() {
            $(".produChoose").hide();
        },
        addClassTit: function() {
            $(".breadcrumbs a").eq(1).css({
                "margin-left" : "4px"
            })
        }
    }
    production.proscroll();
    production.proNav();
    production.proHiddenBtn();
    production.addClassTit();

})
    $(".breadcrumbs a").css({
        "margin-left" : "4px"
    })
      $(window).scroll(function() {
        var speOfferFsH = $(".index-nav").height(),
            speOfferNavH = $(".index-navigation").outerHeight() + 16,
            bodyScroll = document.body.scrollTop || document.documentElement.scrollTop;
        if(bodyScroll >= speOfferFsH + speOfferNavH) {
            $(".speOffer-topFix").css({
                "display" : "block",
                "position" : "fixed",
                "top" : "0",
                "z-index" : "300"
            });
            $(".top-login-bg").hide();
            $(".speOfferF-nav").css({
                "margin-top" : "115px"
            });
        } else {
            $(".speOffer-topFix").css({
                "display" : "none"
            });
            $(".speOfferF-nav").css({
                "margin-top" : "0"
            });
            $(".top-login-bg").show();
        }
    })

    

