var uploader = {};

//这些需要定义全局变量
/*
var savePath = 'uploads/plupload/cars/' + sale_outles_id + '/';    保存图片路径
var deleteUrl = '<?php echo $this->createUrl("/ajax/upload/delete") ?>';  删除图片URL
var uploadUrl = '<?php echo $this->createUrl("/ajax/upload") ?>';  上传图片URL
var flash_swf_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.swf';;
var silverlight_xap_url = '<?php echo Yii::app()->theme->baseUrl; ?>/assets/plug/plupload-2.1.2/js/Moxie.xap';*/
//var HostUrl = '<?php echo yii::app()->params()["hostimg"]   ?>'; 如果需要在图片前面加上你想要的就定义这个变量


$(function(){
    //删除图片时需要做的处理
    $(document).on('click', '.uploader-close', function(){
        var obj = $(this);
        if(obj.hasClass('update')){ //如果是修改的则这样处理
            obj.parent().remove();//获取了后把这整个删除掉
            return true;
        }
        var path = obj.data('path'); //获取图片的整个路径
        var filename = obj.data('filename');//用于把图片从队列中删除
        var id = obj.data('id');
        //console.log(uploader);
        obj.parent().remove();//获取了后把这整个删除掉
        uploader[id].removeFile(filename);//从队列中删除
        //删除这张图片
        $.post(deleteUrl, {filename:path}, function(data){
            return;
        });
        //uploader[id].disableBrowse(false);//让其可用
    }); 	
})


/**
 * 参数传一个对象，返回一个文件对象
 * @return {[type]} [description]
 */
function createUploader(obj){
    obj.flash_swf_url = flash_swf_url;
    obj.silverlight_xap_url = silverlight_xap_url;
    obj.unique_names = true;//生成随机文件名
    obj.filters = {
      mime_types : [{ title : "Image files", extensions : "jpg,gif,png" }],
      max_file_size : '4000kb', //最大只能上传400kb的文件
      prevent_duplicates : true //不允许选取重复文件
    };
    obj.url = uploadUrl;
    var uploader = new plupload.Uploader(obj);
    uploader.init();
    return  uploader;       
}
function sort_img(id){
    return true;
    var sort_id = $("#" + id).next().attr('id');
    $("#" + sort_id).dragsort("destroy");
    $("#" + sort_id).dragsort({
        dragSelector : ".div_img",  //可以不用设置，他会根据$("#tableid")的类型来决定是tr还是li
        dragEnd : function(){                        
        },
        scrollSpeed:0  //默认为5，数值越大拖动的速度越快，为0则拖动时页面不会滚动
    });             
}

//只考虑一张图片的时候
function saveImg(id, name, savePath, more){
    var obj = {browse_button:id};
    if(!more){
        obj.multi_selection = false;//只能选择一个文件
    }
    var obj_postdata = {
        path:savePath,
        file:'file'
    };
    obj.multipart_params = obj_postdata; //POST的数据
    
    uploader[id] = createUploader(obj);

    //当添加到上传队列之后就可以开始上传
    uploader[id].bind('FilesAdded',function(uploader,files){
        uploader.start();
    });

    uploader[id].bind('Error',function(uploader,errObject){
        alert('上传错误，原因：' + errObject.message);
    });


    if(more){
        sort_img(id);
    }       
    //responseObject这个为服务器返回的信息
    uploader[id].bind('FileUploaded',function(uploader,file,responseObject){
        var hidden = "<input type='hidden' value='"+responseObject.response+"' name='"+name+"'/>";
        //图片前面加上链接http:// 需要自己定义一个变量HostUrl
        if(typeof(HostUrl) != 'undefined'){
            var img = "<img style='width:100px;height:100px' src='"+HostUrl+responseObject.response+"'/>";            
        }else{
            var img = "<img style='width:100px;height:100px' src='"+responseObject.response+"'/>"; 
        }
        var close = "<div data-id='"+id+"' data-filename='"+file.id+"' data-path='"+responseObject.response+"' class='uploader-close'></div>";
        if(more){
            //$("#" + id).next().append("<div class='div_img'>"+img+hidden+close+"</div>");
            $("#" + id).after("<div class='div_img'>"+img+hidden+close+"</div>");
        }else{
            //只是一张图片看是否要把原来的图给删除。
            var obj_div_img = $("#" + id).next('.div_img');
            if(obj_div_img.length){
                if(!(obj_div_img.find('.uploader-close').hasClass('update'))){
                    var path = obj_div_img.find('.uploader-close').data('path');
                    $.post(deleteUrl, {filename:path}, function(data){
                        return;
                    });                     
                }
                obj_div_img.remove();           
            }
            $("#" + id).after("<div class='div_img'>"+img+hidden+close+"</div>");                
        }

        //锁
        if(!more){
            //uploader.disableBrowse(true);
        }
        //排序
        if(more){
            sort_img(id);
        }    
    });

}