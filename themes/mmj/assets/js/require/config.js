requirejs.config({
    baseUrl: '/themes/mmj/assets/js',
    paths: {

    },
    shim: {

    }

});
$(document).ready(function() {
    require(['zclip/jquery.zclip.min']);
    
    require(['front/common']);
    
    //require(['lib/jquery.superslide']);
    
    require(['lib/jquery.lazyload'], function() {
        $("img.lazy").lazyload();
    });
    require(['lib/jquery.marquee'], function() {
        $('.marquee').marquee({           
            duration: 15000,
            gap: 500,
            delayBeforeStart: 0,
            direction: 'left',
            duplicated: true,
            pauseOnHover: true,
            scrollamount:'3',
            behavior:'scroll'           
        });
    });


    require(['front/homepage']);


});


